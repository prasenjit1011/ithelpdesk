<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequiredsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requireds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('form'); //ticket,requester or organisation
            $table->string('field');
            $table->string('agent'); //agent required
            $table->string('client'); //client required
            $table->integer('parent'); //inheriting raw id
            $table->integer('option'); //for values for select,checkboz etc
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requireds');
    }
}
