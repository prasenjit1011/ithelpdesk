<?php

use Illuminate\Database\Seeder;

use App\Model\helpdesk\Settings\CommonSettings;

class CommonSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CommonSettings::create(['option_name' => 'user_registration', 'status' => 0]);
        CommonSettings::create(['option_name' => 'user_show_org_ticket', 'status' => 0]);
        CommonSettings::create(['option_name' => 'user_reply_org_ticket', 'status' => 0]);
        CommonSettings::create(['option_name' => 'allow_users_to_create_ticket', 'status' => 0]);
        CommonSettings::create(['option_name' => 'account_actvation_option', 'option_value' => 'email']);
    }
}
