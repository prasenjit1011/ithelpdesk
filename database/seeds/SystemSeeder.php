<?php

use Illuminate\Database\Seeder;
use App\Model\helpdesk\Settings\System;

class SystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
	  System::create(array('id' => '1', 'status' => '1', 'department' => '1', 'date_time_format' => 'Y-m-d H:m:i', 'time_zone' => 'Europe/London','url'=>url('/')));
    }
}
