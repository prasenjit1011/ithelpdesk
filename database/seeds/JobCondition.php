<?php

use Illuminate\Database\Seeder;
use App\Model\MailJob\Condition;

class JobCondition extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            ["job"=>"fetching","value"=>"everyTenMinutes"],
            ["job"=>"notification","value"=>"everyTenMinutes"],
            ["job"=>"work","value"=>"everyTenMinutes"],
            ["job"=>"followup","value"=>"everyTenMinutes"],
            ["job"=>"escalate","value"=>"everyTenMinutes"],
        ];
        foreach($data as $job){
            Condition::create($job);
        }
    }

}
