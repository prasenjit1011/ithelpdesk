 .----------------.  .----------------.  .----------------.  .----------------.  .----------------. 
| .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
| |  _________   | || |      __      | || | ____   ____  | || |  _________   | || |     ____     | |
| | |_   ___  |  | || |     /  \     | || ||_  _| |_  _| | || | |_   ___  |  | || |   .'    `.   | |
| |   | |_  \_|  | || |    / /\ \    | || |  \ \   / /   | || |   | |_  \_|  | || |  /  .--.  \  | |
| |   |  _|      | || |   / ____ \   | || |   \ \ / /    | || |   |  _|  _   | || |  | |    | |  | |
| |  _| |_       | || | _/ /    \ \_ | || |    \ ' /     | || |  _| |___/ |  | || |  \  `--'  /  | |
| | |_____|      | || ||____|  |____|| || |     \_/      | || | |_________|  | || |   `.____.'   | |
| |              | || |              | || |              | || |              | || |              | |
| '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
 '----------------'  '----------------'  '----------------'  '----------------'  '----------------' 

 .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------. 
| .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
| |  ____  ____  | || |  _________   | || |   _____      | || |   ______     | || |  ________    | || |  _________   | || |    _______   | || |  ___  ____   | |
| | |_   ||   _| | || | |_   ___  |  | || |  |_   _|     | || |  |_   __ \   | || | |_   ___ `.  | || | |_   ___  |  | || |   /  ___  |  | || | |_  ||_  _|  | |
| |   | |__| |   | || |   | |_  \_|  | || |    | |       | || |    | |__) |  | || |   | |   `. \ | || |   | |_  \_|  | || |  |  (__ \_|  | || |   | |_/ /    | |
| |   |  __  |   | || |   |  _|  _   | || |    | |   _   | || |    |  ___/   | || |   | |    | | | || |   |  _|  _   | || |   '.___`-.   | || |   |  __'.    | |
| |  _| |  | |_  | || |  _| |___/ |  | || |   _| |__/ |  | || |   _| |_      | || |  _| |___.' / | || |  _| |___/ |  | || |  |`\____) |  | || |  _| |  \ \_  | |
| | |____||____| | || | |_________|  | || |  |________|  | || |  |_____|     | || | |________.'  | || | |_________|  | || |  |_______.'  | || | |____||____| | |
| |              | || |              | || |              | || |              | || |              | || |              | || |              | || |              | |
| '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
 '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------' 
|====================================================
| v1.9.14
|====================================================
Updates and Enhancements
    
    Enhancements
        * Customized form builder with more advance options and Settings
        * Media files and attachment upload option with drag and drop functionality
        * RTL transformation of layout pages in Admin, agent and client panel
        * SMS plugins with templates, supports OTP verification for mobile
        * White label plugin
        * Migration Plugin for migrating data from other system to Faveo
        * Assets management reports

    System Updates
        * create more specific templates for alerts and notices via email or SMS
        * various UI/UX enhancements
        * Admin LTE theme updates
        * API Updates
            - consistency in parameter names
            - Specify records to be fetched in an API call while fetching tickets
        * Bug fixes in Service Desk plugin
        * Updated permissions for agents

    Bug fixes
        * various UI/UX bugs fixed




|====================================================
| v1.9.13
|====================================================
Update and bug-fix patch

Bug-fixes
    * Updated getProilePicture function to retrieve profile pics of users
    * Updated ticket_thread API to include system generated internal notes to show in mobile app
    * Ticket number format issue fixed
    * Saving users first and last name in respective fields in database while creating tickets
    * Saving iOS and android app messages body in same format to show on web (Issue reported by Eng. Kashif)
    * Removing warning alert for mail configuration when an admin configures a system mail.

|====================================================
| v1.9.12
|====================================================
Bug-fix-Patch

Bug fixed
   * Ticket title UTF decoding issue in all ticket tables
   * Sending registration confirmation mail to 'CC' users can be handled from alert and notices. Linked mail sending functionality to 'CC' users with alert and notices.
   * Reset Password bug fixed.

|====================================================
| v1.9.11
|====================================================
API bug fix patch
   
Bug fixed    
    Enabled push notification in iOS pro app
    Fixed APIs for create and edit tickets

|====================================================
| v1.9.10
|====================================================
Update Patch for API and smart version

Bug fixed
   * Updated ticket listing API for returning correct data in response
   * Returing thread in decreasing order of last update in API response
   * Showing correct count of deleted ticekt in dashboard
   * Fixed horizontal overflow issue in ticket timeline (cleint panel and agent panel)
   * Updated socail and telephony plugins for ticket creation issues
   * Fixed cleint panel Mytickets issue
   * Removed old notification
   * Fixed dummy data installation issue in smart encoded version 

|====================================================
| v1.9.9
|====================================================
Bug fix patch

Bug fixes
    * Fixed create ticket API
    * Fixed edit ticket API
    * Fixed ticket listing APIs for inbox, closed, trash, unassigned
    * Fixed ticket visibility issue in client panel
    * Fixed ticket status change issue from my tickets in client panel
    * Fixed Facebook login token parsing issue
|====================================================
| v1.9.8
|====================================================
Hot fix patch for Mobile apps issue

Bug fixes

    * Changed status code for Token expired error from 401 to 200 (Fix for mobile)   
    * Updated inbox, closed, trash, unassigend ticket APIs response for correct number of records(tickets)
    * Removed white spaces from image file name for saving profile pictures
    * Showing profile pic for both public and non public URL
    * Fixed token mismatch error in plugins (chat and zapier)


|====================================================
| v1.9.7
|====================================================
This release provides another update for security patch and prevents attackers to execute any backdoor scripts uploaded using hidden attachment or as an attachment itself.

Bug Fixes and enhancements
    
    * Preventing execution of backdoor scripts
    * Preventing php file uploads in user's profile pic
    * Handling token mismatch error when the page is opened for too long ans session gets expired.
    * Hid custom form data table headings when there is no custom data available for a ticket.
    
|====================================================
| v1.9.6
|====================================================
Bug-fix-patch
    This is the bugfix patch for last release v1.9.5 for simple file rename

Change-log
    renamed model portal to Portal as servers are case sensative and this throws class not found exception.
    
|====================================================
| v1.9.5
|====================================================

This release mainly focuses on the updation of the framework used in the application. Along with framework update this release offers minor bug fixes for ticket filtration, UI releated issues and business hours, SLA issues, CSRF token checking.

Change log

    * Updated Laravel framework version to v5.4
    * Enabled CSRF token checking while submitting form request

Bug Fixes and enhancements

    * Fixed issued in filteration by created/updated or due-on date
    * Showing Proper user information in user filteration of tickets
    * Dashboard report date range validation
    * Fixed language for alets and notices in mails and in-app notifications

|=====================================================
| v1.9.4
|=====================================================
Enhancements

    * Improved alert and notifications with activity log
        # Tracking each activity done on the ticket by any agent or system and displaying it in ticket thread.
        # Improved in app notification style
        # Loading notification data using ajax calls to prevent unnecesarry data from loading
        # Showing detailed information in notifications
        # You can choose options to send alerts via mail or mobile notifications.
        # You can select to whom these alerts should go.
        # You can choose events to send the alerts.
    * SLA and workflow improvements
    * Updated system lanuage translation
        # System level translation
        # User account level translation
        # Session level translation
    * Advnance reports
    * Advance Filtration

Bug Fixes
    * Fixed bugs in latest SLA Module.
    * Removed duplicate and ambiguous options from various settings in admin panel.
    * Fixed Iframe scroll bar issue in ticket time line.
    * Fixed bugs listed in GitHub and reported by other users
    * Fixed ticket pages tooltip UI format issue
    * Change log

Change Log
    * Updated Faveo logo

To read more about the updates and feature enhancements follow the link below 
http://ithelpdesk.com/support/show/faveo-helpdesk-pro-v194-released

|======================================================
| v1.9.1
|======================================================
Bug Fix Patches
    This release contains fixes for various bugs and some UI changes and enhancements.

|======================================================
| v1.9.0
|======================================================
Enhancements
    Add agents in Multiple departments
    Auto assignment of the tickets using Round Robin algorithm
    Dynamic SLA
    Advance escalation reports
    Tags and Labels for ticket filtration
    Add bill for tickets
    ITIL module
    Advance reports
    Multiple ticket assignment to agents and team

Bug-fixes
    Used Yajra Datatable in all ticket listing pages
    Used Yajra Datatable in users list page
    Advance search option in users list page
    Used Yajra Datatable in agent list page
    Removed duplicate queries from layout blade files


Change log
End User License Agreement updated


|=======================================================
| v1.0.8.0 
|=======================================================

Enhancements

    Advance user module
    One command for all cron jobs
    System report
    Custom URL redircetion and SSL support in URL
    Socail login
    Due today tickets
    Custom forms update
    Dynamic priority settings
    Email non mandatory 
    Laravle logs and queues
    Ticket change status to show in client panel via settings.
    Not to submit two tickets on double click of Ticket submit button.
    Dashboard linking.
    Incoming mail format check.
    Added a thread for ticket reopen via email.
    Ticket linking minor issue updated.
    Nl2br added for ticket.
    Different department users can view tickets if assigned.
    Preventing fall back language from deleting.
    Send email if checked to send email for first registration via agent panel.
    Checking storage file permission while installation.
    File info extension check during installation.
    Curl extension check during installation.
    Check for API mandatory.
    RTL feature in email sending.

Plugins

    Calendar: add events on tickets and get reminders
    SMS: Send message notification on client/agent 's mobile
        -> MSG91
        -> SMSLive247
    Podio integration
    Twitter integration
    Facebook integration
    Phone service integrations to create tickets from calls
        -> Exotel
        -> Mcube
        -> Knowlarity
    Zapier integration for chat
    Chat servises
        -> Livserv
        ->  HappyFox

Bug Fixes

    Moved media files to upload folder.
    Multiple file upload via attachment exception handling.
    Attachment issue fixed for ticket reply.
    File uploading according to server configuration of post_max_size.
    Code cleanup.
    Department ticket issue and naming convention.
    Help topic issue fixed in ticket creation.
    Pdf issue.
    Custom Form issue fixed for text area.
    Agent panel layout color changed for differentiation.
    Nesting level issue fixed.
    Proper ticket threads count in client panel.
    Validating dot files access via URL.
    Breadcrumbs Update.
    Exception handling for database server down issue.
    In app notification fixed for different department tickets.
    First name and user name issue fixed.
    YouTube icon link fixed.
    Clear error form fields on resubmit of form in email create/edit.
    Exception handling on re-uploads of pre-uploaded plugin.
    Department wise ticket creation had some exception.
    Changed naming convention for organization manager.
    Fixed trash side bar numbering.

    
|=======================================================
| v1.0.7.7
|=======================================================

Bug Fixes

    Removed multifile writing for installation.
    Bug fixed on Installation for unwanted redirection back to first step
    Email subject issue fixed for encrypted formats
    Close ticket workflow issue fixed

    |------------------------------------------
    |Updating from 1.0.7.6 to 1.0.7.7
    |------------------------------------------

    To upgrade read instructions here http://ithelpdesk.com/support/show/v1077

|=======================================================
| v1.0.7.6
|=======================================================

Bug Fixes

    Updated Config file for releasing

|=======================================================
| v1.0.7.5
|=======================================================

Enhancement

    Changed email template from file system to database
    Advanced Rating module
    In App Notification Module is now per User
    In App notification settings in admin panel
    POP feature is added for fetching emails
    Send mail from server using PHP mail feature is added for sending emails
    Added more encryption options for sending emails
    Removed Duplicate settings from system settings
    One can do more advanced email diagnostics by choosing different emails
    System Off line module Enhanced
    Everything comes from language files now
    Auto Close ticket workflow for closing a ticket in predefined days
    Security settings added for secured login into the system
    Error Debugging option added
    Bugsnag integration added
    Webhooks added for ticket creation
    Added 3 new languages thanks to some of our clients (German, Russian, Italian)
    Graph added to user profile
    Remove organisation added in Client profile page
    Graph and ticket list added Organisation page
    GeoIp plugin added for Phone country code pickup
    Faveo will be able to auto upgrade to new release from this version onwards
    XSS Vulnerability check added
    Upgrade from Laravel 5.0 to 5.2
    Breadcrumbs
    Auto update module

Bug Fixes

    Registration Module email issue
    Pages in knowledge base had some issue
    Complete UI improved and proper error messages are now displayed
    Bug fixed in edit system workflow
    Dashboard Graph smoothened
    Check ticket details without logging
    Install module – check for file permission issue
    Added dummy data for dummy installation
    
    |------------------------------------------
    |Updating from 1.0.7.4 to 1.0.7.5
    |------------------------------------------

    To upgrade read instructions here http://ithelpdesk.com/support/show/v1075

|=======================================================
| v1.0.7.4
|=======================================================

Enhancement

    Added STARTTLS encryption feature on email fetching settings

|=======================================================
| v1.0.7.3
|=======================================================

Bug Fixes

    Email settings update issue

|=======================================================
| v1.0.7.2
|=======================================================

Bug Fixes

    System failing if no system email is configured issue fixed

Enhancement

    Plugin Module updated for Pro Version

|=======================================================
| v1.0.7.1
|=======================================================

Bug Fixed

    In app notification bug fixed
    Ticket Listing hover issue fixed
    Agent panel issue fixed for role agents
    Template issue fixed    
    
|=======================================================
| v1.0.7
|=======================================================

Enhancements

    Ticket Workflow
    Ticket Overdue Listing
    In App Notification
    Cron job separate settings
    Select all to delete permanently from trash
    Added Read Unread features in ticket listing pages
    User linking to where ever usernames are displayed
    Attachment upload-able while creating ticket from client panel
    Enhanced email check while setting emails
    Ticket rating enhanced

Bug Fixed

    Date sorting issue fixed
    Department folder issue fixed
    Group update issue fixed
    Templates issue fixed
    Email checking issue fixed
    Ticket rating issue fixed
    
    |------------------------------------------
    |Updating from 1.0.6.10 to 1.0.7
    |------------------------------------------
    
    To upgrade read instructions here http://ithelpdesk.com/support/show/v107
    
|======================================================
| v1.0.6.10
|======================================================

Bug Fixed

    Redirection issue fixed from ticket details page
    Login dropdown box responsitivity issue fixed
    Mailing issue for ticket reply and new ticket creation fixed
    Email template issue fixed for replying tickets and auto response for ticket creation
    Settings pages error display issue fixed for html type errors
    Daily notification issue fixed for the new mail system integrated for faveo
    Issue fixed for large size attachments

Enhancements

    Added loader image in datatable processing
    Added readme.txt which will provide install instruction
    Updated auth validation message and showing custom logo in auth/login page