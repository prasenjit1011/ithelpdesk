var options = {
    scales: {
        xAxes: [{
                stacked: true,
                display: false,
            }],
        yAxes: [{
                stacked: true,
                barThickness: 10
            }],
    }
};
//var active_id = $('.bhoechie-tab-content.active').attr('id');
//created ticket
thisAjax('status', 'created-ticket', "#created-ticket-status");
thisAjax('priority', 'created-ticket', "#created-ticket-priority");
thisAjax('source', 'created-ticket', "#created-ticket-source");

//resolved
//thisAjax('status', 'resolved-ticket', "#resolved-ticket-status");
thisAjax('priority', 'resolved-ticket', "#resolved-ticket-priority");
thisAjax('source', 'resolved-ticket', "#resolved-ticket-source");

//unresolved
//thisAjax('status', 'resolved-ticket', "#unresolved-ticket-status");
thisAjax('priority', 'unresolved-ticket', "#unresolved-ticket-priority");
thisAjax('source', 'unresolved-ticket', "#unresolved-ticket-source");

//Reopened
//thisAjax('status', 'resolved-ticket', "#resolved-ticket-status");
thisAjax('priority', 'reopened-ticket', "#reopened-ticket-priority");
thisAjax('source', 'reopened-ticket', "#reopened-ticket-source");

//Avg first response
thisAjax('status', 'avg-first-response', "#avg-first-response-status");
thisAjax('priority', 'avg-first-response', "#avg-first-response-priority");
thisAjax('source', 'avg-first-response', "#avg-first-response-source");

//Avg response
thisAjax('status', 'resolved-ticket', "#resolved-ticket-status");
thisAjax('priority', 'resolved-ticket', "#resolved-ticket-priority");
thisAjax('source', 'resolved-ticket', "#resolved-ticket-source");

//Avg Resolution
thisAjax('status', 'resolved-ticket', "#resolved-ticket-status");
thisAjax('priority', 'resolved-ticket', "#resolved-ticket-priority");
thisAjax('source', 'resolved-ticket', "#resolved-ticket-source");

//First Contact
thisAjax('status', 'resolved-ticket', "#resolved-ticket-status");
thisAjax('priority', 'resolved-ticket', "#resolved-ticket-priority");
thisAjax('source', 'resolved-ticket', "#resolved-ticket-source");

/**
 * To draw a graph using ajax
 * 
 * @param string type
 * @param string active_id
 * @param string canvas
 * @returns graph
 */
function thisAjax(type, active_id, canvas) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "{{url('report/indepth/get')}}",
        data: {'group': type, 'active': active_id},
        success: function (data) {
            var ctx = $(canvas).get(0).getContext("2d");
            var myBarChart = new Chart(ctx,
                    {
                        type: 'horizontalBar',
                        data: data,
                        options: options
                    });
        },
        error: function (error) {
            var ctx = $(canvas).get(0).getContext("2d");
            var myBarChart = new Chart(ctx,
                    {
                        type: 'horizontalBar',
                        data: error.textResponse,
                        options: options
                    });
        }
    });
}