<?php
$request = Request::segments();
?>
<script type="text/javascript">
    jQuery(document).ready(function () {
        oTable = myFunction();
    });
    function myFunction()
    {
        return jQuery('#chumper').dataTable({
            "sPaginationType": "full_numbers",
            "sDom": "<'row'<'col-xs-6'><'col-xs-6'>r>"+
                        "t"+
                        "<'row'<'col-xs-6'><'col-xs-6'>>",
            "bProcessing": true,
            "bServerSide": true,
            "displayLength": 100,
            "columnDefs": [
                { "visible": false, "targets": 3 },
                { "orderable": false, "targets": [0,1,2]},
            ],
            "order": [[ 3, 'desc' ]],
            "ajax": {
                url: "{{route('template-table-data', $request[1])}}",
            },
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
                api.column(3, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="5" style="background:#DDDDDD"><h4>'+group+'</h4></td></tr>'
                        );

                        last = group;
                    }
                } );
                    $('.box-body').css('opacity',1);
            }
        });
    }

    oTable.on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 3 && currentOrder[1] === 'asc' ) {
            table.order( [ 3, 'desc' ] ).draw();
        }
        else {
            table.order( [ 3, 'asc' ] ).draw();
        }
    } );
</script>