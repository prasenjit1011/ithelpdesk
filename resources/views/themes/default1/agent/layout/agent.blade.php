<html ng-app="fbApp">
    <?php
    $segment = "";
    $company = App\Model\helpdesk\Settings\Company::where('id', '=', '1')->first();
    $portal = App\Model\helpdesk\Theme\Portal::where('id', '=', 1)->first();
      $title = App\Model\helpdesk\Settings\System::where('id', '=', '1')->first();
        if (isset($title->name)) {
            $title_name = $title->name;
        } else {
            $title_name = "SUPPORT CENTER";
        }
    ?>
    <head>
        <meta charset="UTF-8">
      
         <title> @yield('title') {!! strip_tags($title_name) !!} </title>
       
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="_token" content="{!! csrf_token() !!}"/>
        <!-- faveo favicon -->
        @if($portal->icon!=0)
       
        
        <link href="{{asset("uploads/icon/$portal->icon")}}" rel="shortcut icon">
        @else
        <link href="{{asset("lb-faveo/media/images/favicon.ico")}}" rel="shortcut icon"> 
        @endif
        <!-- Bootstrap 3.3.2 -->
        <!-- <link href="{{asset("lb-faveo/rtl/css/bootstrap-rtl.min.css")}}" rel="stylesheet" id="bootRTL" type="text/css" disabled='disabled'/> -->
        <link href="{{asset("lb-faveo/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="{{asset("lb-faveo/css/font-awesome.min.css")}}" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{asset("lb-faveo/css/ionicons.min.css")}}" rel="stylesheet"  type="text/css" />
        <!-- Theme style -->
        
        <link href="{{asset("lb-faveo/css/AdminLTE.css")}}" rel="stylesheet" type="text/css" id="adminLTR"/> 
       <!--  <link href="{{asset("lb-faveo/rtl/css/AdminLTE.css")}}" id="adminRTL" rel="stylesheet" type="text/css" disabled='disabled' /> -->
        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link href="{{asset("lb-faveo/css/skins/_all-skins.min.css")}}" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="{{asset("lb-faveo/plugins/iCheck/flat/blue.css")}}" rel="stylesheet" type="text/css" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <link href="{{asset("lb-faveo/css/tabby.css")}}" rel="stylesheet" type="text/css"/>

        <link href="{{asset("lb-faveo/css/jquerysctipttop.css")}}" rel="stylesheet" type="text/css"/>
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <link href="{{asset("lb-faveo/css/editor.css")}}" rel="stylesheet" type="text/css"/>

        <link href="{{asset("lb-faveo/css/jquery.ui.css")}}" rel="stylesheet" rel="stylesheet"/>

        <link href="{{asset("lb-faveo/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet"  type="text/css"/>

        <!-- <link href="{{asset("lb-faveo/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")}}" rel="stylesheet" type="text/css"/> -->

        <link href="{{asset("lb-faveo/css/faveo-css.css")}}" rel="stylesheet" type="text/css" />


       <!--  <link rel="stylesheet" href="dist/css/bootstrap-rtl.min.css">
         <link rel="stylesheet" href="dist/css/rtl.css">
 -->
        

       <!--  <link href="{{asset("lb-faveo/rtl/css/rtl.css")}}" rel="stylesheet" type="text/css" id="cssRTL" disabled='disabled'/> -->

        

        <link href="{{asset("lb-faveo/css/jquery.rating.css")}}" rel="stylesheet" type="text/css" />

        <!-- Notication -->
        <link href="{{asset("lb-faveo/css/angular/ng-scrollable.min.css")}}" rel="stylesheet" type="text/css" />
        <!-- Select2 -->
        <link href="{{asset("lb-faveo/plugins/select2/select2.min.css")}}" rel="stylesheet" type="text/css" />
        <link href="{{asset("css/close-button.css")}}" rel="stylesheet" type="text/css" />

        <!--Daterangepicker-->
        <link rel="stylesheet" href="{{asset("lb-faveo/plugins/daterangepicker/daterangepicker.css")}}" rel="stylesheet" type="text/css" />
        <!--calendar -->
        <!-- fullCalendar 2.2.5-->
        <link href="{{asset('lb-faveo/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset("lb-faveo/js/intlTelInput.css")}}" rel="stylesheet" type="text/css" />
         <script src="{{asset("lb-faveo/js/jquery-2.1.4.js")}}" type="text/javascript"></script>

        <script src="{{asset("lb-faveo/js/jquery2.1.1.min.js")}}" type="text/javascript"></script>

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        
        
        @yield('HeadInclude')
         <style type="text/css">
       
            @font-face {
                      font-family: dubaiFonts;
                      src: url({{asset("lb-faveo/fonts/DubaiW23-Light.woff")}});
                   }
            body{
                font-family:dubaiFonts !important;
            }
        </style>
        <!-- rtl brecket -->

        <style type="text/css">
            #bar {
                border-right: 1px solid rgba(204, 204, 204, 0.41);
            }
            #bar a{
                color: #FFF;
            }
            #bar a:hover, #bar a:focus{
                background-color: #357CA5;
            }
            
        </style>
         <style>
         .input-sm{
                padding: 2px;
            }
        .loader1 {
            height: 2px;
            width: 100%;
            position: relative;
            overflow: hidden;
            background-color: #ff0000;
        }
       .loader1:before{
            display: block;
            position: absolute;
            content: "";
            left: -200px;
            width: 200px;
            height: 4px;
            background-color:#ffffff;
            animation: loading 2s linear infinite;
        }
        @keyframes loading {
            from {left: -200px; width: 30%;}
          50% {width: 30%;}
    70% {width: 70%;}
    80% { left: 50%;}
    95% {left: 120%;}
    to {left: 100%;}
}
.sidebar-mini.sidebar-collapse .content-wrapper, .sidebar-mini.sidebar-collapse .right-side, .sidebar-mini.sidebar-collapse .main-footer {
    margin-left: 0px !important;
    z-index: 840;
}
.sidebar-mini.sidebar-collapse .main-sidebar {
    -webkit-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    -o-transform: translate(0, 0);
    transform: translate(0, 0);
    width: 0px !important;
    z-index: 850;
}
.note-editor{
    border-radius: 0px;
    margin-right: 140px;
}
/*.col-sm-4{
    text-align: right;
}*/
.drop-box {
    width: 170px;
    text-align: center;
    padding: 50px 10px;
    margin: auto;
    width:100%;
    min-height: 250px;
}
.input-hidden {
  position: absolute;
  left: -9999px;
}

input[type=radio]:checked + label>img {
  border: 1px solid #fff;
  box-shadow: 0 0 3px 3px #090;
}
.nav-tabs.control-sidebar-tabs > li.active>.tab-slant::before{
    background-color:#f0f0f0 !important;
 }
 .control-sidebar-dark .nav-tabs.control-sidebar-tabs > li.active > a, .control-sidebar-dark .nav-tabs.control-sidebar-tabs > li.active > a:hover, .control-sidebar-dark .nav-tabs.control-sidebar-tabs > li.active > a:focus, .control-sidebar-dark .nav-tabs.control-sidebar-tabs > li.active > a:active {
    background: transparent !important;
    color:#444 !important;
}
 .nav-tabs.control-sidebar-tabs > li:hover>.tab-slant::before{
    background-color:#f0f0f0 !important;
 }
 .nav-tabs.control-sidebar-tabs > li:hover>a{
    color:#777 !important;
 }

/*input[type=radio]:checked + label>img {
  transform: 
    rotateZ(-10deg) 
    rotateX(10deg);
}*/
.right-side-menu.right-side-menu-open, .right-side-menu.right-side-menu-open + .right-side-menu-bg {
     right:-119px;
    position: fixed;
    overflow-y: auto;
    overflow-x: hidden;
}
.right-side-menu1.right-side-menu-open1, .right-side-menu.right-side-menu-open1 + .right-side-menu-bg1 {
    right: 0;
    position: fixed;
    overflow-y: auto;
    overflow-x: hidden;
   }
.control-sidebar-dark, .control-sidebar-dark + .right-side-menu-bg,.control-sidebar-dark + .right-side-menu-bg1 {
    background: #222d32;
}
.control-sidebar-dark {
    color: #b8c7ce;
}
.right-side-menu1{
    bottom: 0;
    right: -100%;
    width: 100%;
    -webkit-transition: right .3s ease-in-out;
    -o-transition: right .3s ease-in-out;
    transition: right .3s ease-in-out;
}
.right-side-menu,.right-side-menu1{
    position: fixed;
    padding-top: 50px;
    z-index: 1010;
}
.right-side-menu-bg1,.right-side-menu,.right-side-menu-bg{
    bottom: 0;
    right: -76%;
    width: 76%;
    -webkit-transition: right .3s ease-in-out;
    -o-transition: right .3s ease-in-out;
    transition: right .3s ease-in-out;
}
.tab-content{
    padding-left: 13px;
}
.control-sidebar-dark .nav-tabs.control-sidebar-tabs > li > a, .control-sidebar-dark .nav-tabs.control-sidebar-tabs > li > a:hover{
         border-left-color: transparent !important;
         border-bottom-color: transparent !important;
}
.control-sidebar-subheading{
    color: black !important;
}
#trapezoid{
    position: relative;
    padding: 10px 15px;
    outline: 0;
    z-index: 2;
    top: 2px;
}

#trapezoid:after { 
    border-bottom: 50px solid #367fa9;
     border-left: 20px solid transparent; 
     border-right: 20px solid transparent;
         
    
  }
  .search-box{
     position:relative;
     width:95%;  
  }
 .search-box:before {
    display: inline-block;
    font-family: 'Glyphicons Halflings';
    font-style: normal;
    font-weight: 400;
    line-height: 1;
    position: absolute;
    top: 10px;
    left: 12px;
    content: "\e003";
    color: #999;
}
.search-box input[type=text] {
    display: block;
    width: 100%;
    padding: 4px 6px 4px 35px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #e7e8e6;
    -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    margin-bottom: 10px;
    box-shadow: none;
    font-size: 1.2em;
    height: 35px;
    border-radius: 25px;
}
.help-widget-close {
    font-style: normal;
    font-weight: 400;
    line-height: 1;
    top: 0px;
    right: 20px;
    width: 66px;
    background: #555;
    border-bottom: 1px solid #ccc;
    z-index: 0;
}
.control-sidebar-dark .nav-tabs.control-sidebar-tabs > li > a {
    background: transparent;
    color: #eee;
}
.tab-slant::before {
    content: '';
    position: absolute;
    top: -10px;
    right: -10px;
    bottom: -7px;
    left: -10px;
    border: 1px solid #bbb;
    border-radius: 5px;
    z-index: -1;
    background: #d5d5d5;
    -webkit-transform: perspective(100px) rotateX(40deg);
    transform: perspective(100px) rotateX(40deg);
}
.control-sidebar-menu {
    list-style: none;
    padding: 0;
    margin: 0 -15px;
}
.control-sidebar-subheading {
    display: block;
    font-weight: 400;
    font-size: 14px;
}
.control-sidebar-menu .menu-info {
    margin-left: 45px;
    margin-top: 3px;
}
.control-sidebar-menu .menu-icon {
    float: left;
    width: 35px;
    height: 35px;
    border-radius: 50%;
    text-align: center;
    line-height: 35px;
}
.tab-slant {
    border: 0;
    padding: 0;
    margin: 0;
    line-height: 1;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: #d5d5d5;
    color: #999;
    z-index: -1;
}
.control-sidebar-tabs li{
    margin: 0 10px;
    float: right !important;
    width: 125px !important;
    text-align: center;
    z-index: 1;
}
/*.nav-tabs > li.active > a{
    border: none !important;
}*/
.tabs-horizontal > li{
    margin-left: -3px;
}
.skin-blue .main-header .navbar .nav>li>a:hover, .skin-blue .main-header .navbar .nav>li>a:active, .skin-blue .main-header .navbar .nav>li>a:focus, .skin-blue .main-header .navbar .nav .open>a, .skin-blue .main-header .navbar .nav .open>a:hover, .skin-blue .main-header .navbar .nav .open>a:focus, .skin-blue .main-header .navbar .nav>.active>a{
     margin-bottom: 2px;
}
.required label:after {
    color: #e32;
    content: ' *';
    display:inline;
}
        </style>
        
    </head>
    @if($portal->agent_header_color)
    <body class="{{$portal -> agent_header_color}} fixed">
     <div class="loader1" style="z-index:2000"></div>
        @else
    <body class="skin-yellow fixed">
     <div class="loader1" style="z-index:2000"></div>
        @endif
        <div class="wrapper">
            <header class="main-header">
                <a href="{{$company -> website}}" class="logo">

                    @if($portal->logo!=0)
                    <img src='{{ asset("uploads/logo/$portal->logo")}}' class="img-rounded" alt="Cinque Terre" width="304" height="45">
                    @else
                    <img src="{{ asset('lb-faveo/media/images/logo.png')}}" width="100px">
                    @endif
                </a>
                <?php
                $replacetop = \Event::fire('service.desk.agent.topbar.replace', array());

                if (count($replacetop) == 0) {
                    $replacetop = 0;
                } else {
                    $replacetop = $replacetop[0];
                }

                $replaceside = \Event::fire('service.desk.agent.sidebar.replace', array());

                if (count($replaceside) == 0) {
                    $replaceside = 0;
                } else {
                    $replaceside = $replaceside[0];
                }
                ?>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        @if($replacetop==0)
                        <ul class="tabs tabs-horizontal nav navbar-nav navbar-left">
                            <li @yield('Dashboard')><a id="dash" data-target="#tabA" href="{{URL::route('dashboard')}}" onclick="clickDashboard(event);">{!! Lang::get('lang.dashboard') !!}</a></li>
                            <li @yield('Users')><a data-target="#tabB" href="#">{!! Lang::get('lang.users') !!}</a></li>
                            <!--<li @yield('Tickets')><a data-target="#tabC" href="#">{!! Lang::get('lang.tickets') !!}</a></li>-->
                            <li @yield('Tools')><a data-target="#tabD" href="#">{!! Lang::get('lang.tools') !!}</a></li>
                            @if($auth_user_role == 'admin' || $ticket_policy->report())
                            <li @yield('Report')><a href="{{URL::route('report.get')}}" onclick="clickReport(event);">{!! Lang::get('lang.report') !!}</a></li>
                            @endif
                            <?php \Event::fire('calendar.topbar', array()); ?>
                        </ul>
                        @else
                        <?php \Event::fire('service.desk.agent.topbar', array()); ?>
                        @endif

                        <ul class="nav navbar-nav navbar-right" id="right-menu">
                            @if($auth_user_role == 'admin')
                            <li><a href="{{url('admin')}}">{!! Lang::get('lang.admin_panel') !!}</a></li>

                            @endif

                            @include('themes.default1.update.notification')
                            <!-- START NOTIFICATION --> 
                            @include('themes.default1.inapp-notification.notification')
                            
                            <!-- END NOTIFICATION --> 
                            <li class="dropdown">
                                <?php $src = Lang::getLocale().'.png'; ?>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><img src="{{asset("lb-faveo/flags/$src")}}"></img> &nbsp;<span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    @foreach($langs as $key => $value)
                                            <?php $src = $key.".png"; ?>
                                            <li><a href="#" id="{{$key}}" onclick="changeLang(this.id)"><img src="{{asset("lb-faveo/flags/$src")}}"></img>&nbsp;{{$value}}</a></li>
                                    @endforeach       
                                </ul>
                            </li>

                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    @if($auth_user_id)
                                    <img src="{{$auth_user_profile_pic}}"class="user-image" alt="User Image"/>
                                    <span class="hidden-xs">{{$auth_name}}</span>
                                    @endif
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header"  style="background-color:#343F44;">
                                        <img src="{{$auth_user_profile_pic}}" class="img-circle" alt="User Image" />
                                        <p>
                                          <span>{!! Lang::get('lang.hello') !!}</span><br/>
                                            {{$auth_name}} - {{$auth_user_role}}
                                            <small></small>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer" style="background-color:#1a2226;">
                                        <div class="pull-left">
                                            <a href="{{URL::route('profile')}}" class="btn btn-info btn-sm"><b>{!! Lang::get('lang.profile') !!}</b></a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="{{url('auth/logout')}}" class="btn btn-danger btn-sm"><b>{!! Lang::get('lang.sign_out') !!}</b></a>
                                        </div>
                                    </li>

                                </ul>

                            </li>

                        </ul>

                    </div>

                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar" id="sideMenu">
                    <div class="user-panel">
                        @if (trim($__env->yieldContent('profileimg')))
                        <h1>@yield('profileimg')</h1>
                        @else
                        <div class = "row">
                            <div class="col-xs-3"></div>
                            <div class="col-xs-2" style="width:50%;">
                                <a href="{!! url('profile') !!}">
                                    <img src="{{$auth_user_profile_pic}}" class="img-circle" alt="User Image" />
                                </a>
                            </div>
                        </div>
                        @endif
                        <div class="info" style="text-align:center;">
                            @if($auth_user_id)
                            <p>{{$auth_name}}</p>
                            @endif
                            @if($auth_user_id && $auth_user_active==1)
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                            @else
                            <a href="#"><i class="fa fa-circle"></i> Offline</a>
                            @endif
                        </div>
                    </div>
                    <ul id="side-bar" class="sidebar-menu">
                        @if($replaceside==0)
                        @yield('sidebar')
                        <li class="header">{!! Lang::get('lang.Tickets') !!}</li>
                        @if($ticket_policy->create())
                        <li @yield('newticket')>
                            <a href="{{ url('/newticket')}}" >
                                <i class="fa fa-ticket"></i>{!! Lang::get('lang.create_ticket') !!}
                            </a>
                        </li>
                        @endif
                        <li @yield('inbox')>
                             <a href="{{ url('/tickets')}}" id="load-inbox">
                                <i class="fa fa-inbox"></i> <span>{!! Lang::get('lang.inbox') !!}</span> <small class="label pull-right bg-green">{{$tickets -> count()}}</small>                                            
                            </a>
                        </li>
                        <li @yield('myticket')>
                             <a href="{{ url('/tickets?show=mytickets')}}" id="load-myticket">
                                <i class="fa fa-user"></i> <span>{!! Lang::get('lang.my_tickets') !!} </span>
                                <small class="label pull-right bg-green">{{$myticket -> count()}}</small>
                            </a>
                        </li>
                        <!--<li @yield('answered')>
                            <a href="{{ url('/ticket/answered')}}" id="load-answered">
                                {!! Lang::get('lang.answered') !!}
                                </a>
                        </li>
                        <li @yield('assigned')>
                            <a href="{{ url('/ticket/assigned')}}" id="load-assigned" >
                                {!! Lang::get('lang.assigned') !!}
                            </a>
                        </li>-->
                        <li @yield('unassigned')>
                             <a href="{{ url('/tickets?assigned[]=0')}}" id="load-unassigned">
                                <i class="fa fa-user-times"></i> <span>{!! Lang::get('lang.unassigned') !!}</span>
                                <small class="label pull-right bg-green">{{$unassigned -> count()}}</small>
                            </a>
                        </li>
                        <li @yield('overdue')>
                             <a href="{{url('/tickets?show=overdue')}}" id="load-unassigned">
                                <i class="fa fa-calendar-times-o"></i> <span>{!! Lang::get('lang.overdue') !!}</span>
                                <small class="label pull-right bg-green">{{$overdues -> count()}}</small>
                            </a>
                        </li>
                        <li @yield('followup')>
                             <a href="{{ url('/tickets?show=followup')}}" id="load-inbox">
                                <i class="glyphicon glyphicon-import"></i> <span>{!! Lang::get('lang.followup') !!}</span>
                                <small class="label pull-right bg-green">{{$followup_ticket -> count()}}</small>
                            </a>
                        </li>
                        @if($approval_enable->first()->status == 1)
                        <?php
                        $is_team_lead = 0;
                        $is_department_manager = 0;
                        if (\Auth::user()->role == 'admin') {
                            $is_team_lead = 1;
                            $is_department_manager = 1;
                        } else {
                            $is_team_lead = \DB::table('teams')->where('team_lead', '=', \Auth::user()->id)->count();
                            $is_department_manager = \DB::table('department')->where('manager', '=', \Auth::user()->id)->count();
                        }
                        ?>
                        @if($is_team_lead == 1 || $is_department_manager == 1)
                        <li @yield('approval')>
                            <a href="{{url('/tickets?show=approval')}}" id="load-unassigned">
                                <i class="fa fa fa-bell"></i> <span>{!! Lang::get('lang.approval') !!}</span>
                                <small class="label pull-right bg-green">{{$closingapproval -> count()}}</small>
                            </a>
                        </li>
                        @endif
                        @endif
                        <li @yield('closed')>
                            <a href="{{ url('/tickets?show=closed')}}" >
                                 <i class="fa fa-minus-circle"></i> <span>{!! Lang::get('lang.closed') !!}</span>
                                <small class="label pull-right bg-green">{{$closed -> count()}}</small>
                            </a>
                        </li>
                        <li @yield('trash')>
                             <a href="{{ url('/tickets?show=trash')}}">
                                <i class="fa fa-trash-o"></i> <span>{!! Lang::get('lang.trash') !!}</span>
                                <small class="label pull-right bg-green">{{$deleted -> count()}}</small>
                            </a>
                        </li>
                            <?php
                                $segments = \Request::segments();
                                $segment = "";
                                foreach($segments as $seg){
                                    $segment.="/".$seg;
                                }
                                if(count($segments) > 2) {
                                    $dept2 = $segments[1]; 
                                    $status2 = $segments[2];
                                } else {
                                     $dept2 = ''; 
                                    $status2 = '';
                                }
                            ?>
                        
                        @else

                        <?php \Event::fire('service.desk.agent.sidebar', array()); ?>
                        @endif
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php
//$agent_group = $auth_user_assign_group;
//$group = App\Model\helpdesk\Agent\Groups::select('can_create_ticket')->where('id', '=', $agent_group)->first();
$group = true;
                ?>
                    <div class="tab-content" style="background-color: #80B5D3; position: fixed; width:100% ;padding: 0 0px 0 0px; z-index:999" id=slideUp>
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <div class="tabs-content">
                            @if($replacetop==0)
                            <div class="tabs-pane @yield('dashboard-bar')"  id="tabA">
                                <ul class="nav navbar-nav">
                                </ul>
                            </div>
                            <div class="tabs-pane @yield('user-bar')" id="tabB">
                                <ul class="nav navbar-nav" style="margin-left: -15px">
                                    <li id="bar" @yield('user')><a href="{{ url('user')}}" >{!! Lang::get('lang.user_directory') !!}</a></li>
                                    <li id="bar" @yield('organizations')><a href="{{ url('organizations')}}" >{!! Lang::get('lang.organizations') !!}</a></li>

                                </ul>
                            </div>
                            <!--<div class="tabs-pane @yield('ticket-bar')" id="tabC">
                                <ul class="nav navbar-nav">
                                    <li id="bar" @yield('open')><a href="{{ url('/ticket/open')}}" id="load-open">{!! Lang::get('lang.open') !!}</a></li>
                                    <li id="bar" @yield('answered')><a href="{{ url('/ticket/answered')}}" id="load-answered">{!! Lang::get('lang.answered') !!}</a></li>
                                    <li id="bar" @yield('assigned')><a href="{{ url('/ticket/assigned')}}" id="load-assigned" >{!! Lang::get('lang.assigned') !!}</a></li>
                                    <li id="bar" @yield('closed')><a href="{{ url('/ticket/closed')}}" >{!! Lang::get('lang.closed') !!}</a></li>
                                    <?php if ($group) { ?>
                                        <li id="bar" @yield('newticket')><a href="{{ url('/newticket')}}" >{!! Lang::get('lang.create_ticket') !!}</a></li>
                                    <?php } ?>
                                </ul>
                            </div>-->
                            <div class="tabs-pane @yield('tools-bar')" id="tabD">
                                <ul class="nav navbar-nav" style="margin-left: -15px">
                                    <li id="bar" @yield('tools')><a href="{{ url('/canned/list')}}" >{!! Lang::get('lang.canned_response') !!}</a></li>
                                    <li id="bar" @yield('kb')><a href="{{ url('/comment')}}" >{!! Lang::get('lang.knowledge_base') !!}</a></li>
                                </ul>
                            </div>
                            @if($auth_user_role == 'admin')
                            <div class="tabs-pane @yield('report-bar')" id="tabD">
                                <ul class="nav navbar-nav">
                                </ul>
                            </div>
                            @endif
                            @endif
                            <?php \Event::fire('service.desk.agent.topsubbar', array()); ?>
                        </div>
                    </div>
                </div>
                @if ($segment == '/dashboard' || $segment == '/tickets')
                <!-- do nothing-->
                @else
                <br/><br/>
                @endif
                <section class="content-header">
                    @yield('PageHeader')
                    {!! Breadcrumbs::renderIfExists() !!}
                </section>
                <!-- Main content -->
                <section class="content">
                    @if($dummy_installation == 1 || $dummy_installation == '1')
                    <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="icon fa  fa-exclamation-triangle"></i> @if (\Auth::user()->role == 'admin')
                            {{Lang::get('lang.dummy_data_installation_message')}} <a href="{{route('clean-database')}}">{{Lang::get('lang.click')}}</a> {{Lang::get('lang.clear-dummy-data')}}
                        @else
                            {{Lang::get('lang.clear-dummy-data-agent-message')}}
                        @endif
                    </div>
                    @else
                        @if ($is_mail_conigured->count() < 1)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-warning lead">
                                    <h4><i class="fa fa-exclamation-triangle"></i>&nbsp;{{Lang::get('Alert')}}</h4>
                                    <p style="font-size:0.8em">
                                        @if (\Auth::user()->role == 'admin')
                                        {{Lang::get('lang.system-outgoing-incoming-mail-not-configured')}}&nbsp;<a href="{{URL::route('emails.create')}}">{{Lang::get('lang.confihure-the-mail-now')}}</a>
                                        @else
                                        {{Lang::get('lang.system-mail-not-configured-agent-message')}}
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endif
                    @yield('content')
                </section><!-- /.content -->
            </div>
            <footer class="main-footer">
                <div class="hide" style="position: fixed;right:0;bottom:0">
                      <button data-toggle="control-sidebar" onclick="openSlide()" style="margin-right:15px"  class="btn btn-primary helpsection"><i class="fa fa-question-circle" aria-hidden="true">  Kindly Any Help</i></button>
                </div>
                <div class="pull-right hidden-xs">
                    <b>Version</b> {!! Config::get('app.version') !!}
                </div>
                <strong>{!! Lang::get('lang.copyright') !!} &copy; {!! date('Y') !!}  <a href="{!! $company->website !!}" target="_blank">{!! $company->company_name !!}</a>.</strong> {!! Lang::get('lang.all_rights_reserved') !!}. 


                 <?php
                 $Whitelabel = \Event::fire('Whitelabel.faveo', array());
                 ?>
                 @if (count($Whitelabel) !=0) 
                  {!! Lang::get('lang.all_rights_reserved') !!}. {!! Lang::get('lang.powered_by') !!} <a href="#" target="_blank">Helpdesk</a>
                 @else
                 {!! Lang::get('lang.all_rights_reserved') !!}. {!! Lang::get('lang.powered_by') !!} <a href="http://www.faveohelpdesk.com/" target="_blank">Faveo</a>
        
                 @endif



                <!-- {!! Lang::get('lang.powered_by') !!} <a href="http://www.faveohelpdesk.com/" target="_blank">Faveo</a> -->
            </footer>
          <aside class="right-side-menu control-sidebar-dark" id="right"  style="padding-top:0;bottom: 300px;background: transparent">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs"  style="background: transparent">
      <li class="remov"><a href="javascript:void(0)" onclick="openSlide()" class="help-widget-close" style="background-color: black;padding: 13px 15px"><i class="fa fa-times" aria-hidden="true"></i></a></li>
      <li ><a href="#settings-tab" data-toggle="tab" id="trapezoid" style="background-color:transparent;color:#444;" ><i class="fa fa-paper-plane-o" aria-hidden="true" ></i> Mail Us
</a><span class="tab-slant"></span></li>
      <li class="active" ><a href="#home-tab" data-toggle="tab" id="trapezoid" style="background-color: transparent;color:#444;"><i class="fa fa-question-circle" aria-hidden="true"></i> Help</a><span class="tab-slant"></span></li>
  </ul>
    </aside>
    <aside class="right-side-menu1 control-sidebar-dark" id="right1"  style="padding-top:0;height: 300px;background-color: #f0f0f0;border: 1px solid gainsboro;width">
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active row" id="home-tab">
    <div class="col-sm-3" style="margin-top:67px">
      <div style="text-align:center"><h3 class="space-on-bottom-lg" style="color:black"> Prefer email instead? </h3>
       <div><button class="btn btn-default space-on-bottom-20px space-on-top-10px" onclick="mailTab()">
         <span class="icon icon-envelope icon-gray tail5px"></span> Write to us</button>
         <p style="margin-top:7px;color:black">We are super quick in responding to your queries.</p>
        </div>
     </div>
    </div>  
    <div class="col-sm-9" style="border-left:1px solid gainsboro;height:297px"> 
    <ul class="control-sidebar-menu">
        <div class="col-sm-12" style="padding-right: 50px;margin-top: 20px;margin-bottom: 20px">
           <div class="col-sm-1"></div>
           <div class="col-sm-11">
            <div class="search-box"><input type="text" data-autofocus="true" placeholder="What can we help you with? Start typing your question..."></div> 
          </div>        
        </div>
        <div class="col-sm-12" style="padding-left: 10%;padding-right: 3px;margin-top: 20px;margin-bottom: 20px">
         <div class="col-sm-3">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Staff</h4>
  
              </div>
            </a>
          </li>
          </div>
          <div class="col-sm-2">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Email</h4>
              </div>
            </a>
          </li>
         </div>
         <div class="col-sm-2">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-database bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Manage</h4>
              </div>
            </a>
          </li>
          </div>
          <div class="col-sm-2">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-ticket bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Ticket</h4>  
              </div>
            </a>
          </li>
          </div>
          <div class="col-sm-3">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-cog bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Setting</h4>  
              </div>
            </a>
          </li>
          </div>
          </div>
          <div class="col-sm-12" style="padding-left: 10%;padding-right: 3px;margin-top: 20px;margin-bottom: 20px">
          <div class="col-sm-3">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-bug bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Error logs and debugging</h4>  
              </div>
            </a>
          </li>
          </div>
          <div class="col-sm-2">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-pie-chart bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Widgets</h4>  
              </div>
            </a>
          </li>
          </div>
          <div class="col-sm-2">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-plug bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Plugin</h4>  
              </div>
            </a>
          </li>
          </div>
          <div class="col-sm-2">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-cogs bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">API</h4>  
              </div>
            </a>
          </li>
          </div>
          <div class="col-sm-3">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-lock bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Logs</h4>  
              </div>
            </a>
          </li>
          </div>
        </div>
        </ul>
        </div>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane row" id="settings-tab">
      <div class="col-sm-3" style="margin-top:128px">
          <button class="btn btn-default" style="margin-left:60px" onclick="helpTab()"><i class="glyphicon glyphicon-search"></i>&nbsp;Browse Help Articles</button>
    </div>  
      <div class="col-sm-9" style="border-left:1px solid gainsboro;height:296px">
        <form method="post" style="margin-top: 25px">
          <div class="col-sm-12" style="padding-left: 2px;padding-right: 5px">
             <div class="col-sm-2">
             </div>
             <div class="col-sm-2" style="padding-left: 0px;padding-right: 0px">
                <label style="line-height: 2.0;color: black">Email</label>
             </div>
             <div class="col-sm-5" style="padding-left: 0px;padding-right: 0px">
                <input type="eamil" name="" value="support@ithelpdesk.com" class="form-control" placeholder="Enter Your Email" style="border-radius: 0px;" disabled="disabled">
             </div>
          </div>
        
        <div class="col-sm-12" style="margin-top: 20px;padding-left: 2px;padding-right: 5px">
            <div class="col-sm-2">
             </div>
            <div class="col-sm-2" style="padding-left: 0px;padding-right: 0px">
                <label style="line-height: 2.0;color: black">Subject</label>
             </div>
             <div class="col-sm-5" style="padding-left: 0px;padding-right: 0px">
                <input type="email" name="" value="" class="form-control" placeholder="Subject" style="border-radius: 0px;">
             </div>
        </div>
        <div class="col-sm-12" style="margin-top: 20px;padding-left: 2px;padding-right: 5px">
            <div class="col-sm-2">
             </div>
            <div class="col-sm-2" style="padding-left: 0px;padding-right: 0px">
                <label style="line-height: 2.0;color: black">Message</label>
             </div>
             <div class="col-sm-5" style="padding-left: 0px;padding-right: 0px">
                <textarea name="" class="form-control" style="border-radius: 0px;" placeholder="Message Here"></textarea>
             </div>
        </div>
        <div class="col-sm-12" style="text-align: center;margin-top: 20px;">
           <div class="col-sm-4">
           </div>
           <div class="col-sm-3">
              <div style="float:left"><div class="fileupload-wrapper" style="position: relative; overflow: hidden; direction: ltr;"><a href="#" tabindex="-1" class="file-attach-link" style="color: #00c0ef;"><i class="glyphicon glyphicon-paperclip small"></i>Attach files</a><input qq-button-id="9f785c98-e24f-476a-ad1d-790faf404528" multiple="" type="file" name="doc" title="File input" style="position: absolute; right: 0px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0; height: 100%;"></div></div>
           </div>
           <div class="col-sm-3">
            <button class="btn btn-primary">Send</button>
          </div>
        </div>     
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
        </div><!-- ./wrapper -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="{{asset("lb-faveo/js/ajax-jquery.min.js")}}" type="text/javascript"></script>

        <!-- moment.js -->
        <script src="{{asset("lb-faveo/plugins/moment/moment.js")}}" type="text/javascript"></script>

        <script src="{{asset("lb-faveo/js/bootstrap-datetimepicker4.7.14.min.js")}}" type="text/javascript"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="{{asset("lb-faveo/js/bootstrap.min.js")}}" type="text/javascript"></script>
        <!-- Slimscroll -->
        
        <!-- FastClick -->
        <script src="{{asset("lb-faveo/plugins/fastclick/fastclick.min.js")}}"  type="text/javascript"></script>
        <!-- AdminLTE App -->
       <script src="{{asset("lb-faveo/plugins/slimScroll/jquery.slimscroll.min.js")}}" type="text/javascript"></script>
       
        <!-- AdminLTE App -->
        <script src="{{asset("lb-faveo/js/app.min.js")}}" type="text/javascript"></script>
<!--         iCheck -->
        <script src="{{asset("lb-faveo/plugins/iCheck/icheck.min.js")}}" type="text/javascript"></script>
        <!-- jquery ui -->
        <script src="{{asset("lb-faveo/js/jquery.ui.js")}}" type="text/javascript"></script>


        <script src="{{asset("lb-faveo/plugins/datatables/dataTables.bootstrap.js")}}" type="text/javascript"></script>

        <script src="{{asset("lb-faveo/plugins/datatables/jquery.dataTables.js")}}" type="text/javascript"></script>
        <!-- Page Script -->
        <script src="{{asset("lb-faveo/js/jquery.dataTables1.10.10.min.js")}}" type="text/javascript" ></script>

        <script type="text/javascript" src="{{asset("lb-faveo/plugins/datatables/dataTables.bootstrap.js")}}"  type="text/javascript"></script>

        <script src="{{asset("lb-faveo/js/jquery.rating.pack.js")}}" type="text/javascript"></script>

        <script src="{{asset("lb-faveo/plugins/select2/select2.full.min.js")}}" type="text/javascript"></script>


        <!-- full calendar-->
        <script src="{{asset('lb-faveo/plugins/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('lb-faveo/plugins/daterangepicker/daterangepicker.js')}}" type="text/javascript"></script>

        <script src="{{asset("vendor/unisharp/laravel-ckeditor/ckeditor.js")}}" type="text/javascript"></script>
      <script src="{{asset("lb-faveo/js/intlTelInput.js")}}" type="text/javascript"></script>

      <script type="text/javascript">
        if($('#ckeditor')[0]){
          CKEDITOR.replace('ckeditor', {
              toolbarGroups: [
                {"name": "basicstyles", "groups": ["basicstyles"]},
                {"name": "links", "groups": ["links"]},
                {"name": "paragraph", "groups": ["list", "blocks"]},
                {"name": "document", "groups": ["mode"]},
                {"name": "insert", "groups": ["insert"]},
                {"name": "styles", "groups": ["styles"]}
            ],
            removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar',
            disableNativeSpellChecker: false
            }).on('change',function(){
                $('#submit').removeAttr('disabled');
            });
      }
      </script>
        <script type="text/javascript">
         if($('#ckeditor1')[0]){
          CKEDITOR.replace('ckeditor1', {
              toolbarGroups: [
                {"name": "basicstyles", "groups": ["basicstyles"]},
                {"name": "links", "groups": ["links"]},
                {"name": "paragraph", "groups": ["list", "blocks"]},
                {"name": "document", "groups": ["mode"]},
                {"name": "insert", "groups": ["insert"]},
                {"name": "styles", "groups": ["styles"]}
               ],
            removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar',
            disableNativeSpellChecker: false
            }).on('change', function() { 
                     $('#submit').removeAttr('disabled');
            });
        }
      </script>
      <!--for page module-->
              <script type="text/javascript">
        if($('#myNicEditor')[0]){
          CKEDITOR.replace('myNicEditor', {
              toolbarGroups: [
                {"name": "basicstyles", "groups": ["basicstyles"]},
                {"name": "links", "groups": ["links"]},
                {"name": "paragraph", "groups": ["list", "blocks"]},
                {"name": "document", "groups": ["mode"]},
                {"name": "insert", "groups": ["insert"]},
                {"name": "styles", "groups": ["styles"]}
            ],
            removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar',
            disableNativeSpellChecker: false
            }).on('change', function() { 
                     $('#submit').removeAttr('disabled');
            });
        }
      </script>
      

        <script>
            $(document).ready(function () {
                //tabs
                
                $('.tabs').find('a').on('click',function(){
                    alert
                    if($(this).hasClass('active')){
                        if($('#slideUp')[0]){
                             $('#slideUp').slideToggle(); 
                        }
                        if($('.content-header').prevUntil('.tab-content').is('br')){
                           $('.content-header').prevUntil('.tab-content').remove()
                        }
                        else{
                           $('.tab-content + .content-header').before($('<br><br>'));
                        }
                    }
                })
                // for loader
                $(window).load(function(){
                      setTimeout(function(){
                              $('.loader1').css('display','none');
                        },800);
                })

             $('.sidebar-toggle').click(function () {
                   $('.main-sidebar').toggle();;
             });
            $('.noti_User').click(function () {
            var id = this.id;
                    var dataString = 'id=' + id;
                    $.ajax
                    ({
                    type: "POST",
                            url: "{{url('mark-read')}}" + "/" + id,
                            data: dataString,
                            cache: false,
                            success: function (html)
                            {
                            }
                    });
            });
            });
                    $('#read-all').click(function () {

            var id2 = <?php echo $auth_user_id ?>;
                    var dataString = 'id=' + id2;
                    $.ajax
                    ({
                    type: "POST",
                            url: "{{url('mark-all-read')}}" + "/" + id2,
                            data: dataString,
                            cache: false,
                            beforeSend: function () {
                            $('#myDropdown').on('hide.bs.dropdown', function () {
                            return false;
                            });
                                    $("#refreshNote").hide();
                                    $("#notification-loader").show();
                            },
                            success: function (response) {
                            $("#refreshNote").load("<?php echo $_SERVER['REQUEST_URI']; ?>  #refreshNote");
                                    $("#notification-loader").hide();
                                    $('#myDropdown').removeClass('open');
                            }
                    });
            });</script>
        <script>
                    $(function() {
                    // Enable check and uncheck all functionality
                    $(".checkbox-toggle").click(function() {
                    var clicks = $(this).data('clicks');
                            if (clicks) {
                    //Uncheck all checkboxes
                    $("input[type='checkbox']", ".mailbox-messages").iCheck("uncheck");
                    } else {
                    //Check all checkboxes
                    $("input[type='checkbox']", ".mailbox-messages").iCheck("check");
                    }
                    $(this).data("clicks", !clicks);
                    });
                            //Handle starring for glyphicon and font awesome
                            $(".mailbox-star").click(function(e) {
                    e.preventDefault();
                            //detect type
                            var $this = $(this).find("a > i");
                            var glyph = $this.hasClass("glyphicon");
                            var fa = $this.hasClass("fa");
                            //Switch states
                            if (glyph) {
                    $this.toggleClass("glyphicon-star");
                            $this.toggleClass("glyphicon-star-empty");
                    }
                    if (fa) {
                    $this.toggleClass("fa-star");
                            $this.toggleClass("fa-star-o");
                    }
                    });
                    });</script>

        <script src="{{asset("lb-faveo/js/tabby.js")}}" type="text/javascript"></script>

        <script src="{{asset("lb-faveo/plugins/filebrowser/plugin.js")}}" type="text/javascript"></script>

        <!-- <script src="{{asset("lb-faveo/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}" type="text/javascript"></script> -->

        <script type="text/javascript">
                    $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                    });</script>
        <script type="text/javascript">
                    function clickDashboard(e) {
                    if (e.ctrlKey === true) {
                    window.open('{{URL::route("dashboard")}}', '_blank');
                    } else {
                    window.location = "{{URL::route('dashboard')}}";
                    }
                    }

            function clickReport(e) {
            if (e.ctrlKey === true) {
            window.open('{{URL::route("report.get")}}', '_blank');
            } else {
            window.location = "{{URL::route('report.get')}}";
            }
            }
        </script>
        <script src="{{asset("lb-faveo/js/angular/angular.min.js")}}" type="text/javascript"></script>
        <script src="{{asset("lb-faveo/js/angular/ng-scrollable.min.js")}}" type="text/javascript"></script>
        <script src="{{asset("lb-faveo/js/angular/angular-moment.min.js")}}" type="text/javascript"></script>
<!--       <script>
    $(function() {
        $('input[type="checkbox"]:not(.not-apply)').iCheck({
            checkboxClass: 'icheckbox_flat-blue'
        }); 
        // $('input[type="radio"]:not(.not-apply)').iCheck({
        //     radioClass: 'iradio_flat-blue'
        // });
    
    });        
</scrip>-->
<script type="text/javascript">
    function changeLang(lang) {
        var link = "{{url('swtich-language')}}/"+lang;
        window.location = link;
    }
</script>
<script src="{{asset('lb-faveo/js/angular/ng-flow-standalone.js')}}"></script>
<script src="{{asset('lb-faveo/js/angular/fusty-flow.js')}}"></script>
<script src="{{asset('lb-faveo/js/angular/fusty-flow-factory.js')}}"></script>
<script src="{{asset('lb-faveo/js/angular/ng-file-upload.js')}}"></script>
<script src="{{asset('lb-faveo/js/angular/ng-file-upload-shim.min.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/ui-bootstrap-tpls.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/main.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/handleCtrl.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/nodeCtrl.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/nodesCtrl.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/treeCtrl.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/uiTree.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/uiTreeHandle.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/uiTreeNode.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/uiTreeNodes.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/helper.js')}}"></script>
<script type="text/javascript">
    function helpTab(){
     $('.nav-tabs li a[href="#home-tab"]').tab('show');
};
function mailTab(){
    $('.nav-tabs li a[href="#settings-tab"]').tab('show');
}
function openSlide(){
if ( document.getElementById("right").className.match(/(?:^|\s)right-side-menu control-sidebar-dark(?!\S)/) ){
         document.getElementById("right").className += "right-side-menu control-sidebar-dark right-side-menu-open";
}
else{
     document.getElementById("right").className = "right-side-menu control-sidebar-dark";
}
if ( document.getElementById("right1").className.match(/(?:^|\s)right-side-menu1 control-sidebar-dark(?!\S)/) ){
         document.getElementById("right1").className += "right-side-menu1 control-sidebar-dark right-side-menu-open1";
}
else{
     document.getElementById("right1").className = "right-side-menu1 control-sidebar-dark";
}
}
</script>
<script>
 var app = angular.module('fbApp', ['angularMoment','flow','ngFileUpload',,'ui.tree','ui.bootstrap']).directive('whenScrolled', function() {
    return function(scope, elm, attr) {
        var raw = elm[0];
        
        elm.bind('scroll', function() {

            if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                scope.$apply(attr.whenScrolled);
            }
        });
    };
});
app.constant("CSRF_TOKEN", '{{ csrf_token() }}');
app.directive('mediaLibScrolled', function() {
    return function(scope, elm, attr) {
        var raw = elm[0];
        console.log(raw);
        elm.bind('scroll', function() {

            if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                scope.$apply(attr.mediaLibScrolled);
            }
        });
    };
});
var fileProcess=null;
app.config(['flowFactoryProvider', function (flowFactoryProvider,$scope,$http) {
    fileProcess=flowFactoryProvider;
    
    flowFactoryProvider.on('fileError', function (file,message) {
        $('#errorFileMsg').css('display','block');
    });
   
    flowFactoryProvider.factory = fustyFlowFactory;
  }]);
app.run(function($http,$rootScope) {
            $rootScope.arrayImage=[];
       fileProcess.on('fileSuccess', function (file,message) {
        $('#mytabs a[href="#menu1"]').tab('show');
        $("#progressHide").hide();
        $("#progressHide").find('.transfer-box').remove();

        var URL=localStorage.getItem('mediaURL');

        $http.get(URL).success(function(data){
                
                $rootScope.arrayImage=data;

                $rootScope.disable=false;
                $rootScope.preview=true;
           $rootScope.viewImage=$rootScope.arrayImage.data[0]
           if($rootScope.arrayImage.data[0].type=="image"){
               
               $rootScope.inlineImage=false;
               $rootScope.viewImage=$rootScope.arrayImage.data[0].base_64;
               $rootScope.pathName=$rootScope.arrayImage.data[0].pathname;
               $rootScope.fileName=$rootScope.arrayImage.data[0].filename;
           }
           else if($rootScope.arrayImage.data[0].type=="text"){
               $rootScope.inlineImage=true;
               $rootScope.viewImage="{{asset('lb-faveo/media/images/txt.png')}}";
               $rootScope.pathName=$rootScope.arrayImage.data[0].pathname;
               $rootScope.fileName=$rootScope.arrayImage.data[0].filename;
           }
           else{
               $rootScope.inlineImage=true;
               $rootScope.viewImage="{{asset('lb-faveo/media/images/common.png')}}";
               $rootScope.pathName=$rootScope.arrayImage.data[0].pathname;
               $rootScope.fileName=$rootScope.arrayImage.data[0].filename;
           }
            
            setTimeout(function(){
                 $('label[for="happy0"]>img').css({'border': '1px solid #fff','box-shadow': '0 0 0 4px #0073aa'});
                 $('label[for="happy1"]>img').css({'border': 'none','box-shadow': 'none'});
           },100)
        })
        
    });
    
});
</script>
<script>
    $(document).ready(function(){
        
        if($('.alert-success').html()){
              
                setInterval(function(){
                    $('.alert-success').slideUp( 3000, function() {});
                }, 2000);
            }
        
    })
</script>
<?php Event::fire('show.calendar.script', array()); ?>
        <?php Event::fire('load-calendar-scripts', array()); ?>
        @yield('FooterInclude')
        @stack('scripts')
    </body>
    <script type="text/javascript">
 $(function () {
       if('{{Lang::getLocale()}}'=='ar'){
    var adminRtl = document.createElement('link');
    adminRtl.id = 'id-rtl';
    adminRtl.rel = 'stylesheet';
    adminRtl.href = '{{asset("lb-faveo/rtl/css/AdminLTE.css")}}';
    document.head.appendChild(adminRtl);

     var cssRtl = document.createElement('link');
    cssRtl.id = 'id-csstrtl';
    cssRtl.rel = 'stylesheet';
    cssRtl.href = '{{asset("lb-faveo/rtl/css/rtl.css")}}';
    document.head.appendChild(cssRtl);

     var bootRtl = document.createElement('link');
    bootRtl.id = 'id-bootrtl';
    bootRtl.rel = 'stylesheet';
    bootRtl.href = '{{asset("lb-faveo/rtl/css/bootstrap-rtl.min.css")}}';
    document.head.appendChild(bootRtl);


        
          $('#adminLTR').remove();
        $('.container').attr('dir','RTL');
        $('.formbilder').attr('dir','RTL');
        $('.content-area').attr('dir','RTL');
        // agentpanel
        $('.content').attr('dir','RTL');
        $('.info').attr('dir','RTL');
        $('.table').attr('dir','RTL');
        $('.box-primary').attr('dir','RTL');
        // box-header with-borderclass="box box-primary"
        $('.dataTables_paginate').find('.row').attr('dir','RTL');
        // dataTables_paginate paging_full_numbers
        $('.sidebar-menu').attr('dir','RTL');
        $('.sidebar-menu').find('.pull-right').removeClass("pull-right");
        $('.sidebar-menu').find('.label').addClass("pull-left");
        $('.content').find('.btn').removeClass("pull-right");
        $('.content').find('.btn').addClass("pull-left");
        $('.tabs-horizontal').removeClass("navbar-left");
        $('.tabs-horizontal').addClass("navbar-right");
        $('#right-menu').removeClass("navbar-right");
        $('#right-menu').addClass("navbar-left");
        $('.navbar-nav').find('li').css("float","right");
        $('#rtl1').css('display','none');
        $('#ltr1').css('display','block');
        $('#rtl2').css('display','none');
        $('#ltr2').css('display','block');
        $('#rtl3').css('display','none');
        $('#ltr3').css('display','block');  
        $('#rtl4').css('display','none');
        $('#ltr4').css('display','block');  
        $('.box-header').find('.pull-right').addClass("pull-left");
       $('.box-header').find('.pull-right').removeClass("pull-right");
         $('.btn').removeClass("pull-left");
        $('.iframe').attr('dir','RTL');
         $('.box-footer').find('a').removeClass("pull-right");
         $('.box-footer').find('a').addClass("pull-left");
         $('.box-footer').find('div').removeClass("pull-right");
         $('.box-footer').find('div').addClass("pull-left");
         $('.col-md-3').css('float','right');
         $('.user-footer').css('float','none');
         $('.user-header').css('float','none');
         $('.sidebar-toggle').css('width','60px');
         $('.dropdown-menu').css('right','inherit');
         $('.dropdown-menu').css('left','0');
// chart-data
        // label
          $('.box-header').find('.btn-primary').find('.pull-right').removeClass("pull-right");
        $('.box-header').find('.btn-primary').addClass("pull-left");
        $('.main-footer').find('.pull-right').removeClass("pull-right");
        $('.main-footer').find('.hidden-xs').addClass("pull-left");
        
        setTimeout(function(){  
        $('#cke_details').addClass( "cke_rtl" );
        $(".cke_wysiwyg_frame").contents().find("body").attr('dir','RTL');

        }, 3000);
        $('iframe').contents().find("body").attr('dir','RTL');
        /*$('#wys-form').remove();
        $('#mobile-RTL').css('display','block');
        $('#mobile-normal').css('display','none');
        $('#form-foot1').css('display','block');
        $('.list-inline').attr('dir','RTL');*/
       };
    });
</script>


@push('scripts')
<script src="{{asset("lb-faveo/plugins/slimScroll/jquery.slimscroll.min.js")}}" type="text/javascript"></script>
<script src="{{asset("lb-faveo/js/app.min.js")}}" type="text/javascript"></script>
@endpush

</html> 