<!DOCTYPE html>
<html  ng-app="fbApp">
 <?php
    $company = App\Model\helpdesk\Settings\Company::where('id', '=', '1')->first();
    $portal = App\Model\helpdesk\Theme\Portal::where('id', '=', 1)->first();
      $title = App\Model\helpdesk\Settings\System::where('id', '=', '1')->first();
        if (isset($title->name)) {
            $title_name = $title->name;
        } else {
            $title_name = "SUPPORT CENTER";
        }
 ?>
    <head>
        <meta charset="UTF-8">
       
         <title> @yield('title') {!! strip_tags($title_name) !!} </title>
      
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- faveo favicon -->
        @if($portal->icon!=0)
            <link href="{{asset("uploads/icon/$portal->icon")}}" rel="shortcut icon">
        @else
        <link href="{{asset("lb-faveo/media/images/favicon.ico")}}" rel="shortcut icon"> 
        @endif
        <!-- Bootstrap 3.3.2 -->
        <link href="{{asset("lb-faveo/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="{{asset("lb-faveo/css/font-awesome.min.css")}}" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{asset("lb-faveo/css/ionicons.min.css")}}" rel="stylesheet" type="text/css" >
        <!-- Theme style -->
        <link href="{{asset("lb-faveo/css/AdminLTE.css")}}" rel="stylesheet" type="text/css" id="adminLTR" />
        <!-- <link href="{{asset("lb-faveo/rtl/css/AdminLTE.css")}}" id="adminRTL" rel="stylesheet" type="text/css" disabled='disabled' />
         -->
         <!-- <link href="{{asset("lb-faveo/css/AdminLTE.css")}}" rel="stylesheet" type="text/css" id="adminLTR"/>  -->
        <!-- RTL -->
        <!-- <link href="{{asset("lb-faveo/rtl/css/rtl.css")}}" rel="stylesheet" type="text/css" id="cssRTL" disabled='disabled'/> -->
        <link href="{{asset("lb-faveo/css/jquery.rating.css")}}" rel="stylesheet" type="text/css" />

        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link href="{{asset("lb-faveo/css/skins/_all-skins.min.css")}}" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="{{asset("lb-faveo/plugins/iCheck/flat/blue.css")}}" rel="stylesheet" type="text/css" />
        <!-- This controlls the top tabs -->
        <link href="{{asset("lb-faveo/css/tabby.css")}}" rel="stylesheet" type="text/css" >
        <!-- In app notification style -->
        <link href="{{asset("css/notification-style.css")}}" rel="stylesheet" type="text/css">

        <link href="{{asset("lb-faveo/css/jquerysctipttop.css")}}" rel="stylesheet" type="text/css">

        <link  href="{{asset("lb-faveo/css/editor.css")}}" rel="stylesheet" type="text/css">

        <link href="{{asset("lb-faveo/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")}}" rel="stylesheet" type="text/css" />

        <link href="{{asset("lb-faveo/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css" >
        <!--select 2-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
        <!-- Colorpicker -->
        
        <link href="{{asset("lb-faveo/plugins/colorpicker/bootstrap-colorpicker.min.css")}}" rel="stylesheet" type="text/css" />
 <!--        <link href="{{asset('lb-faveo/js/form/app.css')}}" rel="stylesheet" type="text/css" >
        <link href="{{asset('lb-faveo/js/form/angular-ui-tree.css')}}" rel="stylesheet" type="text/css" > 
        -->
        <script src="{{asset("lb-faveo/plugins/filebrowser/plugin.js")}}" type="text/javascript"></script>

        <script src="{{asset("lb-faveo/js/jquery-2.1.4.js")}}" type="text/javascript"></script>

        <script src="{{asset("lb-faveo/js/jquery2.1.1.min.js")}}" type="text/javascript"></script>
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        @yield('HeadInclude')
         <style type="text/css">
         
            @font-face {
                      font-family: dubaiFonts;
                      src: url({{asset("lb-faveo/fonts/DubaiW23-Light.woff")}});
                   }
            body{
                font-family:dubaiFonts !important;
            }
        </style>
        <!-- rtl brecket -->
<!--  <style type="text/css">
     *:after {
    content: "\200E‎";
}
 </style> -->
        <style>
            .content
            {
                padding: 19px;
            }
        .loader {
            height: 2px;
            width: 100%;
            position: relative;
            overflow: hidden;
            background-color:#ff0000;
        }
       .loader:before{
            display: block;
            position: absolute;
            content: "";
            left: -200px;
            width: 200px;
            height: 4px;
            background-color:#ffffff;
            animation: loading 2s linear infinite;
        }
        @keyframes loading {
            from {left: -200px; width: 30%;}
          50% {width: 30%;}
    70% {width: 70%;}
    80% { left: 50%;}
    95% {left: 120%;}
    to {left: 100%;}
}
.sidebar-mini.sidebar-collapse .content-wrapper, .sidebar-mini.sidebar-collapse .right-side, .sidebar-mini.sidebar-collapse .main-footer {
    margin-left: 0px !important;
    z-index: 840;
}
.sidebar-mini.sidebar-collapse .main-sidebar {
    -webkit-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    -o-transform: translate(0, 0);
    transform: translate(0, 0);
    width: 0px !important;
    z-index: 850;
}
.note-editor{
    border-radius: 0px;
    margin-right: 140px;
}
/*.col-sm-4{
    text-align: right;
}*/
.drop-box {
    width: 170px;
    text-align: center;
    padding: 50px 10px;
    margin: auto;
    width:100%;
    min-height: 250px;
}
.input-hidden {
  position: absolute;
  left: -9999px;
}

input[type=radio]:checked + label>img {
  border: 1px solid #fff;
  box-shadow: 0 0 3px 3px #090;
}
.nav-tabs.control-sidebar-tabs > li.active>.tab-slant::before{
    background-color:#f0f0f0 !important;
 }
 .control-sidebar-dark .nav-tabs.control-sidebar-tabs > li.active > a, .control-sidebar-dark .nav-tabs.control-sidebar-tabs > li.active > a:hover, .control-sidebar-dark .nav-tabs.control-sidebar-tabs > li.active > a:focus, .control-sidebar-dark .nav-tabs.control-sidebar-tabs > li.active > a:active {
    background: transparent !important;
    color:#444 !important;
}
 .nav-tabs.control-sidebar-tabs > li:hover>.tab-slant::before{
    background-color:#f0f0f0 !important;
 }
 .nav-tabs.control-sidebar-tabs > li:hover>a{
    color:#777 !important;
 }

/*input[type=radio]:checked + label>img {
  transform: 
    rotateZ(-10deg) 
    rotateX(10deg);
}*/
.right-side-menu.right-side-menu-open, .right-side-menu.right-side-menu-open + .right-side-menu-bg {
     right:-119px;
    position: fixed;
    overflow-y: auto;
    overflow-x: hidden;
}
.right-side-menu1.right-side-menu-open1, .right-side-menu.right-side-menu-open1 + .right-side-menu-bg1 {
    right: 0;
    position: fixed;
    overflow-y: auto;
    overflow-x: hidden;
   }
.control-sidebar-dark, .control-sidebar-dark + .right-side-menu-bg,.control-sidebar-dark + .right-side-menu-bg1 {
    background: #222d32;
}
.control-sidebar-dark {
    color: #b8c7ce;
}
.right-side-menu1{
    bottom: 0;
    right: -100%;
    width: 100%;
    -webkit-transition: right .3s ease-in-out;
    -o-transition: right .3s ease-in-out;
    transition: right .3s ease-in-out;
}
.right-side-menu,.right-side-menu1{
    position: fixed;
    padding-top: 50px;
    z-index: 1010;
}
.right-side-menu-bg1,.right-side-menu,.right-side-menu-bg{
    bottom: 0;
    right: -76%;
    width: 76%;
    -webkit-transition: right .3s ease-in-out;
    -o-transition: right .3s ease-in-out;
    transition: right .3s ease-in-out;
}
.tab-content{
    padding-left: 13px;
}
.control-sidebar-dark .nav-tabs.control-sidebar-tabs > li > a, .control-sidebar-dark .nav-tabs.control-sidebar-tabs > li > a:hover{
         border-left-color: transparent !important;
         border-bottom-color: transparent !important;
}
.control-sidebar-subheading{
    color: black !important;
}
#trapezoid{
    position: relative;
    padding: 10px 15px;
    outline: 0;
    z-index: 2;
    top: 2px;
}

#trapezoid:after { 
    border-bottom: 50px solid #367fa9;
     border-left: 20px solid transparent; 
     border-right: 20px solid transparent;
         
    
  }
  .search-box{
     position:relative;
     width:95%;  
  }
 .search-box:before {
    display: inline-block;
    font-family: 'Glyphicons Halflings';
    font-style: normal;
    font-weight: 400;
    line-height: 1;
    position: absolute;
    top: 10px;
    left: 12px;
    content: "\e003";
    color: #999;
}
.search-box input[type=text] {
    display: block;
    width: 100%;
    padding: 4px 6px 4px 35px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #e7e8e6;
    -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    margin-bottom: 10px;
    box-shadow: none;
    font-size: 1.2em;
    height: 35px;
    border-radius: 25px;
}
.help-widget-close {
    font-style: normal;
    font-weight: 400;
    line-height: 1;
    top: 0px;
    right: 20px;
    width: 66px;
    background: #555;
    border-bottom: 1px solid #ccc;
    z-index: 0;
}
.control-sidebar-dark .nav-tabs.control-sidebar-tabs > li > a {
    background: transparent;
    color: #eee;
}
.tab-slant::before {
    content: '';
    position: absolute;
    top: -10px;
    right: -10px;
    bottom: -7px;
    left: -10px;
    border: 1px solid #bbb;
    border-radius: 5px;
    z-index: -1;
    background: #d5d5d5;
    -webkit-transform: perspective(100px) rotateX(40deg);
    transform: perspective(100px) rotateX(40deg);
}
.control-sidebar-menu {
    list-style: none;
    padding: 0;
    margin: 0 -15px;
}
.control-sidebar-subheading {
    display: block;
    font-weight: 400;
    font-size: 14px;
}
.control-sidebar-menu .menu-info {
    margin-left: 45px;
    margin-top: 3px;
}
.control-sidebar-menu .menu-icon {
    float: left;
    width: 35px;
    height: 35px;
    border-radius: 50%;
    text-align: center;
    line-height: 35px;
}
.tab-slant {
    border: 0;
    padding: 0;
    margin: 0;
    line-height: 1;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: #d5d5d5;
    color: #999;
    z-index: -1;
}
.control-sidebar-tabs li{
    margin: 0 10px;
    float: right !important;
    width: 125px !important;
    text-align: center;
    z-index: 1;
}
.nav-tabs > li.active > a{
    border: none !important;
}
.notific{
    padding: 15px !important;
}
        </style>
    </head>
    @if($portal->admin_header_color)
    <body class="{{$portal->admin_header_color}} fixed" ng-controller="MainCtrl">
    <div class="loader" style="z-index:2000"></div>
    @else
    <body class="skin-yellow fixed" ng-controller="MainCtrl">
    <div class="loader" style="z-index:2000"></div>
    @endif
        <?php
        $replacetop = 0;
        $replacetop = \Event::fire('service.desk.admin.topbar.replace', array());
        if (count($replacetop) == 0) {
            $replacetop = 0;
        } else {
            $replacetop = $replacetop[0];
        }
        $replaceside = 0;
        $replaceside = \Event::fire('service.desk.admin.sidebar.replace', array());
        if (count($replaceside) == 0) {
            $replaceside = 0;
        } else {
            $replaceside = $replaceside[0];
        }
        //dd($replacetop);
        ?>
        <div class="wrapper">
            <header class="main-header">
                <!-- <a href="http://www.faveohelpdesk.com" class="logo"> -->
                 <a href="{{$company->website}}" class="logo">

                    @if($portal->logo!=0)
                <img src='{{ asset("uploads/logo/$portal->logo")}}' class="img-rounded" alt="Cinque Terre" width="304" height="45">
               @else
                <img src="{{ asset('lb-faveo/media/images/logo.png') }}" width="100px">
               @endif
               </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <?php $notifications = App\Http\Controllers\Common\NotificationController::getNotifications(); ?>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-collapse">
 
                        <ul class="nav navbar-nav navbar-left" id="left-menu">
                            @if($replacetop==0)
                            <li @yield('settings')><a href="{!! url('dashboard') !!}">{!! Lang::get('lang.agent_panel') !!}</a></li>
                            @else
                            <?php \Event::fire('service.desk.admin.topbar', array()); ?>
                            @endif
                        </ul>
                        <ul class="nav navbar-nav navbar-right" id="right-menu">
                            <li><a href="{{url('admin')}}">{!! Lang::get('lang.admin_panel') !!}</a></li>
                            <!-- START NOTIFICATION --> 
                            @include('themes.default1.inapp-notification.notification')
                            
                            <!-- END NOTIFICATION --> 
                        <li class="dropdown">
                            <?php $src = Lang::getLocale().'.png'; ?>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><img src="{{asset("lb-faveo/flags/$src")}}"></img> &nbsp;<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                @foreach($langs as $key => $value)
                                            <?php $src = $key.".png"; ?>
                                            <li><a href="#" id="{{$key}}" onclick="changeLang(this.id)"><img src="{{asset("lb-faveo/flags/$src")}}"></img>&nbsp;{{$value}}</a></li>
                                @endforeach       
                            </ul>
                        </li>                        
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                @if(Auth::user())

                                <img src="{{Auth::user()->profile_pic}}"class="user-image" alt="User Image"/>

                                <span class="hidden-xs">{!! Auth::user()->first_name." ".Auth::user()->last_name !!}</span>
                                @endif
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header" style="background-color:#343F44;">
                                    @if(Auth::user())
                                    <img src="{{Auth::user()->profile_pic}}" class="img-circle" alt="User Image" />
                                    
                                    <p>
                                    <span>{!! Lang::get('lang.hello') !!}</span><br/>
                                        {!! Auth::user()->first_name !!}{!! " ". Auth::user()->last_name !!} - {{Auth::user()->role}}
                                        <small></small>
                                    </p>
                                    @endif
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer"  style="background-color:#1a2226;">
                                    <div class="pull-left">
                                        <a href="{{url('admin-profile')}}" class="btn btn-info btn-sm"><b>{!! Lang::get('lang.profile') !!}</b></a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{url('auth/logout')}}" class="btn btn-danger btn-sm"><b>{!! Lang::get('lang.sign_out') !!}</b></a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <div class="user-panel" style="height: 150px;">
                        <div class = "row">
                            <div class="col-xs-3"></div>
                            <div class="col-xs-2" style="width:50%;">
                                <a href="{!! url('profile') !!}">
                                    <img src="{{Auth::user()->profile_pic}}" class="img-circle" alt="User Image" />
                                </a>
                            </div>
                        </div>
                        <div class="info" style="text-align:center;">
                            @if(Auth::user())
                            <p>{!! Auth::user()->first_name !!}{!! " ". Auth::user()->last_name !!}</p>
                            @endif
                            @if(Auth::user() && Auth::user()->active==1)
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                            @else
                            <a href="#"><i class="fa fa-circle"></i> Offline</a>
                            @endif
                        </div>
                    </div>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        @if($replaceside==0)
                        <center><a href="{{url('admin')}}"><li class="header"><span style="font-size:1.5em;">{{ Lang::get('lang.admin_panel') }}</span></li></a></center>
                        <li class="header">{!! Lang::get('lang.settings-2') !!}</li>
                        <li class="treeview @yield('Staffs')">
                            <a  href="#">
                                <i class="fa fa-users"></i> <span>{!! Lang::get('lang.staffs') !!}</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @yield('agents')><a href="{{ url('agents') }}"><i class="fa fa-user "></i>{!! Lang::get('lang.agents') !!}</a></li>
                                <li @yield('departments')><a href="{{ url('departments') }}"><i class="fa fa-sitemap"></i>{!! Lang::get('lang.departments') !!}</a></li>
                                <li @yield('teams')><a href="{{ url('teams') }}"><i class="fa fa-users"></i>{!! Lang::get('lang.teams') !!}</a></li>
                                {{--<li @yield('groups')><a href="{{ url('groups') }}"><i class="fa fa-key"></i>{!! Lang::get('lang.groups') !!}</a></li>--}}
                                {{--<li @yield('report')><a href="{{ url('report/get') }}"><i class="fa fa-pie-chart"></i>{!! Lang::get('lang.report') !!}</a></li>--}}
                            </ul>
                        </li>

                        <li class="treeview @yield('Emails')">
                            <a href="#">
                                <i class="fa fa-envelope-o"></i>
                                <span>{!! Lang::get('lang.email') !!}</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @yield('emails')><a href="{{ url('emails') }}"><i class="fa fa-envelope"></i>{!! Lang::get('lang.emails') !!}</a></li>
                                <li @yield('ban')><a href="{{ url('banlist') }}"><i class="fa fa-ban"></i>{!! Lang::get('lang.ban_lists') !!}</a></li>
                                <li @yield('template')><a href="{{ url('template-sets') }}"><i class="fa fa-mail-forward"></i>{!! Lang::get('lang.templates') !!}</a></li>
                                <li @yield('email')><a href="{{url('getemail')}}"><i class="fa fa-at"></i>{!! Lang::get('lang.email-settings') !!}</a></li>
                                <li @yield('queue')><a href="{{ url('queue') }}"><i class="fa fa-upload"></i>{!! Lang::get('lang.queues') !!}</a></li>
                                <li @yield('diagnostics')><a href="{{ url('getdiagno') }}"><i class="fa fa-plus"></i>{!! Lang::get('lang.diagnostics') !!}</a></li>
                               
                                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Auto Response</a></li> -->
                                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Rules/a></li> -->
                                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Breaklines</a></li> -->
                                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Log</a></li> -->
                            </ul>
                        </li>

                        <li class="treeview @yield('Manage')">
                            <a href="#">
                                <i class="fa  fa-cubes"></i>
                                <span>{!! Lang::get('lang.manage') !!}</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @yield('help')><a href="{{url('helptopic')}}"><i class="fa fa-file-text-o"></i>{!! Lang::get('lang.help_topics') !!}</a></li>
                                <li @yield('sla')><a href="{{url('sla')}}"><i class="fa fa-clock-o"></i>{!! Lang::get('lang.sla_plans') !!}</a></li>
                                <li @yield('business_hours')><a href="{{url('sla/business-hours/index')}}"><i class="fa fa-calendar"></i>{!! Lang::get('lang.business_hours') !!}</a></li>
                                <!-- <li>
                                        <a href="{{route('sla.business.hours.index')}}"><i class="fa fa-clock-o"></i>{!! Lang::get('lang.business_hours') !!}</a>
                                    </li> -->
                                <li @yield('forms')><a href="{{url('forms/create')}}"><i class="fa fa-file-text"></i>{!! Lang::get('lang.forms') !!}</a></li>
                                <li @yield('workflow')><a href="{{url('workflow')}}"><i class="fa fa-sitemap"></i>{!! Lang::get('lang.workflow') !!}</a></li>
                                <li @yield('priority')><a href="{{url('ticket/priority')}}"><i class="fa fa-asterisk"></i>{!! Lang::get('lang.priority') !!}</a></li>
                                <li @yield('ticket-types')><a href="{{url('ticket-types')}}"><i class="fa fa-list-ol"></i>{!! Lang::get('lang.ticket_type') !!}</a></li>
                                
                            </ul>
                        </li>
                        <li class="treeview @yield('Tickets')">
                            <a  href="#">
                                <i class="fa fa-ticket"></i> <span>{!! Lang::get('lang.tickets') !!}</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @yield('tickets')><a href="{{url('getticket')}}"><i class="fa fa-file-text"></i>{!! Lang::get('lang.ticket') !!}</a></li>
                                <li @yield('auto-response')><a href="{{url('getresponder')}}"><i class="fa fa-reply-all"></i>{!! Lang::get('lang.auto_response') !!}</a></li>
                                <li @yield('alert')><a href="{{url('alert')}}"><i class="fa fa-bell"></i>{!! Lang::get('lang.alert_notices') !!}</a></li>
                                <li @yield('status')><a href="{{url('setting-status')}}"><i class="fa fa-plus-square-o"></i>{!! Lang::get('lang.status') !!}</a></li>
                                <li @yield('labels')><a href="{{url('labels')}}"><i class="fa fa-lastfm"></i>{!! Lang::get('lang.labels') !!}</a></li>
                                <li @yield('ratings')><a href="{{url('getratings')}}"><i class="fa fa-star"></i>{!! Lang::get('lang.ratings') !!}</a></li>
                                <li @yield('close-workflow')><a href="{{url('close-workflow')}}"><i class="fa fa-sitemap"></i>{!! Lang::get('lang.close-workflow') !!}</a></li>
                                <li @yield('tags')><a href="{{url('tag')}}"><i class="fa fa-tags"></i>{!! Lang::get('lang.tags') !!}</a></li>
                                <li @yield('bill')><a href="{{url('bill')}}"><i class="fa fa-money"></i>{!! Lang::get('lang.bill') !!}</a></li>
                                <li @yield('auto-assign')><a href="{{url('auto-assign')}}"><i class="fa fa-check-square-o"></i>{!! Lang::get('lang.auto_assign') !!}</a></li>
                            </ul>
                        </li>
                        <li class="treeview @yield('Settings')">
                            <a href="#">
                                <i class="fa fa-cog"></i>
                                <span>{!! Lang::get('lang.settings') !!}</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @yield('company')><a href="{{url('getcompany')}}"><i class="fa fa-building"></i>{!! Lang::get('lang.company') !!}</a></li>
                                <li @yield('system')><a href="{{url('getsystem')}}"><i class="fa fa-laptop"></i>{!! Lang::get('lang.system') !!}</a></li>
                                <li @yield('user-options')><a href="{{url('user-options')}}"><i class="fa fa-user"></i>{!! Lang::get('lang.user-options') !!}</a></li>
                                <li @yield('social-login')><a href="{{ url('social/media') }}"><i class="fa fa-globe"></i> {!! Lang::get('lang.social-login') !!}</a></li>
                                <li @yield('languages')><a href="{{url('languages')}}"><i class="fa fa-language"></i>{!! Lang::get('lang.language') !!}</a></li>
                                <li @yield('cron')><a href="{{url('job-scheduler')}}"><i class="fa fa-hourglass"></i>{!! Lang::get('lang.cron') !!}</a></li>
                                <li @yield('security')><a href="{{url('security')}}"><i class="fa fa-lock"></i>{!! Lang::get('lang.security') !!}</a></li>
                                <li @yield('notification')><a href="{{url('settings-notification')}}"><i class="fa fa-bell"></i>{!! Lang::get('lang.notifications') !!}</a></li>
                                <li @yield('Approval')><a href="{{url('approval/settings')}}"><i class="glyphicon glyphicon-ok-sign"></i>{!! Lang::get('lang.approval') !!}</a></li>
                                <li @yield('storage')><a href="{{url('storage')}}"><i class="fa fa-save"></i>{!! Lang::get('storage::lang.storage') !!}</a></li>
                                @if($dummy_installation == 1 || $dummy_installation == '1')
                                <li @yield('clean')><a href="{{route('clean-database')}}"><i class="fa fa-undo"></i>{!! Lang::get('lang.delete_dummy_data') !!}</a></li>
                                @endif
                            </ul>
                        </li>
                        <li class="treeview @yield('error-bugs')">
                            <a href="#">
                                <i class="fa fa-heartbeat"></i>
                                <span>{!! Lang::get('lang.error-debug') !!}</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <!-- <li @yield('error-logs')><a href="{{ route('error.logs') }}"><i class="fa fa-list-alt"></i> {!! Lang::get('lang.view-logs') !!}</a></li> -->
                                <li @yield('debugging-option')><a href="{{ route('err.debug.settings') }}"><i class="fa fa-bug"></i> {!! Lang::get('lang.debug-options') !!}</a></li>
                            </ul>
                        </li>
                        <li class="treeview @yield('Themes')">
                            <a href="#">
                                <i class="fa fa-pie-chart"></i>
                                <span>{!! Lang::get('lang.widgets') !!}</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @yield('widget')><a href="{{ url('widgets') }}"><i class="fa fa-list-alt"></i> {!! Lang::get('lang.widgets') !!}</a></li>
                                <li @yield('socail')><a href="{{ url('social-buttons') }}"><i class="fa fa-cubes"></i> {!! Lang::get('lang.social') !!}</a></li>
                            </ul>
                        </li>
                        <li class="treeview @yield('Plugins')">
                            <a href="{{ url('plugins') }}">
                                <i class="fa fa-plug"></i>
                                <span>{!! Lang::get('lang.plugin') !!}</span>
                            </a>
                        </li>
                        <li class="treeview @yield('API')">
                            <a href="{{ url('api') }}">
                                <i class="fa fa-cogs"></i>
                                <span>{!! Lang::get('lang.api') !!}</span>
                            </a>
                        </li>
                        <li class="treeview @yield('Log')">
                            <a href="{{ url('logs') }}">
                                <i class="fa fa-lock"></i>
                                <span>{!! Lang::get('lang.logs') !!}</span>
                            </a>
                        </li>
                        @endif
                        <?php \Event::fire('service.desk.admin.sidebar', array()); ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header"  style="margin-top: 5px">
                    <!--<div class="row">-->
                    <!--<div class="col-md-6">-->
                    @yield('PageHeader')
                    <!--</div>-->
                    {!! Breadcrumbs::renderIfExists() !!}
                    <!--</div>-->
                </section>

                <!-- Main content -->
                <section class="content">
                    @if($dummy_installation == 1 || $dummy_installation == '1')
                    <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="icon fa  fa-exclamation-triangle"></i> {{Lang::get('lang.dummy_data_installation_message')}} <a href="{{route('clean-database')}}">{{Lang::get('lang.click')}}</a> {{Lang::get('lang.clear-dummy-data')}}
                    </div>
                    @else
                        @if ($is_mail_conigured->count() < 1)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-warning lead">
                                    <h4><i class="fa fa-exclamation-triangle"></i>&nbsp;{{Lang::get('Alert')}}</h4>
                                    <p style="font-size:0.8em">
                                        @if (\Auth::user()->role == 'admin')
                                        {{Lang::get('lang.system-outgoing-incoming-mail-not-configured')}}&nbsp;<a href="{{URL::route('emails.create')}}">{{Lang::get('lang.confihure-the-mail-now')}}</a>
                                        @else
                                        {{Lang::get('lang.system-mail-not-configured-agent-message')}}
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endif
                    @yield('content')
                </section><!-- /.content -->
                <!-- /.content-wrapper -->
            </div>
            <footer class="main-footer">
                <div  class="hide" style="position: fixed;right:0;bottom:0">
                      <button data-toggle="control-sidebar" onclick="openSlide()" style="margin-right:15px"  class="btn btn-primary helpsection"><i class="fa fa-question-circle" aria-hidden="true">  Kindly Any Help</i></button>
                </div>
                <div class="hidden-xs pull-right ">
                    <p><b>Version</b> {!! Config::get('app.version') !!}</p>
                </div>
                <?php
                $company = App\Model\helpdesk\Settings\Company::where('id', '=', '1')->first();
                ?>
                <strong>{!! Lang::get('lang.copyright') !!} &copy; {!! date('Y') !!}  <a href="{!! $company->website !!}" target="_blank">{!! $company->company_name !!}</a>.</strong> {!! Lang::get('lang.all_rights_reserved') !!}.


                <?php
                 $Whitelabel = \Event::fire('Whitelabel.faveo', array());
                 ?>
                 @if (count($Whitelabel) !=0) 

                 {!! Lang::get('lang.all_rights_reserved') !!}. {!! Lang::get('lang.powered_by') !!} <a href="#" target="_blank">Helpdesk</a>
                 
                 @else
                 {!! Lang::get('lang.all_rights_reserved') !!}. {!! Lang::get('lang.powered_by') !!} <a href="http://www.faveohelpdesk.com/" target="_blank">Faveo</a>
        
                 @endif



                 <!-- {!! Lang::get('lang.powered_by') !!} <a href="http://www.faveohelpdesk.com/" target="_blank">Faveo</a> -->
            </footer>
            <aside class="right-side-menu control-sidebar-dark" id="right"  style="padding-top:0;bottom: 300px;background: transparent">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs"  style="padding-left:50%;background: transparent">
      <li class="remov"><a href="javascript:void(0)" onclick="openSlide()" class="help-widget-close" style="background-color: black;"><i class="fa fa-times" aria-hidden="true"></i></a></li>
      <li ><a href="#settings-tab" data-toggle="tab" id="trapezoid" style="background-color:transparent;color:#444;" ><i class="fa fa-paper-plane-o" aria-hidden="true" ></i> Mail Us
</a><span class="tab-slant"></span></li>
      <li class="active" ><a href="#home-tab" data-toggle="tab" id="trapezoid" style="background-color: transparent;color:#444;"><i class="fa fa-question-circle" aria-hidden="true"></i> Help</a><span class="tab-slant"></span></li>
  </ul>
    </aside>
    <aside class="right-side-menu1 control-sidebar-dark" id="right1"  style="padding-top:0;height: 300px;background-color: #f0f0f0;border: 1px solid gainsboro;width">
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active row" id="home-tab">
    <div class="col-sm-3" style="margin-top:67px">
      <div style="text-align:center"><h3 class="space-on-bottom-lg" style="color:black"> Prefer email instead? </h3>
       <div><button class="btn btn-default space-on-bottom-20px space-on-top-10px" onclick="mailTab()">
         <span class="icon icon-envelope icon-gray tail5px"></span> Write to us</button>
         <p style="margin-top:7px;color:black">We are super quick in responding to your queries.</p>
        </div>
     </div>
    </div>  
    <div class="col-sm-9" style="border-left:1px solid gainsboro;height:297px"> 
    <ul class="control-sidebar-menu">
        <div class="col-sm-12" style="padding-right: 50px;margin-top: 20px;margin-bottom: 20px">
           <div class="col-sm-1"></div>
           <div class="col-sm-11">
            <div class="search-box"><input type="text" data-autofocus="true" placeholder="What can we help you with? Start typing your question..."></div> 
          </div>        
        </div>
        <div class="col-sm-12" style="padding-left: 10%;padding-right: 3px;margin-top: 20px;margin-bottom: 20px">
         <div class="col-sm-3">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Staff</h4>
  
              </div>
            </a>
          </li>
          </div>
          <div class="col-sm-2">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Email</h4>
              </div>
            </a>
          </li>
         </div>
         <div class="col-sm-2">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-database bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Manage</h4>
              </div>
            </a>
          </li>
          </div>
          <div class="col-sm-2">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-ticket bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Ticket</h4>  
              </div>
            </a>
          </li>
          </div>
          <div class="col-sm-3">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-cog bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Setting</h4>  
              </div>
            </a>
          </li>
          </div>
          </div>
          <div class="col-sm-12" style="padding-left: 10%;padding-right: 3px;margin-top: 20px;margin-bottom: 20px">
          <div class="col-sm-3">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-bug bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Error logs and debugging</h4>  
              </div>
            </a>
          </li>
          </div>
          <div class="col-sm-2">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-pie-chart bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Widgets</h4>  
              </div>
            </a>
          </li>
          </div>
          <div class="col-sm-2">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-plug bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Plugin</h4>  
              </div>
            </a>
          </li>
          </div>
          <div class="col-sm-2">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-cogs bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">API</h4>  
              </div>
            </a>
          </li>
          </div>
          <div class="col-sm-3">
          <li class="category-tile">
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-lock bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading" style="line-height: 2.5">Logs</h4>  
              </div>
            </a>
          </li>
          </div>
        </div>
        </ul>
        </div>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane row" id="settings-tab">
      <div class="col-sm-3" style="margin-top:128px">
          <button class="btn btn-default" style="margin-left:60px" onclick="helpTab()"><i class="glyphicon glyphicon-search"></i>&nbsp;Browse Help Articles</button>
    </div>  
      <div class="col-sm-9" style="border-left:1px solid gainsboro;height:296px">
        <form method="post" style="margin-top: 25px">
          <div class="col-sm-12" style="padding-left: 2px;padding-right: 5px">
             <div class="col-sm-2">
             </div>
             <div class="col-sm-2" style="padding-left: 0px;padding-right: 0px">
                <label style="line-height: 2.0;color: black">Email</label>
             </div>
             <div class="col-sm-5" style="padding-left: 0px;padding-right: 0px">
                <input type="eamil" name="" value="support@ithelpdesk.com" class="form-control" placeholder="Enter Your Email" style="border-radius: 0px;" disabled="disabled">
             </div>
          </div>
        
        <div class="col-sm-12" style="margin-top: 20px;padding-left: 2px;padding-right: 5px">
            <div class="col-sm-2">
             </div>
            <div class="col-sm-2" style="padding-left: 0px;padding-right: 0px">
                <label style="line-height: 2.0;color: black">Subject</label>
             </div>
             <div class="col-sm-5" style="padding-left: 0px;padding-right: 0px">
                <input type="email" name="" value="" class="form-control" placeholder="Subject" style="border-radius: 0px;">
             </div>
        </div>
        <div class="col-sm-12" style="margin-top: 20px;padding-left: 2px;padding-right: 5px">
            <div class="col-sm-2">
             </div>
            <div class="col-sm-2" style="padding-left: 0px;padding-right: 0px">
                <label style="line-height: 2.0;color: black">Message</label>
             </div>
             <div class="col-sm-5" style="padding-left: 0px;padding-right: 0px">
                <textarea name="" class="form-control" style="border-radius: 0px;" placeholder="Message Here"></textarea>
             </div>
        </div>
        <div class="col-sm-12" style="text-align: center;margin-top: 20px;">
           <div class="col-sm-4">
           </div>
           <div class="col-sm-3">
              <div style="float:left"><div class="fileupload-wrapper" style="position: relative; overflow: hidden; direction: ltr;"><a href="#" tabindex="-1" class="file-attach-link" style="color: #00c0ef;"><i class="glyphicon glyphicon-paperclip small"></i>Attach files</a><input qq-button-id="9f785c98-e24f-476a-ad1d-790faf404528" multiple="" type="file" name="doc" title="File input" style="position: absolute; right: 0px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0; height: 100%;"></div></div>
           </div>
           <div class="col-sm-3">
            <button class="btn btn-primary">Send</button>
          </div>
        </div>     
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
        </div><!-- ./wrapper -->
        <!-- jQuery 2.1.3 -->
        <script src="{{asset("lb-faveo/js/ajax-jquery.min.js")}}" type="text/javascript"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="{{asset("lb-faveo/js/bootstrap.min.js")}}" type="text/javascript"></script>
        <!-- Slimscroll -->
        <script src="{{asset("lb-faveo/plugins/slimScroll/jquery.slimscroll.min.js")}}" type="text/javascript"></script>
        <!-- FastClick -->
        <script src="{{asset("lb-faveo/plugins/fastclick/fastclick.min.js")}}" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{asset("lb-faveo/js/app.min.js")}}" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="{{asset("lb-faveo/plugins/iCheck/icheck.min.js")}}" type="text/javascript"></script>
        
        <script src="{{asset("lb-faveo/plugins/datatables/dataTables.bootstrap.js")}}" type="text/javascript"></script>
        
        <script src="{{asset("lb-faveo/plugins/datatables/jquery.dataTables.js")}}" type="text/javascript"></script>
        <!-- Page Script -->
        <script src="{{asset("lb-faveo/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js")}}" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="{{asset("lb-faveo/js/jquery.dataTables1.10.10.min.js")}}"  type="text/javascript"></script>
        
        <script src="{{asset("lb-faveo/plugins/datatables/dataTables.bootstrap.js")}}"  type="text/javascript"></script>
        <!-- Colorpicker -->
        <script src="{{asset("lb-faveo/plugins/colorpicker/bootstrap-colorpicker.min.js")}}" ></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <!--date time picker-->
        <script src="{{asset("lb-faveo/js/bootstrap-datetimepicker4.7.14.min.js")}}" type="text/javascript"></script>
        <!-- select2 -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
      <!-- <script src="https://cdn.ckeditor.com/4.7.0/standard/ckeditor.js"></script> -->
      <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
      <script src="{{asset("lb-faveo/js/intlTelInput.js")}}"></script>

 


      <script type="text/javascript">
      

          CKEDITOR.replace('ckeditor', {
              toolbarGroups: [
                {"name": "basicstyles", "groups": ["basicstyles"]},
                {"name": "links", "groups": ["links"]},
                {"name": "paragraph", "groups": ["list", "blocks"]},
                {"name": "document", "groups": ["mode"]},
                {"name": "insert", "groups": ["insert"]},
                {"name": "styles", "groups": ["styles"]}
            ],
            removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar',
            disableNativeSpellChecker: false
            }).on('change',function(){
                $('#submit').removeAttr('disabled');
                $('#form_submit').removeAttr('disabled');
            });
      </script>
   <!-- jquery ui -->
        <!-- <script src="{{asset("lb-faveo/js/jquery.ui.js")}}" type="text/javascript"></script> -->
@if (trim($__env->yieldContent('no-toolbar')))
    <h1>@yield('no-toolbar')</h1>
@else
    <script>
    $(function () {


    });
    </script>
@endif
 <script>
                    $(document).ready(function () {

             $('.sidebar-toggle').click(function () {
                   $('.main-sidebar').toggle();;
             });
               });
               </script>
    <script>
        $('#read-all').click(function () {

            var id2 = <?php echo \Auth::user()->id ?>;
            var dataString = 'id=' + id2;
            $.ajax
                    ({
                        type: "POST",
                        url: "{{url('mark-all-read')}}" + "/" + id2,
                        data: dataString,
                        cache: false,
                        beforeSend: function () {
                            $('#myDropdown').on('hide.bs.dropdown', function () {
                                return false;
                            });
                            $("#refreshNote").hide();
                            $("#notification-loader").show();
                        },
                        success: function (response) {
                            $("#refreshNote").load("<?php echo $_SERVER['REQUEST_URI']; ?>  #refreshNote");
                            $("#notification-loader").hide();
                            $('#myDropdown').removeClass('open');
                        }
                    });
        });</script>
    
    <script src="{{asset("lb-faveo/js/tabby.js")}}"></script>
    <script src="{{asset("lb-faveo/plugins/filebrowser/plugin.js")}}"></script>
    <script src="{{asset("lb-faveo/js/angular/angular.min.js")}}" type="text/javascript"></script>
    <script src="{{asset("lb-faveo/js/angular/ng-scrollable.min.js")}}" type="text/javascript"></script>
    <script src="{{asset("lb-faveo/js/angular/angular-moment.min.js")}}" type="text/javascript"></script>
<script src="{{asset('lb-faveo/js/form/ui-bootstrap-tpls.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/main.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/handleCtrl.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/nodeCtrl.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/nodesCtrl.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/treeCtrl.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/uiTree.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/uiTreeHandle.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/uiTreeNode.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/uiTreeNodes.js')}}"></script>
<script src="{{asset('lb-faveo/js/form/helper.js')}}"></script>
<script type="text/javascript">
    function helpTab(){
     $('.nav-tabs li a[href="#home-tab"]').tab('show');
};
function mailTab(){
    $('.nav-tabs li a[href="#settings-tab"]').tab('show');
}
function openSlide(){
if ( document.getElementById("right").className.match(/(?:^|\s)right-side-menu control-sidebar-dark(?!\S)/) ){
         document.getElementById("right").className += "right-side-menu control-sidebar-dark right-side-menu-open";
}
else{
     document.getElementById("right").className = "right-side-menu control-sidebar-dark";
}
if ( document.getElementById("right1").className.match(/(?:^|\s)right-side-menu1 control-sidebar-dark(?!\S)/) ){
         document.getElementById("right1").className += "right-side-menu1 control-sidebar-dark right-side-menu-open1";
}
else{
     document.getElementById("right1").className = "right-side-menu1 control-sidebar-dark";
}
}
</script>
   <script>
 var app = angular.module('fbApp', ['angularMoment','flow','ui.tree','ui.bootstrap']).directive('whenScrolled', function() {
    return function(scope, elm, attr) {
        var raw = elm[0];
        console.log(raw);
        elm.bind('scroll', function() {

            if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                scope.$apply(attr.whenScrolled);
            }
        });
    };
});
app.constant("CSRF_TOKEN", '{{ csrf_token() }}');
app.directive('mediaLibScrolled', function() {
    return function(scope, elm, attr) {
        var raw = elm[0];
        console.log(raw);
        elm.bind('scroll', function() {

            if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                scope.$apply(attr.mediaLibScrolled);
            }
        });
    };
});
var fileProcess=null;
app.config(['flowFactoryProvider', function (flowFactoryProvider,$scope,$http) {
    fileProcess=flowFactoryProvider;
    
    flowFactoryProvider.on('fileError', function (file,message) {
        $('#errorFileMsg').css('display','block');
    });
   
    flowFactoryProvider.factory = fustyFlowFactory;
  }]);
app.run(function($http,$rootScope) {
            $rootScope.arrayImage=[];
       fileProcess.on('fileSuccess', function (file,message) {
        $('#mytabs a[href="#menu1"]').tab('show');
        $("#progressHide").hide();
        $("#progressHide").find('.transfer-box').remove();
        $http.get("{{url('media/files')}}").success(function(data){
           
                $rootScope.arrayImage=data;
                $rootScope.disable=false;
                $rootScope.preview=true;
           $rootScope.viewImage=$rootScope.arrayImage.data[0]
           if($rootScope.arrayImage.data[0].type=="image"){
               
               $rootScope.inlineImage=false;
               $rootScope.viewImage=$rootScope.arrayImage.data[0].base_64;
               $rootScope.pathName=$rootScope.arrayImage.data[0].pathname;
               $rootScope.fileName=$rootScope.arrayImage.data[0].filename;
           }
           else if($rootScope.arrayImage.data[0].type=="text"){
               $rootScope.inlineImage=true;
               $rootScope.viewImage="{{asset('lb-faveo/media/images/txt.png')}}";
               $rootScope.pathName=$rootScope.arrayImage.data[0].pathname;
               $rootScope.fileName=$rootScope.arrayImage.data[0].filename;
           }
           else{
               $rootScope.inlineImage=true;
               $rootScope.viewImage="{{asset('lb-faveo/media/images/common.png')}}";
               $rootScope.pathName=$rootScope.arrayImage.data[0].pathname;
               $rootScope.fileName=$rootScope.arrayImage.data[0].filename;
           }
            
            setTimeout(function(){
                 $('label[for="happy0"]>img').css({'border': '1px solid #fff','box-shadow': '0 0 0 4px #0073aa'});
                 $('label[for="happy1"]>img').css({'border': 'none','box-shadow': 'none'});
           },100)
          
        })
        
    });
    
});
</script>

    @yield('FooterInclude')
    @stack('scripts')
    
</body>
<script>
  $(window).load(function(){
     setTimeout(function(){
      $('.loader').css('display','none');
    },800);
  })
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });
    $(function() {
      
        // $('input[type="checkbox"]').iCheck({
        //     checkboxClass: 'icheckbox_flat-blue'
        // });
        // $('input[type="radio"]').iCheck({
        //     radioClass: 'iradio_flat-blue'
        // });
    
    });        
</script>
<script type="text/javascript">
    function changeLang(lang) {
        var link = "{{url('swtich-language')}}/"+lang;
        window.location = link;
    }
</script>
<script>
    $(document).ready(function(){
        
        if($('.alert-success').html()){
              
                setInterval(function(){
                    $('.alert-success').slideUp( 3000, function() {});
                }, 2000);
            }
        
    })
</script>
       <script type="text/javascript">
 $(function () {
       if('{{Lang::getLocale()}}'=='ar'){

        // $('#cssRTL').removeAttr('disabled');
        //   $('#bootRTL').removeAttr('disabled');
    $('#adminLTR').attr('disabled','disabled');
    var adminRtl = document.createElement('link');
    adminRtl.id = 'id-rtl';
    adminRtl.rel = 'stylesheet';
    adminRtl.href = '{{asset("lb-faveo/rtl/css/AdminLTE.css")}}';
    document.head.appendChild(adminRtl);

     var cssRtl = document.createElement('link');
    cssRtl.id = 'id-csstrtl';
    cssRtl.rel = 'stylesheet';
    cssRtl.href = '{{asset("lb-faveo/rtl/css/rtl.css")}}';
    document.head.appendChild(cssRtl);

     var bootRtl = document.createElement('link');
    bootRtl.id = 'id-bootrtl';
    bootRtl.rel = 'stylesheet';
    bootRtl.href = '{{asset("lb-faveo/rtl/css/bootstrap-rtl.min.css")}}';
    document.head.appendChild(bootRtl);


        
        $('#adminLTR').remove();
        $('.container').attr('dir','RTL');
        $('.formbilder').attr('dir','RTL');
        $('.content-area').attr('dir','RTL');
        // agentpanel
        $('.content').attr('dir','RTL');
        $('.info').attr('dir','RTL');
        $('.table').attr('dir','RTL');
        $('.box-primary').attr('dir','RTL');
        // box-header with-borderclass="box box-primary"
        $('.dataTables_paginate').find('.row').attr('dir','RTL');
        // dataTables_paginate paging_full_numbers
        $('.sidebar-menu').attr('dir','RTL');
        $('.sidebar-menu').find('.pull-right').removeClass("pull-right");
        $('.sidebar-menu').find('.label').addClass("pull-left");
        $('.content').find('.btn').removeClass("pull-right");
        $('.content').find('.btn').addClass("pull-left");
        $('.tabs-horizontal').removeClass("navbar-left");
        $('.tabs-horizontal').addClass("navbar-right");
        $('#right-menu').removeClass("navbar-right");
        $('#right-menu').addClass("navbar-left");
        $('.navbar-nav').find('li').css("float","right");
        $('#rtl1').css('display','none');
        $('#ltr1').css('display','block');
        $('#rtl2').css('display','none');
        $('#ltr2').css('display','block');
        $('#rtl3').css('display','none');
        $('#ltr3').css('display','block');  
        $('#rtl4').css('display','none');
        $('#ltr4').css('display','block');  
        $('.box-header').find('.pull-right').addClass("pull-left");
       $('.box-header').find('.pull-right').removeClass("pull-right");
         $('.btn').removeClass("pull-left");
        $('.iframe').attr('dir','RTL');
         $('.box-footer').find('a').removeClass("pull-right");
         $('.box-footer').find('a').addClass("pull-left");
         $('.box-footer').find('div').removeClass("pull-right");
         $('.box-footer').find('div').addClass("pull-left");
         // $('.col-md-3').css('float','right');
         $('.user-footer').css('float','none');
         $('.user-header').css('float','none');
         $('.sidebar-toggle').css('width','60px');
         $('.dropdown-menu').css('right','inherit');
         $('.dropdown-menu').css('left','0');
// chart-data
        // label

        $('.box-header').find('.btn-primary').find('.pull-right').removeClass("pull-right");
        $('.box-header').find('.btn-primary').addClass("pull-left");

        $('.main-footer').find('.pull-right').removeClass("pull-right");
        $('.main-footer').find('.hidden-xs').addClass("pull-left");
        
        
        
        setTimeout(function(){  
        $('#cke_details').addClass( "cke_rtl" );
        $(".cke_wysiwyg_frame").contents().find("body").attr('dir','RTL');

        }, 3000);
        $('iframe').contents().find("body").attr('dir','RTL');
        /*$('#wys-form').remove();
        $('#mobile-RTL').css('display','block');
        $('#mobile-normal').css('display','none');
        $('#form-foot1').css('display','block');
        $('.list-inline').attr('dir','RTL');*/
       };
    });
</script>

<!-- for submit button loader-->
<script>
    $(function(){
    $('#submit').attr('disabled','disabled');
    $('#Form').on('input',function(){
        $('#submit').removeAttr('disabled');
    });
    $('#Form').on('change',':input',function(){
        $('#submit').removeAttr('disabled');
    });
    });
    $('#Form').submit(function () {
        var $this = $('#submit');
        $this.button('loading');
       $('#Edit').modal('show');
       
    });
   
</script>
</html>