@extends('themes.default1.admin.layout.admin')

<!-- @section('Users')
class="active"
@stop

@section('user-bar')
active
@stop

@section('team')
class="active"
@stop -->

@section('HeadInclude')
@stop
<!-- header -->
@section('PageHeader')

<h1>{!! Lang::get('lang.department_profile') !!} </h1>

@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')

@stop
<!-- /breadcrumbs -->
<!-- content -->
@section('content')
<!-- success message -->
<div id="alert-success" class="alert alert-success alert-dismissable" style="display:none;">
    <i class="fa fa-check-circle"> </i> <b>  <span id="get-success"></span></b>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
</div>
<!-- INfo message -->
<div id="alert-danger" class="alert alert-danger alert-dismissable" style="display:none;">
    <i class="fa fa-check-circle"> </i> <b>  <span id="get-danger"></span></b>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
</div>
@if(Session::has('success1'))
<div id="success-alert" class="alert alert-success alert-dismissable">
    <i class="fa  fa-check-circle"> </i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{Session::get('success1')}}
</div>
@endif
<!-- failure message -->
@if(Session::has('fails1'))
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"> </i> <b> {!! Lang::get('lang.alert') !!} ! </b>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{Session::get('fails1')}}
</div>
@endif
<div class="row">
    <div class="col-md-3">
        <div class="box box-primary" >
            <div class="box-header">

            </div>
            <div class="box-body ">
                <div>
                    <center>
                        <img src="{{asset("lb-faveo/media/images/department.jpg")}} " class="img-circle" alt="User Image" style="border:3px solid #CBCBDA;padding:3px;">  

                        <h3 class="">{{$departments->name}}</h3>

                    </center>
                </div>
            </div>
            <div class="box-footer">
                <b>{{Lang::get('lang.department_maneger_name')}}</b>
                <!-- <h3 class=""></h3> -->
                <span class="pull-right-container">
                    <span class="pull-right">
                        @if($dept_manager_name!=null)
                        @if($dept_manager_name->first_name && $dept_manager_name->last_name)
                        {!! $dept_manager_name->first_name.' '.$dept_manager_name->last_name !!}
                        @else
                        {!! $dept_manager_name->user_name !!}

                        @endif 
                        @else

                        Not available
                        @endif
                    </span>
                </span>
            </div>




            <div class="box-footer">
                <b>{{Lang::get('lang.department_size')}}</b>
                <span class="pull-right-container">
                    <span class="label label-primary pull-right">{{$department_members->count()}}</span>
                </span>

            </div>

    
        </div>
    </div>

    <div class="col-md-8">
        <div class="box box-primary">

            <div class="box-header with-border">

                <h3 class="box-title">{!! Lang::get('lang.list_of_department_members')!!}</h3>
                <div class="pull-right" style="margin-top:-25px;margin-bottom:-25px;">

                </div>
            </div>   
            <div class="box-body">
                <table id="users-table" class="table display" cellspacing="0" width="100%" styleClass="borderless">

                    <thead><tr>
                            <th>{!! Lang::get('lang.user_name') !!}</th>
                            <th>{!! Lang::get('lang.email') !!}</th>

                            <th>{!! Lang::get('lang.status') !!}</th>
                            <th>{!! Lang::get('lang.ban') !!}</th>

                        </tr></thead>


                </table>
            </div>    
        </div>


    </div>



    <!-- page script -->   

    @stop
    @push('scripts')
    <script>
        function confirmDelete(id) {
            var check = confirm('Are you sure?');
            if (check == true) {

                window.location = '{!! url("delete") !!}/' + id;

            } else {
                return false;
            }
        }
    </script>



    <script>
        $('#users-table').DataTable({
            processing: true,

            serverSide: true,
            ajax: '{!! route('department.userprofile.show',$departments->id) !!}',
            columns: [
                {data: 'user_name', name: 'user_name'},
                {data: 'email', name: 'email'},
                {data: 'active', name: 'active'},
                {data: 'ban', name: 'ban'}

            ]
        });


    </script>

    @endpush


