@extends('themes.default1.admin.layout.admin')

@section('Tickets')
active
@stop

@section('manage-bar')
active
@stop

@section('tags')
class="active"
@stop

@section('HeadInclude')
@stop
<!-- header -->
@section('PageHeader')
<h1>{{trans('lang.tags')}}</h1>
@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">
</ol>
@stop
<!-- /breadcrumbs -->
<!-- content -->
@section('content')
 {!! Form::open(['url'=>'tag','method'=>'post', 'id' => 'label-form']) !!}
@if(Session::has('success'))
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-check-circle"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{Session::get('success')}}
</div>
@endif
@if(Session::has('fails'))
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{Session::get('fails')}}
</div>
@endif
@if(Session::has('warn'))
<div class="alert alert-warning alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{Session::get('warn')}}
</div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Alert !</strong><br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="box">
    <div class="box-header">
        <div class="box-title">
            {{trans('lang.create_new_tag')}}
        </div>
       
    </div>
    <div class="box-body">
        <table class="table table-borderless">

            <tr>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <td>{!! Form::label('name',trans('lang.name')) !!}<span class="text-red"> *</span></td>
                <td>
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        {!! Form::text('name',null,['class'=>'form-control']) !!}
                    </div>
                </td>
            </div>
            </tr>
            <tr>
                <td>{!! Form::label('description',trans('lang.description')) !!}</td>
                <td>
                    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                        {!! Form::textarea('description', null,['class'=>'form-control','id'=>'ckeditor']) !!}
                    </div>
                </td>
            </tr>
            
             <tr>
                <td class="hide">{!! Form::label('order','Order') !!}<span class="text-red"> *</span></td>
                <td class="hide">
                    <div class="form-group {{ $errors->has('order') ? 'has-error' : '' }}">
            {!! Form::input('number', 'order', null, array('class' => 'form-control')) !!}
    </div>
</td>
</tr>

<tr>
    <td class="hide">{!! Form::label('status','Status') !!}</td>
    <td class="hide"><input type="checkbox" value="1" name="status" id="status" checked="true">&nbsp;&nbsp;{{Lang::get('lang.enable')}}</td>
</tr>
</table>
</div>
<div class="box-footer">
<!--    {!! Form::submit(trans('lang.create'),['class'=>'btn btn-primary']) !!}-->
<!--{!!Form::button('<i class="fa fa-floppy-o" aria-hidden="true">&nbsp;&nbsp;</i>'.Lang::get('lang.save'),['type' => 'submit', 'class' =>'btn btn-primary'])!!}-->
<button type="submit" id="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'>&nbsp;</i> Saving..."><i class="fa fa-floppy-o">&nbsp;&nbsp;</i>{!!Lang::get('lang.save')!!}</button>
  

    {!! Form::close() !!}
</div>
</div>

<!--demo loader-->
<script src="dist/spin.min.js"></script>
      <script src="dist/ladda.min.js"></script>
      <link rel="stylesheet" href="dist/ladda.min.css">
<script>
Ladda.bind( 'button', {
callback: function( instance ) {
var progress = 0;
var interval = setInterval( function() {
progress = Math.min( progress + Math.random() * 0.1, 1 );
instance.setProgress( progress );
if( progress === 1 ) {
instance.stop();
clearInterval( interval );
}
}, 200 );
}
} );
</script>
<script>
var l = Ladda.create( document.querySelector( 'button' ) );
l.start();
l.stop();
l.toggle();
l.isLoading();
l.setProgress( 0-1 );
</script>

<!-- for submit button loader-->
<script>
    $(function(){
    $('#submit').attr('disabled','disabled');
    $('#label-form').on('input',function(){
        $('#submit').removeAttr('disabled');
    });
    $('#label-form').on('change',':input',function(){
        $('#submit').removeAttr('disabled');
    });
    });
    $('#label-form').submit(function () {
        var $this = $('#submit');
        $this.button('loading');
       $('#Edit').modal('show');
       
    });
   
</script>
@stop