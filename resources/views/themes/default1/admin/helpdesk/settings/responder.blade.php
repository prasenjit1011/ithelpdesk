@extends('themes.default1.admin.layout.admin')

@section('Tickets')
active
@stop

@section('tickets-bar')
active
@stop

@section('auto-response')
class="active"
@stop

@section('HeadInclude')
@stop
<!-- header -->
@section('PageHeader')
<h1>{{ Lang::get('lang.auto_responce') }}</h1>
@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">
</ol>
@stop
<!-- /breadcrumbs -->
<!-- content -->
@section('content')
<!-- open a form -->
{!! Form::model($responders,['url' => 'postresponder/'.$responders->id, 'method' => 'PATCH','id'=>'Form']) !!}
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">{{Lang::get('lang.auto_responce-settings')}}</h3> 
    </div>
    <!-- New Ticket: CHECKBOX	 Ticket Owner   -->
    <div class="box-body">
        <!-- check whether success or not -->
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <i class="fa  fa-check-circle"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!!Session::get('success')!!}
        </div>
        @endif
        <!-- failure message -->
        @if(Session::has('fails'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <b>{!! lang::get('lang.alert') !!}!</b>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!!Session::get('fails')!!}
        </div>
        @endif
        <div class="form-group">
            {!! Form::checkbox('new_ticket',1) !!} &nbsp;
            {!! Form::label('new_ticket',Lang::get('lang.new_ticket')) !!}
        </div>
        <!-- New Ticket by Agent: CHECKBOX	 Ticket Owner   -->
        <div class="form-group">
            {!! Form::checkbox('agent_new_ticket',1) !!}&nbsp;
            {!! Form::label('agent_new_ticket',Lang::get('lang.new_ticket_by_agent')) !!}
        </div>
    </div>
    <div class="box-footer">
<!--        {!! Form::submit(Lang::get('lang.save'),['class'=>'btn btn-primary','id'=>'formSave'])!!}-->
<!--        {!!Form::button('<i class="fa fa-floppy-o" aria-hidden="true">&nbsp;&nbsp;</i>'.Lang::get('lang.save'),['type' => 'submit', 'class' =>'btn btn-primary','id'=>'formSave'])!!}-->
            <button type="submit" class="btn btn-primary" id="submit" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'>&nbsp;</i> Saving..."><i class="fa fa-floppy-o">&nbsp;&nbsp;</i>{!!Lang::get('lang.save')!!}</button>
        
    </div>
</div>

<script>
    $(document).ready(function() {
        if($('.alert-success').html()){
          document.getElementById('formSave').setAttribute("disabled", "disabled");   
        }
    $("#formSave").click(function(){
       setTimeout(function(){
        document.getElementById('formSave').setAttribute("disabled", "disabled"); 
            },100);
        }); 
});
  
</script>    
@stop
