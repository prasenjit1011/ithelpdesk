@extends('themes.default1.admin.layout.admin')

@section('Settings')
active
@stop

@section('settings-bar')
active
@stop

@section('user-options')
class="active"
@stop

@section('HeadInclude')
@stop
<!-- header -->
@section('PageHeader')
<h1>{{Lang::get('lang.settings')}}</h1>
@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">
</ol>
@stop
<!-- /breadcrumbs -->
<!-- content -->
@section('content')
<!-- open a form -->
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">{{Lang::get('lang.user-options')}}</h3> 
    </div>
    <div class="box-body">
        <!-- check whether success or not -->
            @if(Session::has('success'))
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check-circle"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!!Session::get('success')!!}
            </div>
            @endif
            <!-- failure message -->
            @if(Session::has('fails'))
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>{!! Lang::get('lang.alert') !!}!</b><br/>
                <li class="error-message-padding">{!!Session::get('fails')!!}</li>
            </div>
            @endif
            @if(Session::has('errors'))
            <?php //dd($errors); ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <b>{!! Lang::get('lang.alert') !!}!</b>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <br/>
            </div>
            @endif
        <div class="row">
                {!! Form::open(['url' => 'user-options', 'method' => 'POST' , 'id'=>'formID']) !!}
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('send_otp',Lang::get('lang.allow_unverified_users_to_create_ticket')) !!}
                        <a href="#" data-toggle="tooltip" title="{!! Lang::get('lang.unauth_user_ticket_create_info') !!}"><i class="fa fa-question-circle" style="padding: 0px;"></i></a>
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="radio" name="allow_users_to_create_ticket" value="0" @if($allow_users_to_create_ticket->status == '0')checked="true" @endif>&nbsp;{{Lang::get('lang.yes')}}
                            </div>
                            <div class="col-xs-6">
                                <input type="radio" name="allow_users_to_create_ticket" value="1" @if($allow_users_to_create_ticket->status == '1')checked="true" @endif>&nbsp;{{Lang::get('lang.no')}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('user_set_ticket_status',Lang::get('lang.user_set_ticket_status')) !!}
                        <div class="row">
                         <div class="col-xs-5">
                                <input type="radio" name="user_set_ticket_status" value="1" @if($user_set_ticket_status->status == '1')checked="true" @endif>&nbsp;{{Lang::get('lang.yes')}}
                            </div>
                            <div class="col-xs-6">
                                <input type="radio" name="user_set_ticket_status" value="0" @if($user_set_ticket_status->status == '0')checked="true" @endif>&nbsp;{{Lang::get('lang.no')}}
                            </div>
                           
                        </div>
                    </div>
                </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('allow_user_registration',Lang::get('lang.allow_users_registration')) !!}
                    <!-- <a href="#" data-toggle="tooltip" title="{!! Lang::get('lang.otp_usage_info') !!}"><i class="fa fa-question-circle" style="padding: 0px;"></i></a> -->
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="radio" name="allow_user_registration" value="0" @if($user_registration->status == '0')checked="true" @endif>&nbsp;{{Lang::get('lang.yes')}}
                        </div>
                        <div class="col-xs-6">
                            <input type="radio" name="allow_user_registration" value="1" @if($user_registration->status == '1')checked="true" @endif>&nbsp;{{Lang::get('lang.no')}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('email_mandatory',Lang::get('lang.make-email-mandatroy')) !!}
                    <a href="#" data-toggle="tooltip" title="{!! Lang::get('lang.email_man_info') !!}"><i class="fa fa-question-circle" style="padding: 0px;"></i></a>
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="radio" name="email_mandatory" value="1" @if($email_mandatory->status == '1')checked="true" @endif>&nbsp;{{Lang::get('lang.yes')}}
                        </div>
                        <div class="col-xs-6">
                            <input type="radio" name="email_mandatory" value="0" @if($email_mandatory->status == '0')checked="true" @endif>&nbsp;{{Lang::get('lang.no')}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('allow_user_show_org_tkt',Lang::get('lang.allow_users_to_show_organization_tickets')) !!}
                    <!-- <a href="#" data-toggle="tooltip" title="{!! Lang::get('lang.otp_usage_info') !!}"><i class="fa fa-question-circle" style="padding: 0px;"></i></a> -->
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="radio" name="allow_user_show_org_tkt" value="0" @if($allow_user_show_org_tkt->status == '0')checked="true" @endif>&nbsp;{{Lang::get('lang.yes')}}
                        </div>
                        <div class="col-xs-6">
                            <input type="radio" name="allow_user_show_org_tkt" value="1" @if($allow_user_show_org_tkt->status == '1')checked="true" @endif>&nbsp;{{Lang::get('lang.no')}}
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('allow_user_reply_org_tkt',Lang::get('lang.allow_users_to_reply_organization_tickets')) !!}
                    <!-- <a href="#" data-toggle="tooltip" title="{!! Lang::get('lang.otp_usage_info') !!}"><i class="fa fa-question-circle" style="padding: 0px;"></i></a> -->
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="radio" name="allow_user_reply_org_tkt" value="0" @if($allow_user_reply_org_tkt->status == '0')checked="true" @endif>&nbsp;{{Lang::get('lang.yes')}}
                        </div>
                        <div class="col-xs-6">
                            <input type="radio" name="allow_user_reply_org_tkt" value="1" @if($allow_user_reply_org_tkt->status == '1')checked="true" @endif>&nbsp;{{Lang::get('lang.no')}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
    </div>
    <div class="box-header with-border">
        <h3 class="box-title">{{Lang::get('lang.user-account-activation-and-verifcation')}}</h3> 
    </div>
    <div class="box-body">
        <div class="form-group">
            {!! Form::checkbox('activation-option[]','email', in_array('email', $aoption), ["class" => "aoption"]) !!}  {!! Lang::get('lang.actvate-by-email') !!}
        </div>
        <div class="form-group">
            <?php \Event::fire('mobile_account_verification_show', [[$aoption]]); ?>
        </div>
    </div>
    <div class="box-footer">
        {!! Form::submit(Lang::get('lang.submit'),['onclick'=>'sendForm()','class'=>'btn btn-primary'])!!}
    </div>
</div>

@stop
@push('scripts')
<script type="text/javascript">
    /*$('.aoption').on('click', function(e){
        var $box = $(this);
        if(!$box.is(':checked')) {
            e.preventDefault();
        } else {
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            $(group).prop("checked", false);
            $box.prop("checked", true);
        }
    })*/
    if($('.aoption').is(':checked')&&$('.boption').is(':checked')){
            $('.boption').removeAttr('onclick');
            $('.aoption').removeAttr('onclick');
    }
    else if($('.aoption').is(':checked')){
                $('.aoption').attr('onclick','return false');
             }
    else if($('.boption').is(':checked')){
                $('.boption').attr('onclick','return false');
             }
    $('.aoption').on('click',function(event) {
          if($(this).is(':checked')) {
               $('.boption').removeAttr('onclick'); 
           }
          else{
             if($('.boption').is(':checked')){
                $('.boption').attr('onclick','return false');
             }
         }
    });
    $('.boption').on('click',function(event) {
           if($(this).is(':checked')) {
               $('.aoption').removeAttr('onclick'); 
           }
          else{
             if($('.aoption').is(':checked')){
                $('.aoption').attr('onclick','return false');
             }
         }
    });
</script>
@endpush
