<?php

namespace App\AutoAssign\Controllers;

use App\Http\Controllers\Controller;
use DB;

/**
 * Calculate the agent id using roundrobin algorithm
 * 
 * @author Ladybird Web Solution <admin@ithelpdesk.com>
 * @name RoundRobin
 * 
 */

class RoundRobin extends Controller {

    protected $only_login;
    protected $thresold;
    protected $assign_not_accept;
    protected $settings;
    
    public function __construct() {
        if (isAutoAssign()) {
            $settings = new \App\Model\helpdesk\Settings\CommonSettings();
            $this->settings = $settings;
            $this->only_login = $this->setOnlyLogin();
            $this->thresold = $this->setThresold();
            $this->assign_not_accept = $this->setAssignNotAccept();
        }
    }
    /**
     * 
     * Set the islogin flag
     * 
     * @param int $only_login
     * @return int
     */
    public function setOnlyLogin($only_login = "") {
        $this_only_login = 0;
        if($only_login){
            $this_only_login = $only_login;
        }
        $islogin = $this->settings->getOptionValue('auto_assign', 'only_login')->option_value;
        if($islogin){
            $this_only_login = $this->settings->getOptionValue('auto_assign', 'only_login')->option_value;
        }
        return $this_only_login;
        
    }
    /**
     * 
     * Set the threshold value of an agent
     * 
     * @param int $thresold
     * @return int
     */
    public function setThresold($thresold = "") {
        $this_thresold_value = 100;
        if ($thresold) {
            $this_thresold_value = $thresold;
        }
        $this_thresold =  $this->settings->getOptionValue('auto_assign', 'thresold')->option_value;
        if($this_thresold){
            $this_thresold_value = $this_thresold;
        }
        return $this_thresold_value;
    }
    /**
     * 
     * set the forcing flag for vaccasion agents
     * 
     * @param int $assign_not_accept
     * @return int
     */
    public function setAssignNotAccept($assign_not_accept = "") {
        $assign_not_accept_value = 0;
        if ($assign_not_accept) {
            $assign_not_accept_value = $assign_not_accept;
        }
        $is_assign = $this->settings->getOptionValue('auto_assign', 'assign_not_accept')->option_value;
        if($is_assign){
            $assign_not_accept_value = $is_assign;
        }
        return $assign_not_accept_value;
    }
    /**
     * 
     * Get login flag
     * 
     * @return boolean
     */
    public function isOnlyLogin() {
        $check = false;
        if ($this->only_login == 1) {
            $check = true;
        }
        return $check;
    }
    
    /**
     * 
     * get Threshold
     * 
     * @return int
     */
    public function thresold() {
        return $this->thresold;
    }
    /**
     * 
     * get forcing flag where agent in vacation
     * 
     * @return boolean
     */
    public function isAssignIfNotAccept() {
        $check = false;
        if ($this->assign_not_accept == 1) {
            $check = true;
        }
        return $check;
    }
    /**
     * 
     * get the assined agent with count
     * 
     * @param int $dept_id
     * @return array
     */
    public function tickets($dept_id="") {
        //dd($dept_id);
        $users = new \App\User();
        $agents = $users
                ->when(isOnlyLogin(), function($query) {
                    return $query->where('users.is_login', '=', 1);
                })
//                
                ->when(!isAssignIfNotAccept(), function($query) {
                    return $query->where('users.not_accept_ticket', '=', 0);
                })
                ->where('users.role', '!=', 'user')
                ->where('users.active', 1)
                ->leftJoin('tickets', function($join) {
                    $join->on('tickets.assigned_to', '=', 'users.id');
                })
                ->leftJoin('department_assign_agents', function($join){
                    $join->on('users.id', '=', 'department_assign_agents.agent_id');
                })
                ->when($dept_id, function($query) use($dept_id){
                    return $query->where('department_assign_agents.department_id', '=', $dept_id);
                })
                ->leftJoin('ticket_status', 'tickets.status', '=', 'ticket_status.id')
                ->leftJoin('ticket_status_type', function($join) {
                        $join->on('ticket_status.purpose_of_status', '=', 'ticket_status_type.id')
                        ->where('ticket_status_type.name', '=', 'open');
                });
//                dd($agents->select('users.id as agent', 'users.role as role', 'ticket_status_type.name as status','department_assign_agents.department_id')
//                ->addSelect(DB::raw('COUNT(ticket_status_type.name)  as count'))
//                ->groupBy('users.id', 'ticket_status_type.name')
//                ->orderBy('ticket_status_type.name', 'desc')
//                ->distinct(['users.id'])
//                ->get());
//                
        $agents = $agents->select('users.id as agent', 'users.role as role', 'ticket_status_type.name as status')
                ->addSelect(DB::raw('COUNT(ticket_status_type.name)  as count'))
                ->groupBy('users.id', 'ticket_status_type.name')
                ->orderBy('ticket_status_type.name', 'desc')
                ->distinct(['users.id'])
                ->get()
                ->whereIn('status', ['open', null])
                ->unique('agent')
                ->sortBy('count')
                ->filter(function ($user) {
                    return $user->count < $this->thresold;
                })
                ->first();
                //dd($agents);
        if ($agents) {
            $agents = $agents->toArray();
        }
        return $agents;
    }

}
