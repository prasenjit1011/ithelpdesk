<?php

namespace App\AutoAssign\Controllers;

use App\Http\Controllers\Controller;
use App\Model\helpdesk\Settings\CommonSettings;
use Illuminate\Http\Request;
use Exception;
use Lang;

/**
 * Assigning the ticket via Auto assign module
 * 
 * @abstract controller
 * @author Ladybird Web Solution <admin@ithelpdesk.com>
 * @name SettingsController
 * 
 */


class SettingsController extends Controller {

    public function __contructor() {

        $this->middleware(['roles', 'install']);
    }
    /**
     * 
     * get the settings blade view
     * 
     * @return view
     */
    public function getSettings() {
        return view('assign::setting');
    }
    /**
     * 
     * Posting the settings request
     * 
     * @param Request $request
     * @return string
     */
    public function postSettings(Request $request) {
        try {
            $all = $request->except('_token');
            if (count($all) > 0) {
                $this->delete();
                $this->save($all);
            }
            return redirect()->back()->with('success',Lang::get('lang.auto_assign_updated'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }
    /**
     * 
     * get the icon on admin panel
     * 
     * @return string
     */
    public function settingsView() {
        return ' <div class="col-md-2 col-sm-6">
                    <div class="settingiconblue">
                        <div class="settingdivblue">
                            <a href="' . url('auto-assign') . '">
                                <span class="fa-stack fa-2x">
                                    <i class="fa fa-check-square-o fa-stack-1x"></i>
                                </span>
                            </a>
                        </div>
                        <p class="box-title" >'.Lang::get('lang.auto_assign').'</p>
                    </div>
                </div>';
    }
    /**
     * deleting the settings
     */
    public function delete() {
        $setting = new CommonSettings();
        $settings = $setting->where('option_name', 'auto_assign')->get();
        if ($settings->count() > 0) {
            foreach ($settings as $assign) {
                $assign->delete();
            }
        }
    }
    /**
     * 
     * Saving the posted values
     * 
     * @param array $fields
     */
    public function save($fields) {
        $setting = new CommonSettings();
        foreach ($fields as $optional => $value) {
            $setting->create([
                'option_name' => 'auto_assign',
                'optional_field' => $optional,
                'option_value' => $value
            ]);
        }
    }

}
