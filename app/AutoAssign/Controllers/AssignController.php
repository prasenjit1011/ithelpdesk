<?php

namespace App\AutoAssign\Controllers;

use App\AutoAssign\Controllers\RoundRobin;

/**
 * Assigning the ticket via Auto assign module
 * 
 * @abstract RoundRobin
 * @author Ladybird Web Solution <admin@ithelpdesk.com>
 * @name AssignController
 * 
 */


class AssignController extends RoundRobin {
    
    /**
     * Old Method
     * 
     * @param type $ticket
     */
    
    public function oldcreateTicket($ticket) {
        $tickets = $this->tickets();
        //dd($tickets);
        if ($tickets) {
            $agent_id = checkArray('agent', $tickets);
        }
        if (isAutoAssign() && $this->checkAgentLoginFlag($agent_id)) {
            $this->saveAssignedTo($ticket, $agent_id);
        }
    }
    
    /**
     * 
     * Get the agent id
     * 
     * @param type $dept_id
     * @return int
     */

    public function createTicket($dept_id="") {
        $tickets = $this->tickets($dept_id);
        //dd($tickets);
        $agent_id = NULL;
        if ($tickets) {
            $agent_id = checkArray('agent', $tickets);
        }
        if (isAutoAssign() && $this->checkAgentLoginFlag($agent_id)) {
            //dd($agent_id);
            return $agent_id;
        }
        return NULL;
    }
    /**
     * 
     * Saving the ticket with assign_to
     * 
     * @param type $ticket
     * @param type $agent_id
     */
    public function saveAssignedTo($ticket, $agent_id) {
        if ($agent_id && $agent_id !== "") {
            $ticket->assigned_to = $agent_id;
            $ticket->save();
        }
    }
    /**
     * 
     * Check agent is logged in
     * 
     * @param type $agent_id
     * @return boolean
     */
    public function isAgentLoggedIn($agent_id) {
        $agent = \App\User::where('id', $agent_id)->where('is_login', 1)->first();
        $check = false;
        if ($agent) {
            $check = true;
        }
        return $check;
    }

    /**
     * 
     * Check the login condition from the settings of the module
     * 
     * @param type $agent_id
     * @return boolean
     */
    public function checkAgentLoginFlag($agent_id) {
        $check = false;
        if (!$this->isOnlyLogin()) {
            $check = true;
        } elseif ($this->isAgentLoggedIn($agent_id)) {
            $check = true;
        }
        return $check;
    }

}
