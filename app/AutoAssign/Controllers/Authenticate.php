<?php

namespace App\AutoAssign\Controllers;

use App\Http\Controllers\Controller;
use Auth;

/**
 * While agent loging procedure
 * 
 * @abstract Controller
 * @author Ladybird Web Solution <admin@ithelpdesk.com>
 * @name Authenticate
 * 
 */
class Authenticate extends Controller {

    /**
     * When agent loging
     */
    public function login() {
        $user = Auth::user();
        if ($user) {
            $user->is_login = 1;
            $user->save();
            if ($user->role != 'user' && $this->isAccept($user)) {
                $this->assign($user->id);
            }
        }
    }

    /**
     * When agent log out
     */
    public function logout() {
        $user = Auth::user();
        if ($user) {
            $user->is_login = 0;
            $user->save();
        }
    }

    /**
     * 
     * Assigning tickets
     * 
     * @param int $agent_id
     */
    public function assignTickets($agent_id) {
        $thresold = thresold();
        if ($thresold == "") {
            $thresold = 10;
        }
        $tickets = new \App\Model\helpdesk\Ticket\Tickets();
        $tickets->whereNull('assigned_to')
                ->chunk($thresold, function($ticket) use($agent_id) {
                    foreach ($ticket as $update) {
                        $update->assigned_to = $agent_id;
                        $update->save();
                    }
                });
    }

    /**
     * 
     * Assigning 
     * 
     * @param int $agent_id
     */
    public function assign($agent_id) {
        if (isOnlyLogin()) {
            $this->assignTickets($agent_id);
        }
    }

    /**
     * 
     * Check is accept mode
     * 
     * @param type $agent
     * @return boolean
     */
    public function isAccept($agent) {
        $check = false;
        if (isAssignIfNotAccept()) {
            $check = true;
        } elseif ($agent->not_accept_ticket == 0) {
            $check = true;
        }
        return $check;
    }

}
