@extends('themes.default1.admin.layout.admin')

@section('Tickets')
active
@stop

@section('tickets-bar')
active
@stop

@section('auto-assign')
class="active"
@stop

@section('HeadInclude')
@stop

@section('PageHeader')
<h1>{{ Lang::get('lang.auto_assign') }}</h1>
@stop

@section('content')

<div class="box box-primary">

        
        {!! Form::open(['url'=>'auto-assign','method'=>'post','id'=>'Form']) !!}

    <!-- /.box-header -->
    <div class="box-body">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if (Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('success')}}
        </div>
        @endif
        <!-- fail message -->
        @if(Session::has('fails'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <b>{{Lang::get('message.alert')}}!</b> {{Lang::get('message.failed')}}.
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('fails')}}
        </div>
        @endif
        <div class="row">
            <div class="form-group col-md-6 {{ $errors->has('status') ? 'has-error' : '' }}">
                <div>
                    {!! Form::label('status',Lang::get('lang.enable')) !!}
                </div>
                <div>
                    <div class="col-md-3">
                        <p> {!! Form::radio('status',1,isAutoAssign()) !!} {!! Lang::get('lang.yes') !!}</p>
                    </div>
                    <div class="col-md-3">
                        <p> {!! Form::radio('status',0,!isAutoAssign()) !!} {!!Lang::get('lang.no')!!}</p>
                    </div>
                </div>             
            </div>
            <div class="form-group col-md-6 {{ $errors->has('only_login') ? 'has-error' : '' }}" >
                <div>
                    {!! Form::label('only_login',Lang::get('lang.only-login-agents')) !!}
                </div>
                <div>
                    <div class="col-md-3">
                        <p> {!! Form::radio('only_login',1,isOnlyLogin()) !!} {!! Lang::get('lang.yes') !!}</p>
                    </div>
                    <div class="col-md-3">
                        <p> {!! Form::radio('only_login',0,!isOnlyLogin()) !!} {!! Lang::get('lang.no') !!}</p>
                    </div>
                    
                </div>             
            </div>
            
             <div class="form-group col-md-6 {{ $errors->has('assign_not_accept') ? 'has-error' : '' }}">
                <div>
                    {!! Form::label('assign_not_accept',Lang::get('lang.assign-ticket-even-agent-in-non-acceptable-mode')) !!}
                </div>
                <div>
                    <div class="col-md-3">
                        <p> {!! Form::radio('assign_not_accept',1,isAssignIfNotAccept()) !!} {!! Lang::get('lang.yes') !!}</p>
                    </div>
                    <div class="col-md-3">
                        <p> {!! Form::radio('assign_not_accept',0,!isAssignIfNotAccept()) !!} {!! Lang::get('lang.no') !!}</p>
                    </div>
                    
                </div>             
            </div>
            
            
            <div class="form-group col-md-6 {{ $errors->has('thresold') ? 'has-error' : '' }}">
                {!! Form::label('thresold',Lang::get('lang.maximum-number-of-ticket-can-assign-to-agent')) !!}
                {!! Form::text('thresold',thresold(),['class'=>'form-control','placeholder'=>'100']) !!}             
            </div>
                                                

        </div>
        <!-- /.box-body -->
    </div>
    <div class="box-footer">
     <!--  {!! Form::submit(Lang::get('lang.save'),['class'=>'btn btn-primary']) !!} -->
    <button type="submit" class="btn btn-primary" id="submit" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'>&nbsp;</i> Saving..."><i class="fa fa-floppy-o">&nbsp;&nbsp;</i>{!!Lang::get('lang.save')!!}</button>
     
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
</div>

@stop