<?php

\Event::listen('ticket.assign', function($dept_id) {
    $assign = new \App\AutoAssign\Controllers\AssignController();
    return $assign->createTicket($dept_id);
});
\Event::listen('user.logout', function() {
    $assign = new \App\AutoAssign\Controllers\Authenticate();
    $assign->logout();
});

\Event::listen('user.login', function() {
    $assign = new \App\AutoAssign\Controllers\Authenticate();
    $assign->login();
});

\Event::listen('settings.ticket.view', function() {
    $set = new App\AutoAssign\Controllers\SettingsController();
    echo $set->settingsView();
});

Route::group(['middleware' => ['web','auth','roles']], function() {
    Route::get('auto-assign', [
        'as' => 'auto.assign',
        'uses' => 'App\AutoAssign\Controllers\SettingsController@getSettings'
    ]);

    Route::post('auto-assign', [
        'as' => 'post.auto.assign',
        'uses' => 'App\AutoAssign\Controllers\SettingsController@postSettings'
    ]);
});

//Route::get('round', [
//    'as' => 'round',
//    'uses' => 'App\AutoAssign\Controllers\RoundRobin@tickets'
//]);
