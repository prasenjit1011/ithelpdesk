<?php

namespace App\Model\helpdesk\Manage\Sla;

use App\BaseModel;

class BusinessHours extends BaseModel
{
    protected $table = 'business_hours';
    
    protected $fillable = ['name', 'description', 'status', 'timezone', 'is_default'];
    
    public function schedule(){
        $related = 'App\Model\helpdesk\Manage\Sla\BusinessHoursSchedules';
        $foreignKey = 'business_hours_id';
        return $this->hasMany($related, $foreignKey);
    }
    
    public function holiday(){
        $related = 'App\Model\helpdesk\Manage\Sla\BusinessHoliday';
        $foreignKey = 'business_hours_id';
        return $this->hasMany($related, $foreignKey);
    }
}
