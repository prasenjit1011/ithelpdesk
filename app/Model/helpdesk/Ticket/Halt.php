<?php

namespace App\Model\helpdesk\Ticket;

use Illuminate\Database\Eloquent\Model;

class Halt extends Model
{
    protected $table = "halts";
    protected $fillable = ['ticket_id','halted_at','time_used','halted_time'];
    protected $dates = ['halted_at'];
}
