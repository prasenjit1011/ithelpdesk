<?php

namespace App\Model\helpdesk\Ticket;

use App\BaseModel;

class Ticket_Form_Data extends BaseModel {

    protected $table = 'ticket_form_data';
    protected $fillable = ['ticket_id', 'title', 'content','key'];

}
