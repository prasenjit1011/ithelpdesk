<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Model\MailJob\Condition;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\Inspire',
        'App\Console\Commands\SendReport',
        'App\Console\Commands\CloseWork',
        'App\Console\Commands\TicketFetch',
        'App\Console\Commands\FollowUp',
        'App\Console\Commands\SendMessage',
        'App\Console\Commands\RemindCalendar',
        'App\Console\Commands\SendEscalate',
        'App\Console\Commands\UpdateEncryption',
        \App\Console\Commands\DropTables::class,
        \App\Console\Commands\Install::class,
        \App\Console\Commands\InstallDB::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        if (env('DB_INSTALL') == 1) {
            try {
                if ($this->getCurrentQueue() != "sync") {
                    $schedule->command('queue:listen ' . $this->getCurrentQueue() . ' --sleep 60')->everyMinute();
                }
                $this->execute($schedule, 'fetching');
                $this->execute($schedule, 'notification');
                $this->execute($schedule, 'work');
                $this->execute($schedule, 'followup');
                $this->execute($schedule, 'message');
                $this->execute($schedule, 'remind');
                $schedule->command('send:escalation')->everyMinute();//everyThirtyMinutes();
                //loging('cron', 'executed successfully');
            } catch (\Exception $ex) {
                loging('cron', $ex->getMessage());
                $this->error($ex->getMessage());
            }
        }
    }

    public function execute($schedule, $task) {

        $condition = new Condition();
        $command = $condition->getConditionValue($task);
        switch ($task) {
            case "fetching":
                return $this->getCondition($schedule->command('ticket:fetch'), $command);
            case "notification":
                return $this->getCondition($schedule->command('report:send'), $command);
            case "work":
                return $this->getCondition($schedule->command('ticket:close'), $command);
            case "followup":
                return $this->getCondition($schedule->command('users:followup'), $command);
            case "message":
                return $this->getCondition($schedule->command('message:send'), $command);
            case "remind":
                return $this->getCondition($schedule->command('events:remind'), $command);
            case "escalation":
                return $this->getCondition($schedule->command('send:escalation'), $command);
        }
    }

    public function getCondition($schedule, $command) {
        $condition = $command['condition'];
        $at = $command['at'];
        switch ($condition) {
            case "everyMinute":
                return $schedule->everyMinute();
            case "everyFiveMinutes":
                return $schedule->everyFiveMinutes();
            case "everyTenMinutes":
                return $schedule->everyTenMinutes();
            case "everyThirtyMinutes":
                return $schedule->everyThirtyMinutes();
            case "hourly":
                return $schedule->hourly();
            case "daily":
                return $schedule->daily();
            case "dailyAt":
                return $this->getConditionWithOption($schedule, $condition, $at);
            case "weekly":
                return $schedule->weekly();
            case "monthly":
                return $schedule->monthly();
            case "yearly":
                return $schedule->yearly();
            default :
                return $schedule->everyMinute();
        }
    }

    public function getConditionWithOption($schedule, $command, $at) {
        switch ($command) {
            case "dailyAt":
                return $schedule->dailyAt($at);
        }
    }

    public function getCurrentQueue() {
        $queue = 'database';
        $services = new \App\Model\MailJob\QueueService();
        $current = $services->where('status', 1)->first();
        if ($current) {
            $queue = $current->short_name;
        }
        return $queue;
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands() {
        require base_path('routes/console.php');
    }

}
