<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SendEscalate extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:escalation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To send the escation message when ticket violates SLA';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        if (env('DB_INSTALL') == 1) {
            try {
                $apply_sla = new \App\Http\Controllers\SLA\ApplySla();
                $apply_sla->send();
                loging('sending-escalation-cron', 'Escalation has send', 'info');
                $this->info("Escalation has send");
            } catch (\Exception $ex) {
                loging('sending-escalation-cron', $ex->getMessage());
                $this->error($ex->getMessage());
            }
        }
    }

}
