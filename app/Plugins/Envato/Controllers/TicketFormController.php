<?php

namespace App\Plugins\Envato\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Form;
use Illuminate\Database\Schema\Blueprint;
use Schema;
use App\Plugins\Envato\Model\Envato;
use App\Plugins\Envato\Model\EnvatoPurchase;
use App\User;
use App\Model\helpdesk\Ticket\Ticket_Thread;
use DateTime;
use DateTimeZone;
use App\Model\helpdesk\Ticket\Tickets;

class TicketFormController extends Controller {

    public function ClientForm() {

        $this->CreateTables();
        $model = new Envato;
        $model = $model->where('id', '1')->first();
        if ($model) {
            if ($model->mandatory == '1') {
                echo "<div class='col-md-6 form-group'>
            " . Form::label('purchase_code', 'Purchase Code') ."<span class='text-red'> *</span>".
                Form::text('purchase_code', null, ['class' => 'form-control']) . "

        </div>";
            }
        } 
    }

    public function AgetTimeLineForm($ticket) {
        $user = new User;
        $userid = $ticket->user_id;
        $user = $user->where('id', $userid)->first();
        $email = $user->email;
        //dd($email);
        $purchased = new EnvatoPurchase;
        $purchased = $purchased->where('username', $email)->first();
        if ($purchased) {
            $code = $purchased->purchase_code;
        } else {
            $code = '';
        }
        $model = new Envato;
        $model = $model->where('id', '1')->first();
        if ($model) {
            if ($model->mandatory == '1') {
                echo "<div class='form-group'>
                            <div class='row'>
                                <div class='col-md-2'>
                                    <label>Purchase Code</label> <span class='text-red'> *</span>
                                </div>
                                <div class='col-md-10'>
                                  <input type=text name=purchase_code class=form-control style=width:55% value=" . $code . ">  
                                </div>
                            </div>
                        </div>";
            }
        } 
    }

    public function PostPurchase($formdata, $username, $source) {
        try {
            if ($source == 1 || $source == 3 || $source == '') {
                $purchased = new EnvatoPurchase;
                $envato = new EnvatoController;
                $model = new Envato;
                $model = $model->where('id', '1')->first();
                if ($model) {
                    $token = $model->token;

                    if ($model->mandatory == '1') {
                        $purchase = $formdata['purchase_code'];
                        //dd($purchase);
                        $v = \Validator::make($formdata, ['purchase_code' => 'required']
                        );
                        if ($v->fails()) {
                            //dd($v);
                            throw new \Exception('Purchase Code Required');
                        }
                        $pur = $purchased->where('username', $username)->where('purchase_code', $purchase)->first();
                        if (!$pur) {
                            $envato->set_personal_token($token);
                            $json = $envato->verify_purchase_code($purchase);
                            $json = json_decode($json, true);
                            //dd($json);
                            if ($json) {
                                //dd($json);
                                if (array_key_exists('supported_until', $json)) {
                                    //dd($json);
                                    $support_until = $json['supported_until'];
                                    $support_zone = new DateTime($support_until);
                                    $support = $support_zone->setTimezone(new DateTimeZone('UTC'));
                                    $now = new DateTime();
                                    if ($now < $support) {
                                        $sold = $json['sold_at'];
                                        $sold_zone = new DateTime($sold);
                                        $sold_at = $sold_zone->setTimezone(new DateTimeZone('UTC'));
                                        $license = $json['license'];
                                        $purchased->create(['purchase_code' => $purchase, 'username' => $username, 'sold_at' => $sold_at, 'supported_until' => $support_until, 'licence' => $license]);
                                    } else {
                                        throw new \Exception('Your Support has expired');
                                        //echo "Your Support has expired";
                                    }
                                } else {
                                    throw new \Exception($json['description']);
                                    //return redirect('getform')->with('fails', $json['description']);
                                }
                            } else {
                                throw new \Exception('Invalid Purchase Code.');
                                //return redirect('getform')->with('fails', 'Invalid Purchase Code.');
                            }
                        }
                    }
                } else {

                    throw new \Exception('Envato Credentails Required');
                }
            }
        } catch (\Exception $ex) {
            throw new \Exception("Sorry! We could not identify $purchase please check the purchase code");
        }
    }

    public function CreateTables() {
        if (!Schema::hasTable('envato_purchases')) {
            Schema::create('envato_purchases', function (Blueprint $table) {
                $table->increments('id');
                $table->string('username');
                $table->string('purchase_code');
                $table->dateTime('sold_at');
                $table->dateTime('supported_until');
                $table->string('licence');
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('envato')) {
            Schema::create('envato', function(Blueprint $table) {
                $table->increments('id');
                $table->string('token');
                $table->integer('mandatory');
                $table->timestamps();
            });
        }
    }

}
