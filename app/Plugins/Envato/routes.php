<?php

\Event::listen('App\Events\ClientTicketForm',function($event){
    $controller = new App\Plugins\Envato\Controllers\TicketFormController;
    $controller->ClientForm();
            
});

\Event::listen('App\Events\TimeLineFormEvent',function($event){
    $controller = new App\Plugins\Envato\Controllers\TicketFormController;
    $controller->AgetTimeLineForm($event->para1);
            
});

\Event::listen('App\Events\ClientTicketFormPost',function($event){
    $controller = new App\Plugins\Envato\Controllers\TicketFormController;
    $controller->PostPurchase($event->para1,$event->para2,$event->para3);
});
Route::group(['middleware' => 'web'], function(){
    Route::get('envato/settings', ['as' => 'envanto-settings', 'uses' => 'App\Plugins\Envato\Controllers\EnvatoSettingsController@SettingsForm']);
    Route::patch('postsettings',  'App\Plugins\Envato\Controllers\EnvatoSettingsController@PostSettings');
});

\Event::listen('App\Events\TicketDetailTable',function($event){
    $controller = new App\Plugins\Envato\Controllers\TicketTimeLineController;
    $controller->ShowPurcaseCode($event->para1);
});

\Event::listen('App\Events\TopNavEvent',function($event){
    $controller = new App\Plugins\Envato\Controllers\EnvatoSettingsController;
    $controller->TopNav();
});