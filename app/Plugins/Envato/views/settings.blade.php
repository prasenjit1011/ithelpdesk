@extends('themes.default1.admin.layout.admin')

@section('Plugins')
class="active"
@stop

@section('plugin-bar')
active
@stop

@section('Envato')
class="active"
@stop

@section('HeadInclude')
@stop
<!-- header -->
@section('PageHeader')

@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">

</ol>
@stop
<!-- /breadcrumbs -->
<!-- content -->
@section('content')

<!-- open a form -->
    
	{!! Form::model($settings,['url' => 'postsettings', 'method' => 'PATCH','files'=>true]) !!}
    
<!-- <div class="form-group {{ $errors->has('company_name') ? 'has-error' : '' }}"> -->
	<!-- table  -->

<div class="box box-primary">
	<div class="box-header">
        <h4 class="box-title">Envato</h4>{!! Form::submit(Lang::get('lang.save'),['class'=>'form-group btn btn-primary pull-right'])!!}
    </div>

    <!-- check whether success or not -->

		<!-- Name text form Required -->
 		<div class="box-body">
         @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <i class="fa  fa-check-circle"></i>
            <b>Success!</b>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('success')}}
        </div>
        @endif
        <!-- failure message -->
        @if(Session::has('fails'))
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <b>Alert!</b> Failed.
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{Session::get('fails')}}
            </div>
        @endif
            <div class="row">

                

                <div class="col-md-6 form-group {{ $errors->has('token') ? 'has-error' : '' }}">

                    {!! Form::label('token','Personal Token') !!}
                    {!! $errors->first('token', '<spam class="help-block">:message</spam>') !!}
                    {!! Form::text('token',null,['class' => 'form-control']) !!}

                </div>



                <div class="col-md-6 form-group {{ $errors->has('sender_id') ? 'has-error' : '' }}">

                                 <div class="col-md-6">

                                     {!! Form::label('mandatory','Only for valid Envato purchase') !!}

                                 </div>
                                 <div class="col-md-6">

                                     <div class="col-md-6">
                                        {!! Form::radio('mandatory',1,true) !!}<span>Yes</span>
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::radio('mandatory',0,null) !!}<span>No</span>
                                    </div>

                                </div>

                </div>

                

               
    </div>

@stop

@section('FooterInclude')

@stop

<!-- /content -->
