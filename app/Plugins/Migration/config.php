<?php

return [
    'name' => 'Migration',
    'description' => 'Faveo Migration allow you to migrate from osTicket',
    'author' => 'ladybird',
    'website' => 'www.ithelpdesk.com',
    'version' => '1.0.0',
    'settings' => 'migration/get-migration',
    
];

