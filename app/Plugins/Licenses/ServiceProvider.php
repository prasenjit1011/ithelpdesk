<?php

namespace App\Plugins\Licenses;

class ServiceProvider extends \App\Plugins\ServiceProvider {

    public function register() {
        parent::register('Licenses');
    }

    public function boot() {
        /** 
         *View
         */
        $view_path = app_path().DIRECTORY_SEPARATOR.'Plugins'.DIRECTORY_SEPARATOR.'Licenses'.DIRECTORY_SEPARATOR.'views';
        $this->loadViewsFrom($view_path, 'licenses');
        parent::boot('Licenses');
        
        // if (class_exists('Breadcrumbs')) {
        //     require __DIR__ . '/breadcrumbs.php';
        // }
        
        /**
         *language
         */
        $trans = app_path() . DIRECTORY_SEPARATOR . 'Plugins' . DIRECTORY_SEPARATOR . 'Licenses' . DIRECTORY_SEPARATOR . 'lang';
        $this->loadTranslationsFrom($trans, 'Licenses');
    }
    

}
