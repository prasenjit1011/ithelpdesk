<?php

namespace App\Plugins\Licenses\Requests;

use App\Http\Requests\Request;

/**
 * AgentUpdate.
 *
 * @author  Ladybird <info@ithelpdesk.com>
 */
class LicensesTypeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'licenses_type'           => 'required',
            'licenses_key'          => 'required',
             'activated_on'              => 'required',
            'start_date'                => 'required',
            'expires_on'               => 'required',
            
        ];
    }
}
