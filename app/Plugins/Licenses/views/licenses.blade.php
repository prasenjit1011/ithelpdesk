
  <div class="box box-primary">
    <div class="box-header with-border">
        <span class="lead border-right">{!! Lang::get('Licenses::lang.licenses') !!}</span>

 <div class="pull-right">

@if(Auth::user()->role!='user')
  <a href="{!!URL::route('organizations.create.licenses', $org_id)!!}" class="btn btn-primary">{{Lang::get('Licenses::lang.create_licenses')}}</a>
    @endif

  </div>

    <div class="box-body">

    


<table id="myTable" class="display" cellspacing="0" width="100%">
                       <thead>
                   
                    <tr>
                            <th>{{ Lang::get('Licenses::lang.licenses_type')}}</th>
                            <th>{{  Lang::get('Licenses::lang.licenses_key')}}</th>
                            <th>{{   Lang::get('Licenses::lang.activated_on')}}</th>
                            <th>{{ Lang::get('Licenses::lang.start_date')}}</th>
                            <th>{{ Lang::get('Licenses::lang.expires_on')}}</th>

                            @if(Auth::user()->role!='user')
                            <th>{{  Lang::get('Licenses::lang.action')}}</th> 
                            @endif
                      </tr>
                        </thead> 
                       
       <?php
       $licenses_lists=App\Plugins\Licenses\Model\Licenses::where('org_id','=',$org_id)->select('id','licenses_type','licenses_key','activated_on','start_date','expires_on')->get()
       ?>
        @foreach($licenses_lists as $licenses_list)
                      
                          <tbody>
                        <tr>
                           <td>{!! $licenses_list->licenses_type !!}</td>
                            <td>{!! $licenses_list->licenses_key !!}</td>
                            <td>{!! $licenses_list->activated_on !!}</td>
                            <td>{!! $licenses_list->start_date !!}</td>
                            <td>{!! $licenses_list->expires_on !!}</td>
                             @if(Auth::user()->role!='user')
                            <td>

                                      <a href="{!! route('org.licenses.edit',$licenses_list->id) !!}"><button class="btn btn-info btn-xs" title="{!! Lang::get('lang.edit') !!}"> <i class='fa fa-edit'> </i></button></a>
                        <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#{{$licenses_list->id}}delete"> <i class='fa fa-trash'> </i></button>



                        <div class="modal fade" id="{{$licenses_list->id}}delete">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">{!! Lang::get('lang.delete') !!}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>{!! Lang::get('lang.are_you_sure_you_want_to_delete') !!}</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                         <a href="{!! route('org.licenses.destroy',$licenses_list->id) !!}"><button class="btn btn-info">Submit</button></a>
                                     
                                    </div>
                                </div> 
                            </div>
                        </div>






                             </td>   
                              @endif 
                         </tr>
                    
                    </tbody>
                      @endforeach
                </table>

 </div>
    <div class="box-footer">
    </div>
</div>

<!-- <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/css/jquery.dataTables.css">
   <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/css/jquery.dataTables_themeroller.css">
   <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js"></script>
   <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/jquery.dataTables.min.js"></script> -->


<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
   <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    // $('#myTable1').DataTable();
     $('#myTable').DataTable();
</script>