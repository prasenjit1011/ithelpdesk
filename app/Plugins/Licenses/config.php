<?php

return  [
    'name' =>'Licenses',
    'description'=>'Faveo Licenses plugin provides an option to add licenses to an organization.',
	'author'=>'ladybird',
	'website'=>'www.ithelpdesk.com',
	'version'=>'1.0.0',
	'settings'=>'licenses/settings'
    
];

