<?php

namespace App\Plugins\Licenses\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Plugins\Reseller\Model\Reseller;
use Illuminate\Database\Schema\Blueprint;
use Schema;
use Lang;

use App\Model\helpdesk\Settings\Plugin;

class SettingsController extends Controller {

    public function index() {


        $licenses_status=Plugin::where('name','=','Licenses')->select('status')->first();
        return view('licenses::settings',compact('licenses_status'));
    }

    public function update(Request $request) {
        
        try {

            $change_status=$request->settings_licenses;
            $htrunk_status_update=Plugin::where('name','=','Licenses')->update(['status' => $change_status]);
            return Lang::get('Licenses::lang.your_status_updated');
            // return $this->successResponse('Updated Successfully');
        } catch (\Exception $ex) {

            return $this->failsResponse($ex->getMessage());
        }
    }

   

}
