<?php

return [
    'telephone-integration' => 'Telephone Integration',
    'integrate'=>'Integrate',
    'provider'=>'Provider',
    'settings'=>'Settings',
    'action'=>'Action',
    'save'=>'Save',
    'incoming-call-recieved-on'=>'Incoming call recieved on',
    'listen-to-call-recording'=>'Listen to call recording',
];