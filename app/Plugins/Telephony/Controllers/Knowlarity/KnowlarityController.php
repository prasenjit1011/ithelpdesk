<?php

namespace App\Plugins\Telephony\Controllers\Knowlarity;

use App\Http\Controllers\Controller;
use App\Plugins\Telephony\Model\Core\Telephone;
use Exception;
use App\Plugins\Telephony\Model\Core\TelephoneDetail;
use Illuminate\Http\Request;

class KnowlarityController extends Controller {

    public function setSettings() {
        $settings = new \App\Plugins\Telephony\Controllers\Core\SettingsController();
        return $settings;
    }

    public function passThrough($modelid,$model,$request) {
        loging('knowlarity', json_encode($request->all()), 'info');
        $requests = $request->all();
        $callid = $requests['callid'];
        $provider = "knowlarity";
        $requ = $this->setParameter($requests);
        $settings = $this->setSettings();
        $settings->saveCall($callid, $provider, $requ,$modelid,$model);
        return response("success", 200);
    }

    public function setParameter($requests) {
        $settings = $this->setSettings();
        
        return[
            'from' => $settings->checkKey('caller_id', $requests), //$requests['From'],
            'to' => $settings->checkKey('dispnumber', $requests), //$requests['To'],
            'record' => $settings->checkKey('resource_url', $requests), //$requests['RecordingUrl'],
            'toWhom' => $settings->checkKey('dispnumber', $requests), //$requests['DialWhomNumber'],
            'date' => $settings->checkKey('start_time', $requests), //$requests['Created'],
        ];
    }

    public function getValues($modelid,$model) {
        $values = $this->dummyValues();
        echo "<form action='".url('telephone/knowlarity/pass/'.$modelid.'/'.$model)."' name='redirect'>";
        echo $values;
        echo '</form>';
        echo "<script language='javascript'>document.redirect.submit();</script>";
    }

    public function dummyValues() {
        $values = "";
        $json = $this->dummy();
        $array = json_decode($json);
        foreach ($array as $key => $value) {
            $values .="<input type='text' name='" . $key . "' value='" . $value . "'>";
        }
        return $values;
    }

    public function dummy() {
        return '{  
   "dispnumber":"+919019101000",
   "extension":"None",
   "callid":"ec8a4ba6-c2eb-4e71-888f-c3acbf585111",
   "call_duration":"7",
   "destination":"Welcome Sound",
   "caller_id":"+919966679850",
   "end_time":"2016-09-27 09:19:34.667036+05:30",
   "action":"6",
   "timezone":"Asia/Kolkata",
   "resource_url":"",
   "hangup_cause":"900",
   "type":"dtmf",
   "start_time":"2016-09-27 03:49:44.391601+00:00",
   "call_type":"incoming"
}';
    }

}
