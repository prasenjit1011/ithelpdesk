<?php
namespace App\Plugins\Telephony\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Provider extends Controller{
    
    protected $request;
    
    public function __construct(Request $request) {
        $this->request = $request;
        loging('telephone', json_encode($request->all()), 'info');
    }


    public function provider($provider,$modelid="",$model=""){
        $this->switchProvider($provider, $modelid, $model);
    }
    
    public function switchProvider($provider,$modelid,$model){
        switch ($provider){
            case "exotel":
                $exotel = new \App\Plugins\Telephony\Controllers\Exotel\ExotelController();
                $exotel->passThrough($modelid,$model,$this->request);
                break;
            case "mcube":
                $mcube = new \App\Plugins\Telephony\Controllers\Mcube\McubeController();
                
                $mcube->passThrough($modelid,$model,$this->request);
                break;
            case "knowlarity":
                $knowlarity = new \App\Plugins\Telephony\Controllers\Knowlarity\KnowlarityController();
                $knowlarity->passThrough($modelid,$model,$this->request);
                break;
        }
    }
    
}

