<?php

Route::group(['middleware' => ['web', 'auth', 'roles'], 'prefix' => 'telephone'], function() {
    Route::get('settings', ['as' => 'telephone.settings', 'uses' => 'App\Plugins\Telephony\Controllers\Core\SettingsController@settings']);
    Route::get('seed', ['as' => 'telephone.seed', 'uses' => 'App\Plugins\Telephony\Controllers\Core\SettingsController@seed']);
    Route::get('{short}/settings', ['as' => 'telephone.provider.setting', 'uses' => 'App\Plugins\Telephony\Controllers\Core\SettingsController@settingsProvider']);
    Route::post('{short}', ['as' => 'telephone.provider', 'uses' => 'App\Plugins\Telephony\Controllers\Core\SettingsController@postSettingsProvider']);
    Route::get('ajax-url',['as'=>'telephone.ajax','uses'=>'App\Plugins\Telephony\Controllers\Core\SettingsController@ajax']);
});

/**
 * Exotel test
 */
Route::get('test/exotel/{modelid?}/{model?}', ['as' => 'telephone.exotel.values', 'uses' => 'App\Plugins\Telephony\Controllers\Exotel\ExotelController@getValues']);
/**
 * Knowrality test
 */
Route::get('test/knowlarity/{modelid?}/{model?}', ['as' => 'telephone.knowlarity.test', 'uses' => 'App\Plugins\Telephony\Controllers\Knowlarity\KnowlarityController@getValues']);

/**
 * mcube test
 */
Route::get('test/mcube/{modelid?}/{model?}', ['as' => 'telephone.knowlarity.test', 'uses' => 'App\Plugins\Telephony\Controllers\Mcube\McubeController@getValues']);


Route::match(['get','post'],'telephone/{provider}/pass/{modelid?}/{model?}',['as' => 'telephone.pass', 'uses' => 'App\Plugins\Telephony\Controllers\Core\Provider@provider']);
