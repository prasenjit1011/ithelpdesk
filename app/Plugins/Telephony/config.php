<?php

return [
    'name' => 'Telephony',
    'description' => 'Faveo Telephony is a plugin to provide telephone integration',
    'author' => 'ladybird',
    'website' => 'www.ithelpdesk.com',
    'version' => '1.0.0',
    'settings' => 'telephone/settings',
    
];

