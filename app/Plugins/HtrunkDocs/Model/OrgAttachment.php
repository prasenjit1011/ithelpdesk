<?php namespace App\Plugins\HtrunkDocs\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\helpdesk\Agent_panel\Organization;

class OrgAttachment extends Model {

	 protected $table = 'org_attachment';

    /* Define the fillable fields */
    protected $fillable = ['id', 'org_id', 'file_name', 'created_at', 'updated_at'];

}

