<?php

namespace App\Plugins\HtrunkDocs;

class ServiceProvider extends \App\Plugins\ServiceProvider {

    public function register() {
        parent::register('HtrunkDocs');
    }

    public function boot() {
        /** 
         *View
         */
        $view_path = app_path().DIRECTORY_SEPARATOR.'Plugins'.DIRECTORY_SEPARATOR.'HtrunkDocs'.DIRECTORY_SEPARATOR.'views';
        $this->loadViewsFrom($view_path, 'htrunkDocs');
        parent::boot('HtrunkDocs');
        
        // if (class_exists('Breadcrumbs')) {
        //     require __DIR__ . '/breadcrumbs.php';
        // }
        
        /**
         *language
         */
        $trans = app_path() . DIRECTORY_SEPARATOR . 'Plugins' . DIRECTORY_SEPARATOR . 'HtrunkDocs' . DIRECTORY_SEPARATOR . 'lang';
        $this->loadTranslationsFrom($trans, 'HtrunkDocs');
    }
    

}
