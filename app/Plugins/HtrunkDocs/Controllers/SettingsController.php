<?php

namespace App\Plugins\HtrunkDocs\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Plugins\Reseller\Model\Reseller;
use Illuminate\Database\Schema\Blueprint;
use Schema;
use Lang;


use App\Model\helpdesk\Settings\Plugin;

class SettingsController extends Controller {

    public function index() {

        $htrunk_docs_status=Plugin::where('name','=','HtrunkDocs')->select('status')->first();
        return view('htrunkDocs::settings',compact('htrunk_docs_status'));
    }

    public function update(Request $request) {
        
        try {

            $change_status=$request->settings_htrunk;
            $htrunk_status_update=Plugin::where('name','=','HtrunkDocs')->update(['status' => $change_status]);

            return Lang::get('HtrunkDocs::lang.your_status_updated');
            // return 'Your Status Updated';
            // return $this->successResponse('Updated Successfully');
        } catch (\Exception $ex) {

            return $this->failsResponse($ex->getMessage());
        }
    }

   

}
