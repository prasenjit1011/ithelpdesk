<?php

namespace App\Plugins\HtrunkDocs\Controllers;

// controllers
use App\Http\Controllers\Controller;
// requests
use App\Http\Requests\helpdesk\OrganizationRequest;
/* include organization model */
use App\Http\Requests\helpdesk\OrganizationUpdate;
// models
/* Define OrganizationRequest to validate the create form */
use App\Model\helpdesk\Agent_panel\Organization;
use App\Model\helpdesk\Agent_panel\User_org;
use App\Plugins\HtrunkDocs\Model\OrgAttachment;
use App\Model\helpdesk\Manage\Sla\Sla_plan;
use App\Model\helpdesk\Settings\Plugin;
/* Define OrganizationUpdate to validate the create form */
use App\User;
// classes
use Exception;
use Lang;
use Illuminate\Http\Request;
use Datatables;
use Input;
use DB;
use File;
use Auth;

class OrganizationsController extends Controller {

    /**
     * Show the form for creating a new organization.
     *
     * @return type Response
     */
    public function canuploadfile() {

        echo view('htrunkDocs::canuploadfile')->render();
  }

    /**
     * Show the form for creating a new organization.
     *
     * @return type Response
     */
    public function docslist($org_id) {
        
             $status_check = Plugin::where('name', '=', 'HtrunkDocs')->select('status')->first();
             $license_docs_list = OrgAttachment::where('org_id', '=', $org_id)->select('id', 'org_id', 'file_name', 'created_at')->get();
              if ($status_check->status == 1) {
             echo view('htrunkDocs::index', compact('org_id', 'license_docs_list'))->render();
        }
    }

    /**
     * Show the form for creating a new organization.
     *
     * @return type Response
     */
    public function uploadfile($org_id) {
   echo view('htrunkDocs::uploadfile', compact('org_id', 'license_docs_list'))->render();

    }

    /**
     * Display a listing of the resource.
     *
     * @param type Organization $org
     *
     * @return type Response
     */
    public function index() {
        try {
            $table = \ Datatable::table()
                    ->addColumn(
                            Lang::get('lang.name'), Lang::get('lang.website'), Lang::get('phone'), Lang::get('action'))  // these are the column headings to be shown
                    ->noScript();
            /* get all values of table organization */
            return view('themes.default1.agent.helpdesk.organization.index', compact('table'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

   
    /**
     * Show the form for creating a new organization.
     *
     * @return type Response
     */
    public function create() {

        try {

             $status_check = Plugin::where('name', '=', 'Htrunk')->select('status')->first();
                 if ($status_check->status == 1) {
                echo view('htrunk::create')->render();
            }

            // return \view('Htrunk::create');
            // return view('themes.default1.agent.helpdesk.organization.create');
            // return view('themes.default1.agent.helpdesk.organization.customformorgs');
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

  

    /**
     * Show the form for editing the specified organization.
     *
     * @param type              $id
     * @param type Organization $org
     *
     * @return type view
     */
    public function edit() {
        try {

            // dd('id');

            $status_check = Plugin::where('name', '=', 'Htrunk')->select('status')->first();

            if ($status_check->status == 1) {
                echo view('htrunk::edit')->render();
            }
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    public function orgDocsAttachment(Request $request) {

        $org_id = $request->orgs_id;

// dd($org_id);
        for ($j = 0; $j <= 100; $j++) {
            // dd(Input::file('org_docs'.$j));
            if (Input::file('org_docs' . $j)) {

                $name = Input::file('org_docs' . $j)->getClientOriginalName();
                $destinationPath = 'uploads/organization/';
                $fileName = rand(0000, 9999) . '.' . $name;
                Input::file('org_docs' . $j)->move($destinationPath, $fileName);
                $attachment = new OrgAttachment();
                $attachment->org_id = $request->orgs_id;
                $attachment->file_name = $fileName;
                $attachment->save();
            }
        }


        return redirect()->back()->with('orgdocssuccess', Lang::get('HtrunkDocs::lang.organization_docs_update_successfully'));
    }

    
    public function downloadDocs($file_name) {
       
        // public_path() . '/uploads/organizations/' . $fileName ;
        $file_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'organization' . DIRECTORY_SEPARATOR . $file_name;


        return response()->download($file_path);
    }

    public function deleteDocs($id) {
        

        $licenses_docs = OrgAttachment::where('id', '=', $id)->select('file_name')->first();
         $path = 'uploads/organization/';
     
        File::delete($path . $licenses_docs->file_name);
         $delete_licenses_docs = OrgAttachment::where('id', '=', $id)->delete();

        return redirect()->back()->with('orgdoclistssuccess', Lang::get('HtrunkDocs::lang.document_deleted_successfully'));
        ;
    }

  
    /**
     * Delete a specified organization from storage.
     *
     * @param type int $id
     *
     * @return type Redirect
     */
    public function destroy($id, Organization $org, User_org $user_org) {
        try {
            /* select the field by id  */
            $orgs = $org->whereId($id)->first();
            $user_orgs = $user_org->where('org_id', '=', $id)->get();
            foreach ($user_orgs as $user_org) {
                $user_org->delete();
            }
            /* Delete the field selected from the table */

            $orgs->delete();
            /* redirect to Index page with Success Message */
            return redirect('organizations')->with('success', Lang::get('lang.organization_deleted_successfully'));
        } catch (Exception $e) {
            /* redirect to Index page with Fails Message */
            return redirect('organizations')->with('fails', $e->getMessage());
        }
    }

   

}
