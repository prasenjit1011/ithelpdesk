
<?php
$user_assign_group = App\User::where('id', '=', Auth::user()->id)->select('assign_group')->first();
$group = App\Model\helpdesk\Agent\Groups::where('user_id', '=', Auth::user()->id)->first();
$keys = $group->permision;
?>


@if(Auth::user()->role!='user')
@if(array_key_exists('organisation_document_upload', $keys))


<div class="box box-primary">

    @if(Session::has('orgdocssuccess'))
    <div id="success-alert" class="alert alert-success alert-dismissable">
        <i class="fa  fa-check-circle"> </i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('orgdocssuccess')}}
    </div>
    @endif
    <!-- failure message -->
    @if(Session::has('fails'))
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"> </i> <b> {!! Lang::get('lang.alert') !!} ! </b>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('fails')}}
    </div>
    @endif

    <div class="box box-default">
        <div class="box-header with-border">
            {{Lang::get('HtrunkDocs::lang.document_of_the_organization')}}
            <!-- {{Lang::get('lang.document_of_the_organization')}} -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="modal-body">
            </div>
            <div class="panel-body hide" >

                <div class="row source-time-track source-counter">
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <label class=" control-label1"><span class="glyphicon glyphicon glyphicon-minus-sign resolved-violated" aria-hidden="true" style="color:#990000;"></span>&nbsp;&nbsp;&nbsp; Remove</label>
                    </div>

                    <div class="col-xs-6 form-group">
                        <input type="file" class="form-control docs_attach" id="docs_attach"  name="org_docs" accept="image/file/video*" />
                    </div>

                </div>
            </div>
            </br>
            <button class=" control-label add-time-grid" style="font-family:inherit;">
                <i class="fa fa-paperclip" aria-hidden="true" style="color:#009853; ">
                </i >
                &nbsp; &nbsp;Add Attachment</button> 

            <form action="{!!URL::route('post.org-docs')!!}" method="post" role="form" enctype="multipart/form-data">
                {{ csrf_field() }}
                <span class="row target-area-div" id="target-area-div">
                    <input type="hidden" name="orgs_id" value="{{$org_id }}">

                </span>                               
        </div>
        <div class="box-footer">
            {!! Form::submit(Lang::get('lang.submit'),['class'=>'btn btn-primary'])!!}
        </div>
        </form>

    </div><!-- /.box-body -->
</div><!-- /.box -->


@endif

@endif

<script type="text/javascript">

    $('.add-time-grid').on('click', function(event) {
        var counter = $('#target-area-div').find('.source-counter').length;
        $('#target-area-div').append($(".source-time-track").clone());
        $('#target-area-div').find('.source-time-track').removeClass("source-time-track");
        var counter = $('#target-area-div').find('.source-counter').length;
        $('#target-area-div').find('.docs_attach').each(function(counter) {
            $(this).attr('name', 'org_docs' + counter);
        });
    });
</script>

<script type="text/javascript">
    var counter = $('#target-area-div').find('.source-counter').length;
    if (counter != 0) {
        $('#target-area-div').find('.docs_attach').each(function(counter) {
            $(this).attr('name', 'org_docs' + counter);
        });
    }
    // remove function
    $(document).on("click", ".resolved-violated", function(event) {
        $(this).parent().parent().parent().remove();
        var counter = $('#target-area-div').find('.source-counter').length;
        $('#target-area-div').find('.docs_attach').each(function(counter) {
            $(this).attr('name', 'org_docs' + counter);
        });
    });
</script>
