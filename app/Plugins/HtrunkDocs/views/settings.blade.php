@extends('themes.default1.admin.layout.admin')
@section('content')

<div class="alert alert-success alert-dismissable" style="display: none;">
    <i class="fa  fa-check-circle"></i>
    <span class="success-msg"></span>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    
</div>

<section class="content-heading-anchor">
    <h1>
        <!-- {{Lang::get('chat::lang.chat-integrations')}}   -->

HtrankDocs
    </h1>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.css" rel="stylesheet">
</section>


<!-- Main content -->

<div class="box box-primary">
    <div class="box-header with-border">
        <h4> {{Lang::get('HtrunkDocs::lang.applications')}}  </h4>
        
        <!-- /.box-header -->
        <!-- form start -->
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('success')}}
        </div>
        @endif
        <!-- fail message -->
        @if(Session::has('fails'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <b>{{Lang::get('message.alert')}}!</b> {{Lang::get('message.failed')}}.
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('fails')}}
        </div>
        @endif

    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-borderless table-responsive">
                    <thead>
                        <tr>
                           
                            <th>Application</th>
                            <th>Action</th>
                          
                        </tr>
                    </thead>
                    <tbody>
                        
                        <tr>
                            <td>HtrunkDocs</td>
                            <td> 
                            <div class="col-md-6">
                            <div class="btn-group" id="toggle_event_editing">
                                <button type="button"  class="btn {{$htrunk_docs_status->status == '0' ? 'btn-info' : 'btn-default'}} locked_active">OFF</button>
                                <button type="button"  class="btn {{$htrunk_docs_status->status == '1' ? 'btn-info' : 'btn-default'}} unlocked_inactive">ON</button>
                            </div>
                            <!-- <div class="alert alert-info" id="switch_status"></div> -->
                        </div>

                            </td>
                           
                        </tr>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<script>
    $('#toggle_event_editing button').click(function () {

        var settings_htrunk=1;
        var settings_htrunk=0;
        if ($(this).hasClass('locked_active') ) {
         

            settings_htrunk = 0
        } if ( $(this).hasClass('unlocked_inactive')) {
          
            settings_htrunk = 1;
        }

        /* reverse locking status */
        $('#toggle_event_editing button').eq(0).toggleClass('locked_inactive locked_active btn-default btn-info');
        $('#toggle_event_editing button').eq(1).toggleClass('unlocked_inactive unlocked_active btn-info btn-default');
        $.ajax({
            type: 'post',
            url: '{{route("htrunkDocs.settings.post")}}',
            data: {
                "_token": "{{ csrf_token() }}",
                settings_htrunk: settings_htrunk
            },
            success: function (result) {
                 $('.success-msg').html(result);
                $('.alert-success').css('display', 'block');
                setInterval(function(){
                    $('.alert-success').fadeOut( 3000, function() {});
                }, 500);
            }
        });
    });
</script>
@stop