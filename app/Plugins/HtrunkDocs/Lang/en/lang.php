<?php
return [
    'list_of_documents'=>'List of Documents',
    'document_of_the_organization'=>'Document of the organization',
    'can_upload_document'=>'Can upload document',
   'organization_docs_update_successfully'=>'Organization Docs update successfully',
   'document_deleted_successfully'=>'Document deleted successfully',
   'name'=>'Name',
     'created_date'=>'Created date',
     'action'=>'Action',
     'applications'=>'Applications',
     'your_status_updated'=>'Your Status Updated',
];
