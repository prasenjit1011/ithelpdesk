


<?php

\Event::listen('orgdocs.show',function($org_id){

	// dd($org_id);
    $controller = new App\Plugins\HtrunkDocs\Controllers\OrganizationsController;
	 echo $controller->docslist($org_id);

   $controller = new App\Plugins\HtrunkDocs\Controllers\OrganizationsController;
	 echo $controller->uploadfile($org_id);

});

\Event::listen('can.orgdocs.upload',function(){

	dd($org_id);
    $controller = new App\Plugins\HtrunkDocs\Controllers\OrganizationsController;
   echo $controller->canuploadfile();


});




/**
 * Auth route
 */
Route::group(['middleware' =>'web'], function() {
    // Route::get('/organizations/index',['as'=>'organizations.index1','uses'=>'App\Plugins\Htrunk\Controllers\OrganizationsController@index']);
    //  Route::get('org-list', ['as' => 'org.list', 'uses' => 'Agent\helpdesk\OrganizationController@org_list']);

  Route::get('htrunkDocs/settings',['as'=>'htrunkDocs.settings.get','uses'=>'App\Plugins\HtrunkDocs\Controllers\SettingsController@index']);

   Route::post('htrunkDocs/postsettings',['as'=>'htrunkDocs.settings.post','uses'=>'App\Plugins\HtrunkDocs\Controllers\SettingsController@update']);

 Route::post('org-docs', ['as' => 'post.org-docs', 'uses' => 'App\Plugins\HtrunkDocs\Controllers\OrganizationsController@orgDocsAttachment']);

Route::get('/download/{file_name}', ['as' => 'org.download.docs', 'uses' => 'App\Plugins\HtrunkDocs\Controllers\OrganizationsController@downloadDocs']);

Route::get('org/docs_list', ['as' => 'org.docs_list', 'uses' => 'App\Plugins\HtrunkDocs\Controllers\OrganizationController@licensesDocs']);

Route::get('org/docs_list/{org_id}', ['as' => 'org.docs_lists', 'uses' => 'App\Plugins\HtrunkDocs\Controllers\OrganizationsController@licensesDocs']);

Route::get('/download/delete/{id}', ['as' => 'org.download.delete.docs', 'uses' => 'App\Plugins\HtrunkDocs\Controllers\OrganizationsController@deleteDocs']);

});

 
