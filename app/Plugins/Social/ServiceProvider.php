<?php

namespace App\Plugins\Social;

class ServiceProvider extends \App\Plugins\ServiceProvider {

    public function register() {
        parent::register('Social');
    }

    public function boot() {
        /**
         * Views
         */
        $view_path = app_path() . DIRECTORY_SEPARATOR . 'Plugins' . DIRECTORY_SEPARATOR . 'Social' . DIRECTORY_SEPARATOR . 'views';
        $this->loadViewsFrom($view_path, 'social');

//        if (class_exists('Breadcrumbs')) {
//            require __DIR__ . '/breadcrumbs.php';
//        }

        /**
         * Translation
         */
        $trans = app_path() . DIRECTORY_SEPARATOR . 'Plugins' . DIRECTORY_SEPARATOR . 'Social' . DIRECTORY_SEPARATOR . 'lang';
        $this->loadTranslationsFrom($trans, 'social');
        
        $controller = new Controllers\Core\SettingsController();
        $controller->migrate();

        parent::boot('Social');
    }

}
