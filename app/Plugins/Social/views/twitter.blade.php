<a href="#twitter"  data-toggle="modal" class="btn btn-primary" data-target="#twitter">Twitter</a>
<div class="modal fade" id="twitter">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{!! Lang::get('social::lang.twitter') !!}</h4>
                {!! Form::open(['url'=>'twitter/save','method'=>'post']) !!}
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                       {!! Form::label('client_id',Lang::get('social::lang.client-id')) !!}
                       {!! Form::text('client_id',$social->getvalueByKey('twitter','client_id'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-12">
                       {!! Form::label('client_secret',Lang::get('social::lang.client-secrete')) !!}
                       {!! Form::text('client_secret',$social->getvalueByKey('twitter','client_secret'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-12">
                       {!! Form::label('redirect',Lang::get('social::lang.redirect-url')) !!}
                       {!! Form::text('redirect',$social->getvalueByKey('twitter','redirect'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-12">
                       {!! Form::label('access_token',Lang::get('social::lang.access-token')) !!}
                       {!! Form::text('access_token',$social->getvalueByKey('twitter','access_token'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-12">
                       {!! Form::label('access_secret',Lang::get('social::lang.access-secrete')) !!}
                       {!! Form::text('access_secret',$social->getvalueByKey('twitter','access_secret'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-12">
                       {!! Form::label('query',Lang::get('social::lang.query')) !!}
                       {!! Form::text('query',$social->getvalueByKey('twitter','query'),['class'=>'form-control','placeholder'=>'@Faveo']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit(Lang::get('social::lang.save'),['class'=>'btn btn-primary pull-left']) !!}
                {!! Form::close() !!}
            </div>

            <!-- /Form -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


