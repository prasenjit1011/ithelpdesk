<a href="#facebook"  data-toggle="modal" class="btn btn-primary" data-target="#facebook-credentials">Facebook Credentials</a>
<div class="modal fade" id="facebook-credentials">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{!! Lang::get('social::lang.facebook') !!}</h4>
                {!! Form::open(['url' => 'social/media/facebook', 'method' => 'POST']) !!}
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::label('client_id',Lang::get('social::lang.app-id')) !!}
                        {!! Form::text('client_id',$social->getvalueByKey('facebook','client_id'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-12">
                        {!! Form::label('client_secret',Lang::get('social::lang.app-secrete')) !!}
                        {!! Form::text('client_secret',$social->getvalueByKey('facebook','client_secret'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-12">
                        {!! Form::label('redirect',Lang::get('social::lang.redirect-url')) !!}
                        {!! Form::text('redirect',$social->getvalueByKey('facebook','redirect'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::label('status',Lang::get('lang.status')) !!}
                                    {!! $errors->first('status', '<spam class="help-block">:message</spam>') !!}

                                </div>
                                <div class="col-md-6">
                                    <p>{!! Form::radio('status',1,$social->checkActive('facebook'))!!} Active</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{!! Form::radio('status',0,$social->checkInactive('facebook')) !!} Inactive</p>
                                </div>
                                <div class="col-md-12">
                                    <i>Activate login via {{ucfirst('facebook')}}</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit(Lang::get('social::lang.save'),['class'=>'btn btn-primary pull-left']) !!}
                {!! Form::close() !!}
            </div>


            <!-- /Form -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
