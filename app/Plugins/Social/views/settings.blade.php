@extends('themes.default1.admin.layout.admin')
@section('content')

<section class="content-heading-anchor">
    <h1>
        {{Lang::get('social::lang.social-integration')}}  


    </h1>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if($warn!=="")
        <div class="alert alert-warning alert-dismissable">
            <i class="fa  fa-check-circle"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!!$warn!!}
        </div>
        @endif
        <!-- check whether success or not -->
        @if(Session::has('warn'))
        <div class="alert alert-warning alert-dismissable">
            <i class="fa  fa-check-circle"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!!Session::get('warn')!!}
        </div>
        @endif
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('success')}}
        </div>
        @endif
        <!-- fail message -->
        @if(Session::has('fails'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <b>{{Lang::get('message.alert')}}!</b> {{Lang::get('message.failed')}}.
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('fails')}}
        </div>
        @endif



</section>


<!-- Main content -->
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3>{!! Lang::get('social::lang.facebook') !!}</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        @include('social::facebook')
                    </div>
                    <div class="col-md-6">
                        @include('social::facebook-credentials')
                        <h6>{!!Lang::get('social::lang.app-id')!!}: <b>{{str_limit($social->getvalueByKey('facebook','client_id'),20)}}</b></h6>
                        <h6>{!!Lang::get('social::lang.app-secrete')!!}: <b>{{str_limit($social->getvalueByKey('facebook','client_secret'),20)}}</b></h6>
                        <h6>{!!Lang::get('lang.redirect')!!}: <b>{{$social->getvalueByKey('facebook','redirect')}}</b></h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3>{!! Lang::get('social::lang.twitter') !!}</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        @include('social::twitter')
                        <h6>{!!Lang::get('lang.client_id')!!}: <b>{{str_limit($social->getvalueByKey('twitter','client_id'),30)}}</b></h6>
                        <h6>{!!Lang::get('lang.client_secret')!!}: <b>{{str_limit($social->getvalueByKey('twitter','client_secret'),30)}}</b></h6>
                        <h6>{!!Lang::get('lang.redirect')!!}: <b>{{$social->getvalueByKey('twitter','redirect')}}</b></h6>
                        <h6>{!!Lang::get('social::lang.access-token')!!}: <b>{{str_limit($social->getvalueByKey('twitter','access_token'),30)}}</b></h6>
                        <h6>{!!Lang::get('social::lang.access-secrete')!!}: <b>{{str_limit($social->getvalueByKey('twitter','access_secret'),30)}}</b></h6>
                        <h6>{!!Lang::get('social::lang.query')!!}: <b>{{$social->getvalueByKey('twitter','query')}}</b></h6>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

@stop
