<a href="#facebook"  data-toggle="modal" class="btn btn-primary" data-target="#facebook">Connect a new page</a>
<div class="modal fade" id="facebook">
    <div class="modal-dialog">
        <div class="modal-content">
            @if($social->getvalueByKey('facebook','access_token'))
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{!! Lang::get('social::lang.choose-a-page') !!}</h4>
                {!! Form::open(['url'=>'facebook/page/save','method'=>'post']) !!}
            </div>
            <div class="modal-body">
                <ul class="list-unstyled">
                   @if($facebook->getFacebookPages())
                    @forelse($facebook->getFacebookPages() as $page)
                    <li><input type="checkbox" name="page[{{$page['name']}}][id]" value="{{$page['id']}}">  {{$page['name']}}</li>
                    @empty 
                    <li>{!! Lang::get('social::lang.your-are-not-amin-of-any-pages') !!}</li>
                    @endforelse
                    @else 
                    <li>{!! Lang::get('social::lang.your-are-not-amin-of-any-pages') !!}</li>
                    @endif
                </ul>
            </div>
            <div class="modal-footer">
                {!! Form::submit(Lang::get('social::lang.save'),['class'=>'btn btn-primary pull-left']) !!}
                {!! Form::close() !!}
                <a href="{{$facebook->accessUrl()}}">{!! Lang::get('social::lang.connect-to-your-facebook-account') !!}</a>
            </div>
            @else 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{!! Lang::get('social::lang.facebook') !!}</h4>
            </div>
            <div class="modal-body">
                <a class="btn btn-block btn-social btn-facebook" href="{{$facebook->accessUrl()}}" style="background-color: #3B5998;color: white;">
                    <span class="fa fa-facebook"></span> {!! Lang::get('social::lang.get-access-from-facebook') !!}
                </a>
               

            </div>

            @endif

            <!-- /Form -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@if(count($facebook->getPagesFromDb())>0)
<div class="clearfix">
    <br>
</div>
<div>
    <table class="table table-bordered">
        
        @forelse($facebook->getPagesFromDb() as $page)
        @if(is_array($page))
        @foreach($page as $name=>$values)
        <tr>
            <td>{{$name}}
            
                <div class="btn btn-group pull-right">
                    <a href="#facebook"  data-toggle="modal" class="" data-target="#facebook">edit</a>
                </div>
                
            </td>
        </tr>
        @endforeach
        @endif
        @empty 
        <tr><td>{!! Lang::get('social::lang.you-havent-choosen-any-pages') !!}</td></tr>
        @endforelse
    </table>
</div>
@endif