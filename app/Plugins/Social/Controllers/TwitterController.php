<?php

namespace App\Plugins\Social\Controllers;

use League\OAuth1\Client\Server\Twitter;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\helpdesk\SocialMedia\SocialMediaController;
use Config;
use Illuminate\Http\Request;
use App\Model\helpdesk\Settings\SocialMedia;
use GuzzleHttp\Client;
use Exception;

class TwitterController extends Controller {

    public function __construct() {
        $this->middleware(['auth', 'roles']);
        $social = new SocialMediaController();
        $social->configService();
    }

    public function twitter() {
        //127.0.0.1
        $redirect = url('twitter/callback');
        Config::set('services.twitter.redirect', $redirect);
        $credentials = Config::get('services.twitter');
        $clientCredentials = [
            'identifier' => $credentials['client_id'],
            'secret' => $credentials['client_secret'],
            'oauth_callback' => $credentials['redirect'],
        ];
        //dd($clientCredentials);
        $twitter = new Twitter($clientCredentials);
        return $twitter;
    }

    public function authUrl() {
        $twitter = $this->twitter();
        $response = $twitter->getTemporaryCredentials();
        $identifier = $response->getIdentifier();
        $url = $twitter->getAuthorizationUrl($identifier);
        return $url;
    }

    public function callBack(Request $request) {
        $twitter = $this->twitter();
        $temp = $twitter->getTemporaryCredentials();
        $identifier = $temp->getIdentifier();
        $verifier = $request->input('oauth_verifier');
        $response = $twitter->getTokenCredentials($temp, $identifier, $verifier);
        dd($response);
    }

    public function getResponse($url) {
        $client = new Client();
        $response = $client->get($url);
        return $response;
    }

    public function getMessage() {
        $url = "https://api.twitter.com/1.1/direct_messages.json";
        $twitter = $this->getApiExchange();
        $since_id = $this->getSinceIdForTwitter('message');
        return $twitter->setGetfield('since_id=' . $since_id)
                        ->buildOauth($url, 'GET')
                        ->performRequest();
        //dd($response);
    }

    public function getTweets() {
        $url = "https://api.twitter.com/1.1/search/tweets.json";
        $twitter = $this->getApiExchange();
        $social = new SocialMedia();
        $since_id = $this->getSinceIdForTwitter('tweet');

        $q = $social->getvalueByKey('twitter', 'query');
        //dd($q);
        $tweets = $twitter->setGetfield('q=' . $q . '&since_id=' . $since_id)
                        ->buildOauth($url, 'GET')
                        ->performRequest();
        return $tweets;
    }

    public function showMessage($id) {
        $url = "https://api.twitter.com/1.1/direct_messages/show.json";
        $twitter = $this->getApiExchange();

        return $twitter->setGetfield('id=' . $id)
                        ->buildOauth($url, 'GET')
                        ->performRequest();
    }

    public function getAuthToken() {
        $social = new SocialMedia();
        $auth_token = $social->getvalueByKey('twitter', 'oauth_token');
        if (!$auth_token) {
            return redirect('twitter/auth');
        }
        return $auth_token;
    }

    public function getApiExchange() {
        $settings = $this->settings();
        $api = new TwitterAPIExchange($settings);
        return $api;
    }

    public function settings() {
        $social = new SocialMedia();
        $credentials = $social->getvalueByKey('twitter');
        return [
            'oauth_access_token' => $credentials['access_token'],
            'oauth_access_token_secret' => $credentials['access_secret'],
            'consumer_key' => $credentials['client_id'],
            'consumer_secret' => $credentials['client_secret'],
        ];
    }

    public function save(Request $request) {
        $requests = $request->except('_token');
        $rules = [];
        foreach ($requests as $key => $req) {
            $rules[$key] = "required";
        }
        $this->validate($request, $rules);
        try {
            $social_controller = new SocialMediaController();
            $social_controller->insertProvider('twitter', $requests);
            return redirect()->back()->with('success', 'Saved');
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function getSinceIdForTwitter($via) {
        $id = "";
        $channels = new \App\Plugins\Social\Model\SocialChannel();
        $channel = $channels->where('channel', 'twitter')->where('via', $via)->orderBy('posted_at', 'desc')->first();
        if ($channel) {
            $id = $channel->message_id;
        }
        return $id;
    }

    public function replyTweet($messageid, $content) {
        $postdata = [
            'status' => $content,
            'in_reply_to_status_id' => $messageid,
        ];
        $url = "https://api.twitter.com/1.1/statuses/update.json";
        $twitter = $this->getApiExchange();
        $tweet = $twitter->setPostfields($postdata)
                ->buildOauth($url, 'POST')
                ->performRequest();
        return $tweet;
    }

    public function replyMessage($userid, $username, $content) {
        $postdata = [
            'user_id' => $userid,
            'screen_name' => $username,
            'text' => $content,
        ];
        $url = "https://api.twitter.com/1.1/direct_messages/new.json";
        $twitter = $this->getApiExchange();
        $tweet = $twitter->setPostfields($postdata)
                ->buildOauth($url, 'POST')
                ->performRequest();
        return $tweet;
    }

}
