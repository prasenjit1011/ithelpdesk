<?php

namespace App\Plugins\Social\Controllers\Core;

use App\Http\Controllers\Controller;
use App\Model\helpdesk\Settings\SocialMedia;
use App\Plugins\Social\Controllers\FacebookController;
use App\Plugins\Social\Controllers\TwitterController;
use Exception;
use Artisan;
use Schema;
use App\Plugins\Social\Controllers\Core\TwitterTicketController;

class SettingsController extends Controller {

    protected $twitter;
    protected $facebook;

    public function __construct() {
        $twitter = new TwitterController();
        $this->twitter = $twitter;
        $facebook = new FacebookController();
        $this->facebook = $facebook;
    }

    public function settings() {
        try {
            $social = new SocialMedia();
            $facebook = new FacebookController();
            $twitter = $this->twitter;
            $warn = $this->socialIntegrationWarning();
            //dd($warn);
            return view('social::settings', compact('social', 'facebook', 'twitter', 'warn'));
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function tweets() {
        $tweets_json = $this->twitter->getTweets();
        $tweets_array = json_decode($tweets_json);
        return $tweets_array->statuses;
    }

    public function twitterMessages() {
        $messages_json = $this->twitter->getMessage();
        $messages_array = json_decode($messages_json);
        return $messages_array;
    }

    public function facebookPageMessages() {
        $messages_array = $this->facebook->getMessagesFromPage();
        return $messages_array;
    }

    public function showMessage($msgid) {
        $messages_json = $this->twitter->showMessage($msgid);
        $messages_array = json_decode($messages_json);
        return $messages_array;
    }

    public function migrate() {
        if (env('DB_INSTALL') == 1 && !Schema::hasTable('social_channel')) {
            $path = "app" . DIRECTORY_SEPARATOR . "Plugins" . DIRECTORY_SEPARATOR . "Social" . DIRECTORY_SEPARATOR . "database" . DIRECTORY_SEPARATOR . "migrations";
            Artisan::call('migrate', [
                '--path' => $path,
                '--force' => true,
            ]);
        }
    }

    public function tweetFields() {
        $array = [];
        $tweets = $this->tweets();
        if (count($tweets) > 0) {
            foreach ($tweets as $key => $tweet) {
                $array[$key]['channel'] = 'twitter';
                $array[$key]['via'] = 'tweet';
                $array[$key]['posted_at'] = $tweet->created_at;
                $array[$key]['message_id'] = $tweet->id_str;
                $array[$key]['message'] = $tweet->text;
                $array[$key]['user_id'] = $tweet->user->id_str;
                $array[$key]['username'] = $tweet->user->screen_name;
                $array[$key]['avatar'] = $tweet->user->profile_image_url;
            }
        }
        return $this->arraySort($array);
    }

    public function msgFields() {
        $array = [];
        $tweets = $this->twitterMessages();
        if (count($tweets) > 0) {
            foreach ($tweets as $key => $tweet) {
                if(is_array($tweet) && array_key_exists(0, $tweet) && $tweet[0]->message){
                    loging('twitter-message', $tweet[0]->message, 'warning');
                    exit();
                }
                $array[$key]['channel'] = 'twitter';
                $array[$key]['via'] = 'message';
                $array[$key]['posted_at'] = $tweet->created_at;
                $array[$key]['message_id'] = $tweet->id_str;
                $array[$key]['message'] = $tweet->text;
                $array[$key]['user_id'] = $tweet->sender->id_str;
                $array[$key]['username'] = $tweet->sender->screen_name;
                $array[$key]['avatar'] = $tweet->sender->profile_image_url;
            }
        }
        return $this->arraySort($array);
    }

    public function arraySort($arr) {
        $array = array_values(array_sort($arr, function ($value) {
                    return $value['posted_at'];
                }));
        return $array;
    }

    public function getUser($social_fields) {
        $user = [];
        $channel = $this->checkArray('channel', $social_fields);
        $name = $this->checkArray('username', $social_fields);
        $user_id = $this->checkArray('user_id', $social_fields);
        $avatar = $this->checkArray('avatar', $social_fields);
        $email = $this->checkArray('email', $social_fields);
        if ($email == "") {
            $email = null;
        }
        $user['username'] = $name;
        $user['user_id'] = $user_id;
        $user['avatar'] = $avatar;
        $user['email'] = $email;
        return $user;
    }

    public function checkArray($key, $array) {
        $value = "";
        if (is_array($array)) {
            if (key_exists($key, $array)) {
                $value = $array[$key];
            }
        }
        return $value;
    }

    public function getSystemDefaultHelpTopic() {
        $ticket_settings = new \App\Model\helpdesk\Settings\Ticket();
        $ticket_setting = $ticket_settings->find(1);
        $help_topicid = $ticket_setting->help_topic;
        return $help_topicid;
    }

    public function getSystemDefaultSla() {
        $ticket_settings = new \App\Model\helpdesk\Settings\Ticket();
        $ticket_setting = $ticket_settings->find(1);
        $sla = $ticket_setting->sla;
        return $sla;
    }

    public function getSystemDefaultPriority() {
        $ticket_settings = new \App\Model\helpdesk\Settings\Ticket();
        $ticket_setting = $ticket_settings->find(1);
        $priority = $ticket_setting->priority;
        return $priority;
    }

    public function getSystemDefaultDepartment() {
        $systems = new \App\Model\helpdesk\Settings\System();
        $system = $systems->find(1);
        $department = $system->department;
        return $department;
    }

    public function fetchSocialTicket($array) {
        $facebookController = new FacebookTicketController($this);
        $twitterController = new TwitterTicketController($this);
        //dd($twitterController);
        $facebookController->createNewTicketByFacebookPageMsg();
        $twitterController->createNewTicketByTweet();
        $twitterController->createNewTicketByTwitterMsg();
    }

    public function checkCoreFacebookIntegration() {
        $check = false;
        $social = new SocialMedia();
        $clientid = $social->getvalueByKey('facebook', 'client_id');
        //dd($clientid);
        $client_secret = $social->getvalueByKey('facebook', 'client_secret');
        $redirect = $social->getvalueByKey('facebook', 'redirect');
        if ($clientid && $client_secret) {
            
            $check = true;
        }
        return $check;
    }

    public function checkCoreTwiterIntegration() {
        $check = false;
        $social = new SocialMedia();
        $clientid = $social->getvalueByKey('twitter', 'client_id');
        $client_secret = $social->getvalueByKey('twitter', 'client_secret');
        $redirect = $social->getvalueByKey('twitter', 'redirect');
        if ($clientid !== "" && $client_secret !== "" && $redirect !== "") {
            $check = true;
        }
        return $check;
    }

    public function socialIntegrationWarning() {
        $warn = "";
        $check_facebook = $this->checkCoreFacebookIntegration();
        //$check_twitter = $this->checkCoreTwiterIntegration();
        if ($check_facebook == false) {
            $warn .="Please configure your facebook! <a href=" . url('social/media/facebook') . " target='_blank'>Click here</a><br>";
        }
//        if($check_twitter==false){
//            $warn .="Please configure your twitter! <a href=".url('social/media/twitter').">Click here</a><br>";
//        }
        return $warn;
    }

}
