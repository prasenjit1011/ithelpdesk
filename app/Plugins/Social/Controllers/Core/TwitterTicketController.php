<?php

namespace App\Plugins\Social\Controllers\Core;

use App\Http\Controllers\Controller;
use App\Plugins\Social\Controllers\Core\SettingsController;

class TwitterTicketController extends Controller {

    protected $settings;

    public function __construct(SettingsController $settings) {
        $this->settings = $settings;
    }

    public function ticketController() {
        $PhpMailController = new \App\Http\Controllers\Common\PhpMailController();
        $NotificationController = new \App\Http\Controllers\Common\NotificationController();
        $ticket_controller = new \App\Http\Controllers\Agent\helpdesk\TicketController($PhpMailController, $NotificationController);
        return $ticket_controller;
    }

    public function createNewTicketByTweet() {
        $social_fields = $this->settings->tweetFields();
        $this->createTicket($social_fields);
    }

    public function createTicket($social_fields, $subject = "Tweet from Twitter", $provider = "twitter", $via = 'tweet') {
        $result = "";
        $ticket_controller = $this->ticketController();
        if (count($social_fields) > 0) {
            foreach ($social_fields as $social_field) {

                $users = $this->settings->getUser($social_field);
                $email = NULL;
                $username = $this->settings->checkArray('username', $social_field);
                //echo $username."<br>";
                $userid = $this->settings->checkArray('user_id', $social_field);
                $body = $this->settings->checkArray('message', $social_field);
                $priority = $this->settings->getSystemDefaultPriority();
                $department = $this->settings->getSystemDefaultDepartment();
                $helptopic = $this->settings->getSystemDefaultHelpTopic();
                $phone = "";
                $phonecode = "";
                $mobile = "";
                $source = $ticket_controller->getSourceByname($provider)->id;
                $headers = [];
                $assignto = NULL;
                $from_data = [];
                $auto_response = "";
                $status = "";
                $sla = "";
                $priority = "";
                $type = "";
                $ticket_type = \App\Model\helpdesk\Manage\Tickettype::select('id')->first();
                if ($ticket_type) {
                    $type = $ticket_type->id;
                }
                if ($this->checkReply($provider, $userid) == true) {
                    $result = $this->reply($provider, $via, $body, $userid);
                } else {
                    $result = $ticket_controller->create_user($email, $username, $subject, $body, $phone, $phonecode, $mobile, $helptopic, $sla, $priority, $source, $headers, $department, $assignto, $from_data, $auto_response, $status, $type);
                }
                if (is_array($result)) {
                    $this->insertInfo($social_field, $result, $provider, $via);
                }
            }
        }
        \Log::info("Twitter has read");
    }

    public function createNewTicketByTwitterMsg() {
        $social_fields = $this->settings->msgFields();
        $subject = "Message from Twitter";
        $provider = "twitter";
        $via = "message";
        $this->createTicket($social_fields, $subject, $provider, $via);
    }

    public function insertInfo($info, $result, $provider, $via) {
        $userid = $this->findUserFromTicketCreateUserId($result);
        $PhpMailController = new \App\Http\Controllers\Common\PhpMailController();
        $guest_controller = new \App\Http\Controllers\Client\helpdesk\GuestController($PhpMailController);
        $user = $this->settings->getUser($info);
        $guest_controller->update($userid, $user, $provider);
        $this->socialChannel($info, $result, $provider, $via);
    }

    public function reply($provider, $via, $body, $userid) {
        $ticket_id = $this->getTicketIdForReply($provider, $via, $userid);
        $ticket_user_id = $this->ticketUserId($userid);
        $uri = url('/');
        $method = "POST";
        $parameters = [
            'ticket_id' => $ticket_id,
            'content' => $body,
            'do-not-send' => true,
        ];
        $request = \Illuminate\Http\Request::create($uri, $method, $parameters, [], ['attachment' => [0 => ""]]);
        
        $ticket_controller = $this->ticketController();
        $result = $ticket_controller->reply($request, $ticket_id, false, false,$ticket_user_id,false);
        return $result;
    }
    public function ticketUserId($twitter_userid){
        $user_name = "";
        $social_channel = new \App\Plugins\Social\Model\SocialChannel();
        $channel = $social_channel->where('user_id',$twitter_userid)->select('id')->first();
        if($channel){
           $user_name = $channel->username; 
        }
        return $user_name;
    }
    public function getTicketIdForReply($provider, $via, $userid) {
        $social_channel = new \App\Plugins\Social\Model\SocialChannel();
        $threads = new \App\Model\helpdesk\Ticket\Ticket_Thread();
        $channel = $social_channel->getChannelVia($provider, $via, $userid);
        if ($channel) {
            $ticket_id = $channel->ticket_id;
            return $ticket_id;
        }
    }

    public function checkReplyTweetMsg($userid) {
        $check = false;
        $social_channel = new \App\Plugins\Social\Model\SocialChannel();
        $channel = $social_channel->getChannelVia('twitter', 'message', $userid);
        if ($channel) {
            $check = true;
        }
        return $check;
    }

    public function checkReply($provider, $userid) {
        //dd($provider);
        switch ($provider) {
            case "twitter":
                return $this->checkReplyTweetMsg($userid);
        }
    }

    public function socialChannel($info, $result, $provider, $via) {
        $users = $this->settings->getUser($info);
        $array['channel'] = $provider;
        $array['via'] = $via;
        $array['message_id'] = $this->settings->checkArray('message_id', $info);
        $array['user_id'] = $this->settings->checkArray('user_id', $users);
        $array['ticket_id'] = $this->lastTicket($result);
        $array['username'] = $this->settings->checkArray('username', $users);
        $array['posted_at'] = $this->settings->checkArray('posted_at', $info);
        $this->updateSocialChannel($array);
    }

    public function lastTicket($result) {
        $ticket = $this->findTicketFromTicketCreateUser($result);
        //$threads = new \App\Model\helpdesk\Ticket\Ticket_Thread();
        if ($ticket) {
            $ticket_id = $ticket->id;
            //$thread_id = $threads->where('ticket_id', $ticket_id)->max('id');
            return $ticket_id;
        }
    }

    public function updateSocialChannel($array) {
        $social_channel = new \App\Plugins\Social\Model\SocialChannel();
        $social_channel->create($array);
    }

    public function findTicketFromTicketCreateUser($result = []) {
        $ticket_number = $this->settings->checkArray('0', $result);
        if ($ticket_number !== "") {
            $tickets = new \App\Model\helpdesk\Ticket\Tickets();
            $ticket = $tickets->where('ticket_number', $ticket_number)->first();
            if ($ticket) {
                return $ticket;
            }
        }
    }

    public function findUserFromTicketCreateUserId($result = []) {
        $ticket = $this->findTicketFromTicketCreateUser($result);
        if ($ticket) {
            $userid = $ticket->user_id;
            return $userid;
        }
    }

    public function replyTwitter($data) {
        $ticketid = $data['ticket_id'];
        $channel = $this->getChannel($ticketid);
        if ($channel && $channel->channel == 'twitter') {
            $ticketid = $data['ticket_id'];
            $string = $data['body'];
            $content = strip_tags(str_replace("&nbsp;", "", $string));
            $channel = $this->getChannel($ticketid);
            $user_id = $channel->user_id;
            $user = $channel->username;
            $username = "@$user";
            $msgid = $channel->message_id;
            $body = $username . " " . $content;
            $twitter_con = new \App\Plugins\Social\Controllers\TwitterController();
            if ($channel->via == 'tweet') {
                $twitter_con->replyTweet($msgid, $body);
            }

            $twitter_con->replyMessage($user_id, $user, $content);
        }
    }

    public function getLatestTicketId($ticketid) {
        $threads = new \App\Model\helpdesk\Ticket\Ticket_Thread();
        $thread = $threads->where('ticket_id', $ticketid)->max('id');
        return $thread;
    }

    public function getChannel($ticketid) {
        $channels = new \App\Plugins\Social\Model\SocialChannel();
        $channel = $channels->where('ticket_id', $ticketid)->first();
        //dd($channel);
        return $channel;
    }

}
