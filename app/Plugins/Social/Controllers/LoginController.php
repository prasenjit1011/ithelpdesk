<?php

namespace App\Plugins\Social\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use App\Plugins\Social\Facebook\Facebook;
use Input;
use Illuminate\Http\Request;
use Schema;
use DB;
use App\Plugins\Social\Model\SocialLogin;
use Auth;

class LoginController extends Controller {

    public function __construct() {
        $this->createSocialTable();
    }

    public function showIcons() {
        try {
            //dd('dshcdhs');
            $social = new SocialLogin();
            $facebook = $social->find(1);
            //show facebook
            if ($facebook->facebook_isactive == 1) {

                $loginUrl = $this->facebookIcon();
                echo "<p><a href=" . $loginUrl ." class='btn btn-facebook'><i class='fa fa-facebook'></i> book</a></p>";
            }
        } catch (Exception $ex) {
            //dd($ex);
        }
    }

    public function settings() {
        try {
            
            $social = new SocialLogin();
            $social = $social->find(1);
            $path = app_path() . '/Plugins/Social/views';
            \View::addNamespace('plugins', $path);
            return view('plugins::settings', compact('social'));
        } catch (Exception $ex) {
            //dd($ex);
        }
    }

    public function postSettings(Request $request, SocialLogin $social) {
        $this->validate($request, [
            'facebook_app_id' => 'required_if:facebook_isactive,1',
            'facebook_app_secrete' => 'required_if:facebook_isactive,1',
            'facebook_default_graph_version' => 'required_if:facebook_isactive,1',
            'facebook_callback_url' => 'required_if:facebook_isactive,1|url',
                ], [
            'facebook_app_id.required_if' => ':attribute is required',
            'facebook_app_secrete.required_if' => ':attribute is required',
            'facebook_default_graph_version.required_if' => ':attribute is required',
            'facebook_callback_url.required_if' => ':attribute is required',
        ]);

        try {
            $social = $social->find(1);
            $social->fill($request->input())->save();
            return redirect()->back()->with('success', 'Settings has updated successfully');
        } catch (Exception $ex) {
            // dd($ex);
        }
    }

    public function facebookIcon() {
        $social = new SocialLogin();
        $facebook = $social->find(1);
        $app_id = $facebook->facebook_app_id;
        $app_secrete = $facebook->facebook_app_secrete;
        $default_graph_version = $facebook->facebook_default_graph_version;
        $fb = new Facebook(['app_id' => $app_id
            , 'app_secret' => $app_secrete
            , 'default_graph_version' => $default_graph_version
        ]);
        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email', 'user_likes']; // optional
        $loginUrl = $helper->getLoginUrl($facebook->facebook_callback_url . 'facebook/login-callback', $permissions);
        return $loginUrl;
    }

    public function facebookCallback(Request $request) {
        try {
            $social = new SocialLogin();
            $facebook = $social->find(1);
            $app_id = $facebook->facebook_app_id;
            $app_secrete = $facebook->facebook_app_secrete;
            $default_graph_version = $facebook->facebook_default_graph_version;
            $fb = new Facebook(['app_id' => $app_id
                , 'app_secret' => $app_secrete
                , 'default_graph_version' => $default_graph_version
            ]);
            $state = $request->get('state');
            \Session::put('state', $state);
            $helper = $fb->getRedirectLoginHelper();
            $accessToken = $helper->getAccessToken();
            $response = $fb->get('/me?fields= id,email,first_name,gender,last_name,name', $accessToken);
            $userNode = $response->getGraphUser();
            $user = $this->createUser($userNode['email'],$userNode['first_name'],$userNode['last_name']);
           
            Auth::login($user);
            return redirect('/');
            
        } catch (Exception $ex) {
            dd($ex);
        }
    }

    public function createUser($email, $firstname, $lastname) {
        try {
            $user = new \App\User();
            $username = $email;
            $company = $this->company();
            $isexist = $user->where('email', $email)->first();
            if (!$isexist) {
                
                $str = self::getToken(10);
                $user->user_name = $email;
                $user->first_name = $firstname;
                $user->last_name = $lastname;
                $user->email = $email;
                $user->password = \Hash::make($str);
                $user->role = 'user';
                if ($user->save()) {
                    $PhpMailController = new \App\Http\Controllers\Common\PhpMailController();
                    $PhpMailController->sendmail($from = $PhpMailController->mailfrom('1', '0'), $to = ['name' => $username, 'email' => $email], $message = ['subject' => 'Welcome to ' . $company . ' helpdesk', 'scenario' => 'registration-notification'], $template_variables = ['user' => $username, 'email_address' => $email, 'user_password' => $str]);
                }
            } else {
                
                $user = $isexist;
            }
            
            return $user;
            
        } catch (Exception $ex) {
            dd($ex);
        }
    }

    public static function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 1)
            return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    public static function getToken($length) {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[self::crypto_rand_secure(0, $max)];
        }
        return $token;
    }

    /**
     * company.
     *
     * @return type
     */
    public function company() {
        $company = \App\Model\helpdesk\Settings\Company::Where('id', '=', '1')->first();
        if ($company->company_name == null) {
            $company = 'Support Center';
        } else {
            $company = $company->company_name;
        }

        return $company;
    }

    public function createSocialTable() {
        if (!Schema::hasTable('social-login')) {
            Schema::create('social-login', function($table) {
                $table->increments('id');
                $table->string('facebook_app_id');
                $table->string('facebook_app_secrete');
                $table->string('facebook_default_graph_version');
                $table->string('facebook_callback_url');
                $table->integer('facebook_isactive');
                $table->timestamps();
            });
            $this->seedFacebook();
        }
    }

    public function seedFacebook() {
        if (Schema::hasTable('social-login')) {
            DB::table('social-login')->insert(['id' => 1]);
        }
    }

}
