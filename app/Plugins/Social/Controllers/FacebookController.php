<?php

namespace App\Plugins\Social\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\helpdesk\Settings\SocialMedia;
use Exception;
use Session;
use App\Plugins\Social\Facebook\Exceptions\FacebookResponseException;
use App\Plugins\Social\Facebook\FacebookRequest;
use App\Plugins\Social\Facebook\Facebook;
use App\Plugins\Social\Facebook\FacebookApp;
use App\Http\Controllers\Admin\helpdesk\SocialMedia\SocialMediaController;

class FacebookController extends Controller {

    public function __construct() {
        $this->middleware(['auth', 'roles']);
    }

    /**
     * Get credential of facebook
     * @return array
     */
    public function getCredentials() {
        $social = new SocialMedia();
        $socials = $social->where('provider', 'facebook')->pluck('value', 'key')->toArray();
        if (!$socials) {
            $socials = $this->credentials();
        }
        return $socials;
    }

    /**
     * get the intance of facebook object
     * @return \App\Plugins\Social\Facebook\Facebook
     * @throws Exception
     */
    public function facebook($default_access_token = "") {
        $credentials = $this->getCredentials();
        if (count($credentials) > 0) {
            $app_id = $credentials['client_id'];
            $app_secret = $credentials['client_secret'];
            $facebook = new Facebook([
                'app_id' => $app_id,
                'app_secret' => $app_secret,
                'default_graph_version' => 'v2.6',
                "default_access_token" => $default_access_token,
            ]);
            return $facebook;
        }
        //throw new Exception('Undefined facebook credentials');
    }

    public function facebookApp() {
        $credentials = $this->getCredentials();
        if (count($credentials) > 0) {
            $app_id = $credentials['client_id'];
            $app_secret = $credentials['client_secret'];
            $facebook = new FacebookApp(
                    $app_id, $app_secret
            );
            return $facebook;
        }
        throw new Exception('Undefined facebook credentials');
    }

    /**
     * get facebook login helper function
     * @return type
     */
    public function getLoginHelper() {
        $facebook = $this->facebook();
        $helper = $facebook->getRedirectLoginHelper();
        return $helper;
    }

    /**
     * get the access code
     * @return type
     */
    public function getFacebookAccessToken() {
        $helper = $this->getLoginHelper();
        $accessToken = $helper->getAccessToken();
        return $accessToken;
    }

    /**
     * get all pages
     */
    public function getFacebookPages() {
        try {
            $access_token = $this->getFacebookAccessToken();
            if ($access_token) {
                $access_token = $this->longLivedToken($access_token);
            }
            $facebook = $this->facebook();
            $token = $this->getTokenFromSession();
            //dd($token);
            $page = $facebook->get('/me/accounts', $token);
            $pages = $page->getGraphEdge()->asArray();
            $long_lived_pages = [];
            foreach($pages as $page){
                $page_access = $page['access_token'];
                $page_id = $page['id'];
                $name = $page['name'];
                $long_lived_pages[] = $this->longLivedPageToken($page_access,$page_id,$name);
            }
            $this->putPageAccessSession(json_encode($long_lived_pages));
            return $pages;
        } catch (FacebookResponseException $e) {
            if ($e->getCode() == 190) {
                Session::forget('facebook_access_token');
                $loginUrl = $this->accessUrl();
                echo "<script>window.top.location.href='" . $loginUrl . "'</script>";
            }
        }
    }

    public function getMessagesFromPage() {
        try {
            $pages = $this->getFacebookPagesFromDb();
            foreach ($pages as $name=>$page) {
                $pageid = $page['id'];
                $page_token = $page['token'];
                $message[$name] = array_collapse($this->getmessageByPageid($pageid, $page_token));
            }
            return $message;
        } catch (Exception $ex) {
            $this->requestLimit($ex);
        }
    }

    public function getFacebookPagesFromDb() {
        $media = new SocialMedia();
        $page = $media->getvalueByKey('facebook', 'pages');
        $pages = json_decode($page,true);
        return array_collapse($pages);
    }

    public function getmessageByPageid($pageid, $page_access_token) {
        try {
            $fb = $this->facebook($page_access_token);
            $response = $fb->get('/' . $pageid . '/conversations', $page_access_token);
            $data = $response->getDecodedBody()['data'];
            $conversation = $this->readConversation($data,$page_access_token);
            return $conversation;
        } catch (Exception $ex) {
            $this->requestLimit($ex);
        }
    }

    public function readConversation($data,$page_access_token) {
        $values = [];
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $con_id = $value['id'];
                $values[$key] = $this->conversation($con_id,$page_access_token);
            }
        }
        return $values;
    }

    public function conversation($id,$page_access_token) {
        try {
            //$page_token = $this->getPageTokenFromSession();
            $fb = $this->facebook($page_access_token);
            $response = $fb->get('/' . $id . '/messages', $page_access_token);
            $data = $response->getDecodedBody()['data'];
            return $this->getMessages($data, $id,$page_access_token);
        } catch (Exception $ex) {
            $this->requestLimit($ex);
        }
    }

    public function getMessages($data, $con_id,$page_access_token) {
        $values = [];
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $msg_id = $value['id'];
                $values[$key] = $this->messages($msg_id, $con_id,$page_access_token);
            }
        }
        return $values;
    }

    public function messages($id, $con_id,$page_access_token) {
        try {
            $fb = $this->facebook($page_access_token);
            $response = $fb->get('/' . $id . "?fields=from,message,subject,created_time,id", $page_access_token);
            $data = $response->getDecodedBody();
            $field['channel'] = 'facebook';
            $field['name'] = $data['from']['name'];
            $field['user_id'] = $data['from']['id'];
            $field['username'] = $data['from']['email'];
            $field['email'] = $data['from']['email'];
            $field['message'] = $data['message'];
            $field['posted_at'] = $data['created_time'];
            $field['message_id'] = $data['id'];
            $field['con_id'] = $con_id;
            return $field;
        } catch (Exception $ex) {
            $this->requestLimit($ex);
        }
    }

    public function accessUrl() {
        $url = url('/facebook/access');
        $permissions = [
            //'read_mailbox',
            'user_posts',
            'manage_pages',
            'read_page_mailboxes',
            'publish_pages',
            'pages_show_list',
            'pages_manage_cta',
            'pages_manage_instant_articles',
            'pages_messaging',
            'user_posts',
        ];
        $helper = $this->getLoginHelper();
        $loginUrl = $helper->getLoginUrl($url, $permissions);
        return $loginUrl;
    }

    /**
     * facebook access link
     */
    public function getFacebookAccess() {
        if (!$this->getTokenFromSession()) {
            $loginUrl = $this->accessUrl();
            echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
        } else {
            $pages = $this->getFacebookPages();
            foreach ($pages as $page) {
                echo "<a href=" . url('facebook/page?page=' . $page['id']) . ">" . $page['name'] . "</a>";
            }
        }
    }

    public function getPage(Request $request) {
        try {
            $page_id = $request->input('page');
            //dd($page_id);
            $conversation = $this->getMessagesFromPage($page_id);
            dd($conversation);
        } catch (Exception $ex) {
            $this->requestLimit($ex);
        }
    }

    /**
     * get access code
     * @param Request $request
     */
    public function getAccessCode(Request $request) {
        $state = $request->input('state');
        Session::put('state', $state);
        $access_token = $this->getFacebookAccessToken();
        if ($access_token) {
            $access_token = $this->longLivedToken($access_token);
        }
        return redirect('social/settings')->with('success', 'facebook has authenticated');
    }

    /**
     * set the session for access token
     * @param type $token
     */
    public function putAccessSession($token) {
//        if (!Session::has('access_token')) {
//            Session::put('access_token', $token);
//        }
        $social = new SocialMediaController();
        $social->insertProvider('facebook', ['access_token' => $token]);
    }

    public function putPageAccessSession($long_lived_pages) {
        $social = new SocialMediaController();
        $social->insertProvider('facebook', ['pages' => $long_lived_pages]);
    }

    /**
     * make the token long lived
     * @param type $token
     */
    public function longLivedToken($token) {
        $facebook = $this->facebook();
        $oAuth2Client = $facebook->getOAuth2Client();
        $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($token);
        $this->putAccessSession($longLivedAccessToken);
        $session = $this->getTokenFromSession();
        $facebook->setDefaultAccessToken($session);
    }

    public function longLivedPageToken($token,$page_id,$name='') {
        $facebook = $this->facebook();
        $oAuth2Client = $facebook->getOAuth2Client();
        $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($token);
        return [$name=>['id'=>$page_id,'token'=>$longLivedAccessToken->getValue()]];
    }

    /**
     * get access token from session
     * @return type
     */
    public function getTokenFromSession() {
//        if (Session::has('facebook_access_token')) {
//            return Session::get('facebook_access_token');
//        }
        $media = new SocialMedia();
        $token = $media->getvalueByKey('facebook', 'access_token');
        return $token;
    }

    public function getPageAccessToken($pageid) {
        $fb = $this->facebook();
        $token = $this->getTokenFromSession();
        $req = '/' . $pageid . '?fields=access_token';
        $response = $fb->get($req, $token);
        $token = $response->getAccessToken();
        return $token;
    }

    public function getPageTokenFromSession($page) {
        $media = new SocialMedia();
        $pages = $media->getvalueByKey('facebook', 'pages');
        $page_array = json_decode($pages,true);
        $page_array_collape = array_collapse($page_array);
        $token = $page_array_collape[$page]['token'];
        return $token;
    }

    public function pageSave(Request $request) {
        $pages = $request->input('page');
        if (!$request->input('page')) {
            $pages = [];
        }
        $page = json_encode([$pages]);
        $social_controller = new SocialMediaController();
        $social_controller->insertProvider('facebook', ['pages' => $page]);
        return redirect()->back()->with('success', 'Your page(s) has saved');
    }

    public function getPagesFromDb() {
        $media = new SocialMedia();
        $page = $media->getvalueByKey('facebook', 'pages');
        $pages = json_decode($page,true);
        return $pages;
    }

    public function fetchMessages() {
        try {
            $pages = $this->getPagesFromDb();
            foreach ($pages as $page) {
                $pages = explode(":", $page);
                $id = $pages[1];
                $this->getMessagesFromPage($id);
            }
        } catch (Exception $ex) {
            $this->requestLimit($ex);
        }
    }

    public function getPageName() {
        $pages = $this->getPagesFromDb();
        for ($i = 0; $i < count($pages); $i + 2) {
            $names[] = $pages[$i];
        }
        return $names;
    }

    public function replyPageMessage($data) {
        $ticketid = $data['ticket_id'];
        $channel = $this->getChannel($ticketid);
        if ($channel && $channel->channel == 'facebook' && $channel->via) {
            $string = $data['body'];
            $content = str_replace("&nbsp;", "", $string);
            $channel = $this->getChannel($ticketid);
            $msgid = $channel->con_id;
            $body = strip_tags($content);
            $user_id = $channel->user_id;
            $username = $channel->username;
            $this->sendPageMessage($msgid, $body,$ticketid,$user_id,$username,$channel->via);
        }
    }

    public function getChannel($ticketid) {
        $channels = new \App\Plugins\Social\Model\SocialChannel();
        $channel = $channels->where('ticket_id', $ticketid)->first();
        return $channel;
    }

    public function sendPageMessage($msgid, $body,$ticketid,$user_id,$username,$page) {
        $page_token = $this->getPageTokenFromSession($page);
        $fb = $this->facebook($page_token);
        $send = $fb->post('/' . $msgid . '/messages', ['message' => $body], $page_token);
        $m_mid = $send->getDecodedBody()['id'];
        $this->saveChannel($msgid,$m_mid,$ticketid,$user_id,$username,$page);
        
    }
    
    public function saveChannel($t_mid,$m_mid,$ticketid,$userid,$username,$page){
        \App\Plugins\Social\Model\SocialChannel::create([
            'channel'=>'facebook',
            'via'=>$page,
            'con_id'=>$t_mid,
            'message_id'=>$m_mid,
            "ticket_id"=>$ticketid,
            "user_id"=>$userid,
            'username'=>$username,
        ]);
    }

    public function credentials() {
        return [
            'client_id' => '1234',
            'client_secret' => '1234',
            'redirect' => '',
        ];
    }

    public function requestLimit($e) { 
        if ($e->getCode() == 32) {
            $this->getAccessToken();
            return redirect()->back();
        }
    }

    public function getAccessToken() {
        $current_access_token = $this->getTokenFromSession();
        $fb = $this->facebook($current_access_token);
        $page = $fb->get('/me/accounts');
        $pages = $page->getGraphEdge()->asArray();
        $page_access = $pages[0]['access_token'];
        $this->longLivedPageToken($page_access);
    }

}
