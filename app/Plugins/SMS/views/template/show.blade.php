@extends('themes.default1.admin.layout.admin')

@section('Plugins')
active
@stop

@section('settings-bar')
active
@stop

@section('plugin')
class="active"
@stop

@section('HeadInclude')
@stop
<!-- header -->
@section('PageHeader')
<h1>{!! Lang::get('lang.plugins') !!}</h1>
@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">
</ol>
@stop
<!-- /breadcrumbs -->
<!-- content -->
@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">{{Lang::get('SMS::lang.sms-templates')}}</h3>
        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#createtemp"><i class="fa fa-plus">&nbsp;&nbsp;</i>{{Lang::get('SMS::lang.create_template')}}</button> 
    </div>
    <!-- /.box-header -->
    <div class="box-body ">
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check-circle"> </i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('success')}}
        </div>
        @endif
        <!-- failure message -->
        @if(Session::has('fails'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"> </i> <b> {!! Lang::get('lang.alert') !!}! </b>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('fails')}}
        </div>
        @endif
        {!! Form::open(['id'=>'modalpopup', 'route'=>'template-delete','method'=>'post']) !!}
        <!--<div class="mailbox-controls">-->
            <!-- Check all button -->
            <a class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></a>
            
            <input type="submit" class="submit btn btn-default text-orange btn-sm" id="delete" name="submit" value="{!! Lang::get('lang.delete') !!}">
        <!--</div>-->
        <p><p/>
        <div class="mailbox-messages" id="refresh">
            <!--datatable-->
            {!! Datatable::table()
            ->addColumn(
            "",
            Lang::get('SMS::lang.name'),
            Lang::get('SMS::lang.status'),
            Lang::get('lang.template_language'),
            Lang::get('SMS::lang.action'))
            ->setUrl(route('get-sms-template'))
            ->render();
            !!}
            <!-- /.datatable -->
        </div><!-- /.mail-box-messages -->
        {!! Form::close() !!}
    </div><!-- /.box-body -->

</div>
<!-- /.box -->
<!--modal-->
<div class="modal fade" id="createtemp">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['route'=>'template-set-createnew']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{Lang::get('SMS::lang.create_template')}}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">

                    {!! Form::label('folder_name', Lang::get('SMS::lang.template-set-name'),['style'=>'display: block']) !!}

                    {!! Form::text('folder_name',null,['class'=>'form-control', 'placeholder' => Lang::get('SMS::lang.template-set-name-placeholder')])!!}

                    {!! $errors->first('folder', '<spam class="help-block">:message</spam>') !!}
                    
                    <div class="form-group {{ $errors->has('template_language') ? 'has-error' : '' }}">
                    <label for="title">{!! Lang::get('lang.select_template_language') !!}:<span style="color:red;">*</span></label><br>
                    {!! Form::select('template_language',$languages, 'en' ,['class'=>'form-control'])!!}
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    {!! Form::submit(Lang::get('SMS::lang.create'), ['class'=>'btn btn-primary'])!!}
                                                                            
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div> 
    </div>
</div>
<!--.modal-->
<!-- Modal -->   
<div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none; padding-right: 15px;background-color: rgba(0, 0, 0, 0.7);">
    <div class="modal-dialog" role="document">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close closemodal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body" id="custom-alert-body" >
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left yes" data-dismiss="modal">{{Lang::get('lang.ok')}}</button>
                    <button type="button" class="btn btn-default no">{{Lang::get('lang.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('FooterInclude')
<script type="text/javascript">
    var t_id = [];
    var click = 1;
    $(function(){
        $('.checkbox-toggle').on('click', function(){
            if (click%2 != 0) {
                $(".mailbox-messages input[type='checkbox']").iCheck("check");
                $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
                t_id = $('.selectval').map(function() {
                    return $(this).val();
                }).get();
                click = click+1;
            } else {
                $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
                t_id = [];
                click = click-1;
            }  
        });
    });
    function someFunction(id) {
        if (document.getElementById(id).checked) {
            t_id.push(id);
        } else if(document.getElementById(id).checked === undefined){
            var index = t_id.indexOf(id);
            if (index === -1){
                t_id.push(id);
            } else{
                t_id.splice(index, 1);
            }
        } else {
            var index = t_id.indexOf(id);
            t_id.splice(index, 1);
        }
    }
    $("#modalpopup").on('submit', function(e) {
            e.preventDefault();
            var msg = "{{Lang::get('lang.confirm')}}";
            var values = getValues();
            if (values == "") {
                msg = "{{Lang::get('SMS::lang.select-template')}}";
                $('.yes').html("{{Lang::get('lang.ok')}}");
                $('#myModalLabel').html("{{Lang::get('lang.alert')}}");
            } else {
                $('#myModalLabel').html("{{Lang::get('lang.alert')}}");
                $('.yes').html("Yes");
            }
            $('#custom-alert-body').html(msg);
            $("#myModal").css("display", "block");
    });
    function getValues() {
            var values = $('.selectval:checked').map(function() {
                return $(this).val();
            }).get();
            return values;
    }
        $(".closemodal, .no").click(function() {
            $("#myModal").css("display", "none");
        });

        $(".closemodal, .no").click(function() {
            $("#myModal").css("display", "none");
        });

        $('.yes').click(function() {
            var values = getValues();
            if (values == "") {
                $("#myModal").css("display", "none");
            } else {
                $("#myModal").css("display", "none");
                $('#modalpopup').unbind('submit');
                $('#delete').click();
            }
        });
</script>
@stop

<!-- /content -->
