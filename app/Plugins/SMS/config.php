<?php

return [
    'name' => 'SMS',
    'description' => 'Faveo SMS plugin adds mobile text messaging functionality in Faveo system using bulk SMS service providers like MSG91, SMSLive247 etc.',
    'author' => 'Ladybird', //manish.verma@ithelpdesk.com
    'website' => 'www.ithelpdesk.com',
    'version' => '2.0.0',
    'settings' => 'sms/settings',
];
