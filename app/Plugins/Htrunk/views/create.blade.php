 <div class="row">
            <div class="col-xs-4 form-group {{ $errors->has('client_Code') ? 'has-error' : '' }}">
                {!! Form::label('client_Code',Lang::get('Htrunk::lang.client_Code')) !!}

                {!! Form::text('client_Code',null,['class' => 'form-control']) !!}
        <!--         <input class='form-control col-lg-5 itemSearch' name="itemSearch" type='text' placeholder='select item' /> -->
            </div>
            <!-- phone : Text : -->
            <div class="col-xs-4 form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                {!! Form::label('phone1',Lang::get('Htrunk::lang.phone1')) !!}


                <input type="text" name="phone1" class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57" >
                <!-- {!! Form::text('phone',null,['class' => 'form-control']) !!} -->
            </div>
            <!-- website : Text :  -->
            <div class="col-xs-4 form-group {{ $errors->has('Line of business') ? 'has-error' : '' }}">
                {!! Form::label('line_of_business',Lang::get('Htrunk::lang.line_of_business')) !!}




                <select name="line_of_business" class="form-control">
                    <option value="ABCD">ABCD</option>
                    <option value="XYZ">XYZ</option>

                </select>



            </div>
        </div>

        <div class="row">
            <div class="col-xs-4 form-group {{ $errors->has('relation_type') ? 'has-error' : '' }}">
                {!! Form::label('relation_type',Lang::get('Htrunk::lang.relation_type')) !!} 
                <select name="relation_type" class="form-control">
                    <option value="partner">{{Lang::get('Htrunk::lang.partner')}}</option>
                    <option value="direct_client">{{Lang::get('Htrunk::lang.direct_client')}}</option>

                </select>
            </div>

            <div class="col-xs-4 form-group {{ $errors->has('branch') ? 'has-error' : '' }}">
                {!! Form::label('branch',Lang::get('Htrunk::lang.branch')) !!}
                {!! Form::text('branch',null,['class' => 'form-control']) !!}
            </div>


            <div class="col-xs-4 form-group {{ $errors->has('fax') ? 'has-error' : '' }}">
                {!! Form::label('fax',Lang::get('Htrunk::lang.fax')) !!}

                <input type="text" name="fax" class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57" >
                <!-- {!! Form::input('number','fax',null,['class' => 'form-control']) !!} -->
                <!-- {!! Form::text('fax',null,['class' => 'form-control']) !!} -->

            </div>


        </div>