<?php
return [
     'client_Code'=>'Client Code ',
     'line_of_business'=>'Line of Business',
     'relation_type'=>'Relation type',
     'fax'=>'Fax',
     'sla_type'=>'SLA Type',
     'branch'=>'Branch',
     'type_sla'=>'Type sla',
      'partner'=>'Partner',
      'direct_client'=>'Direct Client',
      'new_customform'=>'New customform',
      'customform'=>'Customform',
      'add_customform'=>'Add Customform',
      'this_sla_not_exists'=>'This sla not exists',
      'phone1'=>'Phone1',
      'applications'=>'Applications',
      'your_status_updated'=>'Your Status Updated',
];
