<?php

namespace App\Plugins\Htrunk;

class ServiceProvider extends \App\Plugins\ServiceProvider {

    public function register() {
        parent::register('Htrunk');
    }

    public function boot() {
        /** 
         *View
         */
        $view_path = app_path().DIRECTORY_SEPARATOR.'Plugins'.DIRECTORY_SEPARATOR.'Htrunk'.DIRECTORY_SEPARATOR.'views';
        $this->loadViewsFrom($view_path, 'htrunk');
        parent::boot('Htrunk');
        
        // if (class_exists('Breadcrumbs')) {
        //     require __DIR__ . '/breadcrumbs.php';
        // }
        
        /**
         *language
         */
        $trans = app_path() . DIRECTORY_SEPARATOR . 'Plugins' . DIRECTORY_SEPARATOR . 'Htrunk' . DIRECTORY_SEPARATOR . 'lang';
        $this->loadTranslationsFrom($trans, 'Htrunk');
    }
    

}
