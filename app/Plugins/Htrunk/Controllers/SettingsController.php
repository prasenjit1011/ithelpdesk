<?php

namespace App\Plugins\Htrunk\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Plugins\Reseller\Model\Reseller;
use Illuminate\Database\Schema\Blueprint;
use Schema;
use Lang;


use App\Model\helpdesk\Settings\Plugin;

class SettingsController extends Controller {

    public function index() {


        $htrunk_status=Plugin::where('name','=','Htrunk')->select('status')->first();
        return view('htrunk::settings',compact('htrunk_status'));
    }

    public function update(Request $request) {
        
        try {

            $change_status=$request->settings_htrunk;
            $htrunk_status_update=Plugin::where('name','=','Htrunk')->update(['status' => $change_status]);
           return Lang::get('Htrunk::lang.your_status_updated');
            // return $this->successResponse('Updated Successfully');
        } catch (\Exception $ex) {

            return $this->failsResponse($ex->getMessage());
        }
    }

   

}
