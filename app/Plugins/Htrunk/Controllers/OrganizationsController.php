<?php

namespace App\Plugins\Htrunk\Controllers;

// use App\Http\Requests;
// use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;
// use App\User;
// use App\Plugins\Reseller\Model\Reseller;
// use App\Plugins\Reseller\Controllers\ResellerController;
// use Auth;
// //use Form;
// use Hash;
// use App\Model\plugin\reseller\Country;
// use Illuminate\Database\Schema\Blueprint;
// use Schema;
// use Exception;

// controllers
use App\Http\Controllers\Controller;
// requests
use App\Http\Requests\helpdesk\OrganizationRequest;
/* include organization model */
use App\Http\Requests\helpdesk\OrganizationUpdate;
// models
/* Define OrganizationRequest to validate the create form */
use App\Model\helpdesk\Agent_panel\Organization;
use App\Model\helpdesk\Agent_panel\User_org;
use App\Model\helpdesk\Agent_panel\OrgAttachment;
use App\Model\helpdesk\Manage\Sla\Sla_plan;
use App\Model\helpdesk\Settings\Plugin;
/* Define OrganizationUpdate to validate the create form */
use App\User;
// classes
use Exception;
use Lang;
use Illuminate\Http\Request;
use Datatables;
use Input;
use DB;
use File;
use Auth;

class OrganizationsController extends Controller {

  
    /**
     * Show the form for creating a new organization.
     *
     * @return type Response
     */
    public function create() {

try {


$status_check=Plugin::where('name','=','Htrunk')->select('status')->first();

if($status_check->status==1){
 echo view('htrunk::create')->render(); 
}
   
    // return \view('Htrunk::create');
   // return view('themes.default1.agent.helpdesk.organization.create');
  // return view('themes.default1.agent.helpdesk.organization.customformorgs');
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

  


    /**
     * Show the form for editing the specified organization.
     *
     * @param type              $id
     * @param type Organization $org
     *
     * @return type view
     */
    public function edit($orgs) {
        try {

            

$status_check=Plugin::where('name','=','Htrunk')->select('status')->first();

            if($status_check->status==1){
 echo view('htrunk::edit', compact('orgs'))->render(); 
}
         
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    public function orgDocsAttachment(Request $request) {

        $org_id = $request->orgs_id;


        for ($j = 0; $j <= 100; $j++) {
            // dd(Input::file('org_docs'.$j));
            if (Input::file('org_docs' . $j)) {

                $name = Input::file('org_docs' . $j)->getClientOriginalName();
                $destinationPath = 'uploads/organizations/';
                $fileName = rand(0000, 9999) . '.' . $name;
                Input::file('org_docs' . $j)->move($destinationPath, $fileName);
                $attachment = new OrgAttachment();
                $attachment->org_id = $request->orgs_id;
                $attachment->file_name = $fileName;
                $attachment->save();
            }
        }


        return redirect()->back()->with('orgdocssuccess', Lang::get('lang.organization_docs_update_successfully'));
    }

    

    

    public function getOrgAjax(Request $request) {
        $org = new Organization();
        $q = $request->input('term');
        $orgs = $org->where('name', 'LIKE', '%' . $q . '%')
                ->select('name as label', 'id as value')
                ->get()
                ->toJson();
        return $orgs;
    }

   
}
