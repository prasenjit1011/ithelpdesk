<?php

return [
    'name' => 'Chat',
    'description' => 'Faveo Chat is a plugin to provide any Chat integration',
    'author' => 'ladybird',
    'website' => 'www.ithelpdesk.com',
    'version' => '1.0.0',
    'settings' => 'chat/settings',
    
];

