<?php
/**
 * Auth route
 */
Route::group(['middleware' => ['web', 'auth', 'roles'], 'prefix' => 'chat'], function() {
    Route::get('/settings',['as'=>'chat.settings','uses'=>'App\Plugins\Chat\Controllers\Core\SettingsController@settings']);
    Route::post('/activate/{app}',['as'=>'chat.settings.post','uses'=>'App\Plugins\Chat\Controllers\Core\SettingsController@activateIntegration']);
    Route::get('/ajax-url',['as'=>'chat.settings','uses'=>'App\Plugins\Chat\Controllers\Core\SettingsController@ajax']);

});

/**
 * Non auth routes
 */
Route::group(['middleware' => ['web'], 'prefix' => 'chat'], function() {
    Route::post('data/{app?}/{modelid?}/{model?}',['as'=>'chat.post','uses'=>'App\Plugins\Chat\Controllers\Core\ChatController@chat']);
    Route::get('test',['as'=>'chat.test','uses'=>'App\Plugins\Chat\Controllers\Core\SettingsController@sendTest']);
});

