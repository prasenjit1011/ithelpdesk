<?php
Breadcrumbs::register('chat.post', function($breadcrumbs)
{
    $breadcrumbs->parent('setting');
    $breadcrumbs->push('Chat', route('chat.post'));
});
Breadcrumbs::register('chat.settings', function($breadcrumbs)
{
    $breadcrumbs->parent('setting');
    $breadcrumbs->push('Settings', route('chat.settings'));
});

