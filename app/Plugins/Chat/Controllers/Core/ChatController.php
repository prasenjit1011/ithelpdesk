<?php

namespace App\Plugins\Chat\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Plugins\Chat\Model\Chat;
use Input;

class ChatController extends Controller {

    public function chat($app = "", $modelid = "", $model = "") {
        loging('chat', json_encode(Input::all()), 'info');
        $request = Input::all();
        if (array_key_exists('app', $request)) {
            $app = $request['app'];
        }

        /**
         * check app is active
         */
        if ($this->checkApp($app) == true) {
            $controller = $this->chooseApp($app, $request);
            $response = $this->createTicket($controller, $model, $modelid);
            loging('ticket created', $response . " ticket has created", 'info');
        }
        return response("success", 200);
    }

    /**
     * Get the dummy response
     * @return array
     */
    public function getRequest($json = '') {
        $json = '{"app":"happy_fox_chat","message":"Chat conversation by vijay (vijaysebastian111@gmail.com) on Aug 23rd 2016\n\n\nvijay : dscghsd - 03:21 PM\nvijay (Agent) : dcbdsh - 03:22 PM\n\n\nStats:\nUrl: http:\/\/localhost\/FaveoVersions\/faveo-helpdesk\/public\/\nWaiting Time: 3 seconds\nChat Duration: 3 seconds\nWebsite Profile: Lady\nOperating System: Mac OS X 10.11\nBrowser: Chrome 52.0\n","email":"vijaysebastian111@gmail.com","name":"vijay"}';
        $array = json_decode($json, true);
        return $array;
    }

    /**
     * check the app
     * @param string $app
     * @return boolean
     */
    public function checkApp($app) {
        $chat = new Chat();
        $check = $chat->status($app);
        if ($check == false) {
            loging('chat', "$app is inactive", 'info');
        }
        return $check;
    }

    /**
     * choose the application from zapier
     * @param string $app
     * @param array $request
     * @return \App\Plugins\Zapier\Controllers\HappyFoxChat\ProcessController
     */
    public function chooseApp($app, $request) {
        switch ($app) {
            case 'liv_serv' :
                $controller = new \App\Plugins\Chat\Controllers\LivServ\ProcessController($request);
                return $controller;
        }
    }

    /**
     * get the \App\Http\Controllers\Agent\helpdesk\TicketController instance
     * @return \App\Http\Controllers\Agent\helpdesk\TicketController
     */
    public function ticketController() {
        $PhpMailController = new \App\Http\Controllers\Common\PhpMailController();
        $NotificationController = new \App\Http\Controllers\Common\NotificationController();
        $ticket_controller = new \App\Http\Controllers\Agent\helpdesk\TicketController($PhpMailController, $NotificationController);
        return $ticket_controller;
    }

    public function getHelptopicModel($model, $modelid) {
        if ($modelid && $model == 'helptopic') {
            $model = 'help_topic';
            $schema = \DB::table($model)->where('id', $modelid)->select('id')->first();
            if ($schema) {
                $id  = $schema->id;
            }
        }else{
            $ticket = $this->ticketController();
            $id = $ticket->getSystemDefaultHelpTopic();
        }
        return $id;
    }
    public function getDepartmentModel($model, $modelid) {
        if ($modelid && $model == 'department') {
            $model = 'department';
            $schema = \DB::table($model)->where('id', $modelid)->select('id')->first();
            if ($schema) {
                $id = $schema->id;
            }
        }else{
            $ticket = $this->ticketController();
            $id = $ticket->getSystemDefaultDepartment();
        }
        return $id;
    }

    /**
     * create ticket in system
     * @param object $controller
     */
    public function createTicket($controller, $model, $modelid) {
        $ticket = $this->ticketController();
        $emailadd = $controller->email();
        $username = $controller->email();
        $subject = $controller->subject();
        $body = $controller->message();
        $phone = $controller->phone();
        $phonecode = $controller->phoneCode();
        $mobile_number = $controller->mobile();
        $helptopic = $this->getHelptopicModel($model, $modelid);
        //$sla = $ticket->getSystemDefaultSla();
        $priority = $ticket->getSystemDefaultPriority();
        $source = $ticket->getSourceByname('chat')->id;
        $dept = $this->getDepartmentModel($model, $modelid);
        $sla = "";
        $priority = "";
//        $sla = sla("", "", $dept, $source);
//        if($sla){
//            $priority = priority($sla);
//        }
        //dd($emailadd, $username, $subject, $body, $phone, $phonecode, $mobile_number, $helptopic, $sla, $priority, $source, $headers = [], $dept, $assignto = NULL, $from_data = [], $auto_response = '');
        $result = $ticket->create_user($emailadd, $username, $subject, $body, $phone, $phonecode, $mobile_number, $helptopic, $sla, $priority, $source, $headers = [], $dept, $assignto = NULL, $from_data = [], $auto_response = '', $status = '');
        return json_encode($result);
    }

    /**
     * get the ticket id from the ticket create result
     * @param array $result
     * @return integer
     */
    public function lastTicket($result) {
        $ticket = $this->findTicketFromTicketCreateUser($result);
        if ($ticket) {
            $ticket_id = $ticket->id;
            return $ticket_id;
        }
    }

    /**
     * get the user id from ticket create result
     * @param array $result
     * @return integer
     */
    public function findUserFromTicketCreateUserId($result = []) {
        $ticket = $this->findTicketFromTicketCreateUser($result);
        if ($ticket) {
            $userid = $ticket->user_id;
            return $userid;
        }
    }

    /**
     * get user name from ticket create result
     * @param array $result
     * @return string
     */
    public function findUserFromTicketCreateUsername($result = []) {
        $ticket = $this->findTicketFromTicketCreateUser($result);
        if ($ticket) {
            $userid = $ticket->user_id;
            $user = \App\User::find($userid);
            if ($user) {
                return $user->user_name;
            }
        }
    }

    /**
     * get the ticket object
     * @param array $result
     * @return \Illuminate\Database\Eloquent\Model | NULL
     */
    public function findTicketFromTicketCreateUser($result = []) {
        $ticket_number = $this->checkArray('0', $result);
        if ($ticket_number !== "") {
            $tickets = new \App\Model\helpdesk\Ticket\Tickets();
            $ticket = $tickets->where('ticket_number', $ticket_number)->first();
            if ($ticket) {
                return $ticket;
            }
        }
    }

    /**
     * checking the array has key
     * @param string $key
     * @param array $array
     * @return string
     */
    public function checkArray($key, $array) {
        $value = "";
        if (array_key_exists($key, $array)) {
            $value = $array[$key];
        }
        return $value;
    }

}
