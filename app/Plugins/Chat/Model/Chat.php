<?php

namespace App\Plugins\Chat\Model;

use App\BaseModel;

class Chat extends BaseModel {
    protected $table = "chat";
    protected $fillable = ['name','short','status'];
    
    public function status($app){
        $check = false;
        $chat = $this->where('short',$app)->first();
        if($chat){
            if($chat->status=='true'){
                $check = true;
            }
        }
        return $check;
    }
    
}