<?php

namespace App\Plugins\Ldap;

use Schema;
use Exception;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class Utility {

    public static function isConfigured() {
        try {
            if (Schema::hasTable('ldap')) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (Exception $ex) {
            dd($ex);
        }
    }

    public static function configure() {
        try {
            //if (extension_loaded('ldap')) {

                self::ldapSchema();
//            } else {
//                throw new Exception('Install Ldap extension');
//            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public static function isAgentAuth() {
        try {
            $conf = self::isConfigured();
            if ($conf) {
                $auth = DB::table('ldap')->where('id', 1)->first();
                if ($auth->agent_auth == 1) {
                    return TRUE;
                }
            } else {
                return FALSE;
            }
        } catch (Exception $ex) {
            dd($ex);
        }
    }

    public static function isClientAuth() {
        try {
            $conf = self::isConfigured();
            if ($conf) {
                $auth = DB::table('ldap')->where('id', 1)->first();
                if ($auth->client_auth == 1) {
                    return TRUE;
                }
            } else {
                return FALSE;
            }
        } catch (Exception $ex) {
            dd($ex);
        }
    }

    public static function ldapSchema() {
        try {

            if (!Schema::hasTable('ldap')) {

                Schema::create('ldap', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('domain');
                    $table->string('username');
                    $table->string('password');
                    $table->string('search_base');
                    $table->boolean('agent_auth');
                    $table->boolean('client_auth');
                    $table->timestamps();
                });
                DB::table('ldap')->insert(['id' => 1]);
                
                    //$table->dropUnique('users_email_unique');
                
            }

            if (!Schema::hasColumn('users', 'ldap_username')) {
                Schema::table('users', function(Blueprint $table) {

                    $table->string('ldap_username');
                });
            }
            if (!Schema::hasColumn('users', 'isldapauth')) {
                Schema::table('users', function(Blueprint $table) {
                    $table->integer('isldapauth');
                });
            }
        } catch (Exception $ex) {
            dd($ex);
        }
    }

}
