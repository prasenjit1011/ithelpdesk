@extends('themes.default1.admin.layout.admin')

@section('Plugins')
class="active"
@stop

@section('plugin-bar')
active
@stop

@section('Ldap')
class="active"
@stop

@section('HeadInclude')
@stop
<!-- header -->
@section('PageHeader')

@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">

</ol>
@stop
<!-- /breadcrumbs -->
<!-- content -->
@section('content')

<!-- open a form -->

<!-- <div class="form-group {{ $errors->has('company_name') ? 'has-error' : '' }}"> -->
<!-- table  -->

<div class="box box-primary">
    <div class="box-header">
        <h4 class="box-title">Users</h4>
    </div>
    <div id="gif" style="display:none">
        <div class="col-md-6 col-md-offset-5">
            <img src="{{asset("lb-faveo/media/images/gifloader.gif")}}">
        </div>
    </div>
    <div id="response"></div>
    <div class='alert alert-danger' id="error-div"  style="display:none">
        <ul id="error">

        </ul>
    </div>
    <!-- check whether success or not -->
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <b>Success!</b>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('success')}}
    </div>
    @endif
    <!-- failure message -->
    @if(Session::has('fails'))
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <b>Alert!</b> Failed.
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('fails')}}
    </div>
    @endif
    <div class="box-body">
        <div class="row">
            <div class="col-md-4 col-md-offset-8">
                <form>
                    <div class="col-md-6">
                        <input type="text" name="search" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <a href="#" class="btn btn-primary pull-right" id="submit" onclick="submit();">Search</a>
                    </div>


                </form>
            </div>

            <div class="col-md-12">

                <table class="table table-bordered table-responsive">
                    <thead>
                        <tr>
                            <td>First Name</td>
                            <td>Last Name</td>
                            <td>User Name</td>
                            <td>Ldap Username</td>
                            <td>Email</td>
                            <td>Action</td>

                        </tr>
                    </thead>
                    <tbody id="table">
                        @forelse($users as $user)
                        <tr>

                            <td>{!! $user->first_name !!}</td>
                            <td>{!! $user->last_name !!}</td>
                            <td>{!! $user->user_name !!}</td>
                            <td>{!! $user->ldap_username !!}</td>
                            <td>{!! $user->email !!}</td>
                            <td><a href="{{url('ldap/users/'.$user->id)}}">Edit</a></td>


                        </tr>
                        @empty 
                        <tr><td><p>No records</p></td></tr>
                        @endforelse
                    </tbody>
                    <tbody id="searchtable">

                    </tbody>



                </table>
                <div class="pagination">
                    <?php echo $users->render(); ?>
                </div>
            </div>


        </div>
    </div>
</div>

@stop

@section('FooterInclude')

@stop
@stop

<!-- 
/content -->
<script>
    function submit() {
        $.ajax({
            type: "POST",
            url: "{{url('ldap/postsearch')}}",
            data: $('form').serialize(),
            beforeSend: function () {
                $("#gif").show();
                
            },
            success: function (data) {
                $("#table").hide();
                $("#gif").hide();
                //console.log(data[0]);
                data.forEach(function (entry) {

                    $("#searchtable").append("<tr><td>" + entry.first_name + "</td><td>" + entry.last_name + "</td>" +
                            "<td>" + entry.user_name + "</td>" +
                            "<td>" + entry.ldap_username + "</td>" +
                            "<td>" + entry.email + "</td>" +
                            "<td><a href=users/" + entry.id + ">Edit</a></td></tr>");
                });



            },
            error: function (data) {
                $("#gif").hide();
                $("#error-div").show();
                var errors = data.responseJSON;
                $.each(errors, function (index, value) {
                    $("#error").append("<li>" + value + "</li>");
                });

            }
        });
    }
</script>
