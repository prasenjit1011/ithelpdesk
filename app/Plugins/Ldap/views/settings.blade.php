@extends('themes.default1.admin.layout.admin')

@section('Plugins')
class="active"
@stop

@section('plugin-bar')
active
@stop

@section('Ldap')
class="active"
@stop

@section('HeadInclude')
@stop
<!-- header -->
@section('PageHeader')

@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">

</ol>
@stop
<!-- /breadcrumbs -->
<!-- content -->
@section('content')

<!-- open a form -->

{!! Form::model($settings,['url' => 'postsettings', 'method' => 'PATCH','files'=>true]) !!}

<!-- <div class="form-group {{ $errors->has('company_name') ? 'has-error' : '' }}"> -->
<!-- table  -->

<div class="box box-primary">
    <div class="box-header">
        <h4 class="box-title">Ldap</h4><a href="#" class="btn btn-primary pull-right" id="submit" onclick="submit();">Save</a>
    </div>
    <div id="gif" style="display:none">
        <div class="col-md-6 col-md-offset-5">
        <img src="{{asset("lb-faveo/media/images/gifloader.gif")}}">
        </div>
    </div>
    <div id="response"></div>
    <div class='alert alert-danger' id="error-div"  style="display:none">
        <ul id="error">
            
        </ul>
    </div>
    <!-- check whether success or not -->
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <b>Success!</b>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('success')}}
    </div>
    @endif
    <!-- failure message -->
    @if(Session::has('fails'))
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <b>Alert!</b> Failed.
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('fails')}}
    </div>
    @endif

    <!-- Name text form Required -->
    <div class="box-body">
        <div class="row">



            <div class="col-md-6 form-group {{ $errors->has('domain') ? 'has-error' : '' }}">

                {!! Form::label('domain','Domain') !!}
                {!! Form::text('domain',null,['class' => 'form-control']) !!}

            </div>
            <div class="col-md-6 form-group {{ $errors->has('username') ? 'has-error' : '' }}">

                {!! Form::label('username','Username') !!}
                {!! Form::text('username',null,['class' => 'form-control']) !!}

            </div>

            <div class="col-md-6 form-group {{ $errors->has('password') ? 'has-error' : '' }}">

                {!! Form::label('password','Password') !!}
                {!! Form::text('password',null,['class' => 'form-control']) !!}

            </div>
            <div class="col-md-6 form-group {{ $errors->has('search_base') ? 'has-error' : '' }}">

                {!! Form::label('search_base','Search Basis') !!}
                {!! Form::text('search_base',null,['class' => 'form-control']) !!}

            </div>
            {{--
            <div class="col-md-6 form-group {{ $errors->has('agent_auth') ? 'has-error' : '' }}">

                {!! Form::label('agent_auth','Agent Authentication') !!}
                {!! Form::checkbox('agent_auth',1) !!}

            </div>
            <div class="col-md-6 form-group {{ $errors->has('client_auth') ? 'has-error' : '' }}">

                {!! Form::label('client_auth','Client Authentication') !!}
                {!! Form::checkbox('client_auth',1) !!}

            </div>
    --}}
            {!! Form::close() !!}
            @if($settings->domain)
            {{--
            <div class="col-md-6 form-group">
                <a href="{{url('ldap\users')}}" class="btn btn-primary">Users</a>
            </div>
    --}}
            @endif
        </div>
    </div>
</div>

@stop

@section('FooterInclude')
<!-- /content -->
<script>
    function submit() {
        $.ajax({
            type: "PATCH",
            url: "{{url('postsettings')}}",
            data: $('form').serialize(),
            beforeSend: function(){
                $("#gif").show();
            },
            success: function (data) {
                $("#gif").hide();
                $("#response").html(data)
            },
            error: function (data) {
                $("#gif").hide();
                $("#error-div").show();
                var errors = data.responseJSON;
                $.each(errors, function (index, value) {
                    $("#error").append("<li>"+value+"</li>");
                });

            }
        });
    }
</script>

@stop
