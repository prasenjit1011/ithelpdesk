<?php namespace App\Plugins\Ldap;
 
class ServiceProvider extends \App\Plugins\ServiceProvider {
 
    public function register()
    {
        parent::register('Ldap');
    }
 
    public function boot()
    {
        parent::boot('Ldap');
    }
 
}
