<?php

return [
    'name' => 'Ldap',
    'description' => 'Faveo Ldap is a plugin to provide ldap authentication',
    'author' => 'ladybird',
    'website' => 'www.ithelpdesk.com',
    'version' => '1.0.0',
    'settings' => 'ldap/settings',
    
];

