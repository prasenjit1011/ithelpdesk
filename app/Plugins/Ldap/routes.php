<?php

Route::group(['middleware' => ['web']], function() {
    Route::get('ldap/settings', ['as'=>'ldap.settings','uses'=>'App\Plugins\Ldap\Controllers\Adldap@settings']);
    Route::patch('postsettings', ['as'=>'ldap.settings.post','uses'=>'App\Plugins\Ldap\Controllers\Adldap@postSettings']);

    Route::get('ldap/users', ['as'=>'ldap.users','uses'=>'App\Plugins\Ldap\Controllers\Adldap@userIndex']);
    Route::post('ldap/postsearch', ['as'=>'ldap.users.search','uses'=>'App\Plugins\Ldap\Controllers\Adldap@users']);

    Route::get('ldap/users/{id}', ['as'=>'ldap.user.get','uses'=>'App\Plugins\Ldap\Controllers\Adldap@userEdit']);
    Route::patch('ldap/users/{id}', ['as'=>'ldap.user.post','uses'=>'App\Plugins\Ldap\Controllers\Adldap@patchUser']);

    Event::listen('auth.login.event', function() {
        $ldap_controller = new \App\Plugins\Ldap\Controllers\Adldap();
        $ldap_controller->authLdap();
    });
});
Event::listen('auth.login.form', function()
{
    $ldap_controller = new \App\Plugins\Ldap\Controllers\Adldap();
    echo $ldap_controller->loginForm();
});


