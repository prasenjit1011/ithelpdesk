<?php

namespace App\Plugins\ServiceDesk\Controllers\Changes;

use App\Plugins\ServiceDesk\Controllers\BaseServiceDeskController;
use App\Plugins\ServiceDesk\Model\Changes\SdChanges;
use App\Plugins\ServiceDesk\Model\Changes\SdChangestatus;
use App\Plugins\ServiceDesk\Model\Changes\SdChangepriorities;
use App\Plugins\ServiceDesk\Model\Changes\SdChangetypes;
use App\Plugins\ServiceDesk\Model\Changes\SdImpacttypes;
use App\Plugins\ServiceDesk\Model\Releases\SdLocations;
use App\Plugins\ServiceDesk\Requests\CreateChangesRequest;
use App\User;
use App\Plugins\ServiceDesk\Model\Cab\Cab;
use App\Plugins\ServiceDesk\Model\Assets\SdAssets;
use Exception;
use App\Plugins\ServiceDesk\Requests\CreateReleaseRequest;
use Illuminate\Http\Request;
use Lang;

class ChangesController extends BaseServiceDeskController {

    public function __construct() {
        $this->middleware('auth');
    }

    public function changesindex() {
        try {
            return view('service::changes.index');
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function getChanges() {
        try {
            $change = new SdChanges();
            $changes = $change->select('id', 'description', 'subject',  'status_id', 'priority_id', 'change_type_id', 'impact_id', 'location_id', 'approval_id')->get();
            return \Datatable::Collection($changes)
                            ->showColumns('subject', 'reason')
                            ->addColumn('action', function($model) {
                                $url = url('service-desk/changes/' . $model->id . '/delete');
                                $delete = \App\Plugins\ServiceDesk\Controllers\Library\UtilityController::deletePopUp($model->id, $url, "Delete $model->subject");
                                //dd($delete);
                                return "<a href=" . url('service-desk/changes/' . $model->id . '/edit') . " class='btn btn-primary btn-xs'><i class='fa fa-edit'>&nbsp;&nbsp;</i>Edit</a> &nbsp;"
                                        . $delete
                                        . " <a href=" . url('service-desk/changes/' . $model->id . '/show') . " class='btn btn-primary btn-xs'><i class='fa fa-eye'>&nbsp;&nbsp;</i>View</a>";
                            })
                            ->searchColumns('subject')
                            ->orderColumns('description', 'subject', 'reason', 'status_id', 'priority_id', 'change_type_id', 'impact_id', 'location_id', 'approval_id')
                            ->make();
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function changescreate() {
        try {
            $statuses = SdChangestatus::pluck('name', 'id')->toArray();
            $sd_changes_priorities = SdChangepriorities::pluck('name', 'id')->toArray();
            $sd_changes_types = SdChangetypes::pluck('name', 'id')->toArray();
            $sd_impact_types = SdImpacttypes::pluck('name', 'id')->toArray();
            $sd_locations = SdLocations::pluck('title', 'id')->toArray();
            $users = Cab::pluck('name', 'id')->toArray();
            $assets = SdAssets::pluck('name', 'id')->toArray();
            $requester = User::where('role', 'agent')->orWhere('role', 'admin')->pluck('email', 'id')->toArray();

            return view('service::changes.create', compact('statuses', 'assets', 'sd_changes_priorities', 'sd_changes_types', 'sd_impact_types', 'sd_locations', 'users', 'requester'));
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function changeshandleCreate(CreateChangesRequest $request, $attach = false) {
        //  dd($request->all());
        try {
            $sd_changes = new SdChanges;
            $sd_changes->fill($request->input())->save();
            \App\Plugins\ServiceDesk\Controllers\Library\UtilityController::attachment($sd_changes->id, 'sd_changes', $request->file('attachments'));
            \App\Plugins\ServiceDesk\Controllers\Library\UtilityController::storeAssetRelation('sd_changes', $sd_changes->id, $request->input('asset'));
            if ($attach == false) {
                return \Redirect::route('service-desk.changes.index')->with('message',Lang::get('service::lang.changes_created'));
            }
            return $sd_changes;
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function changesedit($id) {
        try {

            $change = SdChanges::findOrFail($id);
            $statuses = SdChangestatus::pluck('name', 'id')->toArray();
            $sd_changes_status = "";
            $sd_changes_priorities = SdChangepriorities::pluck('name', 'id')->toArray();
            $sd_changes_types = SdChangetypes::pluck('name', 'id')->toArray();
            $sd_impact_types = SdImpacttypes::pluck('name', 'id')->toArray();
            $sd_locations = SdLocations::pluck('title', 'id')->toArray();
            $users = Cab::pluck('name', 'id')->toArray();
            $assets = SdAssets::pluck('name', 'id')->toArray();
            $requester = User::where('role', 'agent')->orWhere('role', 'admin')->pluck('email', 'id')->toArray();
            return view('service::changes.edit', compact('sd_changes_status', 'assets', 'change', 'statuses', 'sd_changes_priorities', 'sd_changes_types', 'sd_impact_types', 'sd_locations', 'users', 'requester'));
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function changeshandleEdit($id, CreateChangesRequest $request) {
        try {

            $sd_changes = SdChanges::findOrFail($id);
            $sd_changes->fill($request->input())->save();
            \App\Plugins\ServiceDesk\Controllers\Library\UtilityController::attachment($sd_changes->id, 'sd_changes', $request->file('attachments'));
            \App\Plugins\ServiceDesk\Controllers\Library\UtilityController::storeAssetRelation('sd_changes', $sd_changes->id, $request->input('asset'));

            return \Redirect::route('service-desk.changes.index')->with('message',Lang::get('service::lang.changes_updated'));
        } catch (Exception $ex) {
            dd($ex);
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function changesHandledelete($id) {
        try {

            $sd_changes = SdChanges::where('id','=',$id)->delete();
            return \Redirect::route('service-desk.changes.index')->with('message', Lang::get('service::lang.changes_deleted'));

        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function show($id) {
        try {
            $changes = new SdChanges();
            $change = $changes->find($id);
            if ($change) {
                return view('service::changes.show', compact('change'));
            } else {
                throw new \Exception('Sorry we can not find your request');
            }
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }
    
    public function close($id){
        try {
            $changes = new SdChanges();
            $change = $changes->find($id);
            if ($change) {
                  $change->status_id = 6;
                  $change->save();
                  return redirect()->back()->with('success','Updated');
            } else {
                throw new \Exception('Sorry we can not find your request');
            }
        } catch (\Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }
    
    public function getReleases(){
        $release = new \App\Plugins\ServiceDesk\Model\Releases\SdReleases();
        $releases = $release->select('id','subject')->get();
        return \Datatable::Collection($releases)
                ->addColumn('id',function($model){
                    return "<input type='radio' name='release' value='".$model->id."'>";
                })
                ->addColumn('subject',function($model){
                    return str_limit($model->subject,20);
                })
                ->orderColumns('subject')
                ->searchColumns('subject')
                ->make();
    }
    
    public function attachNewRelease($id, CreateReleaseRequest $request){
        try{
            $release_controller = new \App\Plugins\ServiceDesk\Controllers\Releses\RelesesController();
            $release = $release_controller->releaseshandleCreate($request, true);
            $this->releaseAttach($id, $release->id);
            if($release){
                return redirect()->back()->with('success',Lang::get('service::lang.Updated'));
            }
        } catch (\Exception $ex) {
            dd($ex);
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }
    
    public function attachExistingRelease($id,Request $request){
        try{
            $releaseid = $request->input('release');
            $store = $this->releaseAttach($id, $releaseid);
            if($store){
                return redirect()->back()->with('success','Updated');
            }
        } catch (\Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }
    
    public function releaseAttach($changeid,$releaseid){
        $relation = new \App\Plugins\ServiceDesk\Model\Changes\ChangeReleaseRelation();
        $relations =  $relation->where('change_id',$changeid)->first();
        if($relations){
            $relations->delete();
        }
        
        return $relation->create([
            'release_id'=>$releaseid,
            'change_id'=>$changeid,
        ]);
    } 
    
    public function detachRelease($changeid){
        try{
            $relations = new \App\Plugins\ServiceDesk\Model\Changes\ChangeReleaseRelation();
            $relation = $relations->where('change_id',$changeid)->first();
            if($relation){
                $relation->delete();
            }
            return redirect()->back()->with('success','Updated');
        } catch (\Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

}
