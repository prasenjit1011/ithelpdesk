<?php

namespace App\Plugins\ServiceDesk\Controllers\Report;

use App\Http\Controllers\Controller;

class AssetReport extends Controller {

    public function reports() {
        return view('service::reports.reports');
    }

    public function agentAssetReport() {
        return view('service::reports.agent-asset-report');
    }

    public function departmentAssetReport() {
        return view('service::reports.department-asset-report');
    }
    
    public function productAssetReport() {
        return view('service::reports.product-asset-report');
    }
    
    public function locationAssetReport() {
        return view('service::reports.location-asset-report');
    }
    
    public function assetTypeAssetReport() {
        return view('service::reports.type-asset-report');
    }

    public function agentAssetReportApi($query=false) {
        $agents = \App\Plugins\ServiceDesk\Model\Assets\Users::with(['asset.product','asset.assetType'])
                ->select('first_name','last_name' ,'id');
        if(!$query){
           $agents =  $agents->paginate(10);
        }
                    
        return $agents;   
    }

    public function departmentAssetReportApi($query=false) {
        $departments = \App\Plugins\ServiceDesk\Model\Assets\Department::with(['asset.product','asset.assetType'])
                ->select('name', 'id');
        if(!$query){
           $departments =  $departments->paginate(10);
        }
                    
        return $departments;
    }
    
    public function productAssetReportApi($query=false) {
        $products = \App\Plugins\ServiceDesk\Model\Contract\Products::with(['asset'])
                ->select('name', 'id');
        if(!$query){
           $products =  $products->paginate(10);
        }
                    
        return $products;
    }
    
    public function locationAssetReportApi($query=false) {
        $products = \App\Plugins\ServiceDesk\Model\Problem\Location::with(['asset'])
                ->select('title', 'id');
        if(!$query){
           $products =  $products->paginate(10);
        }
                    
        return $products;
    }
    
    public function assetTypeAssetReportApi($query=false) {
        $products = \App\Plugins\ServiceDesk\Model\Assets\SdAssettypes::with(['asset'])
                ->select('name', 'id');
        if(!$query){
           $products =  $products->paginate(10);
        }
                    
        return $products;
    }
    
    

}
