<?php

return [
    /*
      |----------------------------------------------------------------------------------------
      | General Pages [English(en)]
      |----------------------------------------------------------------------------------------
      |
      | The following language lines are used in all general issues to translate
      | some words in view to English. You are free to change them to anything you want to
      | customize your views to better match your application.
      |
     */
    /**
     * common
     */
     'show'=>'Show',
    'edit'=>'Edit',

    
    /*
      |--------------------------------------
      |   Error
      |--------------------------------------
     */
    'success' => 'Success',
    'fails' => 'Fails',
    'alert' => 'Alert',
    'required-error' => 'Please fill all required feilds',
    'invalid' => 'Incorrect user id or password',
    /*
      |--------------------------------------
      |   Assets
      |--------------------------------------
     */
    'create_asset' => 'Create asset ',
    'assets' => 'Assets',
    'asset'=>'Asset',
    'name' => 'Name',
    'description' => 'Description',
    'department' => 'Department',
    'asset_type' => 'Asset type',
    'impact_type' => 'Impact type',
    'managed_by' => 'Managed by',
    'used_by' => 'Used by',
    'location' => 'Location',
    'assigned_on' => 'Assigned on',
    'action' => 'Action',
    'add_asset' => 'Add asset',
    'edit_asset' => 'Edit asset',
    'attachment' => 'Attachment',
    'select_depertment_type' => 'Select depertment type',
    'select_asset_type' => 'Select asset type',
    'select_impact' => 'Select impact',
    'updte_asset' => 'Update asset',
    'list_of_assets'=>'List of assets',
    'show-asset'=>'Show asset',
    'additional-details'=>'Additional details',
    'identifier'=>'Identifier',
    'request'=>'Request',
    'created'=>'Created',
    /*
      |--------------------------------------
      |   Releases
      |--------------------------------------
     */
    'releases' => 'Releases',
    'create_new_release' => 'Create new release',
    'subject' => 'Subject',
    'planed_start_date' => 'Planed start date',
    'planed_end_date' => 'Planed end date',
    'status' => 'Status',
    'priority' => 'Priority',
    'release_type' => 'Release type',
    'build_plan' => 'Build plan',
    'test_plan' => 'Test plan',
    'action' => 'Action',
    'add_release' => 'Add release',
    'build_plan_attachment' => 'Build plan attachment',
    'test_plan_attachment' => 'Test plan attachment',
    'edit_release' => 'Edit release',
    'update_release' => 'Update release',
    'select_status' => 'Select status',
    'selct_priority' => 'Selct priority',
    'selct_release_type' => 'Selct release type',
    'selct_location' => 'Selct location',
    'view_release' => 'View release',
    'new_release'=>'New release',
    'type'=>'Type',
    'view_release'=>'View release',
    'list_of_releases'=>'List of releases',
    'show-release'=>'Show release',
    'create_release'=>'Create release',
    'description'=>'Description',
    /*
      |--------------------------------------
      |   Changes
      |--------------------------------------
     */
    'changes' => 'Changes',
    'create_change' => 'Create change',
    'reason' => 'Reason',
    'impact' => 'Impact',
    'rollout_plan' => 'Rollout plan',
    'backout_plan' => 'Backout plan',
    'status' => 'Status',
    'priority' => 'Priority',
    'change_type' => 'Change type',
    'impact' => 'Impact',
    'approval' => 'Approval',
    'list_of_cabs' => 'List of CABS',
    'edit_change' => 'Edit change',
    'select_change_type' => 'Select change type',
    'select_approval' => 'Select approval',
    'action' => 'Action',
    'add_change' => 'Add changes',
    'update_change' => 'Update changes',
    'requester'=>'Requester',
    'impact_type'=>'Impact type',
    'list_of_changes'=>'List of changes',
    'show-change'=>'Show change',
    /*
      |--------------------------------------
      |   Products
      |--------------------------------------
     */
    'products' => 'Products',
    'create_new_product' => ' Create product',
    'manufacturer' => 'Manufacturer',
    'product_status' => 'Product status',
    'product_mode_procurement' => 'Product mode procurement',
    'new_product' => 'New product',
    'department_access' => 'Department access',
    'update_product' => 'Update product',
    'create' => 'Create',
    'new_product' => 'Products',
    'mode_of_procurement' => 'Mode of procurement',
    'vendor' => 'Vendor',
    'update' => 'Update',
    'select_asset' => 'Select asset',
    'select_product' => 'Select product',
    'select_mode_product' => 'Select mode of procurement',
    'select_status' => 'Select status',
    'select_department' => 'Select department',
    'product-add'=>'Product add',
    'list_of_product'=>'List of products',
    'edit_product'=>'Edit product',
    'show-product'=>'Show product',
    'open_new_product'=>'Create new product',
    'update'=>'Update',
    /*
      |--------------------------------------
      |   vendor create lang
      |--------------------------------------
     */
    'open-edit-problem' => 'Open edit problem',
    'vendor' => 'Vendor',
    'vendor-list' => 'Vendor list',
    'name' => 'Name',
    'description' => 'Description',
    'primary-contact' => 'Primary contact',
    'email' => 'Email',
    'address' => 'Address',
    'all_department' => 'All department',
    'status' => 'Status',
    'add-vendors' => 'Add vendor',
    'primary_contact' => 'Primary contact',
    'add-vendor' => 'add-vendor',
    'edit-vendor' => 'Edit vendor',
    'show-vendor'=>'Show vendor',
    /*
      |--------------------------------------
      |   Contract lang
      |--------------------------------------
     */
    'contract'=>'Contracts',
    'open-new-contract' => 'open new contract',
    'name' => 'Name',
    'description' => 'Description',
    'cost' => 'Cost',
    'contract_type' => 'Contract types',
    'select-contact-type' => 'select contact type',
    'approver' => 'Approver',
    'select-approver-id' => 'Select approver id',
    'vendor-type' => 'Vendor',
    'vendor' => 'Vendor',
    'select_license_type' => 'License type',
    'license_type' => 'License type',
    'license_type_id' => 'License type',
    'license_count' => 'License count',
    'attatchment' => 'Attachment',
    'product' => 'Product',
    'select-product' => 'Product',
    'notify_expiry' => 'Notify expiry',
    'contract_start_date' => 'Start date',
    'contract_end_date' => 'End date',
    'update' => 'Update',
    'add_contract' => 'Add contract',
    'create_contract' => 'Create contract',
    'new_contract'=>'Create contract',
    'select_contract_type'=>'Select contract type',
    'select_approver'=>'Select approver',
    'select_vendor'=>'Select vendor',
    'show-contract'=>'Show contract',
    'list_of_contracts'=>'List of contracts',
    /*
      |--------------------------------------
      |   Problem create lang
      |--------------------------------------
     */
    'problem'=>'Problem',
    'new_problem' => 'New problem',
    'create_problem' => 'Create problem',
    'from' => 'From',
    'name' => 'Name',
    'subject' => 'Subject',
    'description' => 'Description',
    'department' => 'Department',
    'status' => 'Status',
    'select-status' => 'Select status',
    'priority' => 'Priority',
    'select-priority' => 'Select priority',
    'impact' => 'Impact',
    'select-impact' => 'Select impact',
    'location' => 'Location',
    'select-location' => 'Select location',
    'group' => 'Group',
    'select_group' => 'Select group',
    'agent' => 'Agent',
    'select-agent' => 'Select agent',
    'assigned-id' => 'Assigned id',
    'assigned-to' => 'Assigned to',
    'associated-asset' => 'Associated asset',
    'attach-asset' => 'Attach asset',
    'attach' => 'Attach',
    'add-asset' => 'Add asset',
    'cancel' => 'Cancel',
    'add-problem' => 'Add problem',
    'problem' => 'Problem',
    'edit_new_contract' => 'Edit new contract',
    'problem_type' => 'Problem type',
    'ticket_type' => 'Ticket type',
    'select_ticket_type' => 'Select ticket type',
    'edit_problem' => 'Edit problem',
    'show-problem'=>'Show problem',
    'problem-add'=>'Add problem',
    'list_of_problems'=>'List of problems',
    
    /*

      |--------------------------------------
      |   Problem index lang
      |--------------------------------------
     */
    'status_type_id' => 'Status',
    'priority_id' => 'Priority',
    'impact_id' => 'Impact',
    'location_type_id' => 'location',
    'group_id' => 'Group',
    'agent_id' => 'Agent',
    'assigned_id' => 'Assigned',
    'action' => 'Action',
    /*
      |--------------------------------------
      |   vendor create lang
      |--------------------------------------
     */
    'open-edit-problem' => 'Open edit problem',
    'new-vendor' => 'Create new vendor',
    'vendor-list' => 'Vendor list',
    'name' => 'Name',
    'description' => 'Description',
    'primary-contact' => 'Primary contact',
    'email' => 'Email',
    'address' => 'Address',
    'all_department' => 'All department',
    'status' => 'Status',
   // 'add-vendors' => 'Add vendor',
    'primary_contact' => 'Primary contact',
   //'add-vendor' => 'Add vendor',
    'edit-vendor' => 'Edit vendor',
    'administrator' => 'Administrator',
    'add_vendor'=>'Add vendor',
    /*
      |--------------------------------------
      |   Procurment create lang
      |--------------------------------------
     */
    'procurement'=>'Procurements',
    'open_new_procurment' => 'Create procurement',
    'open_edit_procurment' => 'Open edit procurement',
    'create' => 'Create',
    'add-procurement'=>'Add procurement',
    'edit-procurement'=>'Edit procurement',
    'update-procurement'=>'Update',
    'list_of_procurements'=>'List of procurements',
    
    /*
      |--------------------------------------
      |   Assets types create lang
      |--------------------------------------
     */
    'assetstypes' => 'Asset types',
    'new_assetstypes' => 'New asset types',
    'created_at' => 'Created at',
    'updated_at' => 'Updated at',
    'add_assetstypes' => 'Add asset types',
    'open_new_assets_type' => 'Open new Asset types',
    'edit_assets_type' => 'Edit asset types name',
    'updte_assets_types' => 'Updte asset types',
    'form'=>'Form',
    'list_of_forms'=>'List of forms',
    'edit_assetstypes'=>'Edit',
    'parent'=>'Parent',
    /*
      |--------------------------------------
      |   Contract types create lang
      |--------------------------------------
     */
    'contract_types' => 'Contract types',
    'new_contracts' => 'Create contract type',
    'create_new_contract_type' => 'Create new contract type',
    'add_contract' => 'Add contract',
    'edit_contract' => 'Edit contract type',
    'updte_contract' => 'Updte contract',
    /*
      |--------------------------------------
      |   License types create lang
      |--------------------------------------
     */
    'license_type' => 'License types',
    'new_license' => 'Create license',
    'open_new_license' => 'Open new license',
    'add_license' => 'Add license',
    'edit_license' => 'Edit license',
    'updte_license' => 'Update license',
    'edit_license_type'=>'Edit license type',
    'update-license-type'=>'Update license type',
    'open_new_license_type'=>'Create new license type',
    'add-license-type'=>'Add license type',
    'update_contract'=>'Update contract',
    'list_of_license_types'=>'List of license types',
    /*
      |--------------------------------------
      |   Location Catagory create lang
      |--------------------------------------
     */
    'location_catagory' => 'Location category',
    'create_location' => 'Create location',
    'open_new_location_catagory' => 'Open new location catagory ',
    'add_locationcatagory' => 'Add location catagory',
    'edit_location_catagory' => 'Edit location catagory',
    'updte_location_catagory' => 'Update location catagory',
    'show-location'=>'Show location',
    'organization'=>'Organization',
    
    /**
     * Cabs
     */
    
    'cabs'=>'CABS',
    'create_cab'=>'Create cab',
    'create_cab'=>'Create cab',
    'head'=>'Head',
    'approvers'=>'Approvers',
    'approval_mandatory'=>'Approval mandatory',
    'edit_cab'=>'Edit cab',
    'members'=>'Members',
    'create_new_location_catagory' => 'Create new location category',
    'add_locationcatagory' => 'Add location category',
    'edit_location_catagory' => 'Edit location category',
    'updte_location_catagory' => 'Update location category',
    'list_of_locationcategory'=>'List of location categories',
    'voting'=>'Voting',
    'mark_your_vote'=>'Mark your vote',
    'vote'=>'Vote',
    'comment'=>'Comment',
    'show-vote'=>'Show vote',
     /*
      |--------------------------------------
      |   Location Catagory create lang
      |--------------------------------------
     */
    'locations' => 'Locations',
    'title'=>'Title',
    'email'=>'Email',
    'phone'=>'Phone',
    'address'=>'Address',
    'location_catagory_name'=>'Location catagory name',
    'location_category'=>'Location category',
    'select_location'=>'Select location',
    'new_location' => 'New location',
    'create_new_location' => 'Create new location',
    'add_location' => 'Add location',
    'edit_location' => 'Edit location',
    'updte_location' => 'Update location',
    'list_of_location'=>'List of locations',
    'edit_location_category'=>'Edit location category',
    'update-location-category'=>'Update location category',
    'open_new_location_category'=>'Open new location category',
    'add-location-category-types'=>'Add location category',
    
    /**
     * Form Builder
     */
    
    'create_form'=>'Create form',
    'form'=>'Form',
    'forms'=>'Forms',
    'edit_form'=>'Edit form',
    'update_form'=>'Update form',
    
//    5/30/2017
    'product_created_successfully'=>'Product created successfully.',
    'product_updated_successfully'=>'Product updated successfully.',
    'product_deleted_successfully'=>'Product deleted successfully.',
    'vendor_created_successfully'=>'Vendor created successfully.',
    'vendor_updated_successfully'=>'Vendor updated successfully.',
    'vendor_deleted_successfully'=>'Vendor deleted successfully.',
    'procurement_created_successfully'=>'Procurement created successfully.',
    'procurement_deleted_successfully'=>'Procurement deleted successfully.',
    'procurement_updated_successfully'=>'Procurement updated successfully.',
    'contract_created_successfully'=>'Contract created successfully.',
    'contract_deleted_successfully'=>'Contract deleted successfully.',
    'contract_updated_successfully'=>'Contract updated successfully.',
    'contract_type_created_successfully'=>'Contract type created successfully.',
    'contract_type_updated_successfully'=>'Contract type  updated successfully.',
    'contract_type_deleted_successfully'=>'Contract type deleted successfully.',
    'license_type_created_successfully'=>'License type created successfully.',
    'license_type_updated_successfully'=>'License type updated successfully.',
    'license_type_deleted_successfully'=>'License type deleted successfully.',
    'asset_type_created_successfully'=>'Asset type created successfully.',
    'asset_type_updated_successfully'=>'Asset type updated successfully.',
    'asset_type_deleted_successfully'=>'Asset type deleted successfully.',
    'cab_created_successfully'=>'CAB created successfully.',
    'cab_updated_successfully'=>'CAB updated successfully.',
    'location_created_successfully'=>'Location  created successfully.',
    'location_updated_successfully'=>'Location  updated successfully.',
    'location_deleted_successfully'=>'Location  deleted successfully.',
    'location_category_created_successfully'=>'Location category created successfully.',
    'location_category_updated_successfully'=>'Location category updated successfully.',
    'location_category_deleted_successfully'=>'Location category deleted successfully.',
    'announced'=>'Announced successfully.',
    
    //    5/31/2017
    'problem_created_successfully'=>'Problem created successfully.',
    'problem_updated_successfully'=>'Problem updated successfully.',
    'problem_deleted_successfully'=>'Problem deleted successfully.',
    'changes_created'=>'Changes created.',
    'changes_updated'=>'Changes updated.',
    'changes_deleted'=>'Changes deleted.',
    'updated'=>'Updated.',
    'release_created_successfully'=>'Release created successfully.',
    'release_updated_successfully'=>'Release updated successfully.',
    'release_deleted_successfully'=>'Release deleted successfully.',
    'asset_created_successfully'=>'Asset created successfully.',
    
    //6/2/17
    'list_of_contract_types'=>'List of contract types',
    'list_of_vendors'=>'List of vendors',
    'create_asset_type'=>'Create asset-type',
    'list_of_asset_types'=>'List of asset types',
    'create_location_category'=>'Create location category',
    'create_new_change'=>'Create new change',
    'create_new_asset'=>'Create new asset',

    'save'           =>'Save',

    
    "agent-asset"=>'Assets by Agents',
    'department-asset'=>'Assets by Department',
    'location-asset'=>'Assets by Location',
    'product-asset'=>'Assets by Product',
    'asset-type-asset'=>'Assets by Asset Type',
    'assigned'=>'Assigned'

];
