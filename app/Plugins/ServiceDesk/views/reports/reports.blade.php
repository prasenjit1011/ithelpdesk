<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Reports</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">

                <div class="col-md-2 col-sm-6">
                    <div class="settingiconblue">
                        <div class="settingdivblue">
                            <a href="{{ url('service-desk/reports/agent/assets') }}">
                                <span class="fa-stack fa-2x">
                                    <i class="fa fa-user-secret"></i>
                                </span>
                            </a>
                        </div>
                        <p class="box-title" >{{Lang::get('service::lang.agent-asset')}}</p>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="settingiconblue">
                        <div class="settingdivblue">
                            <a href="{{ url('service-desk/reports/department/assets') }}">
                                <span class="fa-stack fa-2x">
                                    <i class="fa fa-users"></i>
                                </span>
                            </a>
                        </div>
                        <p class="box-title" >{{Lang::get('service::lang.department-asset')}}</p>
                    </div>
                </div>
                
                <div class="col-md-2 col-sm-6">
                    <div class="settingiconblue">
                        <div class="settingdivblue">
                            <a href="{{ url('service-desk/reports/product/assets') }}">
                                <span class="fa-stack fa-2x">
                                    <i class="fa fa-cubes"></i>
                                </span>
                            </a>
                        </div>
                        <p class="box-title" >{{Lang::get('service::lang.product-asset')}}</p>
                    </div>
                </div>
                
                <div class="col-md-2 col-sm-6">
                    <div class="settingiconblue">
                        <div class="settingdivblue">
                            <a href="{{ url('service-desk/reports/location/assets') }}">
                                <span class="fa-stack fa-2x">
                                    <i class="fa fa-location-arrow"></i>
                                </span>
                            </a>
                        </div>
                        <p class="box-title" >{{Lang::get('service::lang.location-asset')}}</p>
                    </div>
                </div>
                
                <div class="col-md-2 col-sm-6">
                    <div class="settingiconblue">
                        <div class="settingdivblue">
                            <a href="{{ url('service-desk/reports/type/assets') }}">
                                <span class="fa-stack fa-2x">
                                    <i class="fa fa-tags"></i>
                                </span>
                            </a>
                        </div>
                        <p class="box-title" >{{Lang::get('service::lang.asset-type-asset')}}</p>
                    </div>
                </div>
                
               
                
            </div>
        </div>
    </div>
</div>
