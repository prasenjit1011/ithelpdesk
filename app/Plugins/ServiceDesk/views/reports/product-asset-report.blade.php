@extends('themes.default1.admin.layout.admin')

<!-- /breadcrumbs -->
<!-- content -->
@section('content')
<section class="content-header">
    <h1>
        {{Lang::get('service::lang.product-asset')}}

    </h1>

</section>
<link rel="stylesheet" type="text/css" href="{{asset('lb-faveo/css/table.style.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('lb-faveo/css/dataTables.bootstrap.min.css')}}">
<div class="box box-primary" ng-controller="productReportCtrl" style="padding: 15px">
<div class="col-sm-4" style="float: right;"> 
             <input type="search" class="form-control" ng-model="search" placeholder="Search Here..." aria-controls="datatable-responsive">
</div>
<table id="#tableData" class="table table-striped table-bordered responsive no-wrap dataTable dtr-inline" cellspacing="0"          width="100%" role="grid" aria-describedby="datatable-responsive_info" style="width: 100%;" ng-table="userTable">
        <thead>
           <tr role="row">
                <th class="sorting_asc" tabindex="0" aria-controls="datatable-responsive" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column ascending" ng-click="sort(Name)" >
                     product
                </th>
                <th>
                     Assets
                </th>
                </tr>
        </thead>

       <tbody>
               <tr role="row" class="odd" ng-repeat="x in names.data|filter:search|orderBy:sortKey:reverse">
                        <td >@{{x.name}}</td>
                        <td ng-if="x.asset.length!=0" style="padding:0px"><table style="width:100%"><tr ng-repeat="asseto in x.asset" style="border: 1px solid gainsboro"><td style="padding: 7px;height: 34px">@{{asseto.name}}</td></tr></table></td>
                        <td ng-if="x.asset.length==0"></td>
                        
                </tr>   
       </tbody>
   </table>
  <div class="row">
    <div class="col-sm-6" style="float: right;"> 
           <ul class="pagination" id="pagination" style="float: right;"></ul>
     </div>
   </div>
</div>
@stop
@push('scripts')
<!-- api : {{url('service-desk/reports/product/assets/api')}} -->
<script src="{{asset('lb-faveo/js/jquery.twbsPagination.js')}}" type="text/javascript"></script>
<script>
app.controller('productReportCtrl', function($scope, $http) {
  $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    };
$http.get("{{url('service-desk/reports/product/assets/api')}}").success(function(data){
	$scope.names=data;
	$scope.totalPage=Math.ceil(data.total/data.per_page);
	$('#pagination').twbsPagination({
            onPageClick: function (event, page) {
                $http.get("{{url('service-desk/reports/product/assets/api')}}"+'?page='+page).success(function(data){
                	$scope.names=data;
                })
                .error(function(data) {
                	
                });
            },
            totalPages: $scope.totalPage,
            visiblePages: 4
        })
})
   
});
</script>
@endpush