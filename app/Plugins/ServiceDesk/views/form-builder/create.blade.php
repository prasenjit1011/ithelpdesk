@extends('themes.default1.agent.layout.agent')
<link href="{{asset('lb-faveo/js/form/angular-ui-tree.css')}}" rel="stylesheet" type="text/css" > 
<link href="{{asset('lb-faveo/js/form/app.css')}}" rel="stylesheet" type="text/css" >
<style>
     .tree-node{
        margin:10px;
     }
     .angular-ui-tree-empty {
    border: 1px solid gainsboro;
    min-height: 500px;
    height: 100%;
    background-color: white;
    background-image: none;
    margin-bottom: 15px
  }
  .list-group{
    border: 1px solid #ddd;
  }
  .list-group-item{
    border: none !important;
  }
  .list-inline > li {
    padding-left: 0px !important;
    padding-right: 0px!important;
  }
  .is-sticky>#category {
      top: 30px !important;
      z-index: 100 !important;
      -webkit-box-shadow: 0 2px 6px rgba(63,63,63,0.1);
      box-shadow: 0 2px 6px rgba(63,63,63,0.1);
  }
  .btn-xs{
    padding: 4px !important;
  }
  input[type="checkbox"], input[type="radio"]{
    height: 22px !important;
  }
  </style>
  

@section('content')

<section class="content-heading-anchor">
    <h2>
        Form Builder 
    </h2>
</section>

<div class="box box-primary" ng-controller="CloningCtrl">
    <div class="box-header">
        <h4>Create new form</h4>
    </div>
    <div class="box-body">
      <div class="well" style="display: none"></div>
        <div class="row">

            <div id="response" class="col-md-12"></div>
            <div class="col-md-6">
                {!! Form::label('title','Title') !!}<span style="color:red">*</span>
                {!! Form::text('title',null,['class'=>'form-control']) !!}
            </div>
            <div class="row">
            
  <!-- Nested node template -->
<script type="text/ng-template" id="nodes_renderer1.html">
  <div ui-tree-handle class="tree-node tree-node-content" style="margin:0px">
    <span>@{{node.title}}</span>
  </div> 
  <ol ui-tree-nodes="" ng-model="node.nodes" ng-class="{hidden: collapsed}" class="list-inline">
    <li ng-repeat="node in node.nodes" ui-tree-node ng-include="'nodes_renderer1.html'" >
    </li>
  </ol>
</script>
<script type="text/ng-template" id="nodes_renderer2.html">
  <div class="tree-node">
    <div class="pull-left tree-handle" ui-tree-handle>
      <span class="glyphicon glyphicon-list"></span>
    </div>
    <div class="tree-node-content dropdown">
     
       <label>@{{node.title}}</label>
      
      <a class="pull-right btn btn-danger btn-xs" data-nodrag ng-click="remove(this)"><span class="glyphicon glyphicon-remove"></span></a>
      <a class="pull-right btn btn-info btn-xs collapser"  data-toggle="collapse" aria-controls="collapseExample"><span class="glyphicon glyphicon glyphicon-pencil"></span></a>
      <div class="panel-collapse collapse" id="collapseExample" style="margin-top: 10px;">
        <ul class="list-group">
          <li class="list-group-item row" ng-show="node.value==''||node.value!=null" style="margin-left: 0px;margin-right: 0px">
             <div class="col-sm-3" style="line-height: 2.5"><label>Label</label></div>
             <div class="col-sm-9"><input type="text" name="" class="form-control" ng-model="node.label" style="border-radius: 0px" ng-disabled="node.default=='yes'"></div>
          </li>
          <li class="list-group-item row" ng-show="node.required==true||node.required==false" style="margin-left: 0px;margin-right: 0px">
              <div class="col-sm-3" style="line-height: 2.5"><label>Required</label></div>
             <div class="col-sm-9"><input type="checkbox" class="form-control" name="" ng-model="node.required" style="border-radius:0px;width: 7%;"></div>
          </li>
          <li class="list-group-item row"   style="margin-left: 0px;margin-right: 0px" ng-repeat="option in node.options" ng-show="node.type=='radio'||node.type=='checkbox'||node.type=='select'" >
              <div class="col-sm-3" style="line-height: 2.5"><label>Option@{{$index+1}}</label></div>
             <div class="col-sm-6"><input type="text" class="form-control" name="" ng-model="option.optionvalue" style="border-radius: 0px">
             </div>
             <div class="col-sm-3">
                  <a class="pull-right btn btn-danger btn-xs" data-nodrag ng-click="removeOption(this,$index)">
                      <span class="glyphicon glyphicon-remove"></span>
                  </a>
                  <a class="pull-right btn btn-primary btn-xs" ng-show="node.type=='select'&&node.title=='Nested Select'" data-nodrag ng-click="newSubForm(this,node.title,$index)" style="margin-right: 8px;">
                      <span class="glyphicon glyphicon-plus"></span>
                  </a>
             </div>
            <ol class="col-sm-12" ui-tree-nodes="" ng-model="option.nodes" ng-class="{hidden: collapsed}">
                <li ng-repeat="node in option.nodes" ui-tree-node ng-include="'nodes_renderer2.html'"></li>
            </ol> 
          </li>
          <li class="list-group-item row"   style="margin-left: 0px;margin-right: 0px;text-align: center;" ng-show="node.title=='Nested Select'">
                 <input type="button" name="addOption" class="btn btn-default" value="Add Option" ng-click="addOption(this)">
          </li>
          <li class="list-group-item row"   style="margin-left: 0px;margin-right: 0px;text-align: center;" ng-show="node.title=='Type' ||node.type=='radio' || node.type=='checkbox'||node.type=='select'&& node.title!='Nested Select'">
                 <input type="button" name="addOption" class="btn btn-default" value="Add Option" ng-click="addTypeOption(this)">
          </li>
       </ul>
      </div>
    </div>
  </div>
  <ol ui-tree-nodes="" ng-model="node.nodes" ng-class="{hidden: collapsed}">
    <li ng-repeat="node in node.nodes" ui-tree-node ng-include="'nodes_renderer2.html'">
    </li>
  </ol>
</script>
<div class="col-sm-11" style="border: 1px solid gainsboro;margin-left: 30px;" id="content1">
<div  class="col-sm-12" style="background-color: white;margin-top: 20px" id="category">
    <div class="col-sm-6">
    <h3>Drag Here</h3>
    </div>
    <div class="col-sm-5" style="line-height: 4">
        <button type="button" class="btn btn-primary btn-xs" ng-click="editFormValue()" style="float: right"><i class='fa fa-edit'>&nbsp;&nbsp;</i>Create</button>
    </div>
    <div ui-tree id="tree1-root" data-clone-enabled="true" data-nodrop-enabled="true" class="col-sm-12">
      <ol ui-tree-nodes="" ng-model="tree1" class="list-inline" style="margin-left: 40px;margin-bottom: 15px;">
        <li ng-repeat="node in tree1" ng-click="addToTree(node)" ui-tree-node ng-include="'nodes_renderer1.html'"></li>
      </ol>
    </div>
  </div>
  <div class="col-sm-12">
    <h3>Drop Here</h3>
    <div ui-tree id="tree2-root" data-clone-enabled="true" id="drp">
      <ol ui-tree-nodes="" ng-model="tree2" id="drp">
        <li ng-repeat="node in tree2" ui-tree-node ng-include="'nodes_renderer2.html'">
         
        </li>
      </ol>
    </div>
  </div>
</div>

        
        <!--end Code-->
    </div>
        </div>
    </div>
</div>
@stop
@section('FooterInclude')

@push('scripts')

<script src="{{asset("lb-faveo/js/sticky-Header/jquery.sticky.js")}}" type="text/javascript"></script>
<script>
  $(document).ready(function(){
    $("#category").sticky();
  });
</script>
<script>
$(function () {
   $('body').on('click', '[data-toggle=collapse]', function (e) {
        
       $(this).next().collapse('toggle');
});

});
/*$(document).ready(function() {
var stickyNavTop = parseInt($('#category').offset().top)-112;
console.log(stickyNavTop)
var stickyNav = function(){
var scrollTop = $(window).scrollTop();
 console.log(scrollTop)     
if (scrollTop > stickyNavTop) { 
     var width=$('#category').outerWidth(); 
    $('#category').addClass('sticky');
    $('#category').css('width',width);
} else {
    $('#category').removeClass('sticky'); 
}
};
 
stickyNav();
 
$(window).scroll(function() {
  stickyNav();
});
});
*/
</script>

<script>
(function () {
  'use strict';
  app.controller('CloningCtrl', ['$scope','$http', function ($scope,$http) {
    
         $scope.tree2 = [];
     
     $scope.node1=['submit','button'];
      $scope.remove = function (scope) {
        scope.remove();
      };
      $scope.addToTree = function (scope) {
          console.log(scope)
          $scope.tree2.push(scope);
      };
       $scope.clopse = function (scope) {
         console.log(scope);
         $(".collapse").collapse('toggle');
        };

      $scope.toggle = function (scope) {
        scope.toggle();
      };
      $scope.addOption = function (scope) {
          var nodeData = scope.$modelValue.options;
           nodeData.push({
              'optionvalue':'Value',
              'nodes':[]
           })
      };
      $scope.addTypeOption= function (scope) {
          var nodeData = scope.$modelValue.options;
           nodeData.push({
              'optionvalue':'Value'
           })
      };
      
      $scope.removeOption = function (scope,y) {
          var nodeData = scope.$modelValue.options;
           nodeData.splice(y,1);
      };
      
      $scope.editFormValue=function(){
          var title=document.getElementById('title').value;
          $http.post("{{url('service-desk/form-builder/post/create')}}/"+title,$scope.tree2).success(function(data){
            $scope.tree2=[];
        $('.well').css('display','block');      
        $('.well').html(data);
        $('.well').css('color','green');
         $('html, body').animate({scrollTop:0}, 500);
         setTimeout(function(){
             location.reload();
         },2000);      
          }).error(function(data){      
              $('html, body').animate({scrollTop:0}, 500);
              $('.well').css('display','block');      
              $('.well').html(data);
              $('.well').css('color','red');
          })
      }
      $scope.newSubForm=function(scope,x,y){
        var nodeData = scope.$modelValue.options[y];
       
       if(x=='Nested Select'){
        nodeData.nodes.push({
           'title': 'Nested Select',
           'label': 'Nested Select',
           'type': 'select',
           'placeholder':'',
           'value':'',
           'required':false,
           'options':[
              {
                 'optionvalue':'Value',
                 'nodes':[]
              }
            ]
          });
       }
      };


      $scope.tree1 = [{
        'title': 'Text Field',
        'label':'Text Field',
        'type':'text',
        'placeholder':'',
        'name':'',
        'required':false,
        'value':''
      }, {
        'title': 'Text Area',
        'label':'Text Area',
        'type':'textarea',
        'placeholder':'',
        'required':false,
        'name':'',
        'value':''
      }, {
        'title': 'Email',
        'label':'Email',
        'type':'email',
        'placeholder':'',
        'required':false,
        'name':'',
        'value':''
      },{
        'title': 'Number',
        'label':'Number',
        'type':'number',
        'placeholder':'',
        'required':false,
        'name':'',
        'value':''
      },{
        'title': 'Nested Select',
        'label': 'Nested Select',
        'type':'select',
        'placeholder':'select',
        'required':false,
        'name':'',
        'value':'',
        'options':[
           {
              'optionvalue':'Value',
              'nodes':[]
           }
        ]
      },{
        'title': 'Select',
        'label': 'Select',
        'type':'select',
        'placeholder':'select',
        'required':false,
        'name':'',
        'value':'',
        'options':[
           {
              'optionvalue':'Value',
           }
        ]
      },
      {
        'title': 'Radio',
        'label':'Radio',
        'type':'radio',
        'name':'',
        'value':'',
        'required':false,
        'options':[
           {
              'optionvalue':'Value'
           }
        ]
      }, {
        'title': 'Checkbox',
        'label':'Checkbox',
        'type':'checkbox',
        'name':'',
        'value':'',
        'required':false,
        'options':[
           {
              'optionvalue':'Value'
           }
        ]
      },{
        'title': 'Date',
        'label':'Date',
        'type':'date',
        'name':'',
        'required':false,
        'value':''
      }
      ];
      
    }]);

   
})();

</script>

@endpush