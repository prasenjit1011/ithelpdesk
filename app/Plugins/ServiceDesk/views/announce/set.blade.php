@extends('themes.default1.admin.layout.admin')
@section('content')
<section class="content-header">
    <h1> Announcement </h1>

</section>
<div class="box box-primary">
  {!! Form::open(['url'=>'service-desk/announcement','method'=>'post','id'=>'from','id'=>'Form']) !!}
    <div class="box-header with-border">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('success')}}
        </div>
        @endif
        <!-- fail message -->
        @if(Session::has('fails'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <b>{{Lang::get('message.alert')}}!</b> {{Lang::get('message.failed')}}.
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('fails')}}
        </div>
        @endif
      
    </div><!-- /.box-header -->
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">

            <div class="col-md-6">
                <p>{!! Form::label('option','Choose any option') !!}</p>
                <div class="col-md-3">
                    <p> {!! Form::radio('option','organization',['class'=>'option']) !!} Organization</p>
                </div>
                <div class="col-md-3">
                    <p> {!! Form::radio('option','department',['class'=>'option']) !!} Department</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="organization">
                {!! Form::label('organization','Organization') !!}
                {!! Form::select('organization',[''=>'Select','Organizations'=>$organization],null,['class'=>'form-control']) !!}
            </div>
              <div class="form-group col-md-6 {{ $errors->has('department') ? 'has-error' : '' }}" id="department">
           
                {!! Form::label('department','Department') !!}<span class="text-red"> *</span>
                {!! Form::select('department',[''=>'Select','Department'=>$departments],null,['class'=>'form-control']) !!}
            </div>

        </div>

        <div>
            {!! Form::label('announcement','Announcement') !!}<span class="text-red"> *</span>
            {!! Form::textarea('announcement',null,['class'=>'form-control']) !!}
        </div>
        <!-- /.box-body -->
    </div>
    <div class="box-footer">
<!--        {!! Form::submit('Send',['class'=>'btn btn-primary']) !!}-->
       <button type="submit"id="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'>&nbsp;</i> Sending..."><i class="fa fa-paper-plane">&nbsp;&nbsp;</i>{!!Lang::get('lang.send')!!}</button>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
</div>
@stop
