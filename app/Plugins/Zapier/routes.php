<?php
/**
 * Auth route
 */
Route::group(['middleware' => ['web', 'auth', 'roles'], 'prefix' => 'zapier'], function() {
    Route::get('/settings',['as'=>'zapier.settings','uses'=>'App\Plugins\Zapier\Controllers\Core\SettingsController@settings']);
    Route::post('/activate/{app}',['as'=>'zapier.settings.post','uses'=>'App\Plugins\Zapier\Controllers\Core\SettingsController@activateIntegration']);
    Route::get('create-user/hook',function(){
        return response('success', 200);
    });
});

/**
 * Non auth routes
 */
Route::group(['middleware' => ['web'], 'prefix' => 'zapier'], function() {
    Route::post('/',['as'=>'zapier.post','uses'=>'App\Plugins\Zapier\Controllers\Core\ZapierController@zapier']);
    
    
});

