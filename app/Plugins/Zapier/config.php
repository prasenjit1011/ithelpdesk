<?php

return [
    'name' => 'Zapier',
    'description' => 'Faveo Zapier is a plugin to provide Zapier integration',
    'author' => 'ladybird',
    'website' => 'www.ithelpdesk.com',
    'version' => '1.0.0',
    'settings' => 'zapier/settings',
    
];

