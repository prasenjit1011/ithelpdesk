<?php

namespace App\Plugins\Zapier\Controllers\Mailchimp;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class MailchimpController extends Controller {
    
    public function hook(Request $request){
        \Log::info('mailchimp',$request->all());
    }
    
}