<?php
return [
       // Licenses 
     'application' => 'Application',
     'action'=>'Action',
     'whitelabel'=>'WhiteLabel',
     'on'=>'ON',
     'off'=>'OFF',
     'your_status_updated'=>'Your Status Updated',
];
