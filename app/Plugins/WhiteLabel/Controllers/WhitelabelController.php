<?php

namespace App\Plugins\WhiteLabel\Controllers;



use Illuminate\Database\Schema\Blueprint;
use Schema;
// use Exception;

// controllers


use App\Http\Controllers\Controller;
// requests
use App\Plugins\Licenses\Requests\LicensesTypeRequest;
/* include organization model */
use App\Http\Requests\helpdesk\OrganizationUpdate;
// models
/* Define OrganizationRequest to validate the create form */
use App\Plugins\Licenses\Model\Licenses;
use App\Model\helpdesk\Agent_panel\Organization;
use App\Model\helpdesk\Agent_panel\User_org;
use App\Model\helpdesk\Agent_panel\OrgAttachment;
use App\Model\helpdesk\Manage\Sla\Sla_plan;
use App\Model\helpdesk\Settings\Plugin;
/* Define OrganizationUpdate to validate the create form */
use App\User;
// classes
use Exception;
use Lang;
use Illuminate\Http\Request;
use Datatables;
use Input;
use DB;
use File;
use Auth;

class WhitelabelController extends Controller {

    
   

    /**
     * Show the form for creating a new organization.
     *
     * @return type Response
     */
    public function apply() {
        // try {
// 
       
$status_check=Plugin::where('name','=','WhiteLabel')->select('status')->first();

if($status_check->status==1){
//  echo view('Licenses::licenses')->render(); 
    echo view('WhiteLabel::index')->render(); 
}

    }

 public function licensesIndex1() {
        try {
            $licenses = new Licenses();
            $license = $licenses->select('id', 'licenses_type', 'licenses_key', 'activated_on', 'start_date', 'expires_on', 'comments');
           
            return \Datatable::query($license)
                            ->addColumn('licenses_type', function($model) {
                                if (strlen($model->licenses_type) > 12) {
                                    return '<p title="' . $model->licenses_type . '">' . mb_substr($model->licenses_type, 0, 20, 'UTF-8') . '...</p>';
                                } else {
                                    return $model->licenses_type;
                                }
                            })
                            ->showColumns('licenses_key', 'activated_on', 'start_date', 'expires_on')
                            ->addColumn('action', function($model) {

                                return "<a href=" . url('licenses/' . $model->id . '/edit') . " class='btn btn-info btn-xs'>Edit</a>&nbsp;<a class='btn btn-danger btn-xs' onclick='confirmDelete(" . $model->id . ")'>Delete </a>";
                            })
                            ->searchColumns('name')
                            ->orderColumns('name')
                            ->make();
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    /**
     * Show the form for creating a new organization.
     *
     * @return type Response
     */
    public function create($org_id) {

try {
  $org_name=Organization::where('id','=',$org_id)->select('name')->first();
  echo view('licenses::create', compact('org_id','org_name'))->render(); 
 // echo view('licenses::create')->render(); 

        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }



    public function licensesCreate1(LicensesTypeRequest $request) {
        try {

            $org_name= $request->org_name;
            $org_id=Organization::where('name','=',$org_name)->select('id')->first();
           
            $licenses = new Licenses;
            $licenses->licenses_type = $request->licenses_type;
            $licenses->org_id = $org_id->id;
            $licenses->licenses_key = $request->licenses_key;
            $licenses->activated_on = $request->activated_on;
            $licenses->start_date = $request->start_date;
            $licenses->expires_on = $request->expires_on;
            $licenses->comments = $request->comments;
            $licenses->save();

            // return back()->with('success11', (Lang::get('lang.licenses_successfully_created')));
            return redirect()->route('organizations.show', $org_id->id)->with('success1', Lang::get('Licenses::lang.licenses_successfully_created'));
   
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    /**
     * Random Password Genetor for users
     *
     * @param type int  $id
     * @param type User $user
     *
     * @return type view
     */
    public function licensesKey() {
        try {
            $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            $licenses_Key = array(); //remember to declare $pass as an array
            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
            for ($i = 0; $i < 15; $i++) {
                $n = rand(0, $alphaLength);
                $licenses_Key[] = $alphabet[$n];
            }

            return implode($licenses_Key);
        } catch (Exception $e) {
            /* redirect to Index page with Fails Message */
           return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    /**
     * 
     * @param type $priority_id
     * @return type
     */
    public function licensesEdit($id) {

        try {


            // dd($id);
     $licenses = Licenses::whereid($id)->first();
     $org_name=Organization::where('id','=',$licenses->org_id)->select('name')->first();
// dd($licenses);
  // echo view('licenses::edit', compact('licenses','org_name'))->render(); 

            return view('licenses::edit', compact('licenses','org_name'));
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    /**
     * 
     * @param PriorityRequest $request
     * @return type
     */
    public function licensesEdit1(LicensesTypeRequest $request) {
        try {

              $org_name= $request->org_name;
            $org_id=Organization::where('name','=',$org_name)->select('id')->first();
            $id = $request->licenses_id;
            $licenses = Licenses::findOrFail($id);
            $licenses->licenses_type = $request->licenses_type;
            $licenses->org_id = $org_id->id;
            $licenses->licenses_key = $request->licenses_key;
            $licenses->activated_on = $request->activated_on;
            $licenses->start_date = $request->start_date;
            $licenses->expires_on = $request->expires_on;
            $licenses->comments = $request->comments;
            $licenses->save();


            return redirect()->route('organizations.show', $org_id->id)->with('success1', Lang::get('Licenses::lang.licenses_successfully_edit'));

            // return redirect()->back()->with('success11', (Lang::get('lang.licenses_successfully_edit')));
            // return redirect('organizations')->with('success', Lang::get('lang.licenses_successfully_edit'));
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    /**
     * 
     * @param type $priority_id
     * @return type
     */
    public function destroyLicenses($id) {
        try {


            $licenses = Licenses::findOrFail($id);
            $licenses->delete();
           return redirect()->back()->with('success1', (Lang::get('Licenses::lang.licenses_delete_successfully')));
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }
   
}
