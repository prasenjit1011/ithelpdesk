<?php

return  [
    'name' =>'WhiteLabel',
    'description'=>'Faveo White Label plugin provides an option to remove powered by Faveo from the system
.',
	'author'=>'ladybird',
	'website'=>'www.ithelpdesk.com',
	'version'=>'1.0.0',
	'settings'=>'WhiteLabel/settings'
    
];

