@extends('themes.default1.agent.layout.agent')
@section('Staffs')
active
@stop

@section('staffs-bar')
active
@stop

@section('report')
class="active"
@stop

<!-- header -->
@section('PageHeader')
<h1>{!! Lang::get('report::lang.satisfaction-survey') !!}</h1>
@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">
</ol>
@stop
@section('content')
<div class="box box-primary">
    <div class="box-header">
        <button class="btn btn-primary" onclick="showhidefilter();">{!! Lang::get('report::lang.filter') !!}</button>
        @include('report::indepth.filter')
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="btn-group pull-right">
                    <button class="btn btn-default type" value="ticket">{!! Lang::get('report::lang.ticket') !!}</button>
                    <button class="btn btn-default type" value="thread">{!! Lang::get('report::lang.thread') !!}</button>
                </div>
                <canvas id="rating"></canvas>
            </div>
        </div><br>
    </div>
</div>
@stop
@section('FooterInclude')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.js"></script>
<script>
var options_bar = {
    scales: {
        xAxes: [{
            stacked: true,
            display: false,
        }],
        yAxes: [{
            stacked: true,
            barThickness: 10
        }],
    },
    responsive: true,
    maintainAspectRatio: true,
};
var data = $("#filter").serialize();
thisAjax(data,'ticket');

$(".type").on('click',function(){
    var type = $(this).attr("value");
    thisAjax(data,type);
});

function thisAjax(data,rating_type) {
  $.ajax({
        type: "GET",
        dataType: "json",
        url: "{{url('report/rating/get')}}",
        data: data+"&rating_type="+rating_type,
        success: function (data) {
            var ctx = $("#rating").get(0).getContext("2d");
            var myBarChart = new Chart(ctx,
            {
                type: 'horizontalBar',
                data: data,
                options: options_bar
            });
        },
        error: function (error) {
            alert('error');
        }
    });
}
$("#submit-filter").click(function () {
    var data = $("#filter").serialize();
    var type = $(this).attr("value");
    thisAjax(data,type);
});

</script>
@stop

