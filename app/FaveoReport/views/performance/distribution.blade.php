@extends('themes.default1.agent.layout.agent')
@section('Staffs')
active
@stop

@section('staffs-bar')
active
@stop

@section('report')
class="active"
@stop

<!-- header -->
@section('PageHeader')
<h1>{!! Lang::get('report::lang.performance-distribution') !!}</h1>
@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">
</ol>
@stop
@section('content')
<div class="box box-primary">
    <div class="box-header">
        <button class="btn btn-primary" onclick="showhidefilter();">{!! Lang::get('report::lang.filter') !!}</button>
        @include('report::indepth.filter')
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-4" style="height: 300px;">
                <canvas id="first-response-time"></canvas>
            </div>
            <div class="col-md-4" style="height: 300px;">
                <canvas id="avg-response-time"></canvas>
            </div>
            <div class="col-md-4" style="height: 300px;">
                <canvas id="resolution-time"></canvas>
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-12">
                <div class="box-header">
                    <h3>{!! Lang::get('report::lang.avg-first-response-and-response-time-trend') !!}</h3>
                </div>
                <div class="btn-group pull-right">
                    <button class="btn btn-default period" value="day">{!! Lang::get('report::lang.day') !!}</button>
                    <button class="btn btn-default period" value="week">{!! Lang::get('report::lang.week') !!}</button>
                    <button class="btn btn-default period" value="month">{!! Lang::get('report::lang.month') !!}</button>
                    <button class="btn btn-default period" value="year">{!! Lang::get('report::lang.year') !!}</button>
                </div>
            </div>
            <div class="col-md-12">
                <canvas id="firstresponse-response-trend"></canvas>
            </div>
        </div>
    </div>
</div>
@stop
@section('FooterInclude')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.js"></script>
<script>
            var options_bar = {
            scales: {
            xAxes: [{
            stacked: true,
                    display: false,
            }],
                    yAxes: [{
                    stacked: true,
                            barThickness: 10
                    }],
            },
                    responsive: true,
                    maintainAspectRatio: false,
            };
                    /**
                     * To draw a graph using ajax
                     * 
                     * @param string type
                     * @param string active_id
                     * @param string canvas
                     * @returns graph
                     */
                    thisAjax("#first-response-time", data = "");
                    function thisAjax(canvas, data = "") {
                    $.ajax({
                    type: "GET",
                            dataType: "json",
                            url: "{{url('report/performance/first_response')}}",
                            data: data,
                            success: function (data) {
                            var ctx = $(canvas).get(0).getContext("2d");
                                    var myBarChart = new Chart(ctx,
                                    {
                                    type: 'horizontalBar',
                                            data: data.chart,
                                            options: options_bar
                                    });
                            },
                            error: function (error) {
                            alert('error');
                            }
                    });
                    }

            avgResponseAjax("#avg-response-time", data = "");
                    function avgResponseAjax(canvas, data = "") {
                    $.ajax({
                    type: "GET",
                            dataType: "json",
                            url: "{{url('report/performance/avg_response')}}",
                            data: data,
                            success: function (data) {
                            var ctx = $(canvas).get(0).getContext("2d");
                                    var myBarChart = new Chart(ctx,
                                    {
                                    type: 'horizontalBar',
                                            data: data.chart,
                                            options: options_bar
                                    });
                            },
                            error: function (error) {
                            alert('error');
                            }
                    });
                    }

            resolutionAjax("#resolution-time", data = "");
                    function resolutionAjax(canvas, data = "") {
                    $.ajax({
                    type: "GET",
                            dataType: "json",
                            url: "{{url('report/performance/avg_resolution')}}",
                            data: data,
                            success: function (data) {
                            var ctx = $(canvas).get(0).getContext("2d");
                                    var myBarChart = new Chart(ctx,
                                    {
                                    type: 'horizontalBar',
                                            data: data.chart,
                                            options: options_bar
                                    });
                            },
                            error: function (error) {
                            alert('error');
                            }
                    });
                    }
            responseTrends('day', "#firstresponse-response-trend", data = "");
                    $('.period').click(function () {
            period = $(this).attr("value");
                    var data = $("#filter").serialize();
                    responseTrends(period, "#firstresponse-response-trend", data = "");
            });
                    function responseTrends(period, canvas, data = "") {
                    $.ajax({
                    type: "GET",
                            dataType: "json",
                            url: "{{url('report/performance/first_response_response_trend')}}",
                            data: "period=" + period + "&" + data,
                            success: function (data) {
                            var ctx = $(canvas).get(0).getContext("2d");
                                    var myBarChart = new Chart(ctx,
                                    {
                                    type: 'line',
                                            data: data.data,
                                            options: data.option


                                    });
                            },
                            error: function (error) {
                            var ctx = $(canvas).get(0).getContext("2d");
                                    var myBarChart = new Chart(ctx,
                                    {
                                    type: 'line',
                                            data: error.textResponse,
                                            option: options_line

                                    });
                            }
                    });
                    }
</script>
@stop

