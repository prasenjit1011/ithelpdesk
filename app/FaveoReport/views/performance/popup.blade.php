<button class="btn btn-primary pull-right" id="timesheet_export_submit">{!! Lang::get('lang.export') !!}</button>
@push('scripts')
<script type="text/javascript">
    $("#timesheet_export_submit").on('click', function () {
        var data = $("#filter").serialize();
        $.ajax({
            url: "{{url('report/timesheet/export')}}",
            data: data,
            type: "post",
            success: function (data) {
                if (data === 'success') {
                    window.location = "{{url('report/download/csv')}}";
                }else{
                    alert(data);
                }
            }
        });
    });
</script>
@endpush
