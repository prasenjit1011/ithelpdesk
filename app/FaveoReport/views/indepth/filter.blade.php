<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<div id="filter-box" style="display: none;">
    <br><div class="box box-primary">
        <form id="filter">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <label>{!! Lang::get('report::lang.date-range') !!}</label><br>
                        <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                            <span></span> <b class="caret"></b>
                        </div>
                        <input type="hidden" name="start_date" id="start_date">
                        <input type="hidden" name="end_date" id="end_date">
                    </div>
                    <div class="col-md-4">
                        <label>{!! Lang::get('lang.agents') !!}</label><br>
                        <select class="agents-ajax form-control" style="width: 100%" multiple="multiple" name="agents[]" id="agents">
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label>{!! Lang::get('lang.departments') !!}</label><br>
                        <select class="departments-ajax form-control" style="width: 100%" multiple="multiple" name="departments[]" id="departments">
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <label>{!! Lang::get('lang.source') !!}</label><br>
                        <select class="sources-ajax form-control" style="width: 100%" multiple="multiple" name="sources[]" id="sources">
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label>{!! Lang::get('lang.priority') !!}</label><br>
                        <select class="priorities-ajax form-control" style="width: 100%" multiple="multiple" name="priorities[]" id="priorities">
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label>{!! Lang::get('lang.client') !!}</label><br>
                        <select class="clients-ajax form-control" style="width: 100%" multiple="multiple" name="clients[]" id="clients">
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>{!! Lang::get('lang.type') !!}</label><br>
                        <select class="types-ajax form-control" style="width: 100%" multiple="multiple" name="types[]" id="types">
                        </select>
                    </div>
                </div>
            </div>
        </form>
        <div class="box-footer">
            <!--<div class="pull-right">-->
            <button class="btn btn-primary" id="submit-filter">{!! Lang::get('report::lang.generate') !!}</button>
            <button class="btn btn-default pull-right" id="close">{!! Lang::get('report::lang.close') !!}</button>
        </div>
    </div>
</div>

@push('scripts')
<script>
    $("#close").on('click', function () {
        $("#filter-box").hide();
    });
    select('agents');
    select('departments');
    select('clients');
    select('sources');
    select('priorities');
    select('types');
    function select(api) {
        $("." + api + "-ajax").select2({
            ajax: {
                url: "{{url('report/api/get')}}" + '/' + api,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    console.log(data);
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 1,
        });
    }
    function showhidefilter()
    {
        var div = document.getElementById("filter-box");
        if (div.style.display !== "none") {
            div.style.display = "none";
        } else {
            div.style.display = "block";
        }
    }
    $(function () {

        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('#start_date').val(start.format('YYYY-MM-DD'));
            $('#end_date').val(end.format('YYYY-MM-DD'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            maxDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        },cb);
        cb(start, end);

    });
</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

@endpush

