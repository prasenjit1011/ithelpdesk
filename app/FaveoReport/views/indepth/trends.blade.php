@extends('themes.default1.agent.layout.agent')
@section('Staffs')
active
@stop

@section('staffs-bar')
active
@stop

@section('report')
class="active"
@stop

<!-- header -->
@section('PageHeader')
<h1>{!! Lang::get('report::lang.ticket-volume-trends') !!}</h1>
@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">
</ol>
@stop
@section('content')
<style>
    .box-with-left-bar {
        color: rgba(99, 99, 99, 0.86);
        
    }
    .received-tickets {
     border-left: 1px solid #ffa1b5;   
    }
    .received-tickets label {
     color: #ffa1b5;   
    }
    .resolved-tickets {
     border-left: 1px solid rgb(135, 199, 243);   
    }
    .resolved-tickets label{
     color: rgb(135, 199, 243);   
    }
    .unresolved-tickets {
     border-left: 1px solid #ffe199;   
    }
    .unresolved-tickets label{
     color: #ffe199;   
    }
</style>
<div class="box box-primary">
    <div class="box-header">
        <button class="btn btn-primary" onclick="showhidefilter();">{!! Lang::get('report::lang.filter') !!}</button>
        @include('report::indepth.filter')
    </div>
    <div class="box-body">

        <div class="row">
            <div class="col-md-12" id="created-resolved-unsolved-parent">
                <div class="box-header">
                    <h3>{!! Lang::get('report::lang.trend-of-tickets-received-and-resolved-and-unresolved') !!}</h3>

                    <br>
                    <div class="col-md-2 box-with-left-bar received-tickets">
                        {!! Lang::get('report::lang.total-received-tickets') !!}<br><label><span id="recieved_total"></span></label>
                    </div>
                    <div class="col-md-2 box-with-left-bar resolved-tickets">
                        {!! Lang::get('report::lang.total-resolved-tickets') !!}<br><label><span id="resolved_total"></span></label>
                    </div>
                    <div class="col-md-2 box-with-left-bar unresolved-tickets">
                        {!! Lang::get('report::lang.total-unresolved-tickets') !!}<br><label><span id="unresolved_total"></span></label>
                    </div>
                    <div class="col-md-2 box-with-left-bar received-tickets">
                        {!! Lang::get('report::lang.average-received-tickets') !!}<br><label><span id="recieved_avg"></span></label>
                    </div>
                    <div class="col-md-2 box-with-left-bar resolved-tickets">
                        {!! Lang::get('report::lang.average-resolved-tickets') !!}<br><label><span id="resolved_avg"></span></label>
                    </div>
                    <div class="col-md-2 box-with-left-bar unresolved-tickets">
                        {!! Lang::get('report::lang.average-unresolved-tickets') !!}<br><label><span id="unresolved_avg"></span></label>
                    </div>
                </div>
                <br>
                <div class="btn-group pull-right">
                    <button class="btn btn-default period" value="day">{!! Lang::get('report::lang.day') !!}</button>
                    <button class="btn btn-default period" value="week">{!! Lang::get('report::lang.week') !!}</button>
                    <button class="btn btn-default period" value="month">{!! Lang::get('report::lang.month') !!}</button>
                    <button class="btn btn-default period" value="year">{!! Lang::get('report::lang.year') !!}</button>
                </div>
                <canvas id="created-resolved-unsolved"></canvas>
            </div>

        </div><br>
        <div class="row">
            <div class="col-md-12" id="analysis-parent">
                <div class="box-header">
                    <h3>{!! Lang::get('report::lang.load-analysis') !!}</h3>
                </div>
                <div class="btn-group pull-right">
                    <button class="btn btn-default load-period" value="day">{!! Lang::get('report::lang.day') !!}</button>
                    <button class="btn btn-default load-period" value="week">{!! Lang::get('report::lang.week') !!}</button>
                    <button class="btn btn-default load-period" value="month">{!! Lang::get('report::lang.month') !!}</button>
                    <button class="btn btn-default load-period" value="year">{!! Lang::get('report::lang.year') !!}</button>
                </div>
                <canvas id="analysis"></canvas>
            </div>

        </div><br>

        <div class="row">
            <div class="col-md-12" id="day-parent">
                <div class="box-header">
                    <div>
                        <h3>{!! Lang::get('report::lang.day-of-the-week-and-hour-of-the-day-trend') !!}</h3>
                        <div class="col-md-3 box-with-left-bar received-tickets">
                            {!! Lang::get('report::lang.most-tickets-were-received-around') !!}<br><label><span id="recived_max_hours"></span></label>
                        </div>
                        <div class="col-md-3 box-with-left-bar resolved-tickets">
                            {!! Lang::get('report::lang.most-tickets-were-resolved-around') !!}<br><label><span id="resolved_max_hours"></span></label>
                        </div>
                        <div class="col-md-3 box-with-left-bar received-tickets">
                            {!! Lang::get('report::lang.most-tickets-were-received-on') !!}<br><label><span id="recived_max_days"></span></label>
                        </div>
                        <div class="col-md-3 box-with-left-bar resolved-tickets">
                            {!! Lang::get('report::lang.most-tickets-were-resolved on') !!}<br><label><span id="resolved_max_days"></span></label>
                        </div>
                    </div>
                    <div class="btn-group pull-right" style="margin-top: 20px;">
                        <button class="btn btn-default day" value="Mon">{!! Lang::get('report::lang.Mon') !!}</button>
                        <button class="btn btn-default day" value="Tue">{!! Lang::get('report::lang.Tue') !!}</button>
                        <button class="btn btn-default day" value="Wed">{!! Lang::get('report::lang.Wed') !!}</button>
                        <button class="btn btn-default day" value="Thu">{!! Lang::get('report::lang.Thu') !!}</button>
                        <button class="btn btn-default day" value="Fri">{!! Lang::get('report::lang.Fri') !!}</button>
                        <button class="btn btn-default day" value="Sat">{!! Lang::get('report::lang.Sat') !!}</button>
                        <button class="btn btn-default day" value="Sun">{!! Lang::get('report::lang.Sun') !!}</button>
                    </div>
                </div>
                <canvas id="day"></canvas>
            </div>

        </div><br>
    </div>
</div>

@stop
@section('FooterInclude')
<script src="{{asset("lb-faveo/js/jquery.ui.js")}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.js"></script>
<script>
                    var data = $("#filter").serialize();
                    thisAjax('', 'created-ticket', "#created-resolved-unsolved", data);
                    $('.period').click(function () {
            period = $(this).attr("value");
                    var data = $("#filter").serialize();
                    console.log(data);
                    thisAjax("", 'created-ticket', "#created-resolved-unsolved", data, period);
            });
                    thisAjaxAnalysis('', 'created-ticket', "#analysis", data);
                    $('.load-period').click(function () {
            period = $(this).attr("value");
                    var data = $("#filter").serialize();
                    thisAjaxAnalysis("", 'created-ticket', "#analysis", data, period);
            });
                    thisAjaxDay('Mon', 'created-ticket', "#day", data);
                    $("#submit-filter").click(function () {
            var data = $("#filter").serialize();
                    thisAjax('', 'created-ticket', "#created-resolved-unsolved", data);
                    thisAjaxAnalysis('', 'created-ticket', "#analysis", data);
                    thisAjaxDay('Mon', 'created-ticket', "#day", data);
            });
                    $('.day').click(function () {
            period = $(this).attr("value");
                    var data = $("#filter").serialize();
                    thisAjaxDay(period, 'created-ticket', "#day", data);
            });
                    var options = {
                    scales: {
                    xAxes: [{
                    stacked: false,
                            display: true,
                    }],
                            yAxes: [{
                            stacked: false,
                                    barThickness: 10
                            }],
                    },
                   
                    };
                    var options_analysis = {
                    scales: {
                    xAxes: [{
                    stacked: true,
                            display: true,
                    }],
                            yAxes: [{
                            stacked: true,
                                    barThickness: 10
                            }],
                    }
                    };
                    function thisAjaxAnalysis(type, active_id, canvas, data = "", period = 'week') {
                    $.ajax({
                    type: "GET",
                            dataType: "json",
                            url: "{{url('report/trends/analysis/get')}}",
                            data: "group=" + type + "&active=" + active_id + "&" + data + "&period=" + period,
                            success: function (data) {
                                $("#analysis").remove();
                        $('#analysis-parent').append('<canvas id="analysis"><canvas>');
                        
                            var ctx = $(canvas).get(0).getContext("2d");
                                
                                    var myBarChart = new Chart(ctx,
                                    {
                                    type: 'bar',
                                            data: data,
                                            options: options_analysis
                                    });
                            },
                            error: function (error) {
                            var ctx = $(canvas).get(0).getContext("2d");
                                    var myBarChart = new Chart(ctx,
                                    {
                                    type: 'bar',
                                            data: error.textResponse,
                                            options: options_analysis
                                    });
                            }
                    });
                    }

            function thisAjax(type, active_id, canvas, values = "", period = "week") {
            $.ajax({
            type: "GET",
                    dataType: "json",
                    url: "{{url('report/trends/get')}}",
                    data: "group=" + type + "&active=" + active_id + "&" + values + "&period=" + period,
                    success: function (data) {
                        $("#created-resolved-unsolved").remove();
                        $('#created-resolved-unsolved-parent').append('<canvas id="created-resolved-unsolved"><canvas>');
                        var ctx = $(canvas).get(0).getContext("2d");
                        var myBarChart = new Chart(ctx,
                            {
                            type: 'bar',
                                data: data.data,
                                options: options
                            });
                           
                            var recieved_total = data.count.recieved_total;
                            var resolved_total = data.count.resolved_total;
                            var unresolved_total = data.count.unresolved_total;
                            var recieved_avg = data.count.recieved_avg;
                            var resolved_avg = data.count.resolved_avg;
                            var unresolved_avg = data.count.unresolved_avg;
                            $("#recieved_total").html(recieved_total);
                            $("#resolved_total").html(resolved_total);
                            $("#unresolved_total").html(unresolved_total);
                            $("#recieved_avg").html(recieved_avg);
                            $("#resolved_avg").html(resolved_avg);
                            $("#unresolved_avg").html(unresolved_avg);
                    },
                    error: function (error) {
                    var ctx = $(canvas).get(0).getContext("2d");
                            var myBarChart = new Chart(ctx,
                            {
                            type: 'bar',
                                    data: error.textResponse,
                                    options: options
                            });
                    }
            });
            }

            function thisAjaxDay(day, active_id, canvas, data = "") {
            $.ajax({
            type: "GET",
                    dataType: "json",
                    url: "{{url('report/trends/analysis/hour')}}",
                    data: "day=" + day + "&active=" + active_id + "&" + data,
                    success: function (data) {
                        $("#day").remove();
                        $('#day-parent').append('<canvas id="day"><canvas>');
                    var ctx = $(canvas).get(0).getContext("2d");
                        
                            var myBarChart = new Chart(ctx,
                            {
                            type: 'line',
                                    data: data.data,
                                    options: data.option


                            });
                            var recived_max_hours = data.count.recived_max_hours;
                            var resolved_max_hours = data.count.resolved_max_hours;
                            var recived_max_days = data.count.recived_max_days;
                            var resolved_max_days = data.count.resolved_max_days;
                            $("#recived_max_hours").html(recived_max_hours);
                            $("#resolved_max_hours").html(resolved_max_hours);
                            $("#recived_max_days").html(recived_max_days);
                            $("#resolved_max_days").html(resolved_max_days);
                    },
                    error: function (error) {
                    var ctx = $(canvas).get(0).getContext("2d");
                            var myBarChart = new Chart(ctx,
                            {
                            type: 'line',
                                    data: error.textResponse,
                                    option: options_line

                            });
                    }
            });
            }
</script>


@stop

