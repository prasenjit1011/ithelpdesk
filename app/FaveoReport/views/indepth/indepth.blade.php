@extends('themes.default1.agent.layout.agent')
@section('Staffs')
active
@stop

@section('staffs-bar')
active
@stop

@section('report')
class="active"
@stop

<!-- header -->
@section('PageHeader')
<h1>{!! Lang::get('report::lang.helpdesk-in-depth') !!}</h1>
@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">
</ol>
@stop
@section('content')

<style>
    /*  bhoechie tab */
    div.bhoechie-tab-container{
        z-index: 10;
        background-color: #ffffff;
        padding: 0 !important;
        border-radius: 4px;
        -moz-border-radius: 4px;
        border:1px solid #ddd;
        margin-top: 20px;
        margin-left: 50px;
        -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        box-shadow: 0 6px 12px rgba(0,0,0,.175);
        -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        background-clip: padding-box;
        opacity: 0.97;
        filter: alpha(opacity=97);
    }
    div.bhoechie-tab-menu{
        padding-right: 0;
        padding-left: 0;
        padding-bottom: 0;
    }
    div.bhoechie-tab-menu div.list-group{
        margin-bottom: 0;
    }
    div.bhoechie-tab-menu div.list-group>a{
        margin-bottom: 0;
    }
    div.bhoechie-tab-menu div.list-group>a .glyphicon,
    div.bhoechie-tab-menu div.list-group>a .fa {
        color: #5A55A3;
    }
    div.bhoechie-tab-menu div.list-group>a:first-child{
        border-top-right-radius: 0;
        -moz-border-top-right-radius: 0;
    }
    div.bhoechie-tab-menu div.list-group>a:last-child{
        border-bottom-right-radius: 0;
        -moz-border-bottom-right-radius: 0;
    }
    div.bhoechie-tab-menu div.list-group>a.active,
    div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
    div.bhoechie-tab-menu div.list-group>a.active .fa{
        background-color: #5A55A3;
        background-image: #5A55A3;
        color: #ffffff;
    }
    div.bhoechie-tab-menu div.list-group>a.active:after{
        content: '';
        position: absolute;
        left: 100%;
        top: 50%;
        margin-top: -13px;
        border-left: 0;
        border-bottom: 13px solid transparent;
        border-top: 13px solid transparent;
        border-left: 10px solid #5A55A3;
    }

    div.bhoechie-tab-content{
        background-color: #ffffff;
        /* border: 1px solid #eeeeee; */
        padding-left: 20px;
        padding-top: 10px;
    }

    div.bhoechie-tab div.bhoechie-tab-content:not(.active){
        display: none;
    }

</style>
<script>
    $(document).ready(function () {
        $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
    });
</script>
<div class="box box-primary">
    <div class="box-header">
        <button class="btn btn-primary" onclick="showhidefilter();">{!! Lang::get('report::lang.filter') !!}</button>
        @include('report::indepth.filter')
    </div>
    <div class="box-body" id="box-body">
        <div class="row">
            <div class="col-sm-1 col-md-11 col-sm-1 col-xs-12">
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-4 bhoechie-tab-menu">
                    <div class="list-group" id="lists">
                        <a href="#" class="list-group-item active text-center" id="created-ticket">
                            <span></span> <br>{!! Lang::get('report::lang.created-tickets') !!}
                        </a>
                        <a href="#" class="list-group-item text-center" id="resolved-ticket">
                            <span></span> <br>{!! Lang::get('report::lang.resolved-tickets') !!}
                        </a>
                        <a href="#" class="list-group-item text-center" id="unresolved-ticket">
                            <span></span> <br>{!! Lang::get('report::lang.unresolved-tickets') !!}
                        </a>
                        <a href="#" class="list-group-item text-center" id="reopened-ticket">
                            <span></span> <br>{!! Lang::get('report::lang.reopened-tickets') !!}
                        </a>
                        <a href="#" class="list-group-item text-center" id="avg-first-response">
                            <span></span> <br>{!! Lang::get('report::lang.average-first-response-time') !!}
                        </a>
                        <a href="#" class="list-group-item text-center" id="avg-response">
                            <span></span> <br>{!! Lang::get('report::lang.average-response-time') !!}
                        </a>
                        <a href="#" class="list-group-item text-center" id="avg-resolution">
                            <span></span> <br>{!! Lang::get('report::lang.average-resolution-time') !!}
                        </a>
                        <a href="#" class="list-group-item text-center" id="first-contact-resolution">
                            <span></span> <br>{!! Lang::get('report::lang.first-contact-resolution') !!}
                        </a>
                        <a href="#" class="list-group-item text-center" id="response-sla">
                            <span></span> <br>{!! Lang::get('report::lang.first-response-sla') !!} 
                        </a>
                        <a href="#" class="list-group-item text-center" id="resolve-sla">
                            <span></span> <br>{!! Lang::get('report::lang.resolution-sla') !!}
                        </a>

                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                    <!-- Created Tickets -->
                    <div class="bhoechie-tab-content active" id="created-ticket">
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="created-ticket-status"></canvas>
                            </div>
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="created-ticket-priority"></canvas>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="created-ticket-source"></canvas>
                            </div>
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="created-ticket-type"></canvas>
                            </div>
                        </div>
                    </div>
                    <!-- Resolved Tickets -->
                    <div class="bhoechie-tab-content" id="resolved-ticket">
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="resolved-ticket-source"></canvas>
                            </div>
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="resolved-ticket-priority"></canvas>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="resolved-ticket-type"></canvas>
                            </div>
                        </div>
                    </div>

                    <!-- Unresolved Tickets -->
                    <div class="bhoechie-tab-content" id="unresolved-ticket">
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="unresolved-ticket-source"></canvas>
                            </div>
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="unresolved-ticket-priority"></canvas>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="unresolved-ticket-type"></canvas>
                            </div>
                        </div>
                    </div>
                    <!-- Reopened Tickets -->
                    <div class="bhoechie-tab-content" id="reopened-ticket">
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="reopened-ticket-source"></canvas>
                            </div>
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="reopened-ticket-priority"></canvas>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-md-6">
                                <canvas id="reopened-ticket-type"></canvas>
                            </div>
                        </div>
                    </div>

                    <!-- Average first response time -->
                    <div class="bhoechie-tab-content" id="avg-first-response">
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="avg-first-response-status"></canvas>
                            </div>
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="avg-first-response-priority"></canvas>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="avg-first-response-source"></canvas>
                            </div>
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="avg-first-response-type"></canvas>
                            </div>
                        </div>
                    </div>

                    <!-- Average response time -->
                    <div class="bhoechie-tab-content" id="avg-response">
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="avg-response-status"></canvas>
                            </div>
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="avg-response-priority"></canvas>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="avg-response-source"></canvas>
                            </div>
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="avg-response-type"></canvas>
                            </div>
                        </div>
                    </div>

                    <!-- Average resolution time -->
                    <div class="bhoechie-tab-content" id="avg-resolution">
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="avg-resolution-priority"></canvas>
                            </div>
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="avg-resolution-source"></canvas>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="avg-resolution-type"></canvas>
                            </div>
                        </div>

                    </div>

                    <!-- First contact resolution  -->
                    <div class="bhoechie-tab-content" id="first-contact-resolution">
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="first-contact-resolution-priority"></canvas>
                            </div>
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="first-contact-resolution-source"></canvas>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="first-contact-resolution-type"></canvas>
                            </div>
                        </div>
                    </div>

                    <!-- First response SLA  -->
                    <div class="bhoechie-tab-content" id="response-sla">
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="response-sla-status"></canvas>
                            </div>
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="response-sla-priority"></canvas>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="response-sla-source"></canvas>
                            </div>
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="response-sla-type"></canvas>
                            </div>
                        </div>
                    </div>

                    <!-- Resolution SLA  -->
                    <div class="bhoechie-tab-content" id="resolve-sla">
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="resolve-sla-priority"></canvas>
                            </div>
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="resolve-sla-source"></canvas>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="height: 300px;">
                                <canvas id="resolve-sla-type"></canvas>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('FooterInclude')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.js"></script>
<script>
    var options = {
        scales: {
            xAxes: [{
                    stacked: true,
                    display: false,
                }],
            yAxes: [{
                    stacked: true,
                    barThickness: 10
                }],
        }
    };
    //var active_id = $('.bhoechie-tab-content.active').attr('id');
    //created ticket
    var data = "";//$("#filter").serialize();
    thisAjax('status', 'created-ticket', "#created-ticket-status", data);
    thisAjax('priority', 'created-ticket', "#created-ticket-priority", data);
    thisAjax('source', 'created-ticket', "#created-ticket-source", data);
    thisAjax('type', 'created-ticket', "#created-ticket-type", data);
    $("#submit-filter").click(function () {
        var data = $("#filter").serialize();
        thisAjax('status', 'created-ticket', "#created-ticket-status", data);
        thisAjax('priority', 'created-ticket', "#created-ticket-priority", data);
        thisAjax('source', 'created-ticket', "#created-ticket-source", data);
        thisAjax('type', 'created-ticket', "#created-ticket-type", data);
    });
    $("#created-ticket").click(function () {
        var data = $("#filter").serialize();
        thisAjax('status', 'created-ticket', "#created-ticket-status", data);
        thisAjax('priority', 'created-ticket', "#created-ticket-priority", data);
        thisAjax('source', 'created-ticket', "#created-ticket-source", data);
        thisAjax('type', 'created-ticket', "#created-ticket-type", data);
    });
    //resolved
    //thisAjax('status', 'resolved-ticket', "#resolved-ticket-status");
    $("#resolved-ticket").click(function () {
        var data = $("#filter").serialize();
        thisAjax('priority', 'resolved-ticket', "#resolved-ticket-priority", data);
        thisAjax('source', 'resolved-ticket', "#resolved-ticket-source", data);
        thisAjax('type', 'resolved-ticket', "#resolved-ticket-type", data);
    });

    //unresolved
    //thisAjax('status', 'resolved-ticket', "#unresolved-ticket-status");
    $("#unresolved-ticket").click(function () {
        var data = $("#filter").serialize();
        thisAjax('priority', 'unresolved-ticket', "#unresolved-ticket-priority", data);
        thisAjax('source', 'unresolved-ticket', "#unresolved-ticket-source", data);
        thisAjax('type', 'unresolved-ticket', "#unresolved-ticket-type", data);
    });

    //Reopened
    //thisAjax('status', 'resolved-ticket', "#resolved-ticket-status");
    $("#reopened-ticket").click(function () {
        var data = $("#filter").serialize();
        thisAjax('priority', 'reopened-ticket', "#reopened-ticket-priority", data);
        thisAjax('source', 'reopened-ticket', "#reopened-ticket-source", data);
        thisAjax('type', 'reopened-ticket', "#reopened-ticket-type", data);
    });

    //Avg first response
    $("#avg-first-response").click(function () {
        var data = $("#filter").serialize();
        thisAjax('status', 'avg-first-response', "#avg-first-response-status", data);
        thisAjax('priority', 'avg-first-response', "#avg-first-response-priority", data);
        thisAjax('source', 'avg-first-response', "#avg-first-response-source", data);
        thisAjax('type', 'avg-first-response', "#avg-first-response-type", data);
    });

    //Avg response
    $("#avg-response").click(function () {
        var data = $("#filter").serialize();
        thisAjax('status', 'avg-response', "#avg-response-status", data);
        thisAjax('priority', 'avg-response', "#avg-response-priority", data);
        thisAjax('source', 'avg-response', "#avg-response-source", data);
        thisAjax('type', 'avg-response', "#avg-response-type", data);
    });

    //Avg Resolution
    //thisAjax('status', 'avg-resolution', "#avg-resolution-status");
    $("#avg-resolution").click(function () {
        var data = $("#filter").serialize();
        thisAjax('priority', 'avg-resolution', "#avg-resolution-priority", data);
        thisAjax('source', 'avg-resolution', "#avg-resolution-source", data);
        thisAjax('type', 'avg-resolution', "#avg-resolution-type", data);
    });

    //First Contact
    //thisAjax('status', 'first-contact-resolution', "#resolved-ticket-status");
    $("#first-contact-resolution").click(function () {
        var data = $("#filter").serialize();
        thisAjax('priority', 'first-contact-resolution', "#first-contact-resolution-priority", data);
        thisAjax('source', 'first-contact-resolution', "#first-contact-resolution-source", data);
        thisAjax('type', 'first-contact-resolution', "#first-contact-resolution-type", data);
    });
    //First Response sla
    $("#response-sla").click(function () {
        var data = $("#filter").serialize();
        thisAjax('status', 'response-sla', "#response-sla-status", data);
        thisAjax('priority', 'response-sla', "#response-sla-priority", data);
        thisAjax('source', 'response-sla', "#response-sla-source", data);
        thisAjax('type', 'response-sla', "#response-sla-type", data);
    });
    //Resolve Sla
    //thisAjax('status', 'first-contact-resolution', "#resolved-ticket-status");
    $("#resolve-sla").click(function () {
        var data = $("#filter").serialize();
        thisAjax('priority', 'resolve-sla', "#resolve-sla-priority", data);
        thisAjax('source', 'resolve-sla', "#resolve-sla-source", data);
        thisAjax('type', 'resolve-sla', "#resolve-sla-type", data);
    });
    /**
     * To draw a graph using ajax
     * 
     * @param string type
     * @param string active_id
     * @param string canvas
     * @returns graph
     */
    function thisAjax(type, active_id, canvas, data = "") {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "{{url('report/indepth/get')}}",
            data: "group=" + type + "&active=" + active_id + "&" + data,
            success: function (data) {
                var ctx = $(canvas).get(0).getContext("2d");
                var myBarChart = new Chart(ctx,
                        {
                            type: 'horizontalBar',
                            data: data.chart,
                            options: options
                        });
                        console.log(data.chart);
                        console.log(active_id);
                $('#' + active_id + ' span').html(data.count);
            },
            error: function (error) {
                var ctx = $(canvas).get(0).getContext("2d");
                var myBarChart = new Chart(ctx,
                        {
                            type: 'horizontalBar',
                            data: error.textResponse,
                            options: options
                        });
            }
        });
    }
</script>
@stop

