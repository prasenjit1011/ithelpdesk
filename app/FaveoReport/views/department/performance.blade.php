@extends('themes.default1.agent.layout.agent')
@section('Staffs')
active
@stop

@section('staffs-bar')
active
@stop

@section('report')
class="active"
@stop

<!-- header -->
@section('PageHeader')
<h1>{!! Lang::get('report::lang.department-performance') !!}</h1>
@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">
</ol>
@stop
@section('content')
<div class="box box-primary">
    <div class="box-header">
        <button class="btn btn-primary" onclick="showhidefilter();">{!! Lang::get('report::lang.filter') !!}</button>
        @include('report::indepth.filter')
        @include('report::department.popup')
    </div>
    <div class="box-body">
        <table class="table table-bordered" cellspacing="0" width="100%" id="users-table">
            <thead>
                <tr>
                    <th>{!! Lang::get('report::lang.department') !!}</th>
                    <th>{!! Lang::get('report::lang.tickets-assigned') !!}</th>
                    <th>{!! Lang::get('report::lang.tickets-resolved') !!}</th>
                    <th>{!! Lang::get('report::lang.tickets-reopened') !!}</th>
                    <th>{!! Lang::get('report::lang.first-response-sla') !!}</th>
                    <th>{!! Lang::get('report::lang.resolution-sla') !!}</th>
                    <th>{!! Lang::get('report::lang.responses') !!}</th>
                    <th>{!! Lang::get('report::lang.average-response-time') !!}</th>
                    <th>{!! Lang::get('report::lang.average-resolution-time') !!}</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@stop
@section('FooterInclude')
<script>
    $(function () {
        var oTable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ url('report/department/performance/api/datatable') }}",
                data: function (d) {
                    d.start_date = $('input[name=start_date]').val();
                    d.end_date = $('input[name=end_date]').val();
                    d.agents = $('#agents').val();
                    d.departments = $('#departments').val();
                    d.sources = $('#sources').val();
                    d.priorities = $('#priorities').val();
                    d.clients = $('#clients').val();
                    d.types = $('#types').val();
                }
            },
            columns: [
                {data: 'department', name: 'department'},
                {data: 'assigned_tickets', name: 'assigned_tickets'},
                {data: 'closed_tickets', name: 'closed_tickets'},
                {data: 'reopened', name: 'reopened'},
                {data: 'success_response_sla', name: 'success_response_sla'},
                {data: 'success_resolution_sla', name: 'success_resolution_sla'},
                {data: 'number_of_response', name: 'number_of_response'},
                {data: 'avg_response_time', name: 'avg_response_time'},
                {data: 'avg_resolution_time', name: 'avg_resolution_time'}
            ]
        });

        $('#submit-filter').on('click', function (e) {
            oTable.draw();
            e.preventDefault();
        });
    });
</script>

@stop

