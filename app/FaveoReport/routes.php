<?php

Route::group(['middleware'=>['web','auth'],'prefix'=>'report'],function(){
    
    Route::get('get-all',[
        'as'=>'report.all.post',
        'uses'=>'App\FaveoReport\Controllers\ReportController@all'
    ]);
    
    Route::get('all',[
        'as'=>'report.all',
        'uses'=>'App\FaveoReport\Controllers\ReportController@allView'
    ]);
    
    
    Route::get('priority',[
        'as'=>'report.priority',
        'uses'=>'App\FaveoReport\Controllers\ReportController@priority'
    ]);
    
    Route::get('sla',[
        'as'=>'report.sla',
        'uses'=>'App\FaveoReport\Controllers\ReportController@sla'
    ]);
    
    Route::get('helptopic',[
        'as'=>'report.helptopic',
        'uses'=>'App\FaveoReport\Controllers\ReportController@helptopic'
    ]);
    
    Route::get('status',[
        'as'=>'report.status',
        'uses'=>'App\FaveoReport\Controllers\ReportController@status'
    ]);
    
    Route::get('purpose',[
        'as'=>'report.purpose',
        'uses'=>'App\FaveoReport\Controllers\ReportController@statusType'
    ]);
    
    Route::get('source',[
        'as'=>'report.source',
        'uses'=>'App\FaveoReport\Controllers\ReportController@source'
    ]);
    
    /**
     * Departments
     */
    Route::get('department/all',[
        'as'=>'report.department.all',
        'uses'=>'App\FaveoReport\Controllers\DepartmentReport@departmentAll'
    ]);
    
    Route::get('department/priority',[
        'as'=>'report.department.priority',
        'uses'=>'App\FaveoReport\Controllers\DepartmentReport@departmentPriority'
    ]);
    
    Route::get('department/sla',[
        'as'=>'report.department.sla',
        'uses'=>'App\FaveoReport\Controllers\DepartmentReport@departmentSla'
    ]);
    
    Route::get('department/helptopic',[
        'as'=>'report.department.helptopic',
        'uses'=>'App\FaveoReport\Controllers\DepartmentReport@departmentHelptopic'
    ]);
    
    Route::get('department/status',[
        'as'=>'report.department.status',
        'uses'=>'App\FaveoReport\Controllers\DepartmentReport@departmentChart'
    ]);
    
    Route::get('department/purpose',[
        'as'=>'report.department.purpose',
        'uses'=>'App\FaveoReport\Controllers\DepartmentReport@departmentStatusType'
    ]);
    
    Route::get('department/source',[
        'as'=>'report.department.source',
        'uses'=>'App\FaveoReport\Controllers\DepartmentReport@departmentSource'
    ]);
    Route::get('department',[
        'as'=>'report.department',
        'uses'=>'App\FaveoReport\Controllers\DepartmentReport@department'
    ]);
    Route::get('department/datatable',[
        'as'=>'report.department.datatable',
        'uses'=>'App\FaveoReport\Controllers\DepartmentReport@datatable'
    ]);
    Route::post('department/export',[
        'as'=>'report.department.export',
        'uses'=>'App\FaveoReport\Controllers\DepartmentReport@departmentExport'
    ]);
    Route::post('department/mail',[
        'as'=>'report.department.mail',
        'uses'=>'App\FaveoReport\Controllers\DepartmentReport@mail'
    ]);
    
    
    /**
     * Teams
     */
    
    Route::get('team/all',[
        'as'=>'report.team.all',
        'uses'=>'App\FaveoReport\Controllers\TeamReport@teamAll'
    ]);
    
    Route::get('team/priority',[
        'as'=>'report.team.priority',
        'uses'=>'App\FaveoReport\Controllers\TeamReport@teamPriority'
    ]);
    
    Route::get('team/sla',[
        'as'=>'report.team.all',
        'uses'=>'App\FaveoReport\Controllers\TeamReport@teamSla'
    ]);
    
    Route::get('team/helptopic',[
        'as'=>'report.team.helptopic',
        'uses'=>'App\FaveoReport\Controllers\TeamReport@teamHelptopic'
    ]);
    
    Route::get('team/status',[
        'as'=>'report.team.status',
        'uses'=>'App\FaveoReport\Controllers\TeamReport@teamChart'
    ]);
    
    Route::get('team/purpose',[
        'as'=>'report.team.purpose',
        'uses'=>'App\FaveoReport\Controllers\TeamReport@teamStatusType'
    ]);
    
    Route::get('team/source',[
        'as'=>'report.team.source',
        'uses'=>'App\FaveoReport\Controllers\TeamReport@teamSource'
    ]);
    
    Route::get('team',[
        'as'=>'report.team.get',
        'uses'=>'App\FaveoReport\Controllers\TeamReport@getTeam'
    ]);
    Route::get('team/datatable',[
        'as'=>'report.team.datatable',
        'uses'=>'App\FaveoReport\Controllers\TeamReport@datatable'
    ]);
    
    Route::post('team/export',[
        'as'=>'report.team.export',
        'uses'=>'App\FaveoReport\Controllers\TeamReport@teamExport'
    ]);
     Route::post('team/mail',[
        'as'=>'report.team.mail',
        'uses'=>'App\FaveoReport\Controllers\TeamReport@mail'
    ]);
    
    /**
     * Agents
     */
    
    Route::get('agent',[
        'as'=>'report.agent',
        'uses'=>'App\FaveoReport\Controllers\AgentReport@getAgent'
    ]);
    
     Route::get('get/agent',[
        'as'=>'report.agent.get',
        'uses'=>'App\FaveoReport\Controllers\AgentReport@agent'
    ]);
    
    Route::get('agent/status',[
        'as'=>'report.agent.status',
        'uses'=>'App\FaveoReport\Controllers\AgentReport@agentChart'
    ]);
    Route::get('agent/status/datatable',[
        'as'=>'report.agent.status.datatable',
        'uses'=>'App\FaveoReport\Controllers\AgentReport@getAgentStatusTable'
    ]);
    
    Route::post('agent/export',[
        'as'=>'report.agent.export',
        'uses'=>'App\FaveoReport\Controllers\AgentReport@AgentExport'
    ]);
    Route::post('agent/mail',[
        'as'=>'report.agent.mail',
        'uses'=>'App\FaveoReport\Controllers\AgentReport@mail'
    ]);
    
    /**
     * Export
     */
    
    Route::post('export',[
        'as'=>'report.export',
        'uses'=>'App\FaveoReport\Controllers\ReportController@export'
    ]);
    
    Route::post('all/mail',[
        'as'=>'report.mail',
        'uses'=>'App\FaveoReport\Controllers\ReportController@mail'
    ]);
    
//    \Event::listen('settings.agent.view',function(){
//        $req = new \Illuminate\Http\Request();
//        $controller = new App\FaveoReport\Controllers\ReportController($req);
//        echo $controller->icon(); 
//    });
    
    Route::get('get',[
        'as'=>'report.get',
        'uses'=>'App\FaveoReport\Controllers\ReportController@groupView'
    ]);
    
    Route::get('mail/to',[
        'as'=>'report.to',
        'uses'=>'App\FaveoReport\Controllers\ReportController@MailTo'
    ]);
    
    Route::get('download/csv',[
        'as'=>'report.download',
        'uses'=>'App\FaveoReport\Controllers\ReportController@download',
    ]);
    
    Route::get('indepth',[
        'as'=>'report.indepth',
        'uses'=>'App\FaveoReport\Controllers\ReportIndepth@getView',
    ]);
    
    Route::get('indepth/get',[
        'as'=>'report.indepth.api',
        'uses'=>'App\FaveoReport\Controllers\ReportIndepth@getTickets',
    ]);
    
    Route::get('trends/get',[
        'as'=>'report.indepth.api',
        'uses'=>'App\FaveoReport\Controllers\ReportIndepth@getTrends',
    ]);
    
    Route::get('trends',[
        'as'=>'report.trends',
        'uses'=>'App\FaveoReport\Controllers\ReportIndepth@trends',
    ]);
    
    Route::get('trends/analysis/get',[
        'as'=>'report.trends.analysis',
        'uses'=>'App\FaveoReport\Controllers\ReportIndepth@getTrendsLoad',
    ]);
    
    Route::get('trends/analysis/day',[
        'as'=>'report.trends.day',
        'uses'=>'App\FaveoReport\Controllers\ReportIndepth@dayTrends',
    ]);
    
    Route::get('trends/analysis/hour',[
        'as'=>'report.trends.hour',
        'uses'=>'App\FaveoReport\Controllers\ReportIndepth@hour',
    ]);
    
    Route::get('api/get/agents',[
        'as'=>'report.agent.get',
        'uses'=>'App\FaveoReport\Controllers\Utility@agentApi',
    ]);
    
    Route::get('api/get/departments',[
        'as'=>'report.department.get',
        'uses'=>'App\FaveoReport\Controllers\Utility@departmentApi',
    ]);
    
    Route::get('api/get/priorities',[
        'as'=>'report.priority.get',
        'uses'=>'App\FaveoReport\Controllers\Utility@priorityApi',
    ]);
    
    Route::get('api/get/sources',[
        'as'=>'report.source.get',
        'uses'=>'App\FaveoReport\Controllers\Utility@sourceApi',
    ]);
    
    Route::get('api/get/clients',[
        'as'=>'report.client.get',
        'uses'=>'App\FaveoReport\Controllers\Utility@clientApi',
    ]);
    Route::get('api/get/types',[
        'as'=>'report.type.get',
        'uses'=>'App\FaveoReport\Controllers\Utility@typeApi',
    ]);
    
    /**
     * agent
     */
    Route::get('agent/performance',[
        'as'=>'report.agent.performance',
        'uses'=>'App\FaveoReport\Controllers\AgentReport@getView',
    ]);
    
    Route::get('agent/performance/api',[
        'as'=>'report.agent.performance.api',
        'uses'=>'App\FaveoReport\Controllers\AgentReport@agentPerformance',
    ]);
    Route::get('agent/performance/api/datatable',[
        'as'=>'report.agent.performance.api.datatable',
        'uses'=>'App\FaveoReport\Controllers\AgentReport@agentDatatable',
    ]);
    
    /**
     * department
     */
    Route::get('department/performance',[
        'as'=>'report.department.performance',
        'uses'=>'App\FaveoReport\Controllers\DepartmentReport@getView',
    ]);
    
    Route::get('department/performance/api',[
        'as'=>'report.department.performance.api',
        'uses'=>'App\FaveoReport\Controllers\DepartmentReport@departmentPerformance',
    ]);
    Route::get('department/performance/api/datatable',[
        'as'=>'report.department.performance.api.datatable',
        'uses'=>'App\FaveoReport\Controllers\DepartmentReport@departmentDatatable',
    ]);
    
    /**
     * team
     */
    Route::get('team/performance',[
        'as'=>'report.team.performance',
        'uses'=>'App\FaveoReport\Controllers\TeamReport@getView',
    ]);
    
    Route::get('team/performance/api',[
        'as'=>'report.team.performance.api',
        'uses'=>'App\FaveoReport\Controllers\TeamReport@teamPerformance',
    ]);
    Route::get('team/performance/api/datatable',[
        'as'=>'report.team.performance.api.datatable',
        'uses'=>'App\FaveoReport\Controllers\TeamReport@teamDatatable',
    ]);
    
    /**
     * performance
     */
    Route::get('performance',[
        'as'=>'report.performance',
        'uses'=>'App\FaveoReport\Controllers\Performance@getView',
    ]);
    
    Route::get('performance/first_response',[
        'as'=>'report.performance.first_response',
        'uses'=>'App\FaveoReport\Controllers\Performance@firstResponsePerformance',
    ]);
    
    Route::get('performance/avg_response',[
        'as'=>'report.performance.avg_response',
        'uses'=>'App\FaveoReport\Controllers\Performance@avgResponsePerformance',
    ]);
    
    Route::get('performance/avg_resolution',[
        'as'=>'report.performance.avg_resolution',
        'uses'=>'App\FaveoReport\Controllers\Performance@resolutionPerformance',
    ]);
    
    Route::get('performance/first_response_response_trend',[
        'as'=>'report.performance.first_response_response_trend',
        'uses'=>'App\FaveoReport\Controllers\Performance@firstAndResponseTrend',
    ]);
    
    /**
     * Timesheet
     */
    Route::get('timesheet',[
        'as'=>'report.timesheet',
        'uses'=>'App\FaveoReport\Controllers\Performance@timesheetView',
    ]);
    
    Route::get('timesheet/datatable',[
        'as'=>'report.timesheet.datatable',
        'uses'=>'App\FaveoReport\Controllers\Performance@timesheetView',
    ]);
    
    Route::get('timesheet/data',[
        'as'=>'report.timesheet.data',
        'uses'=>'App\FaveoReport\Controllers\Performance@getBillDetails',
    ]);

    Route::get('timesheet/datatable',[
        'as'=>'report.timesheet.datatable',
        'uses'=>'App\FaveoReport\Controllers\Performance@timesheetDatatable',
    ]);
    
    Route::post('timesheet/export',[
        'as'=>'report.timesheet.export',
        'uses'=>'App\FaveoReport\Controllers\Performance@exportTimesheet',
    ]);
    
    Route::post('timesheet/mail',[
        'as'=>'report.timesheet.mail',
        'uses'=>'App\FaveoReport\Controllers\Performance@mail',
    ]);
    
    Route::get('organization/get',[
        'as'=>'report.organization.get',
        'uses'=>'App\FaveoReport\Controllers\ReportIndepth@getOrg',
    ]);
    Route::get('organization',[
        'as'=>'report.organization',
        'uses'=>'App\FaveoReport\Controllers\ReportIndepth@orgView',
    ]);
    
    
    Route::get('rating/get',[
        'as'=>'report.rating.get',
        'uses'=>'App\FaveoReport\Controllers\ReportIndepth@getSatisfaction',
    ]);
    Route::get('rating',[
        'as'=>'report.rating',
        'uses'=>'App\FaveoReport\Controllers\ReportIndepth@satisfactionView',
    ]);
    
});