<?php

namespace App\FaveoReport\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Utility extends Controller {

    public $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function agentApi() {
        $search = $this->request->input('q');
        $items = \App\User::where('role', '!=', 'user')
                ->where(function($q) use($search) {
                    $q->orWhere('first_name', "LIKE", "%" . $search . "%")
                    ->orWhere('last_name', 'LIKE', "%" . $search . "%")
                    ->orWhere('email', 'LIKE', "%" . $search . "%")
                    ->orWhere('user_name', 'LIKE', "%" . $search . "%");
                })
                ->select('id', 'user_name as text')
                ->get()
                ->toArray();

        return response()->json(compact('items'), 200);
    }

    public function departmentApi() {
        $search = $this->request->input('q');
        $items = \App\Model\helpdesk\Agent\Department::
                where('name', 'LIKE', "%" . $search . "%")
                ->select('id', 'name as text')
                ->get()
                ->toArray();

        return response()->json(compact('items'), 200);
    }

    public function priorityApi() {
        $search = $this->request->input('q');
        $items = \App\Model\helpdesk\Ticket\Ticket_Priority::
                where('priority', 'LIKE', "%" . $search . "%")
                ->select('priority_id as id', 'priority as text')
                ->get()
                ->toArray();

        return response()->json(compact('items'), 200);
    }

    public function sourceApi() {
        $search = $this->request->input('q');
        $items = \App\Model\helpdesk\Ticket\Ticket_source::
                where('name', 'LIKE', "%" . $search . "%")
                ->select('id', 'name as text')
                ->get()
                ->toArray();

        return response()->json(compact('items'), 200);
    }

    public function clientApi() {
        $search = $this->request->input('q');
        $items = \App\User::
                where(function($q) use($search) {
                    $q->orWhere('first_name', "LIKE", "%" . $search . "%")
                    ->orWhere('last_name', 'LIKE', "%" . $search . "%")
                    ->orWhere('email', 'LIKE', "%" . $search . "%")
                    ->orWhere('user_name', 'LIKE', "%" . $search . "%");
                })
                ->where('role', '=', 'user')
                ->select('id', 'user_name as text')
                ->get()
                ->toArray();

        return response()->json(compact('items'), 200);
    }

    public function typeApi() {
        $search = $this->request->input('q');
        $items = \App\Model\helpdesk\Manage\Tickettype::
                where('name', 'LIKE', "%" . $search . "%")
                ->select('id', 'name as text')
                ->get()
                ->toArray();

        return response()->json(compact('items'), 200);
    }

}
