<?php
return [
    /**
     * Group
     */
    'helpdesk-in-depth'=>'Helpdesk In-depth',
    'ticket-volume-trends'=>'Ticket Volume Trends',
    'agent-performance'=>'Agent Performance',
    'department-performance'=>'Department Performance',
    'performance-distribution'=>'Performance Distribution',
    'team-performance'=>'Team Performance',
    'time-sheet-summary'=>'Time sheet Summary',
    'helpdesk-analysis'=>'Helpdesk Analysis',
    'productivity'=>'Productivity',
    'top-customer-analysis'=>'Top Customer Analysis',
    'customer-happiness'=>'Customer Happiness',
    
    /**
     * Filter
     */
    'filter'=>'Filter',
    'date-range'=>'Date Range',
    'generate'=>'Generate',
    'export'=>'Export',
    'close'=>'Close',
    
    /**
     * Indepth
     */
    'created-tickets'=>'Created Tickets',
    'resolved-tickets'=>'Resolved Tickets',
    'unresolved-tickets'=>'Unresolved Tickets',
    'reopened-tickets'=>'Reopened Tickets',
    'average-first-response-time'=>'Average first response time',
    'average-response-time'=>'Average response time',
    'average-resolution-time'=>'Average resolution time',
    'first-contact-resolution'=>'First contact resolution',
    'first-response-sla'=>'First Response SLA',
    'resolution-sla'=>'Resolution SLA',
    'split-by'=>'Split by',
    
    /**
     * Ticket Trends
     */
    
    'ticket-volume-trends'=>'Ticket Volume Trends',
    'trend-of-tickets-received-and-resolved-and-unresolved'=>'Trend of tickets received and resolved and unresolved',
    'day'=>'Day',
    'week'=>'Week',
    'month'=>'Month',
    'year'=>'Year',
    'load-analysis'=>'Load Analysis',
    'day-of-the-week-and-hour-of-the-day-trend'=>'Day of the week and hour of the day trend',
    
    'Mon'=>'Monday',
    'Tue'=>'Tuesday',
    'Wed'=>'Wednesday',
    'Thu'=>'Thursday',
    'Fri'=>'Friday',
    'Sat'=>'Saturday',
    'Sun'=>'Sunday',
    'total-received-tickets'=>'Total Received',
    'total-resolved-tickets'=>'Total Resolved',
    'total-unresolved-tickets'=>'Total Unresolved',
    'average-received-tickets'=>'Average Received',
    'average-resolved-tickets'=>'Average Resolved',
    'average-unresolved-tickets'=>'Average Unresolved',
    
    'most-tickets-were-received-around'=>'Most tickets were received around',
    'most-tickets-were-resolved-around'=>'Most tickets were resolved around',
    'most-tickets-were-received-on'=>'Most tickets were received on',
    'most-tickets-were-resolved on'=>'Most tickets were resolved on',
    
    
    /**
     * Agent Performance
     */
    'agent'=>'Agent',
    'tickets-assigned'=>'Tickets Assigned',
    'tickets-resolved'=>'Tickets Resolved',
    'tickets-reopened'=>'Tickets Reopened',
    'first-response-sla'=>'First Response SLA',
    'resolution-sla'=>'Resolution SLA',
    'responses'=>'Responses',
    'average-response-time'=>'Average Response Time',
    'average-resolution-time'=>'Average Resolution Time',
    
    /**
     * Depart ment performance
     */
    
    'department'=>'Department',
    
    /**
     * Team
     */
    
    'team'=>'Team',
    
    /**
     * performance distribution
     */
    'avg-first-response-and-response-time-trend'=>'Avg first response and response time trend',
    
    /**
     * Timesheet
     */
    'hours-tracked'=>'Hours Tracked',
    'billable-hours'=>'Billable Hours',
    'billable-amount'=>'Billable Amount',
    'non-billable-hours'=>'Non-Billable Hours',
    'notes'=>'Notes',
    'customer'=>'Customer',
    'priority'=>'Priority',
    'status'=>'Status',
    'date'=>'Date',
    'department'=>'Department',
    'hours'=>'Hours',
    'amount'=>'Amount',
    
    /**
     * Customer analysis
     */
    
    'recieved-tickets'=>'Recieved tickets',
    'response-sla'=>'Response sla',
    'resolve-sla'=>'Resolve sla',
    'agent-responses'=>'Agent responses',
    'client-responses'=>'Client responses',
    
    /**
     *  Satisfaction Survey
     */
    
    'satisfaction-survey'=>'Satisfaction Survey',
    'ticket'=>'Ticket',
    'thread'=>'Thread',
    
];