<?php
Breadcrumbs::register('report.get', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Report', route('report.get'));
});

Breadcrumbs::register('report.all', function($breadcrumbs)
{
    $breadcrumbs->parent('report.get');
    $breadcrumbs->push('Report Overall', route('report.all'));
});
Breadcrumbs::register('report.agent', function($breadcrumbs)
{
    $breadcrumbs->parent('report.get');
    $breadcrumbs->push('Agent Report', route('report.agent'));
});

Breadcrumbs::register('report.team.get', function($breadcrumbs)
{
    $breadcrumbs->parent('report.get');
    $breadcrumbs->push('Team Report', route('report.team.get'));
});

Breadcrumbs::register('report.department', function($breadcrumbs)
{
    $breadcrumbs->parent('report.get');
    $breadcrumbs->push('Deparment Report', route('report.department'));
});

/**
 * new
 */

Breadcrumbs::register('report.indepth', function($breadcrumbs)
{
    $breadcrumbs->parent('report.get');
    $breadcrumbs->push(Lang::get('report::lang.helpdesk-in-depth'), route('report.indepth'));
});

Breadcrumbs::register('report.trends', function($breadcrumbs)
{
    $breadcrumbs->parent('report.get');
    $breadcrumbs->push(Lang::get('report::lang.ticket-volume-trends'), route('report.trends'));
});

Breadcrumbs::register('report.agent.performance', function($breadcrumbs)
{
    $breadcrumbs->parent('report.get');
    $breadcrumbs->push(Lang::get('report::lang.agent-performance'), route('report.agent.performance'));
});

Breadcrumbs::register('report.department.performance', function($breadcrumbs)
{
    $breadcrumbs->parent('report.get');
    $breadcrumbs->push(Lang::get('report::lang.department-performance'), route('report.department.performance'));
});

Breadcrumbs::register('report.team.performance', function($breadcrumbs)
{
    $breadcrumbs->parent('report.get');
    $breadcrumbs->push(Lang::get('report::lang.team-performance'), route('report.team.performance'));
});

Breadcrumbs::register('report.performance', function($breadcrumbs)
{
    $breadcrumbs->parent('report.get');
    $breadcrumbs->push(Lang::get('report::lang.performance-distribution'), route('report.performance'));
});

Breadcrumbs::register('report.timesheet', function($breadcrumbs)
{
    $breadcrumbs->parent('report.get');
    $breadcrumbs->push(Lang::get('report::lang.time-sheet-summary'), route('report.timesheet'));
});

Breadcrumbs::register('report.organization', function($breadcrumbs)
{
    $breadcrumbs->parent('report.get');
    $breadcrumbs->push(Lang::get('report::lang.top-customer-analysis'), route('report.organization'));
});

Breadcrumbs::register('report.rating', function($breadcrumbs)
{
    $breadcrumbs->parent('report.get');
    $breadcrumbs->push(Lang::get('report::lang.satisfaction-survey'), route('report.rating'));
});
    
