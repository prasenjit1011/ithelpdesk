<?php

namespace App\FaveoReport;

use Illuminate\Support\ServiceProvider;

class ReportServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot() {

        $view_path = app_path() . DIRECTORY_SEPARATOR . 'FaveoReport' . DIRECTORY_SEPARATOR . 'views';
        $this->loadViewsFrom($view_path, 'report');

        $lang_path = app_path() . DIRECTORY_SEPARATOR . 'FaveoReport' . DIRECTORY_SEPARATOR . 'lang';
        $this->loadTranslationsFrom($lang_path, "report");
        
        if (class_exists('Breadcrumbs')){
            require __DIR__ . '/breadcrumbs.php';
        }   
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        // Add routes
        $routes = app_path('/FaveoReport/routes.php');
        if (file_exists($routes)) {
            require $routes;
        }
        $helper = app_path('/FaveoReport/helper.php');
        if (file_exists($helper)) {
            require $helper;
        }
        
    }

   

}
