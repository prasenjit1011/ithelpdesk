<?php

    \Event::listen('settings.ticket.view', function() {
        $set = new App\Bill\Controllers\SettingsController();
        echo $set->settingsLink();
    });
    \Event::listen('App\Events\TimeLineFormEvent',function($event){
        $set = new App\Bill\Controllers\BillController();
        echo $set->threadLevelForm($event);
    });
    \Event::listen('App\Events\FaveoAfterReply',function($event){
        $set = new App\Bill\Controllers\BillController();
        $set->postReply($event);
    });
    \Event::listen('reply.request', function($event) {
        $set = new App\Bill\Controllers\BillController();
        echo $set->requestRule($event);
    });
    \Event::listen('timeline.tab.content', function($event) {
        $set = new App\Bill\Controllers\BillController();
        echo $set->billingTabContent($event);
    });
    \Event::listen('timeline.tab.list', function($event) {
        $set = new App\Bill\Controllers\BillController();
        echo $set->billingTabList($event);
    });
    \Event::listen('ticket.merge', function($event) {
        $set = new App\Bill\Controllers\BillController();
        echo $set->merge($event);
    });
     \Event::listen('ticket.details.more.list', function($event) {
        $set = new App\Bill\Controllers\BillController();
        echo $set->moreList($event);
    });
    \Event::listen('ticket.detail.modelpopup', function($event) {
        $set = new App\Bill\Controllers\BillController();
        echo $set->ticketLevelForm($event);
    });
    Route::group(['middleware' => ['web', 'auth']], function() {
        Route::group(['middleware' => ['roles']], function() {
            Route::get('bill', ['as' => 'bill.setting', 'uses' => 'App\Bill\Controllers\SettingsController@setting']);
        
            Breadcrumbs::register('bill.setting', function ($breadcrumbs) {
                $breadcrumbs->parent('setting');
                $breadcrumbs->push(Lang::get('lang.bill'), route('bill.setting'));
            });

            Route::post('bill', ['as' => 'bill.settings.post', 'uses' => 'App\Bill\Controllers\SettingsController@postSetting']);
        });
        Route::group(['middleware' => ['role.agent']], function() {
            Route::get('bill/{id}/delete',['as'=>'bill.delete','uses'=>'App\Bill\Controllers\BillController@delete']);
            Route::patch('bill/{id}',['as'=>'bill.update','uses'=>'App\Bill\Controllers\BillController@update']);
            Route::post('new-bill',['as'=>'new.bill','uses'=>'App\Bill\Controllers\BillController@store']);
        });
    });


