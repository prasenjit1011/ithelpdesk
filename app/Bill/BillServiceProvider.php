<?php

namespace App\Bill;

use Illuminate\Support\ServiceProvider;

class BillServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot() {

        $view_path = app_path() . DIRECTORY_SEPARATOR . 'Bill' . DIRECTORY_SEPARATOR . 'views';
        $this->loadViewsFrom($view_path, 'bill');

        $lang_path = app_path() . DIRECTORY_SEPARATOR . 'Bill' . DIRECTORY_SEPARATOR . 'lang';
        $this->loadTranslationsFrom($lang_path, "bill");
        if (isInstall()) {
            $controller = new Controllers\ActivateController();
            $controller->activate();
        }

//        if (class_exists('Breadcrumbs')){
//            require __DIR__ . '/breadcrumbs.php';
//        }   
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        // Add routes
        $routes = app_path('/Bill/routes.php');
        if (file_exists($routes)) {
            require $routes;
        }
    }

}
