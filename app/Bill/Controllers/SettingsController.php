<?php

namespace App\Bill\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use App\Model\helpdesk\Settings\CommonSettings;
use App\Model\helpdesk\Ticket\Ticket_Status;
use App\Bill\Requests\BillRequest;
use Lang;
/**
 * Setting for the bill module
 * 
 * @abstract Controller
 * @author Ladybird Web Solution <admin@ithelpdesk.com>
 * @name SettingsController
 * 
 */

class SettingsController extends Controller {

    public function __construct() {
        //$this->middleware(['auth', 'roles']);
    }
    /**
     * 
     * get the setting icon on admin panel
     * 
     * @return string
     */
    public function settingsLink() {
        return ' <div class="col-md-2 col-sm-6">
                    <div class="settingiconblue">
                        <div class="settingdivblue">
                            <a href="' . url('bill') . '">
                                <span class="fa-stack fa-2x">
                                    <i class="fa fa-money fa-stack-1x"></i>
                                </span>
                            </a>
                        </div>
                        <p class="box-title" >'.Lang::get('lang.bill').'</p>
                    </div>
                </div>';
    }
    /**
     * 
     * get the setting view for bill
     * 
     * @return view
     */
    public function setting() {
        try {
            $status = new Ticket_Status();
            $statuses = $status->pluck('name', 'id')->toArray();
            $set = new CommonSettings();
            $level = $set->getOptionValue('bill', 'level');
            $currency = $set->getOptionValue('bill', 'currency');
            return view('bill::settings.setting', compact('statuses','level','currency'));
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }
    /**
     * 
     * Billing post settings request
     * 
     * @param Request $request
     * @return string
     */
    public function postSetting(Request $request) {

        try {
            //dd($request->all());
            $set = new CommonSettings();
            $option_name = "bill";
            $requests = $request->except('_token');
            if (count($requests) > 0) {
                foreach ($requests as $key => $value) {
                    if ($key == 'status') {
                        $create = $set->firstOrCreate(['option_name' => $option_name]);
                        $create->status = $value;
                        $create->save();
                    } else {
                        $create = $set->firstOrCreate(['option_name' => $option_name, 'optional_field' => $key]);
                        $create->option_value = $value;
                        $create->save();
                    }
                }
            }
            return redirect('bill')->with('success',Lang::get('lang.bill_settings_saved_successfully'));
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    
}
