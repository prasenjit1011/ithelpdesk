<?php

namespace App\Bill\Controllers;

use App\Http\Controllers\Controller;
use App\Model\helpdesk\Settings\CommonSettings;
use Exception;
use App\Bill\Models\Bill;
use Auth;
use Illuminate\Http\Request;
/**
 * Bill controller
 * 
 * @abstract Controller
 * @author Ladybird Web Solution <admin@ithelpdesk.com>
 * @name BillController
 * 
 */

class BillController extends Controller {

    public function __construct() {
        //$this->middleware(['auth', 'role.agent']);
    }
    /**
     * 
     * If thread level condition
     * 
     * @return string
     */
    public function threadLevelForm() {

        if ($this->isThreadLevel() == true) {
            return $this->renderThreadLevelForm();
        }
    }
    /**
     * 
     * render thread level HTML Form
     * 
     * @return string
     */
    
    public function renderThreadLevelForm() {
        if ($this->isThreadLevel() == true) {
            return '<div class="form-group">
                            <div class="row">
            <div class="form-group">
                                    <div class="col-md-2">
                                        ' . \Form::label("hours", \Lang::get("lang.hours")).'<span class="text-red"> *</span>'."<span>&nbsp;  <i>(use ':' to seperate hours and minutes)</i></span>".'
                                    </div>
                                    <div class="col-md-10">
                                        <div id="newtextarea">
                                           ' . \Form::text('hour', null, ["class" => "form-control", "style" => "width:25%", "placeholder" => "HH:MM"]) . '
                                        </div>
                                        
                                    </div>
                                </div>
                                </div>
                                </div>
                                <div class="form-group">
                            <div class="row">
            <div class="form-group">
                                    <div class="col-md-2">
                                        ' . \Form::label("billable", \Lang::get("lang.billable")) . '
                                    </div>
                                    
                                        <div class="col-md-1">
                                           ' . \Form::radio('billable', 1, true) ." ".\Lang::get("lang.yes"). '
                                        </div>
                                        <div class="col-md-1">
                                           ' . \Form::radio('billable', 0) ." ".\Lang::get("lang.no").'
                                        </div>
                                        
                                    
                                </div>
                                </div>
                                </div>
                                <div class="form-group">
                            <div class="row">
            <div class="form-group">
                                    <div class="col-md-2">
                                        ' . \Form::label("amount", \Lang::get("lang.amount-per-hour")) . '
                                    </div>
                                    <div class="col-md-10">
                                        <div id="newtextarea">
                                           ' . \Form::text('amount_hourly', null, ["class" => "form-control", "style" => "width:25%"]) . '
                                        </div>
                                        
                                    </div>
                                </div>
                                </div>
                                </div>';
        }
    }
    /**
     * 
     * Validating the request for bill
     * 
     * @param string $request
     */
    public function requestRule($request) {
        if ($this->isThreadLevel() == true) {
            $this->validate($request, [
                'hour' => ['required', 'regex:/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/'],
            ]);
        }
    }
    
    /**
     * 
     * Event listening function from TicketController class to bill
     * 
     * @param object $event
     */
    
    public function postReply($event) {
        if ($this->isThreadLevel() == true) {
            $note = $event->para1;
            $request = $event->para4;
            $ticket = $event->para5;
            $thread = $event->para6;
            try {
                $level = $this->findLevel();
                if ($level) {
                    $model = $this->getModel($level, $ticket, $thread);
                    $modelid = $model->id;
                    $hours = $request->input('hour');
                    $billable = $request->input('billable');
                    $agentid = Auth::user()->id;
                    $ticketid = $request->input('ticket_ID');
                    $amount = $request->input('amount_hourly');
                    $this->saveBill($level, $modelid, $hours, $billable, $agentid, $ticketid, $note, $amount);
                }
            } catch (Exception $ex) {
                dd($ex);
            }
        }
    }
    /**
     * 
     * Get the billing level
     * 
     * @return string
     */
    public function findLevel() {
        if (isBill() == true) {
            $set = new CommonSettings();
            $schema = $set->getOptionValue('bill', 'level');
            if ($schema) {
                return $schema->option_value;
            }
        }
    }
    
    /**
     * 
     * Get the model according to level
     * 
     * @param type $level
     * @param type $ticket
     * @param type $thread
     * @return type object
     */
    
    public function getModel($level, $ticket, $thread) {
        switch ($level) {
            case "ticket":
                return $ticket;
            case "thread":
                return $thread;
        }
    }
    /**
     * 
     * Saving the billing
     * 
     * @param string $level
     * @param int $modelid
     * @param number $hours
     * @param int $billable
     * @param int $agentid
     * @param int $ticketid
     * @param string $note
     * @param number $amount
     */
    public function saveBill($level, $modelid, $hours, $billable, $agentid, $ticketid, $note, $amount) {
        $bill = new Bill();
        $bill->create([
            'level' => $level,
            'model_id' => $modelid,
            'hours' => $hours,
            'billable' => $billable,
            'agent' => $agentid,
            'ticket_id' => $ticketid,
            'note' => $note,
            'amount_hourly' => $amount,
        ]);
    }
    /**
     * 
     * Billing ticket level tab in ticket deatail page
     * 
     * @param int $ticket
     * @return string
     */
    public function billingTabList($ticket) {
        if (isBill() == true) {
            return '<li><a href="#bill" data-toggle="tab"><i class="fa fa-file-text"> </i> Bill</a></li>';
        }
    }
    /**
     * 
     * Billing Tab Content
     * 
     * @param class $ticket
     * @return view
     */
    public function billingTabContent($ticket) {
        if (isBill() == true) {
            $ticketid = $ticket->id;
            $bil = new Bill();
            $billable_hours_array = $bil->where('ticket_id', $ticketid)->where('billable', 1)->pluck('amount_hourly', 'hours')->toArray();
            $billable = $this->getBillableHours($billable_hours_array);
            $nonbillable_array = $bil->where('ticket_id', $ticketid)->where('billable', 0)->pluck('amount_hourly', 'hours')->toArray();
            $nonbillable = $this->getBillableHours($nonbillable_array);
            $bills = $bil->where('ticket_id', $ticketid)
                    ->select('id', 'agent', 'hours', 'amount_hourly', 'note', 'created_at', 'billable')->get();

            return view('bill::bill-display', compact('bills', 'billable', 'nonbillable'));
        }
    }
    /**
     * 
     * deleting thebilling entry
     * 
     * @param int $id
     * @return string
     * @throws Exception
     */
    public function delete($id) {
        try {
            $bills = new Bill();
            $bill = $bills->find($id);
            if (!$bill) {
                throw new Exception('Sorry! We could not find your request');
            }
            $bill->delete();
            return redirect()->back()->with('success', 'deleted');
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }
    /**
     * 
     * Get billable hours
     * 
     * @param array $array
     * @return array
     */
    public function getBillableHours($array) {
        //dd($array);
        $time = 0;
        $amount = 0;
        $out = ['hours' => '', 'min' => ''];
        $subtotal = [];
        if ($array > 0) {
            foreach ($array as $key => $value) {
                if(str_contains($key, ":")){
                    $change = ":";
                }
                if(str_contains($key, ".")){
                    $change = ".";
                }
                $time = explode($change, $key);
                $out['hours'] = $out['hours'] + $time[0];
                $out['min'] = $out['min'] + $time[1];
                $samay = $time[0] . '.' . $time[1];
                $subtotal[] = $samay * $value;
            }
            $hour = $out['hours'];
            $min = $out['min'];
            $amount = array_sum($subtotal);
            $convert_min_to_hour = floor($min / 60);
            if ($convert_min_to_hour > 0) {
                $hour = $hour + $convert_min_to_hour;
                $min = $min % 60;
            }
            if ($hour != "" || $min != "") {
                $time = $hour . ":" . $min;
            }
        }
        return ['time' => $time, 'amount' => $amount];
    }
    /**
     * 
     * get the billing setup values
     * 
     * @param string $field
     * @return string
     */
    public static function billSettings($field) {
        $set = new \App\Model\helpdesk\Settings\CommonSettings();
        $schema = $set->getOptionValue('bill', $field);
        if ($schema) {
            return $schema->option_value;
        }
    }
    /**
     * 
     * get billing currency
     * 
     * @return string
     */
    public static function currency() {
        $currency = self::billSettings('currency');
        if (!$currency) {
            $currency = "";
        }
        return $currency;
    }
    /**
     * 
     * editing for if bill is there
     * 
     * @param object $bill
     * @return string
     * @throws Exception
     */
    public static function edit($bill) {
        try {
            if (!$bill) {
                throw new Exception('Sorry! We can not find your request');
            }
            return self::editPopup($bill);
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }
    /**
     * 
     * Updating the billing entry
     * 
     * @param int $id
     * @param Request $request
     * @return string
     * @throws Exception
     */
    public function update($id, Request $request) {
        $this->validate($request, [
            'note' => 'required',
                //'hour' => 'required|regex:/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/',
        ]);
        try {
            $bills = new Bill();
            $bill = $bills->find($id);
            if (!$bill) {
                throw new Exception('Sorry! We can not find your request');
            }
            $bill->fill($request->input())->save();
            return redirect()->back()->with('success', 'Updated');
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }
    /**
     * 
     *  get bill edit form
     * 
     * @param object $bill
     * @return string
     */
    public static function editPopup($bill) {
        $agents = \App\User::where('role', '!=', 'user')->pluck('user_name', 'id')->toArray();
        return '<a href="#bill-edit" class="btn btn-xs btn-default" data-toggle="modal" data-target="#bill-edit' . $bill->id . '">Edit</a>
<div class="modal fade" id="bill-edit' . $bill->id . '">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit</h4>
                ' . \Form::model($bill, ["url" => "bill/" . $bill->id, "method" => "patch"]) . '
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        ' . \Form::label("agent", "Agent") .
                \Form::select("agent", $agents, null, ["class" => "form-control"]) . '
                    </div>
                    <div class="col-md-12">
                        ' . \Form::label("hours", "Hours") ."<span>&nbsp;  <i>(use ':' to seperate hours and minutes)</i></span>".
                \Form::text("hours", null, ["class" => "form-control", "placeholder" => "HH:MM"]) . '
                    </div>
                    <div class="col-md-12">
                        ' . \Form::label("amount_hourly", "Amount Hourly") .
                \Form::text("amount_hourly", null, ["class" => "form-control"]) . '
                    </div>
                    <div class="col-md-12">
                        ' . \Form::label("note", "Billing Note") .
                \Form::textarea("note", null, ["class" => "form-control"]) . '
                    </div>
                    <div class="col-md-12">
                        
                        ' . \Form::label("billable", "Billable") . '</br>
                            
                       
                            ' . \Form::radio("billable", 1) . ' Yes  &nbsp; &nbsp; &nbsp;
                        
                            ' . \Form::radio("billable", 0) . ' No
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="close" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                ' . \Form::submit("Update", ["class" => "btn btn-default"]) .
                \Form::close() . '
            </div>
        </div>
    </div>
</div>';
    }
    /**
     * 
     * Billing while merging the code
     * 
     * @param array $event
     */
    public function merge($event) {
        try {
            $parent = $event['parent'];
            $child = $event['child'];
            Bill::where('ticket_id', '=', $child)
                    ->update(['ticket_id' => $parent]);
        } catch (Exception $ex) {
            loging('bill-merge', $ex->getMessage());
        }
    }
    /**
     * 
     * In time line more drop down event listening
     * 
     * @param object $ticket
     * @return type
     */
    public function moreList($ticket) {
        if ($this->isTicketLevel() == true) {
            return $this->renderMoreList($ticket);
        }
    }
    /**
     *  
     *get ticket level form if ticket level is true
     * 
     * @param object $ticket
     * @return type
     */
    public function ticketLevelForm($ticket) {
        if ($this->isTicketLevel() == true) {
            return $this->renderTicketLevelForm($ticket);
        }
    }
    /**
     * 
     * check is ticket level setup
     * 
     * @return boolean
     */
    public function isTicketLevel() {
        $check = false;
        $set = new CommonSettings();
        $schema = $set->getOptionValue('bill', 'level');
        if (is_object($schema) && $schema->option_value == 'ticket' && isBill() == true) {
            $check = true;
        }
        return $check;
    }
    /**
     * 
     * check if thread level setup
     * 
     * @return boolean
     */
    public function isThreadLevel() {
        $check = false;
        $set = new CommonSettings();
        $schema = $set->getOptionValue('bill', 'level');
        if (is_object($schema) && $schema->option_value == 'thread' && isBill() == true) {
            $check = true;
        }
        return $check;
    }
    /**
     * 
     * render the more drop down popup
     * 
     * @return string
     */
    public function renderMoreList() {
        if($this->isTicketLevel()==true){
        return '<li data-toggle="modal" data-target="#bill-new"><a href="#"><i class="fa fa-file-text" ></i>Add Bill</a></li>';
        }
    }
    /**
     * 
     * get the ticket level popup
     * 
     * @param object $ticket
     * @return string
     */
    public function renderTicketLevelForm($ticket) {
         if($this->isTicketLevel()==true){
        $agents = \App\User::where('role', '!=', 'user')->pluck('user_name', 'id')->toArray();
        return '<div class="modal fade" id="bill-new">
            <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">New Bill</h4>
                ' . \Form::open(["url" => "new-bill", "method" => "post"]) . '
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        ' . \Form::label("agent", "Agent") .
                \Form::select("agent", $agents, \Auth::user()->id, ["class" => "form-control"]) . '
                    </div>
                    <div class="col-md-12">
                        ' . \Form::label("hours", "Hours") ."<span>&nbsp;  <i>(use ':' to seperate hours and minutes)</i></span>".
                \Form::text("hours", null, ["class" => "form-control", "placeholder" => "HH:MM"]) . '
                    </div>
                    <div class="col-md-12">
                        ' . \Form::label("amount_hourly", "Amount Hourly") .
                \Form::text("amount_hourly", null, ["class" => "form-control"]) .
                \Form::hidden("ticket_id", $ticket->id) . '
                    </div>
                    <div class="col-md-12">
                        ' . \Form::label("note", "Billing Note") .
                \Form::textarea("note", null, ["class" => "form-control"]) . '
                    </div>
                    <div class="col-md-12">
                        
                        ' . \Form::label("billable", "Billable") . '</br>
                            
                       
                            <input name="billable" type="radio" class="not-apply" value="1"> Yes  &nbsp; &nbsp; &nbsp;
                        
                            <input name="billable" type="radio" class="not-apply" value="0">No
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="close" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                ' . \Form::submit("Save", ["class" => "btn btn-default"]) .
                \Form::close() . '
            </div>
        </div>
    </div>
</div>';
         }
    }
    
    /**
     * 
     * Ticket level request saving
     * 
     * @param Request $request
     * @return string
     */
    
    public function store(Request $request) {
        $this->validate($request, [
            'note' => 'required',
            'hours' => ['required', 'regex:/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/'],
        ]);
        try {
            $level = $this->findLevel();
            if ($level) {
                $ticketid = $request->input('ticket_id');
                $agentid = $request->input('agent');
                $billable = $request->input('billable');
                $modelid = $request->input('ticket_id');
                $note = $request->input('note');
                $amount = $request->input('amount_hourly');
                $hours = $request->input('hours');
                $this->saveBill($level, $modelid, $hours, $billable, $agentid, $ticketid, $note, $amount);
            }
            return redirect()->back()->with('success', 'Saved');
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

}
