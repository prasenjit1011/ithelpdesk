@extends('themes.default1.admin.layout.admin')

@section('Tickets')
active
@stop

@section('tickets-bar')
active
@stop

@section('bill')
class="active"
@stop

@section('HeadInclude')
@stop
<!-- header -->
@section('PageHeader')
<h4>{{ Lang::get('lang.edit_bill_settings') }}</h4>
@stop
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">
</ol>
@stop
<!-- /breadcrumbs -->
@section('content')
<div class="box box-primary">

        
        {!! Form::open(['url'=>'bill','method'=>'post','id'=>'Form']) !!}

    <!-- /.box-header -->
    <div class="box-body">
        
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if (Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('success')}}
        </div>
        @endif
        <!-- fail message -->
        @if(Session::has('fails'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <b>{{Lang::get('message.alert')}}!</b> {{Lang::get('message.failed')}}.
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('fails')}}
        </div>
        @endif
        <div class="row">
            <div class="form-group col-md-6 {{ $errors->has('status') ? 'has-error' : '' }}">
                <div>
                    {!! Form::label('status',Lang::get('lang.enable')) !!}
                </div>
                <div>
                    <?php
                    $yes = false;
                    $no = false;
                    if(isBill()==true){
                        $yes = true;
                    }else{
                        $no = true;
                    }
                    ?>

                    <div class="col-md-3">
                        <p> {!! Form::radio('status',1,$yes) !!} {!! Lang::get('lang.yes') !!}</p>
                    </div>
                    <div class="col-md-3">
                        <p> {!! Form::radio('status',0,$no) !!} {!! Lang::get('lang.no') !!}</p>
                    </div>
                </div>             
            </div>
            <div class="form-group col-md-6 {{ $errors->has('level') ? 'has-error' : '' }}">
                <div>
                    {!! Form::label('level',Lang::get('lang.level-of-apply')) !!}
                </div>
                <div>
                     <?php
                    $curr = "";
                    $thread = false;
                    $ticket = false;
                    if($level&&$level->option_value=='ticket'){
                        $ticket = true;
                    }else{
                        $thread = true;
                    }
                    if($currency && $currency->option_value){
                        $curr = $currency->option_value;
                    }
                    ?>

                    <div class="col-md-3">
                        <p> {!! Form::radio('level','thread',$thread,['onclick'=>'nontrigger()']) !!} {!!Lang::get('lang.thread')!!}</p>
                    </div>
                    <div class="col-md-3">
                        <p> {!! Form::radio('level','ticket',$ticket,['onclick'=>'trigger()']) !!} {!! Lang::get('lang.ticket') !!}</p>
                    </div>
                </div>             
            </div>
            
            
            <div class="form-group col-md-6 {{ $errors->has('currency') ? 'has-error' : '' }}">
                {!! Form::label('currency',Lang::get('lang.currency')) !!}
                {!! Form::text('currency',$curr,['class'=>'form-control']) !!}             
            </div>
            
            <div class="form-group col-md-6 {{ $errors->has('trigger_on') ? 'has-error' : '' }}" id="trigger" style="display: none;">
                {!! Form::label('trigger_on',Lang::get('lang.trigger-on')) !!}
                {!! Form::select('trigger_on',[''=>'Select','Statuses'=>$statuses],null,['class'=>'form-control']) !!}             
            </div>
            
            

        </div>
        <!-- /.box-body -->
    </div>
    <div class="box-footer">
       <!-- {!! Form::submit(Lang::get('lang.save'),['class'=>'btn btn-primary']) !!} -->
<!--       {!!Form::button('<i class="fa fa-floppy-o" aria-hidden="true">&nbsp;&nbsp;</i>'.Lang::get('lang.save'),['type' => 'submit', 'class' =>'btn btn-primary','onclick'=>'sendForm()'])!!}-->
           <button type="submit"  class="btn btn-primary" id="submit" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'>&nbsp;</i> Saving..."><i class="fa fa-floppy-o">&nbsp;&nbsp;</i>{!!Lang::get('lang.save')!!}</button>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
</div>
<script>
    function trigger() {
        $("#trigger").show();
    }
    function nontrigger() {
        $("#trigger").hide();
    }
</script>
@stop