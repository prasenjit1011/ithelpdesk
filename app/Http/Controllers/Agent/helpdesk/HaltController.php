<?php

namespace App\Http\Controllers\Agent\helpdesk;

use App\Http\Controllers\Controller;
use App\Model\helpdesk\Ticket\Halt;

class HaltController extends Controller {

    protected $ticket_id;

    public function __construct($ticketid = "") {
        $this->ticket_id = $ticketid;
    }

    public function setTicket($id) {
        $this->ticket_id = $id;
    }

    protected function applySla() {
        $apply_sla = new \App\Http\Controllers\SLA\ApplySla();
        return $apply_sla;
    }

    public function timeUsed($type="response") {
        $sla = $this->applySla();
        $min = $sla->minSpent($this->ticket_id,$type);
        return $min;
    }

    protected function createHalt() {
        $halt = new Halt();
//        if ($halt->where('ticket_id', $this->ticket_id)->first()) {
//            $halt = $halt->where('ticket_id', $this->ticket_id)->first();
//        }
        $ticket = $this->ticket();
        if($ticket->isanswered=='1'){
            //resolve
            $time_used = $this->timeUsed('resolve');
        }else{
            //response
            $time_used = $this->timeUsed();
        }
        $halt->ticket_id = $this->ticket_id;
        $halt->time_used = $time_used;
        $halt->halted_at = \Carbon\Carbon::now();
        $halt->save();

//        $ticket = $this->ticket();
        $ticket->duedate = NULL;
        $ticket->save();
    }

    protected function ticket() {
        $tickets = new \App\Model\helpdesk\Ticket\Tickets();
        $ticket = $tickets->find($this->ticket_id);
        return $ticket;
    }

    protected function changeTicket($due = null) {
        $ticket = $this->ticket();
        if ($ticket) {
            $ticket->duedate = $due->tz('UTC');
            $ticket->save();
        }
    }

    public function changeStatus($statusid) {

        $status = new \App\Http\Controllers\Common\TicketStatus($statusid);
//        dd($status->isHalt());
        if ($status->isHalt()) {
            $this->createHalt();
        } elseif ($this->wasHalt()) {
            $this->recalculateDue();
        }
    }

    public function wasHalt() {
        $ticket = $this->ticket();
        if ($ticket) {
            $status = new \App\Http\Controllers\Common\TicketStatus($ticket->status);
            $check = false;
            if ($status->isHalt()) {
                $check = true;
            }
            return $check;
        }
    }

    public function recalculateDue() {
        $ticket = $this->ticket();
        $halts = new Halt();
        $halt = $halts->where('ticket_id', $ticket->id)->orderBy('id','desc')->first();
        $halted_time = 0;
        if ($halt) {
            $halted_time = $this->haltedTime($halt->halted_at);
            $halt->halted_time = $halted_time;
            $halt->save();
        }
        $total = $ticket->halt()->sum('halted_time');
        $ticket_created = $ticket->created_at;
        $slaid = $ticket->sla;
       
        $apply = $this->applySla();
        $sla = $apply->sla($slaid);
        
        if ($ticket->isanswered == '1') {
            $time = $sla->resolveTime();
        } else {
            $time = $sla->respondTime();
        }
            $resolve = $time + $total;
            $due = $apply->slaResolveDue($slaid, $ticket_created,$resolve,$ticket);
            $this->changeTicket($due);
        
    }
    
     public function haltedTime($halted_at){
        $start = $halted_at;//\Carbon\Carbon::now()->subDay()->tz(timezone());
        //echo "Halted at : ".$start."<br>";
        $end = \Carbon\Carbon::now();//\Carbon\Carbon::now()->tz(timezone());
        //echo "Unhalted at : ".$end."<br>";
        
        $apply = new \App\Http\Controllers\SLA\ApplySla();
        $time  = $apply->businessTime($start, $end, 1);
        return  $time;
    }

}
