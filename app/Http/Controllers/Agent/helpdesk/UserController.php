<?php

namespace App\Http\Controllers\Agent\helpdesk;

// controllers
use App\Http\Controllers\Controller;
use App\Http\Controllers\Common\PhpMailController;
use App\Http\Controllers\Agent\helpdesk\Notifications\NotificationController as Notify;
// requests
/*  Include Sys_user Model  */
use App\Http\Requests\helpdesk\ProfilePassword;
/* For validation include Sys_userRequest in create  */
use App\Http\Requests\helpdesk\ProfileRequest;
/* For validation include Sys_userUpdate in update  */
use App\Http\Requests\helpdesk\Sys_userRequest;
use App\Http\Requests\helpdesk\AgentUpdate;
/*  include guest_note model */
use App\Http\Requests\helpdesk\Sys_userUpdate;
use App\Http\Requests\helpdesk\OtpVerifyRequest;
// change password request 
use App\Http\Requests\helpdesk\ChangepasswordRequest;
// models
use App\Model\helpdesk\Agent_panel\Organization;
use App\Model\helpdesk\Agent_panel\User_org;
use App\Model\helpdesk\Settings\CommonSettings;
use App\Model\helpdesk\Utility\CountryCode;
use App\Model\helpdesk\Utility\Otp;
use App\Model\helpdesk\Email\Emails;
use App\Model\helpdesk\Settings\Email;
use App\Model\helpdesk\Ticket\Tickets;
use App\Model\helpdesk\Agent\Assign_team_agent;
use App\Model\helpdesk\Ticket\Ticket_Thread;
use App\Model\helpdesk\Notification\UserNotification;
use App\Model\helpdesk\Notification\Notification;
use App\Model\helpdesk\Ticket\Ticket_Collaborator;
use App\Model\helpdesk\Agent\Teams;
use App\Model\helpdesk\Ticket\Ticket_attachments;
// use App\Http\Controllers\Agent\helpdesk\Timezones;
use App\Model\helpdesk\Utility\Timezones;
use App\Model\helpdesk\Agent\Groups;
use App\Model\helpdesk\Agent\Department;
use App\Model\helpdesk\Agent\DepartmentAssignAgents;
use App\User;
// classes
use Auth;
use Exception;
use Hash;
use Input;
use Lang;
use Redirect;
use Illuminate\Http\Request;
use DateTime;
use DB;
use Datatables;

/**
 * UserController
 * This controller is used to CRUD an User details, and proile management of an agent.
 *
 * @author      Ladybird <info@ithelpdesk.com>
 */
class UserController extends Controller
{

    /**
     * Create a new controller instance.
     * constructor to check
     * 1. authentication
     * 2. user roles
     * 3. roles must be agent.
     *
     * @return void
     */
    public function __construct(PhpMailController $PhpMailController)
    {
        $this->PhpMailController = $PhpMailController;
        // checking authentication
        $this->middleware('auth');
        // checking if role is agent
        $this->middleware('role.agent');
        //$this->middleware('limit.reached', ['only' => ['store']]);
    }

    /**
     * Display all list of the users.
     *
     * @param type User $user
     *
     * @return type view
     */
    public function index()
    {
        try
        {
            /* get all values in Sys_user */

            $table = \ Datatable::table()
                    ->addColumn(
                            Lang::get('lang.name'), Lang::get('lang.user_name'), Lang::get('lang.email'), Lang::get('lang.phone'), Lang::get('lang.status'), Lang::get('lang.last_login'), Lang::get('lang.role'), Lang::get('lang.action')
                    )  // these are the column headings to be shown
                    ->noScript();
            return view('themes.default1.agent.helpdesk.user.index', compact('table'));
        }
        catch (Exception $e)
        {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    public function deletedUser()
    {
        try
        {
            // dd('here');
            /* get all values in Sys_user */
            return view('themes.default1.agent.helpdesk.user.deleteduser');
        }
        catch (Exception $e)
        {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * This function is used to display the list of users using chumper datatables.
     *
     * @return datatable
     */
    public function user_list(Request $request)
    {
        $type   = $request->input('profiletype');
        $search = $request->input('searchTerm');

        if ($type === 'agents')
        {
            $users = User::where('role', '=', 'agent')->where('is_delete', '=', 0);
        }
        elseif ($type === 'users')
        {
            $users = User::where('role', '=', 'user')->where('is_delete', '=', 0);
        }
        elseif ($type === 'active-users')
        {
            $users = User::where('role', '!=', 'admin')->where('active', '=', 1);
        }
        elseif ($type === 'inactive')
        {
            $users = User::where('role', '!=', 'admin')->where('active', '=', 0);
        }
        elseif ($type === 'deleted')
        {
            $users = User::where('role', '!=', 'admin')->where('is_delete', '=', 1);
        }
        elseif ($type === 'banned')
        {
            $users = User::where('role', '!=', 'admin')->where('ban', '=', 1);
        }
        else
        {
            $users = User::where('role', '!=', 'admin')->where('is_delete', '=', 0);
        }

        $users = $users->select('first_name', 'user_name', 'email', 'mobile', 'active', 'updated_at', 'role', 'id', 'last_name', 'country_code', 'phone_number');

        if ($search !== '')
        {
            $users = $users->where(function ($query) use ($search)
            {
                $query->where('user_name', 'LIKE', '%' . $search . '%');
                $query->orWhere('email', 'LIKE', '%' . $search . '%');
                $query->orWhere('first_name', 'LIKE', '%' . $search . '%');
                $query->orWhere('last_name', 'LIKE', '%' . $search . '%');
                $query->orWhere('mobile', 'LIKE', '%' . $search . '%');
                $query->orWhere('updated_at', 'LIKE', '%' . $search . '%');
                $query->orWhere('country_code', 'LIKE', '%' . $search . '%');
            });
        }
        // displaying list of users with chumper datatables
        // return \Datatable::collection(User::where('role', "!=", "admin")->get())
        return \Datatables::of($users)
                        /* column username */
                        ->removeColumn('id', 'last_name', 'country_code', 'phone_number')
                        ->editColumn('first_name', function ($model)
                        {
                            $name = Lang::get('lang.not-available');
                            if ($model->first_name !== '' && $model->first_name !== null)
                            {
                                $name = $model->first_name . ' ' . $model->last_name;
                                if (strlen($model->first_name . ' ' . $model->last_name) > 15)
                                {
                                    $name = mb_substr($model->first_name . ' ' . $model->last_name, 0, 10, 'UTF-8') . '...';
                                }
                                return '<a  href="' . route('user.show', $model->id) . '" title="' . $model->first_name . ' ' . $model->last_name . '">' . $name . '</a>';
                            }
                            return $name;
                        })
                        ->editColumn('user_name', function ($model)
                        {
                            $user_name = $model->user_name;
                            if (strlen($model->user_name) > 15)
                            {
                                $user_name = mb_substr($model->user_name, 0, 10, 'UTF-8') . '...';
                            }
                            return '<a  href="' . route('user.show', $model->id) . '" title="' . $model->user_name . '">' . $user_name . '</a>';
                        })
                        /* column email */
                        ->addColumn('email', function ($model)
                        {
//                            $email = "<a href='" . route('user.show', $model->id) . "'>" . $model->email . '</a>';
//                            return $email;
                            $email = $model->email;
                            if (strlen($model->email) > 15)
                            {
                                $email = mb_substr($model->email, 0, 10, 'UTF-8') . '...';
                            }
                            return '<a  href="' . route('user.show', $model->id) . '" title="' . $model->email . '">' . $email . '</a>';
                        })
                        /* column phone */
                        ->addColumn('mobile', function ($model)
                        {
                            $phone = '';
                            if ($model->phone_number)
                            {
                                $phone = $model->ext . ' ' . $model->phone_number;
                            }
                            $mobile = '';
                            if ($model->mobile)
                            {
                                $mobile = $model->mobile;
                            }
                            $phone = $phone . '&nbsp;&nbsp;&nbsp;' . $mobile;

                            return $phone;
                        })
                        /* column account status */
                        ->addColumn('active', function ($model)
                        {
                            $status = $model->active;
                            if ($status == 1)
                            {
                                $stat = '<button class="btn btn-success btn-xs" style="pointer-events:none"  >Active</button>';
                            }
                            else
                            {
                                $stat = '<button class="btn btn-danger btn-xs" style="pointer-events:none">Inactive</button>';
                            }

                            return $stat;
                        })
                        /* column last login date */
                        ->addColumn('updated_at', function ($model)
                        {
                            $t = $model->updated_at;

                            return faveoDate($t);
                        })
                        /* column Role */
                        ->addColumn('role', function ($model)
                        {
                            $role = $model->role;

                            return $role;
                        })
                        /* column actions */
                        ->addColumn('Actions', function ($model)
                        {
                            if ($model->is_delete == 0)
                            {
                                return '<a href="' . route('user.edit', $model->id) . '" class="btn btn-primary btn-xs"><i class="fa fa-edit" style="color:white;">&nbsp;</i>' . \Lang::get('lang.edit') . '</a>&nbsp; <a href="' . route('user.show', $model->id) . '" class="btn btn-primary btn-xs"><i class="fa fa-eye" style="color:white;">&nbsp;</i>' . \Lang::get('lang.view') . '</a>';
                            }
                            else
                            {
                                if (Auth::user()->role == 'admin')
                                {
                                    // @if(Auth::user()->role == 'admin')

                                    return '<a href="' . route('user.show', $model->id) . '" class="btn btn-primary btn-xs">' . \Lang::get('lang.view') . '</a>';
                                }

                                if (Auth::user()->role == 'agent')
                                {
                                    // @if(Auth::user()->role == 'admin')
                                    if ($model->role == 'user')
                                    {
                                        return '<a href="' . route('user.show', $model->id) . '" class="btn btn-primary btn-xs">' . \Lang::get('lang.view') . '</a>';
                                    }
                                }
                            }
                        })
                        ->make();
    }

    public function restoreUser($id)
    {
        // dd($id);
        // $delete_all = Input::get('delete_all');
        $users            = User::where('id', '=', $id)->first();
        $users->is_delete = 0;
        $users->active    = 1;
        $users->ban       = 0;
        $users->save();
        return redirect()->back()->with('success1', Lang::get('lang.user_restore_successfully'));
    }

    /**
     * Show the form for creating a new users.
     *
     * @return type view
     */
    public function create(CountryCode $code)
    {
        try
        {
            $aoption         = getAccountActivationOptionValue();
            $email_mandatory = CommonSettings::select('status')->where('option_name', '=', 'email_mandatory')->first();
            $org             = Organization::all();
            return view('themes.default1.agent.helpdesk.user.create', compact('org', 'email_mandatory', 'aoption'));
        }
        catch (Exception $e)
        {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Store a newly created users in storage.
     *
     * @param type User            $user
     * @param type Sys_userRequest $request
     *
     * @return type redirect
     */
    public function store(User $user, Sys_userRequest $request)
    {
        try
        {
            /* insert the input request to sys_user table */
            /* Check whether function success or not */

            // dd($request->org_id);
            if ($request->org_id != "")
            {
                $org_id = Organization::where('id','=',$request->org_id)->select('id')->first();
                // dd($org_id);
                // ->route('user.edit', $id)
                if ($org_id == null)
                {
                    return redirect()->back()->with('fails', Lang::get('lang.please_type_correct_organization_name'));
                }
            }
            if ($request->input('email') != '')
            {
                $user->email = $request->input('email');
            }
            else
            {
                $user->email = null;
            }
            $user->first_name = $request->input('first_name');
            $user->last_name  = $request->input('last_name');
            $user->user_name  = strtolower($request->input('user_name'));
            if ($request->input('mobile') != '')
            {
                $user->mobile = $request->input('mobile');
            }
            else
            {
                $user->mobile = null;
            }
            $user->ext           = $request->input('ext');
            $user->phone_number  = $request->input('phone_number');
            $user->country_code  = $request->input('country_code');
            $user->active        = $request->input('active');
            $user->internal_note = $request->input('internal_note');
            $password            = $this->generateRandomString();
            $user->password      = Hash::make($password);
            $user->role          = 'user';
            $active_code         = '';
            if ($request->get('active') == 0)
            {
                $active_code        = str_random(60);
                $user->email_verify = $active_code;
            }
            $user->mobile_otp_verify = 'verifymobileifenable';
            if ($request->get('country_code') == '' && ($request->get('phone_number') != '' || $request->get('mobile') != ''))
            {
                return redirect()->back()->with(['fails' => Lang::get('lang.country-code-required-error'), 'country_code_error' => 1])->withInput();
            }
            else
            {
                $code = CountryCode::select('phonecode')->where('phonecode', '=', $request->get('country_code'))->get();
                if (!count($code))
                {
                    return redirect()->back()->with(['fails' => Lang::get('lang.incorrect-country-code-error'), 'country_code_error' => 1])->withInput();
                }
            }
            // save user credentails
            if ($user->save() == true)
            {
                if ($request->input('org_id') != "")
                {
                    // $org_id = Organization::where('name', '=', $request->org_id)->select('id')->first();
                    // $add_user_from_org->org_id= $org_id->id;
                    $orgid  =$request->org_id;

                    // $orgid = $request->input('org_id');
                    $role = 'members';
                    $this->storeUserOrgRelation($user->id, $orgid, $role);
                }
                // fetch user credentails to send mail
                $name  = $user->first_name;
                $email = $user->email;
                // send mail on registration

                $notification[] = [
                    'registration_notification_alert' => [
                        'userid'   => $user->id,
                        'from'     => $this->PhpMailController->mailfrom('1', '0'),
                        'message'  => ['subject' => null, 'scenario' => 'registration-notification'],
                        'variable' => ['new_user_name' => $name, 'new_user_email' => $email, 'user_password' => $password]
                    ],
                    'new_user_alert'                  => [
                        'model'    => $user,
                        'userid'   => $user->id,
                        'from'     => $this->PhpMailController->mailfrom('1', '0'),
                        'message'  => ['subject' => null, 'scenario' => 'new-user'],
                        'variable' => ['new_user_name' => $name, 'new_user_email' => $email, 'user_profile_link' => faveoUrl('user/' . $user->id)]
                    ],
                ];
                if ($active_code != '')
                {
                    $notification[] = [
                        'registration_alert' => [
                            'userid'   => $user->id,
                            'from'     => $this->PhpMailController->mailfrom('1', '0'),
                            'message'  => ['subject' => null, 'scenario' => 'registration'],
                            'variable' => ['new_user_name' => $name, 'new_user_email' => $request->input('email'), 'account_activation_link' => faveoUrl('account/activate/' . $active_code)],
                        ]
                    ];
                }
                $notify = new Notify();
                if (!$request->input('email'))
                {
                    $notify->setParameter('send_mail', false);
                }
                $notify->setDetails($notification);
                \Event::fire(new \App\Events\LoginEvent($request));
                return redirect('user')->with('success', Lang::get('lang.user-saved-successfully'));
            }
            /* redirect to Index page with Success Message */
            return redirect()->back()->with('success', Lang::get('lang.user-saved-successfully'));
        }
        catch (\Exception $e)
        {
            /* redirect back with Fails Message */
            return redirect()->back()->with('fails', $e->getMessage())->withInput();
        }
    }

    /**
     * Random Password Genetor for users
     *
     * @param type int  $id
     * @param type User $user
     *
     * @return type view
     */
    public function randomPassword()
    {
        try
        {
            $alphabet    = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~!@#$%^&*(){}[]';
            $pass        = array(); //remember to declare $pass as an array
            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
            for ($i = 0; $i < 10; $i++)
            {
                $n      = rand(0, $alphaLength);
                $pass[] = $alphabet[$n];
            }

            return implode($pass);
        }
        catch (Exception $e)
        {
            /* redirect to Index page with Fails Message */

            return redirect()->back()->with('fails1', $e->getMessage());
            // return redirect('user')->with('fails', $e->getMessage());
        }
    }

    /**
     * Random Password Genetor for users
     *
     * @param type int  $id
     * @param type User $user
     *
     * @return type view
     */
    public function randomPostPassword($id, ChangepasswordRequest $request)
    {
        try
        {
            $changepassword = $request->change_password;
            $user           = User::whereId($id)->first();
            $password       = $request->change_password;
            $user->password = Hash::make($password);
            $user->save();
            $name           = $user->first_name;
            $email          = $user->email;

            $this->PhpMailController->sendmail($from               = $this->PhpMailController
                    ->mailfrom('1', '0'), $to                 = ['name' => $name, 'email' => $email], $message            = ['subject' => null, 'scenario' => 'reset_new_password'], $template_variables = ['user' => $name, 'user_password' => $password]);
            return redirect()->back()->with('success1', Lang::get('lang.password_change_successfully'));
            // return redirect('user')->with('success', Lang::get('lang.password_change_successfully'));
        }
        catch (Exception $e)
        {
            return redirect()->back()->with('fails1', $e->getMessage());
        }
    }

    /**
     * @param type $id
     * @param Request $request
     * @return type
     */
    public function changeRoleAdmin($id, Request $request)
    {
        try
        {
            $user              = User::whereId($id)->first();
            $user->role        = 'admin';
            //$user->assign_group = $request->group;
            $user->primary_dpt = $request->primary_department;
            $user->save();

            $dept_assign_agent = DepartmentAssignAgents::where('agent_id', '=', $user->id)->first();
            if ($dept_assign_agent)
            {
                $dept_assign_agent->agent_id      = $user->id;
                $dept_assign_agent->department_id = $request->primary_department;
                $dept_assign_agent->save();
            }
            else
            {
                $dept_assign_agent                = new DepartmentAssignAgents;
                $dept_assign_agent->agent_id      = $user->id;
                $dept_assign_agent->department_id = $request->primary_department;
                $dept_assign_agent->save();
            }

            // return redirect('user')->with('success', Lang::get('lang.role_change_successfully'));
            return redirect()->back()->with('success1', Lang::get('lang.role_change_successfully'));
        }
        catch (Exception $e)
        {
            /* redirect to Index page with Fails Message */
            // return redirect('user')->with('fails', $e->getMessage());
            return redirect()->back()->with('fails1', $e->getMessage());
        }
    }

    /**
     *
     * @param type $id
     * @param Request $request
     * @return type
     */
    public function changeRoleAgent($id, Request $request)
    {
        try
        {
            $user              = User::whereId($id)->first();
            $user->role        = 'agent';
            //$user->assign_group = $request->group;
            $user->primary_dpt = $request->primary_department;
            $user->save();

            $dept_assign_agent = DepartmentAssignAgents::where('agent_id', '=', $user->id)->first();
            if ($dept_assign_agent)
            {
                $dept_assign_agent->agent_id      = $user->id;
                $dept_assign_agent->department_id = $request->primary_department;
                $dept_assign_agent->save();
            }
            else
            {
                $dept_assign_agent                = new DepartmentAssignAgents;
                $dept_assign_agent->agent_id      = $user->id;
                $dept_assign_agent->department_id = $request->primary_department;
                $dept_assign_agent->save();
            }

            // return redirect('user')->with('success', Lang::get('lang.role_change_successfully'));
            return redirect()->back()->with('success1', Lang::get('lang.role_change_successfully'));
        }
        catch (Exception $e)
        {
            /* redirect to Index page with Fails Message */
            // return redirect('user')->with('fails', $e->getMessage());
            return redirect()->back()->with('fails1', $e->getMessage());
        }
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function changeRoleUser($id)
    {
        try
        {
            $ticket = Tickets::where('assigned_to', '=', $id)->where('status', '=', '1')->get();
            if ($ticket)
            {
                $ticket = Tickets::where('assigned_to', '=', $id)->update(array("assigned_to" => null));
            }
            $user                 = User::whereId($id)->first();
            $user->role           = 'user';
            $user->assign_group   = null;
            $user->primary_dpt    = null;
            $user->remember_token = null;
            $user->save();

            $dept_assign_agent = DepartmentAssignAgents::where('agent_id', '=', $user->id)->delete();


            // return redirect('user')->with('success', Lang::get('lang.role_change_successfully'));
            return redirect()->back()->with('success1', Lang::get('lang.role_change_successfully'));
        }
        catch (Exception $e)
        {
            /* redirect to Index page with Fails Message */
            // return redirect('user')->with('fails', $e->getMessage());
            return redirect()->back()->with('fails1', $e->getMessage());
        }
        // return 'Agent Role Successfully Change to User';
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function deleteAgent($id)
    {
        // try {
        $delete_all = Input::get('delete_all');
        $delete_all = Input::get('delete_all');
        $users      = User::where('id', '=', $id)->first();
        if ($users->role == 'user')
        {
            $users            = User::where('id', '=', $id)->first();
            $users->is_delete = 1;
            $users->active    = 0;
            $users->ban       = 1;
            $users->save();
            // return redirect('user')->with('success', Lang::get('lang.user_delete_successfully'));
            return redirect()->back()->with('success1', Lang::get('lang.user_delete_successfully'));
        }

        if ($users->role == 'agent' || $users->role == 'admin')
        {
            if ($delete_all == null)
            {
                $UserEmail = Input::get('assign_to');
                $assign_to = explode('_', $UserEmail);
                $ticket    = Tickets::where('assigned_to', '=', $id)->get();
                if ($assign_to[0] == 'user')
                {
                    if ($users->id == $assign_to[1])
                    {
                        return redirect()->back()->with('fails1', Lang::get('lang.select_another_agent'));
                    }
                    // $user_detail = User::where('id', '=', $assign_to[1])->first();
                    // $assignee = $user_detail->first_name.' '.$user_detail->last_name;
                    // $ticket_logic1 = Tickets::where('assigned_to', '=', $id)
                    //             ->update(['assigned_to' => $assign_to[1]]);
                    // if ($ticket_logic2 = Tickets::where('user_id', '=', $id)->get()) {
                    //     $ticket_logic2 = Tickets::where('user_id', '=', $id)->update(['user_id' => $assign_to[1]]);
                    // }
                    // if ($ticket_logic3 = Ticket_Thread::where('user_id', '=', $id)->get()) {
                    //     $ticket_logic3 = Ticket_Thread::where('user_id', '=', $id)->update(['user_id' => $assign_to[1]]);
                    // }
                    // if ($ticket_logic4 = User_org::where('user_id', '=', $id)->get()) {
                    //     $ticket_logic4 = User_org::where('user_id', '=', $id)->update(['user_id' => $assign_to[1]]);
                    // }
                    // $thread2 = Ticket_Thread::where('ticket_id', '=', $ticket->id)->first();
                    // $thread2->body = 'This Ticket have been Reassigned to' .' '.  $assignee;
                    // $thread2->save();
                    // UserNotification::where('notification_id', '=', $ticket->id)->delete();
                    // $users = User::where('id', '=', $id)->get();
                    // $organization = User_org::where('user_id', '=', $id)->delete();
                    // Assign_team_agent::where('agent_id', '=', $id)->update(['agent_id' => $assign_to[1]]);
                    $tickets = Tickets::where('assigned_to', '=', $id)->get();


                    foreach ($tickets as $ticket)
                    {
                        # code...

                        $ticket->assigned_to = $assign_to[1];
                        $user_detail         = User::where('id', '=', $assign_to[1])->first();
                        $assignee            = $user_detail->first_name . ' ' . $user_detail->last_name;
                        $ticket_number       = $ticket->ticket_number;
                        $ticket->save();


                        $thread              = new Ticket_Thread();
                        $thread->ticket_id   = $ticket->id;
                        $thread->user_id     = Auth::user()->id;
                        $thread->is_internal = 1;
                        $thread->body        = 'This Ticket has been assigned to ' . $assignee;
                        $thread->save();
                    }
                    $user             = User::find($id);
                    $users->is_delete = 1;
                    $users->active    = 0;
                    $users->ban       = 1;
                    $users->save();

// return redirect('user')->with('success', Lang::get('lang.agent_delete_successfully_and_ticket_assign_to_another_agent'));
                    return redirect()->back()->with('success1', Lang::get('lang.agent_delete_successfully_and_ticket_assign_to_another_agent'));
                }


                // if (User_org::where('user_id', '=', $id)) {
                //     DB::table('user_assign_organization')->where('user_id', '=', $id)->delete();
                // }
                $user             = User::find($id);
                $users->is_delete = 1;
                $users->active    = 0;
                $users->ban       = 1;
                $users->save();

                // return redirect('user')->with('success', Lang::get('lang.agent_delete_successfully'));
                return redirect()->back()->with('success1', Lang::get('lang.agent_delete_successfully'));
            }
            elseif ($delete_all == 1)
            {
                if ($delete_all)
                {
                    // dd('here');
                    $tickets = Tickets::where('assigned_to', '=', $id)->get();
                    // dd($tickets);
                    foreach ($tickets as $ticket)
                    {
                        $ticket->assigned_to = null;
                        $ticket_number       = $ticket->ticket_number;
                        $ticket->save();


                        $thread              = new Ticket_Thread();
                        $thread->ticket_id   = $ticket->id;
                        $thread->user_id     = Auth::user()->id;
                        $thread->is_internal = 1;
                        $thread->body        = 'This Ticket has been unassigned ';
                        $thread->save();
                    }
                    // $users = User::where('id', '=', $id)->get();
                    $user             = User::find($id);
                    $users->is_delete = 1;
                    $users->active    = 0;
                    $users->save();

                    // return redirect('user')->with('success', Lang::get('lang.agent_delete_successfully'));
                    return redirect()->back()->with('success1', Lang::get('lang.agent_delete_successfully'));
                }
                else
                {
                    // Assign_team_agent::where('agent_id', '=', $id)->delete();
                    // User_org::where('user_id', '=', $id)->delete();
                    $user             = User::find($id);
                    $users->is_delete = 1;
                    $users->active    = 0;
                    $users->save();

                    // return redirect('user')->with('success', Lang::get('lang.agent_delete_successfully'));
                    return redirect()->back()->with('success1', Lang::get('lang.agent_delete_successfully'));
                }
            }
            else
            {
                // do something here
            }
        }
        // } catch (Exception $e) {
        /* redirect to Index page with Fails Message */
        // return redirect('user')->with('fails', $e->getMessage());
        // }
    }

    /**
     * Display the specified users.
     *
     * @param type int  $id
     * @param type User $user
     *
     * @return type view
     */
    public function show($id)
    {
        try
        {
            $users = User::where('id', '=', $id)->first();
            if (count($users) > 0)
            {
                return view('themes.default1.agent.helpdesk.user.show', compact('users'));
            }
            else
            {
                return redirect()->back()->with('fails', Lang::get('lang.user-not-found'));
            }
        }
        catch (Exception $e)
        {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param type int  $id
     * @param type User $user
     *
     * @return type Response
     */
    public function edit($id)
    {
        try
        {
            $email_mandatory = CommonSettings::select('status')->where('option_name', '=', 'email_mandatory')->first();
            $aoption         = getAccountActivationOptionValue();
            $user            = User::where('id', '=', $id)->first();
            if ($user)
            {
                $country_code = "auto";
                $country      = CountryCode::select('iso')->where('phonecode', '=', $user->country_code)->first();
                if ($country && $user->country_code != 0)
                {
                    $country_code = $country->iso;
                }
                if ($user->role == 'admin' || $user->role == 'agent')
                {
                    $team        = Teams::where('status', '=', 1)->get();
                    $teams1      = $team->pluck('name', 'id');
                    $timezones   = Timezones::get();
                    // $groups = Groups::where('group_status', '=', 1)->get();
                    $departments = Department::get();
                    $dept        = DepartmentAssignAgents::where('agent_id', '=', $id)->pluck('department_id')->toArray();
                    $table       = Assign_team_agent::where('agent_id', $id)->first();
                    $teams       = $team->pluck('id', 'name')->toArray();
                    $assign      = Assign_team_agent::where('agent_id', $id)->pluck('team_id')->toArray();
                    return view('themes.default1.agent.helpdesk.user.agentedit', compact('teams', 'assign', 'table', 'teams1', 'selectedTeams', 'user', 'timezones', 'departments', 'dept', 'team', 'exp', 'counted', 'country_code', 'aoption'));
                }
                else
                {
                    $users           = $user;
                    $orgs            = Organization::all();
                    $organization_id = User_org::where('user_id', '=', $id)->where('role', '=', 'members')->pluck('org_id')->first();
                    return view('themes.default1.agent.helpdesk.user.edit', compact('users', 'orgs', '$settings', 'email_mandatory', 'organization_id', 'country_code', 'aoption'));
                }
            }
            else
            {
                return redirect()->back()->with('fails', Lang::get('lang.user-not-found'));
            }
        }
        catch (Exception $e)
        {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

// orgAutofill
    /**
     * Update the specified user in storage.
     *
     * @param type int            $id
     * @param type User           $user
     * @param type Sys_userUpdate $request
     *
     * @return type Response
     */
    public function update($id, AgentUpdate $request)
    {
        try
        {
            if ($request->get('country_code') == '' && ($request->get('phone_number') != '' || $request->get('mobile') != ''))
            {
                return redirect()->back()->with(['fails' => Lang::get('lang.country-code-required-error'), 'country_code' => 1])->withInput();
            }
            else
            {
                $code = CountryCode::select('phonecode')->where('phonecode', '=', $request->get('country_code'))->get();
                if (!count($code))
                {
                    return redirect()->back()->with(['fails' => Lang::get('lang.incorrect-country-code-error'), 'country_code' => 1])->withInput();
                }
            }
            $user              = User::whereId($id)->first();
            $old_mobile_number = $user->mobile;
            if ($request->input('country_code') != '' or $request->input('country_code') != null)
            {
                $user->country_code = $request->input('country_code');
            }
            $user->mobile = ($request->input('mobile') == '') ? null : $request->input('mobile');
            $user->fill($request->except('daylight_save', 'limit_access', 'directory_listing', 'vocation_mode', 'assign_team', 'mobile'));
            if ($user->role == 'agent')
            {
                //saving agent specific details
                // storing all the details
                $daylight_save     = $request->input('daylight_save');
                $limit_access      = $request->input('limit_access');
                $directory_listing = $request->input('directory_listing');
                $vocation_mode     = $request->input('vocation_mode');
                //==============================================
                $table             = Assign_team_agent::where('agent_id', $id);
                $table->delete();
                $requests          = $request->input('team');

                if ($requests != null)
                {
                    // inserting team details
                    foreach ($requests as $req)
                    {
                        DB::insert('insert into team_assign_agent (team_id, agent_id) values (?,?)', [$req, $id]);
                    }
                }
                $user->assign_group = $request->group;
                $delete_dept        = DepartmentAssignAgents::where('agent_id', '=', $id)->delete();
                $primary_dpt        = $request->primary_department;
                foreach ($primary_dpt as $primary_dpts)
                {
                    $dept_assign_agent                = new DepartmentAssignAgents;
                    $dept_assign_agent->agent_id      = $user->id;
                    $dept_assign_agent->department_id = $primary_dpts;
                    $dept_assign_agent->save();
                }

                $user->primary_dpt = $primary_dpt[0];
                $user->agent_tzone = $request->agent_time_zone;
            }
            else
            {
                //saving client specific details
                if ($request->org_id != "")
                {
                    $org_id = Organization::where('id', '=', $request->org_id)->select('id')->first();
                    if ($org_id == null)
                    {
                        return redirect()->route('user.edit', $id)->with('fails', Lang::get('lang.please_type_correct_organization_name'));
                    }
                }
                if ($request->input('org_id') == "")
                {
                    $delete_user_from_org = User_org::where('user_id', '=', $id)->delete();
                }
                else
                {
                    $add_user_from_org = User_org::where('user_id', $id)->first();
                    if ($add_user_from_org)
                    {
                        $add_user_from_org->user_id = $id;
                        $org_id                     = Organization::where('id', '=', $request->org_id)->select('id')->first();
                        if ($org_id != null)
                        {
                            $add_user_from_org->org_id = $org_id->id;
                        }
                        $add_user_from_org->role = 'members';
                        $add_user_from_org->save();
                    }
                    else
                    {
                        $add_user_from_org          = new User_org();
                        $add_user_from_org->user_id = $id;
                        $org_id                     = Organization::where('name', '=', $request->org_id)->select('id')->first();
                        if ($org_id != null)
                        {
                            $add_user_from_org->org_id = $org_id->id;
                        }
                        $add_user_from_org->role = 'members';
                        $add_user_from_org->save();
                    }
                }
            }
            if ($old_mobile_number != $request->input('mobile'))
            {
                $user->mobile_otp_verify = "verifymobileifenable";
            }
            $user->save();
            return redirect()->back()->with('success', Lang::get('lang.User-profile-Updated-Successfully'));
        }
        catch (Exception $e)
        {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * get agent profile page.
     *
     * @return type view
     */
    public function getProfile()
    {
        $user = Auth::user();
        try
        {
            return view('themes.default1.agent.helpdesk.user.profile', compact('user'));
        }
        catch (Exception $e)
        {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * get profile edit page.
     *
     * @return type view
     */
    public function getProfileedit(CountryCode $code)
    {
        $user         = Auth::user();
        $country_code = "auto";
        $code         = CountryCode::select('iso')->where('phonecode', '=', $user->country_code)->first();
        if ($code && $user->country_code != 0)
        {
            $country_code = $code->iso;
        }
        $aoption = getAccountActivationOptionValue();
        try
        {
            return view('themes.default1.agent.helpdesk.user.profile-edit', compact('user', 'country_code', 'aoption'));
        }
        catch (Exception $e)
        {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * post profile edit.
     *
     * @param type int            $id
     * @param type ProfileRequest $request
     *
     * @return type Redirect
     */
    public function postProfileedit(ProfileRequest $request)
    {
        try
        {
            // geet authenticated user details
            $user = Auth::user();
            if ($request->get('country_code') == '' && ($request->get('phone_number') != '' || $request->get('mobile') != ''))
            {
                return redirect()->back()->with(['fails' => Lang::get('lang.country-code-required-error'), 'country_code_error' => 1])->withInput();
            }
            else
            {
                $code = CountryCode::select('phonecode')->where('phonecode', '=', $request->get('country_code'))->get();
                if (!count($code))
                {
                    return redirect()->back()->with(['fails' => Lang::get('lang.incorrect-country-code-error'), 'country_code_error' => 1])->withInput();
                }
                $user->country_code = $request->country_code;
            }
            $user->fill($request->except('profile_pic', 'mobile'));
            $user->gender = $request->input('gender');
            $user->save();
            if (Input::file('profile_pic'))
            {
                // fetching picture name
                $name              = Input::file('profile_pic')->getClientOriginalName();
                // fetching upload destination path
                $destinationPath   = public_path('uploads/profilepic');
                // adding a random value to profile picture filename
                $fileName          = rand(0000, 9999) . '.' . str_replace(" ", "_", $name);
                // moving the picture to a destination folder
                Input::file('profile_pic')->move($destinationPath, $fileName);
                // saving filename to database
                $user->profile_pic = $fileName;
            }
            if ($request->get('mobile'))
            {
                $user->mobile = $request->get('mobile');
            }
            else
            {
                $user->mobile = null;
            }
            if ($user->save())
            {
                return Redirect::route('profile')->with('success', Lang::get('lang.Profile-Updated-sucessfully'));
            }
            else
            {
                return Redirect::route('profile')->with('fails', Lang::get('lang.Profile-Updated-sucessfully'));
            }
        }
        catch (Exception $e)
        {
            return Redirect::route('profile')->with('fails', $e->getMessage());
        }
    }

    /**
     * Post profile password.
     *
     * @param type int             $id
     * @param type ProfilePassword $request
     *
     * @return type Redirect
     */
    public function postProfilePassword($id, ProfilePassword $request)
    {
        // get authenticated user
        $user = Auth::user();
        // checking if the old password matches the new password
        if (Hash::check($request->input('old_password'), $user->getAuthPassword()))
        {
            $user->password = Hash::make($request->input('new_password'));
            try
            {
                $user->save();

                return redirect('profile-edit')->with('success1', Lang::get('lang.password_updated_sucessfully'));
            }
            catch (Exception $e)
            {
                return redirect('profile-edit')->with('fails', $e->getMessage());
            }
        }
        else
        {
            return redirect('profile-edit')->with('fails1', Lang::get('lang.password_was_not_updated_incorrect_old_password'));
        }
    }

    /**
     * Assigning an user to an organization.
     *
     * @param type $id
     *
     * @return type boolean
     */
    public function UserAssignOrg($id)
    {
        $org_name = Input::get('org');
        if ($org_name)
        {
            $org = Organization::where('name', '=', $org_name)->pluck('id')->first();
            if ($org)
            {
                $user_org          = new User_org();
                $user_org->org_id  = $org;
                $user_org->user_id = $id;
                $user_org->role    = 'members';
                $user_org->save();

                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 2;
        }
    }

    public function UsereditAssignOrg($id)
    {
        $org_name = Input::get('org');

        if ($org_name)
        {
            $org = Organization::where('name', '=', $org_name)->pluck('id')->first();
            if ($org)
            {
                $user_org          = User_org::where('user_id', '=', $id)->first();
                $user_org->org_id  = $org;
                $user_org->user_id = $id;
                $user_org->role    = 'members';
                $user_org->save();

                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 2;
        }
    }

    public function orgAssignUser($id)
    {
        $org               = Input::get('org');
        $user_org          = new User_org();
        $user_org->org_id  = $id;
        $user_org->user_id = $org;
        $user_org->role    = 'members';
        $user_org->save();

        return 1;
    }

    public function removeUserOrg($id)
    {
        // dd($id);
        $user_org = User_org::where('user_id', '=', $id)->first();

        $user_org->delete();

        return redirect()->back()->with('success1', Lang::get('lang.the_user_has_been_removed_from_this_organization'));
    }

    /**
     * creating an organization in user profile page via modal popup.
     *
     * @param type $id
     *
     * @return type
     */
    public function User_Create_Org($id)
    {
        // checking if the entered value for website is available in database
        if (Input::get('website') != null)
        {
            // checking website
            $check = Organization::where('website', '=', Input::get('website'))->first();
        }
        else
        {
            $check = null;
        }
        // checking if the name is unique
        $check2 = Organization::where('name', '=', Input::get('name'))->first();
        // if any of the fields is not available then return false
        if (\Input::get('name') == null)
        {
            return 'Name is required';
        }
        elseif ($check2 != null)
        {
            return 'Name should be Unique';
        }
        elseif ($check != null)
        {
            return 'Website should be Unique';
        }
        else
        {
            // storing organization details and assigning the current user to that organization
            $org                 = new Organization();
            $org->name           = Input::get('name');
            $org->phone          = Input::get('phone');
            $org->website        = Input::get('website');
            $org->address        = Input::get('address');
            $org->internal_notes = Input::get('internal');
            $org->save();

            $user_org          = new User_org();
            $user_org->org_id  = $org->id;
            $user_org->user_id = $id;
            $user_org->save();
            // for success return 0
            return 0;
        }
    }

    /**
     * Generate a random string for password.
     *
     * @param type $length
     *
     * @return string
     */
    public function generateRandomString($length = 10)
    {
        // list of supported characters
        $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        // character length checked
        $charactersLength = strlen($characters);
        // creating an empty variable for random string
        $randomString     = '';
        // fetching random string
        for ($i = 0; $i < $length; $i++)
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        // return random string
        return $randomString;
    }

    public function storeUserOrgRelation($userid, $orgid, $role)
    {
        $org_relations = new User_org();
        $org_relation  = $org_relations->where('user_id', $userid)->first();
        if ($org_relation)
        {
            $org_relation->delete();
        }
        $org_relations->create([
            'user_id' => $userid,
            'org_id'  => $orgid,
            'role'    => $role,
        ]);
    }

    public function getExportUser()
    {
        try
        {
            return view('themes.default1.agent.helpdesk.user.export');
        }
        catch (Exception $ex)
        {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function exportUser(Request $request)
    {
        try
        {
            $date             = $request->input('date');
            $date             = str_replace(' ', '', $date);
            $date_array       = explode(':', $date);
            $first            = $date_array[0] . " 00:00:00";
            $second           = $date_array[1] . " 23:59:59";
            $first_date       = $this->convertDate($first);
            $second_date      = $this->convertDate($second);
            $users            = $this->getUsers($first_date, $second_date);
            $excel_controller = new \App\Http\Controllers\Common\ExcelController();
            $filename         = "users" . $date;
            $excel_controller->export($filename, $users);
        }
        catch (Exception $ex)
        {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function convertDate($date)
    {
        $converted_date = date('Y-m-d H:i:s', strtotime($date));
        return $converted_date;
    }

    public function getUsers($first, $last)
    {
        $user  = new User();
        $users = $user->leftJoin('user_assign_organization', 'users.id', '=', 'user_assign_organization.user_id')
                ->leftJoin('organization', 'user_assign_organization.org_id', '=', 'organization.id')
                ->whereBetween('users.created_at', [$first, $last])
                //->where('users.role', 'user')
                //->where('users.active', 1)
                ->select('users.user_name as Username', 'users.email as Email', 'users.first_name as Fisrtname', 'users.last_name as Lastname', 'organization.name as Organization', 'users.role as Role', 'users.active as Active')
                ->get()
                ->toArray();
        return $users;
    }

    public function resendOTP(OtpVerifyRequest $request)
    {
        if (\Schema::hasTable('sms'))
        {
            $sms = DB::table('sms')->get();
            if (count($sms) > 0)
            {
                \Event::fire(new \App\Events\LoginEvent($request));
                return 1;
            }
        }
        else
        {
            return "Plugin has not been setup successfully.";
        }
    }

    public function verifyOTP()
    {
        $user = User::select('id', 'mobile', 'country_code', 'user_name', 'mobile_otp_verify', 'updated_at')->where('id', '=', Input::get('u_id'))->first();
        $otp  = Input::get('otp');
        if ($otp != null || $otp != '')
        {
            $otp_length = strlen(Input::get('otp'));
            if (($otp_length == 6 && !preg_match("/[a-z]/i", Input::get('otp'))))
            {
                $otp2     = Hash::make(Input::get('otp'));
                $date1    = date_format($user->updated_at, "Y-m-d h:i:sa");
                $date2    = date("Y-m-d h:i:sa");
                $time1    = new DateTime($date2);
                $time2    = new DateTime($date1);
                $interval = $time1->diff($time2);
                if ($interval->i > 10 || $interval->h > 0)
                {
                    $message = Lang::get('lang.otp-expired');
                    return $message;
                }
                else
                {
                    if (Hash::check(Input::get('otp'), $user->mobile_otp_verify))
                    {
                        User::where('id', '=', $user->id)
                                ->update([
                                    'mobile_otp_verify' => '1',
                                    'mobile'            => Input::get('mobile'),
                                    'country_code'      => str_replace('+', '', Input::get('country_code'))
                        ]);
                        return 1;
                    }
                    else
                    {
                        $message = Lang::get('lang.otp-not-matched');
                        return $message;
                    }
                }
            }
            else
            {
                $message = Lang::get('lang.otp-invalid');
                return $message;
            }
        }
        else
        {
            $message = Lang::get('lang.otp-not-matched');
            return $message;
        }
    }

    /**
     */
    public function getAgentDetails(Request $request)
    {


        // $ids=$request->ticket_id;
        // $ticket_dept_ids = Tickets::whereIn('id',$ids)->pluck('dept_id')->unique();
        // foreach($ticket_dept_ids as $id){
        //     $users_ids = DepartmentAssignAgents::where('department_id',$id);
        // }
        // $agent_ids = $users_ids->pluck('agent_id')->unique()->toArray();
        // $users_ids = DepartmentAssignAgents::whereIn('department_id',$ticket_dept_ids)->pluck('agent_id')->unique()->toArray();
        // $agents = User::whereIn('id',$agent_ids)->select('id','user_name','first_name','last_name')->get();



        $assignto_agent     = User::where('role', '!=', 'user')->select('id', 'user_name', 'first_name', 'last_name')->where('active', '=', 1)->orderBy('first_name')->get();
        $count_assign_agent = count($assignto_agent);
        $teams              = Teams::where('status', '=', '1')->where('team_lead', '!=', null)->get();
        $count_teams        = count($teams);
        $html111            = "";
        $html11             = "";
        $html1              = "";
        // $html111.= "<option value=' '>" . 'select assigner' . "</option>";
        foreach ($assignto_agent as $agent)
        {
            $html1 .= "<option value='user_" . $agent->id . "'>" . $agent->first_name . ' ' . $agent->last_name . "</option>";
        }
        $html11.="<optgroup label=" . 'Agents(' . $count_assign_agent . ')' . ">";
        $html22 = "";
        $html2  = "";
        foreach ($teams as $team)
        {
            $html2 .= "<option value='team_" . $team->id . "'>" . $team->name . "</option>";
        }
        $html22.="<optgroup label=" . 'Teams(' . $count_teams . ')' . ">";

        echo $html11, $html1, $html22, $html2;


        //     if($agents){
        //     foreach ($agents as $user) {
        //          echo "<option value='user_$user->id'>".$user->name().'</option>';
        //     }
        // }
        // else{
        // }
    }

    /**
     *
     * @param Request $request
     * @return string
     */
    public function settingsUpdateStatus(Request $request)
    {
        try
        {
            $user_id     = $request->user_id;
            $user_status = $request->settings_status;
            User::where('id', $user_id)->update(['active' => $user_status]);
            return Lang::get('lang.your_status_updated_successfully');
        }
        catch (Exception $e)
        {
            return Redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     *
     * @param Request $request
     * @return string
     */
    public function settingsUpdateBan(Request $request)
    {
        try
        {
            $user_id  = $request->user_id;
            $user_ban = $request->settings_ban;
            User::where('id', $user_id)->update(['ban' => $user_ban]);
            return Lang::get('lang.your_status_updated_successfully');
            //\Redirect::route('approval.settings')->with('success', Lang::get('lang.approval_settings-created-successfully'));
            // return Redirect('approval.settings')->with('success', Lang::get('lang.approval_settings-created-successfully'));
        }
        catch (Exception $e)
        {
            return Redirect()->back()->with('fails', $e->getMessage());
        }
    }

    public function orgAutofill()
    {
        // dd('ghghgh');
        return view('themes.default1.agent.helpdesk.user.orgautocomplite');
    }

    public function createRequester(Request $request)
    {

        $this->validate($request, [
            'email'     => 'required|max:50|email|unique:users',
            'full_name' => 'required',
        ]);
        try
        {
            $auth_control        = new \App\Http\Controllers\Auth\AuthController();
            $user_create_request = new \App\Http\Requests\helpdesk\RegisterRequest();
            $user                = new User();
            $password            = str_random(8);
            $all                 = $request->all() + ['password' => $password, 'password_confirmation' => $password];
            $user_create_request->replace($all);
            $response            = $auth_control->postRegister($user, $user_create_request, true);
            $status              = 200;
        }
        catch (\Exception $e)
        {
            $response = ['error' => [$e->getMessage()]];
            $status   = 500;
            return response()->json($response, $status);
        }
        return response()->json(compact('response'), $status);
    }

    public function getRequesterForCC(Request $request)
    {
        try
        {
            $this->validate($request, [
                'term' => 'required',
            ]);
            $term      = $request->input('term');
            $requester = User::where('ban', 0)
                    ->where('active', 1)
                    ->where('is_delete', 0)
                    ->whereNotNull('email')
                    ->where('email', 'LIKE', '%' . $term . '%')
                    ->select('id', 'user_name', 'email', 'first_name', 'last_name', 'profile_pic')
                    ->get();
            $response  = $requester->toArray();
            $status    = 200;
        }
        catch (\Exception $e)
        {
            $response = ['error' => [$e->getMessage()]];
            $status   = 500;
            return response()->json($response, $status);
        }
        return response()->json(compact('response'), $status);
    }

    public function find(Request $request) {
        try {
            $term = trim($request->q);
            if (empty($term)) {
                return \Response::json([]);
            }
            $orgs = Organization::where('name', 'LIKE', '%' . $term . '%')->select('id', 'name')->get();
            $formatted_tags = [];

            foreach ($orgs as $org) {
                $formatted_orgs[] = ['id' => $org->id, 'text' => $org->name];
            }

            return \Response::json($formatted_orgs);
        } catch (Exception $e) {
            // returns if try fails with exception meaagse
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

}
