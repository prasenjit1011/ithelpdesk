<?php

namespace App\Http\Controllers\Agent\helpdesk\Filter;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Exception;
use App\Model\helpdesk\Filters\Tag;
use App\Model\helpdesk\Manage\Tickettype;
use Lang;

class TagController extends Controller {
    
    public function __construct() {
        $this->middleware(['auth', 'role.agent']);
    }

    public function store(Request $request) {
        try {
            $tag = new Tag();
            return $this->save($tag, $request->input());
        } catch (Exception $ex) {
            dd($ex);
        }
    }

    public function save($model, $request) {
        $tags = $request->input('tags');
        if (is_array($tags) && count($tags) > 0) {
            foreach ($tags as $t) {
                $tag = $model->where('name', $t)->first();
                if (!$tag) {
                    $model->create([
                        'name' => $t,
                        'description' => '',
                    ]);
                }
            }
        }
    }

    public function addToFilter(Request $request) {
        $ticket_id = $request->input('ticket_id');
        $tags = $request->input('tags');
        $tag = new Tag();
        $this->save($tag, $request);
        $this->saveToFilter($ticket_id, $tags);
    }

    public function saveToFilter($ticket_id, $tags) {
        $filters = new \App\Model\helpdesk\Filters\Filter();
        $filter = $filters->where('key', 'tag')->where('ticket_id', $ticket_id)->get();
        if ($filter->count() > 0) {
            foreach ($filter as $f) {
                $f->delete();
            }
        }
        if ($tags && is_array($tags)) {
            foreach ($tags as $tag) {
                $filters->create([
                    'ticket_id' => $ticket_id,
                    'key' => 'tag',
                    'value' => $tag
                ]);
            }
        }
    }

    public function getTag(Request $request) {
        $term = $request->input('term');
        $tag = new Tag();
        $tags = $tag->where('name', 'LIKE', '%' . $term . '%')->pluck('name')->toArray();
        return $tags;
    }

    public function getType(Request $request) {
        $term = $request->input('term');

        $types = new Tickettype();

        $type = $types->where('name', 'LIKE', '%' . $term . '%')->where('status', '=', 1)->pluck('name')->toArray();

        return $type;
    }

    public function tag() {
        return view('themes.default1.admin.helpdesk.tag.index');
    }

    public function ajaxDatatable() {
        $tags = Tag::leftJoin('filters', function($join) {
                    $join->on('tags.name', '=', 'filters.value')
                    ->where('filters.key', '=', 'tag')
                    ;
                })
                ->select(
                        'tags.id'
                        , 'tags.name'
                        , 'tags.description'
                        , \DB::raw('count(filters.ticket_id) as count')
                )
                ->groupBy('tags.name')
        //->get()        
        ;
        return \Datatables::of($tags)
                        ->addColumn('action', function ($tag) {
                            $delete = deletePopUp($tag->id, url('tag/' . $tag->id . '/delete'));
                            $edit = "<a href='" . url('tag/' . $tag->id . '/edit') . "' class='btn btn-primary btn-xs'><i class='fa fa-edit' style='color:white;'>&nbsp;</i>" . trans('lang.edit') . "</a>&nbsp;";
                           
                            return $edit . " " . $delete;
                        })
                        ->editColumn('count', function($tag) {
                            return "<a href='" . url('/tickets?tags[]=') .$tag->name."'>" . $tag->count . "</a>";
                        })
                        ->removeColumn('id')
                        ->make();
    }

    public function create() {
        try {
            return view('themes.default1.admin.helpdesk.tag.create');
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function edit($id) {
        try {
            $tag = Tag::find($id);
            if (!$tag) {
                return redirect()->back()->with('fails', 'Tag not found');
            }
            return view('themes.default1.admin.helpdesk.tag.edit', compact('tag'));
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function update($id, Request $request) {
        $this->validate($request, [
            'name' => 'max:20|required|unique:tags,name,' . $id,
             'description' => 'max:50'
        ]);
        try {
            $tag = Tag::find($id);
            if ($tag) {
                $tag->update([
                    'name' => $request->input('name'),
                    'description' => $request->input('description')
                ]);
            } else {
                return redirect()->back()->with('fails', 'Tag not found');
            }
            return redirect('tag')->with('success',Lang::get('lang.tag_updated_successfully'));
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function postCreate(Request $request) {
        $this->validate($request, [
            'name' => 'required|unique:tags,name|max:20',
            'description' => 'max:50'
        ]);
        try {
            Tag::create([
                'name' => $request->input('name'),
                'description' => $request->input('description')
            ]);
            return redirect('tag')->with('success',Lang::get('lang.tag_saved_successfully'));
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function destroy($id) {
        try {
            $tag = Tag::find($id);
            if ($tag) {
                $tag->delete();
            } else {
                return redirect()->back()->with('fails', 'Tag not found');
            }
            return redirect('tag')->with('success',Lang::get('lang.tag_deleted_successfully'));
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

}
