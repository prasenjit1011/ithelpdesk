<?php

namespace App\Http\Controllers\Agent\helpdesk;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//model
use App\User;
use App\Model\helpdesk\Agent\Department;
use App\Model\helpdesk\Agent\DepartmentAssignAgents;
use App\Model\helpdesk\Manage\Tickettype;
use App\Model\helpdesk\Ticket\Tickets;
use App\Model\helpdesk\Ticket\Ticket_Priority;
use App\Model\helpdesk\Ticket\Ticket_source;
use App\Model\helpdesk\Ticket\Ticket_Status;
use App\Model\helpdesk\Manage\Sla\Sla_plan;
use App\Model\helpdesk\Filters\Label;
use App\Model\helpdesk\Filters\Tag;
use App\Model\helpdesk\Agent\Teams;
use App\Model\helpdesk\Manage\Help_topic;
// classes
use Exception;
use Lang;

class DropdownController extends Controller
{
    /**
     * Create a new controller instance.
     * constructor to check
     * 1. authentication
     * 2. user roles
     * 3. roles must be agent.
     *
     * @return void
     */
    public function __construct()
    {
        // checking authentication
        $this->middleware('auth');
        // checking if role is agent
        $this->middleware('role.agent');
    }


    /**
     *
     *
     *
     */
    public function getDepartmentDropdownList(Request $req)
    {
        $departments = '';
        if (\Auth::user()->role === 'admin') {
            $departments = Department::select('name as name_id', 'name as text')->where('name', 'like', '%'.$req->get('name').'%')->get();
        } else {
            $dept_id = [];
            $user_departments = DepartmentAssignAgents::select('department_id')->where('agent_id', '=', \Auth::user()->id)->get();
            if ($user_departments) {
                foreach ($user_departments as $user_department) {
                    array_push($dept_id, $user_department->department_id);
                }
            }
            $departments = Department::select('name as name_id', 'name as text')
                ->whereIn('id', $dept_id)
                ->where('name', 'like', '%'.$req->get('name').'%')->get();
        }
        return $this->formatToJson($departments, 'department-list');
    }


    public function getPriorityDropdownList(Request $req, Ticket_Priority $priority)
    {
        $priority = $priority->select('priority as name_id', 'priority as text')->where('priority', 'like', '%'.$req->get('name').'%')->where('status', '=', 1)->get();
        return $this->formatToJson($priority, 'priority-list');
    }

    public function getSLADropdownList(Request $req, Sla_plan $sla_plans)
    {
        $sla_plans = $sla_plans->select('name as name_id', 'name as text')->where('name', 'like', '%'.$req->get('name').'%')->where('status', '=', 1)->get();
        // dd($sla_plans);
        return $this->formatToJson($sla_plans, 'sla-list');
    }

    public function getLabelsDropdownList(Request $req, Label $labels)
    {
        $labels = $labels->select('title as name_id', 'title as text')->where('title', 'like', '%'.$req->get('name').'%')->get();
        return $this->formatToJson($labels, 'labels-list');
    }

    public function getTagsDropdownList(Request $req, Tag $tag)
    {
        $tags = $tag->select('name as name_id', 'name as text')->where('name', 'like', '%'.$req->get('name').'%')->get();
        return $this->formatToJson($tags, 'tag-list');
    }

    public function getSourceDropdownList(Request $req, Ticket_source $source)
    {
        $source = $source->select('name as name_id', 'name as text')->where('name', 'like', '%'.$req->get('name').'%')->get();
        return $this->formatToJson($source, 'source-list');
    }

    public function getTypesDropdownList(Request $req, Tickettype $type)
    {
        $type = $type->select('name as name_id', 'name as text')->where('name', 'like', '%'.$req->get('name').'%')->get();
        return $this->formatToJson($type, 'type-list');
    }

    public function getOwnersDropdownList(Request $req, User $user)
    {
        $users = $user->select(
            'users.user_name as name_id',
            'users.first_name',
            'users.last_name',
            'users.email',
            'users.id',
            'users.profile_pic'
        )->where(function ($query) use ($req) {
                $query->where('user_name', 'like', '%'.$req->get('name').'%')
                ->orWhere('first_name', 'like', '%'.$req->get('name').'%')
                ->orWhere('last_name', 'like', '%'.$req->get('name').'%')
                ->orWhere('email', 'like', '%'.$req->get('name').'%');
        })
        ->where([['role', '=', 'user'], ['active', '=', 1]])
        ->get();
        return $this->formatToJson($users, 'user-list');
    }

    public function getStatusDropdownList(Request $req)
    {
        $status = \DB::table('ticket_status as st')->select('st.name as name_id', 'st.name as text')->where('st.name', 'like', '%'.$req->get('name').'%');
        if ($req->get('showing') == 'inbox' || $req->get('showing') == 'mytickets' || $req->get('showing') == 'followup' || $req->get('showing') == 'overdue') {
            $status = $status->join('ticket_status_type as tst', 'tst.id', '=', 'st.purpose_of_status')->where('tst.name', '=', 'open')->get();
        } elseif ($req->get('showing') == 'closed') {
            $status = $status->join('ticket_status_type as tst', 'tst.id', '=', 'st.purpose_of_status')->where('tst.name', '=', 'closed')->get();
        } elseif ($req->get('showing') == 'trash') {
            $status = $status->join('ticket_status_type as tst', 'tst.id', '=', 'st.purpose_of_status')->where('tst.name', '=', 'deleted')->get();
        } elseif ($req->get('showing') == 'approval') {
            $status = $status->join('ticket_status_type as tst', 'tst.id', '=', 'st.purpose_of_status')->where('tst.name', '=', 'approval')->get();
        } else {
            $status = $status->get();
        }
        return $this->formatToJson($status, 'status-list');
    }

    public function getAssigneesLDropdownList(Request $req, User $user, Teams $teams)
    {
        $users = $user->select('users.user_name as name_id', 'users.first_name', 'users.last_name', 'users.email', 'users.id', 'users.profile_pic')
            ->where(function ($query) use ($req) {
                $query->where('user_name', 'like', '%'.$req->get('name').'%')
                ->orWhere('first_name', 'like', '%'.$req->get('name').'%')
                ->orWhere('last_name', 'like', '%'.$req->get('name').'%')
                ->orWhere('email', 'like', '%'.$req->get('name').'%');
            })
            ->where([['role', '!=', 'user'], ['active', '=', 1]])
        ->get();
        $teams = $teams->select(\DB::raw('CONCAT("t-", name) as name_id'), 'name as text')->where('name', 'like', '%'.$req->get('name').'%')->get();
        return json_encode(array_merge(json_decode($this->formatToJson($users, 'user-list'), true), json_decode($this->formatToJson($teams, 'teams-list'), true)));
    }

    public function postAssigneesLDropdownList(Request $req, User $user, Teams $teams)
    {
        $names = $req->get('name');
        $team_array = [];
        $agent_array = [];
        foreach ($names as $value) {
            if (substr($value, 0, 2) == 'a-') {
                array_push($agent_array, substr($value, 2, strlen($value)));
            } elseif (substr($value, 0, 2) == 't-') {
                array_push($team_array, substr($value, 2, strlen($value)));
            }
        }
        $users = $user->select('users.user_name as name_id', 'users.first_name', 'users.last_name', 'users.email', 'users.id', 'users.profile_pic')->whereIn('user_name', $agent_array)->orWhereIn('email', $agent_array)->get();
        $teams = $teams->select(\DB::raw('CONCAT("t-", name) as name_id'), 'name as text')->whereIn('name', $team_array)->get();
        return json_encode(array_merge(json_decode($this->formatToJson($users, 'user-list'), true), json_decode($this->formatToJson($teams, 'teams-list'), true)));
    }

    public function getTicketNumberDropdownList(Request $req, Tickets $tickets)
    {
        $filter = new \App\Http\Controllers\Agent\helpdesk\Filter\FilterController($req);
        $tickets = $filter->showPage([$req->get('showing')], $tickets);
        // dd($tickets->count());
        $tickets = $tickets->leftJoin('ticket_thread as th', 'th.ticket_id', '=', 'tickets.id')
                    ->select(
                        'ticket_number as name_id',
                        'ticket_number as text',
                        \DB::raw('substring_index(group_concat(th.title order by th.id asc) , ",", 1) as ticket_title')
                    )
                    ->having('ticket_number', 'like', '%'.$req->get('name').'%')
                    ->orHaving('ticket_title', 'LIKE', '%'.$req->get('name').'%')
                    ->groupBy('ticket_number')->get();
        return $this->formatToJson($tickets, 'tickets-list');
    }

    public function getFilteredTicketNumbers(Request $req, Tickets $tickets)
    {
        $tickets = $tickets->select('ticket_number as name_id', 'ticket_number as text')->whereIn('ticket_number', $req->get('name'))->get();
        return $this->formatToJson($tickets, 'tickets-list');
    }

    public function getHelpTopicDropdownList(Request $req, Tickets $tickets, Help_topic $help)
    {
        $help = $help->select('topic as name_id', 'topic as text')->where('topic', 'like', '%'.$req->get('name').'%')->where('status', '=', 1)->get();
        return $this->formatToJson($help, 'help_topic_list');
    }

    public function formatToJson($data, $type)
    {
        $array = [];
        if ($type != 'user-list') {
            if ($type == 'teams-list') {
                foreach ($data as $d) {
                    $array2['id'] = $d->name_id;
                    $array2['text'] = $d->text;
                    $array2['details'] = Lang::get('lang.team').': '.$d->text;
                    array_push($array, $array2);
                }
            } elseif ($type == 'tickets-list') {
                foreach ($data as $d) {
                    $array2['id'] = $d->name_id;
                    $array2['text'] = $d->text;
                    $array2['details'] = '<b>'.Lang::get('lang.ticket_id').'</b> : '.$d->name_id.'<br><b>'.Lang::get('lang.subject').'</b> : '.ucfirst($d->ticket_title);
                    array_push($array, $array2);
                }
            } else {
                foreach ($data as $d) {
                    $array2['id'] = $d->name_id;
                    $array2['text'] = $d->text;
                    array_push($array, $array2);
                }
            }
        } else {
            foreach ($data as $d) {
                $name = ($d->first_name != '' && $d->first_name != null) ? $d->first_name.' '.$d->last_name : $d->name_id;
                $array2['id'] = ($d->email != '' && $d->email != Lang::get('lang.not-available'))? 'a-'.$d->email : 'a-'.$d->name_id;
                $array2['text'] = $name;
                $array2['details'] = '<div class="col-xs-6">
                                <img src="'.$d->profile_pic.'"></div>'.$name.'<br>'.Lang::get('lang.email').': '.$d->email;
                array_push($array, $array2);
            }
        }
        $data_json = json_encode($array);
        return $data_json;
    }
}
