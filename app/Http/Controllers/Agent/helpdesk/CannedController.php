<?php

namespace App\Http\Controllers\Agent\helpdesk;

// controllers
use App\Http\Controllers\Controller;
// requests
use App\Http\Requests\helpdesk\CannedRequest;
use App\Http\Requests\helpdesk\CannedUpdateRequest;
// model
use App\Model\helpdesk\Agent_panel\Canned;
use App\Model\helpdesk\Agent_panel\DepartmentCannedResponse as DeptCann;
use App\Model\helpdesk\Agent\DepartmentAssignAgents;
use App\Model\helpdesk\Agent\Department;
use App\User;
// classes
use Exception;
use Lang;

/**
 * CannedController.
 *
 * This controller is for all the functionalities of Canned response for Agents in the Agent Panel
 *
 * @author      Ladybird <info@ithelpdesk.com>
 */
class CannedController extends Controller
{
    /**
     * Create a new controller instance.
     * constructor to check
     * 1. authentication
     * 2. user roles
     * 3. roles must be agent.
     *
     * @return void
     */
    public function __construct()
    {
        // checking authentication
        $this->middleware('auth');
        // checking if role is agent
        $this->middleware('role.agent');
    }

    /**
     * Display a listing of the Canned Responses.
     *
     * @return type View
     */
    public function index()
    {
        try {
            $Canneds = $this->getCannedBuilder(\Auth::user()->id);
            return view('themes.default1.agent.helpdesk.canned.index', compact('Canneds'));
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    /**
     * Show the form for creating a new Canned Response.
     *
     * @return type View
     */
    public function create()
    {
        try {
            return view('themes.default1.agent.helpdesk.canned.create');
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    /**
     * Store a newly created Canned Response.
     *
     * @param type CannedRequest $request
     * @param type Canned        $canned
     *
     * @return type Redirect
     */
    public function store(CannedRequest $request, Canned $canned)
    {
        try {
            $has_dept_share = false;
            if($request->has('share')) {
                if(!$request->has('d_id')) {
                    return redirect()->back()->withInput()->with('fails', Lang::get('lang.no-department-selected-to-share-respone'));
                } else {
                    $has_dept_share = true;
                }
            }
            // fetching all the requested inputs
            $canned->user_id = \Auth::user()->id;
            $canned->title = $request->input('title');
            $canned->message = $request->input('message');
            // saving inputs
            $canned->save();
            if ($has_dept_share) {
                $dept_ids = \DB::table('department')->whereIn('name', $request->get('d_id'))->pluck('id');
                if(count($dept_ids) >0) {
                    foreach ($dept_ids as $dept_id) {
                        $dept_cann =  new DeptCann;
                        $dept_cann->dept_id   = $dept_id;
                        $dept_cann->canned_id = $canned->id;
                        $dept_cann->save();
                    }
                }
            }

            return redirect()->route('canned.list')->with('success', Lang::get('lang.canned_response_saved_successfully'));
        } catch (Exception $e) {
            
            return redirect()->route('canned.list')->with('fails', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the Canned Response.
     *
     * @param type        $id
     * @param type Canned $canned
     *
     * @return type View
     */
    public function edit($id, Canned $canned)
    {
        try {
            $found = 0;
            $Canneds = $this->getCannedBuilder(\Auth::user()->id);
            foreach ($Canneds->toArray() as $value) {
                if (array_search($id, $value)) {
                    $found = 1;
                    break;
                }
            }
            if ($found == 1) {
                $canned = $canned->where('id', '=', $id)->first();
                // fetching requested canned response
                $dept_cann_count = DeptCann::select('id')->where('canned_id', '=', $id)->count();
                $is_shared = 0;
                if ($dept_cann_count > 0) {
                    $is_shared = 1;
                }
                return view('themes.default1.agent.helpdesk.canned.edit', compact('canned', 'is_shared'));
            } else {
                return redirect()->route('canned.list')->with('fails', Lang::get('lang.not-autherised'));
            }
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    /**
     * Update the Canned Response in database.
     *
     * @param type                     $id
     * @param type CannedUpdateRequest $request
     * @param type Canned              $canned
     *
     * @return type Redirect
     */
    public function update($id, CannedUpdateRequest $request, Canned $canned)
    {
        try {
            if ($request->has('share')) {
                if ($request->has('d_id')) {
                    $dept_ids = \DB::table('department')->whereIn('name', $request->get('d_id'))->pluck('id');
                    $delete = DeptCann::select('id')->whereNotIn('dept_id', $dept_ids)->where('canned_id', '=', $id)->delete();
                    foreach ($dept_ids as $dept_id) {
                        DeptCann::updateOrCreate([
                            'dept_id' => $dept_id,
                            'canned_id' => $id
                        ]);
                    }
                } else {
                    return redirect()->back()->withInput()->with('fails', Lang::get('lang.no-department-selected-to-share-respone'));
                }
            } else {
                $dept_cann = DeptCann::where('canned_id', '=', $id)->delete();
            }
            /* select the field where id = $id(request Id) */
            $canned = $canned->where('id', '=', $id)->first();
            // fetching all the requested inputs
            $canned->user_id = \Auth::user()->id;
            $canned->title = $request->input('title');
            $canned->message = $request->input('message');
            // saving inputs
            $canned->save();
            

            return redirect()->route('canned.list')->with('success', Lang::get('lang.canned_response_updated_successfully'));
        } catch (Exception $e) {
            return redirect()->route('canned.list')->with('fails', $e->getMessage());
        }
    }

    /**
     * Delete the Canned Response from storage.
     *
     * @param type        $id
     * @param type Canned $canned
     *
     * @return type Redirect
     */
    public function destroy($id, Canned $canned, DeptCann $dept_cann)
    {
        try {
            $dept_cann = $dept_cann->where('canned_id', '=', $id)->delete();
            /* select the field where id = $id(request Id) */
            $canned = $canned->whereId($id)->first();
            /* delete the selected field */
            /* Check whether function success or not */
            $canned->delete();
            /* redirect to Index page with Success Message */
            return redirect()->route('canned.list')->with('success', Lang::get('lang.canned_deleted_successfully'));
        } catch (Exception $e) {
            /* redirect to Index page with Fails Message */
            return redirect()->route('canned.list')->with('fails', $e->getMessage());
        }
    }

    /**
     * Fetch Canned Response in the ticket detail page.
     *
     * @param type $id
     *
     * @return type json
     */
    public function get_canned($id)
    {   
        $response_values = [];
        $canned_responses = $this->getCannedBuilder($id);
        if ($canned_responses) {
            foreach ($canned_responses as $canned_response) {
                array_push($response_values, [$canned_response->id, $canned_response->title]);
            }
        }
        if (sizeof($response_values) != 0) {
            $response = $response_values;
        } else {
            $response = [['zzz', 'select canned_response']];
        }
        return json_encode($response);
    }

    /**
     * @category function to get querybuilder collection of canned responses
     * @param type int $id //id of an agent 
     * @var $dept_id, $dept_cann, $user_departments, $user_department, $user_dept_canns, $user_dept_cann, $canned_responses
     * @return builder
     */
    public function getCannedBuilder($id) {
        $dept_id = [];
        $dept_cann = [];
        $user_departments = DepartmentAssignAgents::select('department_id')->where('agent_id', '=', $id)->get();
        if ($user_departments) {
            foreach ($user_departments as $user_department) {
                array_push($dept_id, $user_department->department_id);
            }               
        }
        $user_dept_canns = DeptCann::select('canned_id')->whereIn('dept_id', $dept_id)->groupBy('canned_id')->orderBy('canned_id')->get();
        if ($user_dept_canns) {
            foreach ($user_dept_canns as $user_dept_cann) {
                array_push($dept_cann, $user_dept_cann->canned_id);
            }
        }
        $canned_responses = Canned::select('id', 'title', 'message')->where('user_id', '=', $id)->orWhereIn('id', $dept_cann)->groupBy('id')->orderBy('id')->get();
        return $canned_responses;
    }

    /**
     * @category function to send response to ajax call with depratment names and id to which the calling 
     *canned response is shared with 
     * @param int $id
     * @var $dept, $dept_id, $departments, $department_json
     * @return type json object $departments_json
     */
    public function getCannedDepartments($id)
    {
        $dept = '';
        $dept_id = [];
        $departments = DeptCann::select('dept_id')->where('canned_id', '=', $id)->get();
        if ($departments) {
            foreach ($departments as $department) {
                array_push($dept_id, $department->dept_id);
            }
        }
        if (sizeof($dept_id > 0)) {
            $dept = Department::select('name as name_id', 'name as text')
                ->whereIn('id', $dept_id)->get();
        }
        $dropdown_controller = new \App\Http\Controllers\Agent\helpdesk\DropdownController();
        $departments_json = $dropdown_controller->formatToJson($dept, 'department-list');
        return $departments_json;
    }

    /**
     * @category function to send canned response message body
     * @param int $id 
     * @
     * @return string HTML code
     */
    public function getCannedMessage($id)
    {
        $canned = Canned::select('message')->where('id', '=', $id)->first();
        return $canned->message;
    }
}
