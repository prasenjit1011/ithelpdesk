<?php

namespace App\Http\Controllers\Common;

// Controllers
use App\Http\Controllers\Controller;
// Requests
use Illuminate\Http\Request;
// Models
use App\User;
// classes
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Message\Topics;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Response\DownstreamResponse;
use FCM;
use FCMGroup;

/**
 * **********************************************
 * PushNotificationController
 * **********************************************
 * This controller is used to send notification to FCM cloud which later will 
 * foreward notification to Mobile Application
 *
 * @author      Ladybird <info@ithelpdesk.com>
 */
class PushNotificationController extends Controller {

    public function response($agent_token, $noti) {
//        dd(json_encode($noti));
        try {
            $requester_name = $noti['requester']['first_name']." ".$noti['requester']['last_name'];
            $message = checkArray('message', $noti);
            $optionBuiler = new OptionsBuilder();
            $optionBuiler->setTimeToLive(60 * 20);
            $optionBuiler->setPriority('high');
            $optionBuiler->setContentAvailable(true);
            $activity = "OPEN_ACTIVITY_1";
            if($noti['scenario']==='users'){
                $activity = "OPEN_ACTIVITY_2";
            }
            $notificationBuilder = new PayloadNotificationBuilder($requester_name);
            $notificationBuilder->setBody($message)
                    ->setSound('default')
                    ->setIcon('ic_stat_f1')
                    ->setClickAction($activity);

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData($noti);

            $option = $optionBuiler->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();
            if ($agent_token[1] == "ios") {
                $downstreamResponse = FCM::sendTo($agent_token[0], $option, $notification, $data);
            } else {
                $downstreamResponse = FCM::sendTo($agent_token[0], $option, null, $data);
            }
            

//        $downstreamResponse = new DownstreamResponse($response, $tokens);
            //dd($downstreamResponse);
            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

//return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete();

//return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify();

//return Array - you should try to resend the message to the tokens in the array
            $downstreamResponse->tokensToRetry();
        } catch (\Exception $ex) {
            loging('fcm', $ex->getMessage());
        }

// return Array (key:token, value:errror) - in production you should remove from your database the tokens
    }

    /**
     * function to get the fcm token from the api under a user.
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function fcmToken(Request $request) {
        // return "success 123";
        // get the requested details 
        $user_id = $request->input('user_id');
        $fcm_token = $request->input('fcm_token');
        // check for all the valid details
        if ($user_id != null && $user_id != "" && $fcm_token != null && $fcm_token != "") {
            // search the user_id in database
            $user = User::where('id', '=', $user_id)->first();
            if ($user != null) {
                if ($request->has('os')) { //check if the request is coming from iOS or android
                    $user->i_token = $fcm_token; //save i_token if the request is from iPhone
                    $user->save();
                } else {
                    $user->fcm_token = $fcm_token; //save fcm_token if the request is from android phones
                    $user->save();
                }
                // success response for success case
                return ['response' => 'success'];
            } else {
                // failure respunse for invalid user_id in the system
                return ['response' => 'fail', 'reason' => 'Invalid user_id'];
            }
        } else {
            // failure respunse for invalid input credentials
            return ['response' => 'fail', 'reason' => 'Invalid Credentials'];
        }
    }

}
