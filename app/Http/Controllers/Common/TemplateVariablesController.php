<?php

namespace App\Http\Controllers\Common;

//controllers
use App\Http\Controllers\Controller;
// models
use App\Model\helpdesk\Settings\Company;

//classes
use Finder;
use Lang;

/**
 * |======================================================
 * | Class Template Variables Controller
 * |======================================================
 * This controller is used to get all the varables used in Email/SMS templates
 * @author manish.verma@ithelpdesk.com
 * @copyright Ladybird Web Solutions
 */
class TemplateVariablesController extends Controller
{
    /**
     * @category function to replace template variable with values
     * @param array $template_variables (list of template variables)
     * @return array $variables (value of replaced template variables)
     */
    public function getVariableValues($template_variables, $content = '')
    {
        if (checkArray('message_content', $template_variables) != '') {
            $content = checkArray('message_content', $template_variables);
        }
        $system_from = $this->checkElement('system_from', $template_variables);
        if ($system_from === "") {
            $system_from = $this->company();
        }

        $system_link = $this->checkElement('system_link', $template_variables);
        if ($system_link === "") {
            $system_link = url('/');
        }
        $variables = [
            '{!! $receiver_name !!}' => checkArray('receiver_name', $template_variables),
            '{!! $new_user_name !!}' => checkArray('new_user_name', $template_variables),
            '{!! $new_user_email !!}' => checkArray('new_user_email', $template_variables),
            '{!! $password_reset_link !!}' => checkArray('password_reset_link', $template_variables),
            '{!! $reference_link !!}' => checkArray('reference_link', $template_variables),
            '{!! $user_password !!}' => checkArray('user_password', $template_variables),
            '{!! $account_activation_link !!}' => checkArray('account_activation_link', $template_variables),
            '{!! $ticket_link !!}' => checkArray('ticket_link', $template_variables),
            '{!! $ticket_number !!}' => checkArray('ticket_number', $template_variables),
            '{!! $department_signature !!}' => checkArray('department_signature', $template_variables),
            '{!! $client_name !!}' => checkArray('client_name', $template_variables),
            '{!! $client_email !!}' => checkArray('client_email', $template_variables),
            '{!! $agent_name !!}' => checkArray('agent_name', $template_variables),
            '{!! $agent_email !!}' => checkArray('agent_email', $template_variables),
            '{!! $agent_contact !!}' => checkArray('agent_contact', $template_variables),
            '{!! $agent_signature !!}' => checkArray('agent_signature', $template_variables),
            '{!! $client_name !!}' => checkArray('client_name', $template_variables),
            '{!! $client_email !!}' => checkArray('client_email', $template_variables),
            '{!! $client_contact !!}' => checkArray('client_contact', $template_variables),
            '{!! $user_profile_link !!}' => checkArray('user_profile_link', $template_variables),
            '{!! $activity_by !!}' => checkArray('activity_by', $template_variables),
            '{!! $assigned_team_name !!}' => checkArray('assigned_team_name', $template_variables),
            '{!! $ticket_subject !!}' => checkArray('ticket_subject', $template_variables),
            '{!! $ticket_due_date !!}' => checkArray('ticket_due_date', $template_variables),
            '{!! $ticket_created_at !!}' => checkArray('ticket_created_at', $template_variables),
            '{!! $otp_code !!}' => checkArray('otp_code', $template_variables),
            '{!! $system_from !!}' => $system_from,
            '{!! $system_link !!}' => $system_link,
            '{!! $message_content !!}' => $content,
        ];
        return $variables;
    }

    /**
     * @category function to replace shortcode in template to display in front end
     * @param string $content
     * @var array: $variables, $data, string: $body
     * @return string $body
     */
    public function stringReplaceVariables($contents)
    {
        $variables = [
            '{!! $receiver_name !!}',
            '{!! $new_user_name !!}',
            '{!! $new_user_email !!}',
            '{!! $reference_link !!}',
            '{!! $user_password !!}',
            '{!! $system_from !!}',
            '{!! $system_link !!}',
            '{!! $password_reset_link !!}',
            '{!! $account_activation_link !!}',
            '{!! $ticket_link !!}',
            '{!! $ticket_number !!}',
            '{!! $department_signature !!}',
            '{!! $agent_name !!}',
            '{!! $agent_email !!}',
            '{!! $agent_contact !!}',
            '{!! $agent_signature !!}',
            '{!! $client_name !!}',
            '{!! $client_email !!}',
            '{!! $client_contact !!}',
            '{!! $user_profile_link !!}',
            '{!! $message_content !!}',
            '{!! $activity_by !!}',
            '{!! $assigned_team_name !!}',
            '{!! $ticket_subject !!}',
            '{!! $ticket_due_date !!}',
            '{!! $ticket_created_at !!}',
            '{!! $otp_code !!}',

        ];
        $data = [
            '%receiver_name%',
            '%new_user_name%',
            '%new_user_email%',
            '%reference_link%',
            '%user_password%',
            '%system_name%',
            '%system_link%',
            '%password_reset_link%',
            '%account_activation_link%',
            '%ticket_link%',
            '%ticket_number%',
            '%department_signature%',
            '%agent_name%',
            '%agent_email%',
            '%agent_contact%',
            '%agent_signature%',
            '%client_name%',
            '%client_email%',
            '%client_contact%',
            '%user_profile_link%',
            '%message_content%',
            '%activity_by%',
            '%assigned_team_name%',
            '%ticket_subject%',
            '%ticket_due_date%',
            '%ticket_created_at%',
            '%otp_code%',
        ];
        $body = Finder::replaceTemplateVariables($variables, $data, $contents);
        return $body;
    }

    /**
     * @category function to replace shortcode in template to store in Database
     * @param string $content
     * @var array: $variables, $data, string: $body
     * @return string $body
     */
    public function stringReplaceVariablesReverse($contents)
    {
        $data = [
            '{!! $receiver_name !!}',
            '{!! $new_user_name !!}',
            '{!! $new_user_email !!}',
            '{!! $reference_link !!}',
            '{!! $user_password !!}',
            '{!! $system_from !!}',
            '{!! $system_link !!}',
            '{!! $password_reset_link !!}',
            '{!! $account_activation_link !!}',
            '{!! $ticket_link !!}',
            '{!! $ticket_number !!}',
            '{!! $department_signature !!}',
            '{!! $agent_name !!}',
            '{!! $agent_email !!}',
            '{!! $agent_contact !!}',
            '{!! $agent_signature !!}',
            '{!! $client_name !!}',
            '{!! $client_email !!}',
            '{!! $client_contact !!}',
            '{!! $user_profile_link !!}',
            '{!! $message_content !!}',
            '{!! $activity_by !!}',
            '{!! $assigned_team_name !!}',
            '{!! $ticket_subject !!}',
            '{!! $ticket_due_date !!}',
            '{!! $ticket_created_at !!}',
            '{!! $otp_code !!}',

        ];
        $variables = [
            '%receiver_name%',
            '%new_user_name%',
            '%new_user_email%',
            '%reference_link%',
            '%user_password%',
            '%system_name%',
            '%system_link%',
            '%password_reset_link%',
            '%account_activation_link%',
            '%ticket_link%',
            '%ticket_number%',
            '%department_signature%',
            '%agent_name%',
            '%agent_email%',
            '%agent_contact%',
            '%agent_signature%',
            '%client_name%',
            '%client_email%',
            '%client_contact%',
            '%user_profile_link%',
            '%message_content%',
            '%activity_by%',
            '%assigned_team_name%',
            '%ticket_subject%',
            '%ticket_due_date%',
            '%ticket_created_at%',
            '%otp_code%',
        ];
        $body = Finder::replaceTemplateVariables($variables, $data, $contents);
        return $body;
    }

    /**
     * @category function to return list of avaialable variables for templates
     * @param string $scenario (type of event)
     * @var array $variables
     * @return array $variables list of available variables
     */
    public function getAvailableTemplateVariables($scenario)
    {
        //base template variables
        $variables['%receiver_name%'] = Lang::get('lang.shortcode_receiver_name_description');
        $variables['%system_link%'] = Lang::get('lang.shortcode_system_link_description');
        $variables['%system_name%'] = Lang::get('lang.shortcode_system_from_description');
        //common template variables
         $variables['%new_user_name%'] = Lang::get('lang.shortcode_new_user_name_description');
        $variables['%new_user_email%'] = Lang::get('lang.shortcode_new_user_email_description');
        $variables['%user_password%'] = Lang::get('lang.shortcode_user_password_description');
        $variables['%password_reset_link%'] = Lang::get('lang.shortcode_password_reset_link_description');
        // template links
        $variables['%account_activation_link%'] = Lang::get('lang.shortcode_account_activation_link_description');
        $variables['%ticket_link%'] = Lang::get('lang.shortcode_ticket_link_description');
        $variables['%ticket_number%'] = Lang::get('lang.shortcode_ticket_number_description');
        $variables['%ticket_subject%'] = Lang::get('lang.shortcode_ticket_subject_description');
        $variables['%ticket_due_date%'] = Lang::get('lang.shortcode_ticket_due_date_description');
        $variables['%ticket_created_at%'] = Lang::get('lang.shortcode_ticket_created_at_description');
        $variables['%department_signature%'] = Lang::get('lang.shortcode_department_signature_description');
        $variables['%agent_name%'] = Lang::get('lang.shortcode_agent_name_description');
        $variables['%agent_email%'] = Lang::get('lang.shortcode_agent_email_description');
        $variables['%agent_contact%'] = Lang::get('lang.shortcode_agent_contact_description');
        $variables['%agent_signature%'] = Lang::get('lang.shortcode_agent_signature_description');
        $variables['%client_name%'] = Lang::get('lang.shortcode_client_name_description');
        $variables['%client_email%'] = Lang::get('lang.shortcode_client_email_description');
        $variables['%client_contact%'] = Lang::get('lang.shortcode_client_contact_description');
        $variables['%user_profile_link%'] = Lang::get('lang.shortcode_user_profile_link_description');
        $variables['%message_content%'] = Lang::get('lang.shortcode_message_content_description');
        $variables['%activity_by%'] = Lang::get('lang.shortcode_activity_by_description');
        $variables['%assigned_team_name%'] = Lang::get('lang.shortcode_assigned_team_name_description');
        $variables['%otp_code%'] = Lang::get('lang.shortcode_otp_code_description');
        
        return $variables;
    }

    public function checkElement($element, $array)
    {
        $value = "";
        if (is_array($array)) {
            if (key_exists($element, $array)) {
                $value = $array[$element];
            }
        }
        return $value;
    }

    /**
     * Fetching comapny name to send mail.
     *
     * @return type
     */
    public function company()
    {
        $company = Company::Where('id', '=', '1')->first();
        if ($company->company_name == null) {
            $company = 'Support Center';
        } else {
            $company = $company->company_name;
        }

        return $company;
    }
}
