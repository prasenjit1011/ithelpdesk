<?php

namespace App\Http\Controllers\SLA;

// controllers
use App\Http\Controllers\Controller;
// requests
use App\Http\Requests\helpdesk\BusinessRequest;
use App\Http\Requests\helpdesk\BusinessUpdateRequest;
use App\Http\Requests\helpdesk\SlaUpdateRequest;
use Illuminate\Http\Request;
// models
use App\Model\helpdesk\Manage\Sla\BusinessHours;
use App\Model\helpdesk\Manage\Sla\BusinessHoursSchedules;
use App\Model\helpdesk\Manage\Sla\BusinessHoursSchedulesCustomTime;
use App\Model\helpdesk\Manage\Sla\BusinessHoliday;
use App\Model\helpdesk\Utility\Timezones;
use App\Model\helpdesk\Manage\Sla\SlaTargets;
use App\Model\helpdesk\Ticket\Ticket_Priority;
//classes
use DB;
use Exception;
use Lang;

/**
 * SlaController.
 *
 * @author      Ladybird <info@ithelpdesk.com>
 */
class BusinessHoursController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return type void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('roles');
    }

    /**
     * Display a listing of the resource.
     *
     * @param type Sla_plan $sla
     *
     * @return type Response
     */
    public function index() {
        return view('themes.default1.admin.helpdesk.manage.businesshours.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @param type Sla_plan $sla
     *
     * @return type Response
     */
    public function getindex() {
// displaying list of users with chumper datatables
        return \Datatable::collection(BusinessHours::whereIn('status', [1, 0])->get())
                        /* searchable column name and description */
                        ->searchColumns('name')
                        /* order column name and description */
                        ->orderColumns('name')
                        /* column username */
                        ->showColumns('name')
                        ->addColumn('status', function($model) {
                            if ($model->status == 1) {
                                return "<a style='color:green'> <button type='button' class='btn btn-success btn-xs' style='pointer-events:none'>Active</button></a>";
                            } elseif ($model->status == 0) {
                                Ticket_Priority::where('priority_id', '=', '$priority_id')
                                ->update(['priority_id' => '']);
                                return "<a style='color:red'> <button type='button' class='btn btn-danger btn-xs' style='pointer-events:none'>Inactive</button></a>";
                            }
                        })
                        /* column actions */
                        ->addColumn('Actions', function ($model) {
                            if ($model->is_default == 1) {

                                 return '<a href="' . route('sla.business.hours.edit', $model->id) . '" class="btn btn-primary btn-xs">
                                  <i class="fa fa-edit" style="color:white;"> </i>&nbsp;
                                 ' . \Lang::get('lang.edit') . '</a>&nbsp; <a href="#" class="btn btn-primary btn-xs" disabled="disabled">
                                  <i class="fa fa-trash" style="color:white;"> &nbsp;</i>&nbsp;
                                 ' . \Lang::get('lang.delete') . '</a>';
                                // return '<a href="' . route('sla.business.hours.edit', $model->id) . '" class="btn btn-warning btn-xs">' . \Lang::get('lang.edit') . '</a>';
                                // &nbsp;
                                //  <a href="' . route('sla.business.hours.delete', $model->id) . '" class="btn btn-primary btn-xs" disabled="disabled">' . \Lang::get('lang.delete') . '</a>';
                            } else {

                                 return '<a href="' . route('sla.business.hours.edit', $model->id) . '" class="btn btn-primary btn-xs">
                                 <i class="fa fa-edit" style="color:white;">&nbsp; </i>
                                 ' . \Lang::get('lang.edit') . '</a>&nbsp; <a class="btn btn-primary btn-xs" onclick="confirmDelete(' . $model->id . ')">
                                  <i class="fa fa-trash" style="color:white;"> &nbsp;</i>
                                 ' . \Lang::get('lang.delete') . '</a>';

                                // return '<a href="' . route('sla.business.hours.edit', $model->id) . '" class="btn btn-warning btn-xs">' . \Lang::get('lang.edit') . '</a>&nbsp; <a href="' . route('sla.business.hours.delete', $model->id) . '" class="btn btn-primary btn-xs">' . \Lang::get('lang.delete') . '</a>';
                                // return "<a href=" . route('sla.business.hours.edit', $model->id) . " class='btn btn-warning btn-xs'>Edit</a>&nbsp;<a class='btn btn-danger btn-xs' onclick='confirmDelete(" . $model->id . ")'>Delete </a>";
                            }
                        })
                        ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return type Response
     */
    public function create() {
        $timezones = new Timezones();
        $timezones = $timezones->pluck('name','name')->toArray();
        return view('themes.default1.admin.helpdesk.manage.businesshours.create', compact('timezones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param type Sla_plan   $sla
     * @param type SlaRequest $request
     *
     * @return type Response
     */
    public function store(BusinessRequest $request) {
//        dd($request);
        try {

            $business_hours = new BusinessHours;
            $business_hours->name = $request->name;
            $business_hours->description = $request->description;
            $business_hours->timezone = $request->time_zone;
            $business_hours->status = $request->status;
            $business_hours->save();
            $day = '';

            if ($request->hours == 1) {
                 for ($i = 0; $i < 7; $i++) {

                    $schedules = new BusinessHoursSchedules;
                    $schedules->business_hours_id = $business_hours->id;
                    $schedules->days = \Lang::get('lang.day' . $i);
                    $schedules->status = $request->input('type' . $i);
                    $schedules->save();

                    if ($schedules->status == "Open_custom") {
                       $check =checkArray($i,$request->input('fromHour'));
 
                       if(!$check){
                         $update_schedules=BusinessHoursSchedules::where('id','=', $schedules->id)->first();
                               $update_schedules->status='Open_fixed';
                               $update_schedules->save();
                     
                         }
                         else{
                             $j = 0;
                      foreach ($request->input('fromHour')[$i] as $value) {
                      $open_custom_time = new BusinessHoursSchedulesCustomTime;
                            $open_custom_time->business_schedule_id = $schedules->id;
                            $open_custom_time->open_time = $request->input('fromHour')[$i][$j] . ':' . $request->input('fromMinute')[$i][$j];
                            $open_custom_time->close_time = $request->input('toHour')[$i][$j] . ':' . $request->input('toMinute')[$i][$j];
                            $open_custom_time->save();
                            // $j++;
                        }

                              }

                       }
                }
                // for ($i = 0; $i < 7; $i++) {

                //     $schedules = new BusinessHoursSchedules;
                //     $schedules->business_hours_id = $business_hours->id;
                //     $schedules->days = \Lang::get('lang.day' . $i);
                //     $schedules->status = $request->input('type' . $i);
                //     $schedules->save();

                //     if ($schedules->status == "Open_custom") {
                //         $j = 0;
                //         foreach ($request->input('fromHour')[$i] as $value) {

                //             $open_custom_time = new BusinessHoursSchedulesCustomTime;
                //             $open_custom_time->business_schedule_id = $schedules->id;
                //             $open_custom_time->open_time = $request->input('fromHour')[$i][$j] . ':' . $request->input('fromMinute')[$i][$j];
                //             $open_custom_time->close_time = $request->input('toHour')[$i][$j] . ':' . $request->input('toMinute')[$i][$j];
                //             $open_custom_time->save();
                //             $j++;
                //         }
                //     }
                // }
            } else {
                for ($i = 0; $i < 7; $i++) {
                    $schedules = new BusinessHoursSchedules;
                    $schedules->business_hours_id = $business_hours->id;
                    $schedules->days = \Lang::get('lang.day' . $i);
                    $schedules->status = 'Open_fixed';
                    $schedules->save();
                }
            }


            $months = $request->month;

            if ($months) {
                $months = $request->month;
                $k = 0;
                foreach ($months as $month) {
                    $holiday = new BusinessHoliday;
                    $holiday->date = $request->input('month')[$k] . '-' . $request->input('day')[$k];
                    $holiday->name = $request->input('holyday_name')[$k];
                    $holiday->business_hours_id = $business_hours->id;
                    $holiday->save();
                    $k++;
                }
            }
//            $holiday->        
            /* redirect to Index page with Success Message */
            return redirect()->route('sla.business.hours.index')->with('success', Lang::get('lang.business_hour_saved_successfully'));
        } catch (Exception $e) {
            /* redirect to Index page with Success Message */
            return redirect()->route('sla.business.hours.index')->with('fails', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param type int      $id
     * @param type Sla_plan $sla
     *
     * @return type Response
     */
    public function edit($id) {
        try {
            $business_hours = BusinessHours::where('id', '=', $id)->first();
            $select_time_zone = $business_hours->timezone;

            $timezones = new Timezones();
            $timezones = $timezones->pluck('name','name')->toArray();
            $businessHours_Schedules = BusinessHoursSchedules::where('business_hours_id', '=', $id)->orderBy('id', 'ASC')->get();

            // dd($businessHours_Schedules);
            foreach ($businessHours_Schedules as $value) {
                $dayss = $value['days'];
                $sla_day_info_inst = BusinessHoursSchedules::select('id')->where(['business_hours_id' => $id, 'days' => $dayss])->first();

                $idss = (int) $sla_day_info_inst->id;

                $sla_time_list = BusinessHoursSchedulesCustomTime::where('business_schedule_id', '=', $idss)->get();

                if ($sla_time_list) {
                    $value['timeList'] = $sla_time_list;
                }
            }
            foreach ($businessHours_Schedules as $value) {

                $sla_open_fixed_check = BusinessHoursSchedules::where(['business_hours_id' => $id])->pluck('status')->toArray();
            }

            $business_holidays = BusinessHoliday::where('business_hours_id', '=', $id)->get();

            // dd($business_hours);
            return view('themes.default1.admin.helpdesk.manage.businesshours.edit', compact('select_time_zone', 'timezones', 'business_hours', 'businessHours_Schedules', 'business_holidays', 'sla_open_fixed_check'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param type int       $id
     * @param type Sla_plan  $sla
     * @param type SlaUpdate $request
     *
     * @return type Response
     */
    public function update($id,BusinessUpdateRequest $request) {
        try {

        
            // $id = $request->input('business_hours_id');

              $sla_schedule_infos = BusinessHoursSchedules::where('business_hours_id', '=', $id)->get();
            foreach ($sla_schedule_infos as $sla_schedule_info) {
                if ($sla_schedule_info->status == 'Open_custom') {
                    BusinessHoursSchedulesCustomTime::wherebusiness_schedule_id($sla_schedule_info->id)->delete();
                }
                $sla_schedule_info->delete();
            }

            // $business_holidays = BusinessHoliday::where('business_hours_id', '=', $id)->get();
            // if ($business_holidays) {
            //     $business_holidays = BusinessHoliday::where('business_hours_id', '=', $id)->delete();
            // }

            $business_holidays = BusinessHoliday::where('business_hours_id', '=', $id)->delete();

            $business_hours = BusinessHours::where('id', '=', $id)->first();

            $business_hours->name = $request->name;
            $business_hours->description = $request->description;
             $business_hours->timezone = $request->time_zone;
            $business_hours->status = $request->status;
            $business_hours->save();
            $day = '';

            if ($request->hours == 1) {
                for ($i = 0; $i < 7; $i++) {

                    $schedules = new BusinessHoursSchedules;
                    $schedules->business_hours_id = $business_hours->id;
                    $schedules->days = \Lang::get('lang.day' . $i);
                    $schedules->status = $request->input('type' . $i);
                    $schedules->save();

                    if ($schedules->status == "Open_custom") {
                       $check =checkArray($i,$request->input('fromHour'));
 
                       if(!$check){
                         $update_schedules=BusinessHoursSchedules::where('id','=', $schedules->id)->first();
                               $update_schedules->status='Open_fixed';
                               $update_schedules->save();
                     
                         }
                         else{
                             $j = 0;
                      foreach ($request->input('fromHour')[$i] as $value) {
                      $open_custom_time = new BusinessHoursSchedulesCustomTime;
                            $open_custom_time->business_schedule_id = $schedules->id;
                            $open_custom_time->open_time = $request->input('fromHour')[$i][$j] . ':' . $request->input('fromMinute')[$i][$j];
                            $open_custom_time->close_time = $request->input('toHour')[$i][$j] . ':' . $request->input('toMinute')[$i][$j];
                            $open_custom_time->save();
                            // $j++;
                        }

                              }

                       }
                }
            } else {
                for ($i = 0; $i < 7; $i++) {
                    $schedules = new BusinessHoursSchedules;
                    $schedules->business_hours_id = $business_hours->id;
                    $schedules->days = \Lang::get('lang.day' . $i);
                    $schedules->status = 'Open_fixed';
                    $schedules->save();
                }
            }


            $months = $request->month;
      

            if ($months) {
               
                $months = $request->month;
                 $k = 0;
                foreach ($months as $month) {

                    $holiday = new BusinessHoliday;
                    $holiday->date = $request->input('month')[$k] . '-' . $request->input('day')[$k];
                    $holiday->name = $request->input('holyday_name')[$k];
                    $holiday->business_hours_id = $business_hours->id;
                    $holiday->save();
                    $k++;
                }
            }

             if ($request->input('default_business_hours') == 'on') {
            BusinessHours::where('is_default', '>', 0)
                    ->update(['is_default' => 0]);
            BusinessHours::where('id', '=', $id)
                    ->update(['is_default' => 1]);
        }

            /* redirect to Index page with Success Message */
            return redirect()->route('sla.business.hours.index')->with('success', Lang::get('lang.business_hours_edit_successfully'));
        } catch (Exception $e) {
            /* redirect to Index page with Fails Message */
            return redirect('sla.business.hours.index')->with('fails', Lang::get('lang.sla_plan_can_not_update') . '<li>' . $e->getMessage() . '</li>');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param type int      $id
     * @param type Sla_plan $sla
     *
     * @return type Response
     */
    public function businessHoursDelete($id) {

        $default_business_hours = BusinessHours::where('id', '=', 1)->first();
        $default_business_hours_id = $default_business_hours->id;

        $apply_businesshours=SlaTargets::where('business_hour_id','=',$id)->update(['business_hour_id' => $default_business_hours_id]);

         $business_holidays = BusinessHoliday::where('business_hours_id', '=', $id)->get();
               if ($business_holidays) {
            $business_holidays = BusinessHoliday::where('business_hours_id', '=', $id)->delete();
        }


        $sla_schedule_infos = BusinessHoursSchedules::where('business_hours_id', '=', $id)->get();
        foreach ($sla_schedule_infos as $sla_schedule_info) {
            if ($sla_schedule_info->status == 'Open_custom') {
                $update = BusinessHoursSchedulesCustomTime::where('business_schedule_id','=',$sla_schedule_info->id)->delete();
            }
           // $sla_schedule_info->delete();
        }
  $sla_schedule_infos = BusinessHoursSchedules::where('business_hours_id', '=', $id)->delete();
       
        $businesshours = BusinessHours::where('id', '=', $id)->first();

        $businesshours->delete();


        return redirect()->route('sla.business.hours.index')->with('success', Lang::get('lang.business_hours_delete_successfully'));
    }

}
