<?php

namespace App\Http\Controllers\SLA;

// controllers
use App\Http\Controllers\Controller;
// requests
use App\Http\Requests\helpdesk\SlaRequest;
use App\Http\Requests\helpdesk\SlaUpdateRequest;
// models
use App\Model\helpdesk\Manage\Sla\Sla_plan;
use App\Model\helpdesk\Settings\Ticket;
use App\Model\helpdesk\Manage\Sla\SlaTargets;
use App\Model\helpdesk\Agent\Department;
use App\Model\helpdesk\Settings\Company;
use App\Model\helpdesk\Manage\Tickettype;
use App\Model\helpdesk\Ticket\Ticket_source;
use App\Model\helpdesk\Manage\Sla\SlaApproachesResponse;
use App\Model\helpdesk\Manage\Sla\SlaApproachesResolution;
use App\Model\helpdesk\Manage\Sla\SlaViolatedResponse;
use App\Model\helpdesk\Manage\Sla\SlaViolatedResolved;
use App\Model\helpdesk\Manage\Sla\SlaApproachEscalate;
use App\Model\helpdesk\Manage\Sla\SlaViolatedEscalate;
use App\Model\helpdesk\Agent_panel\Organization;
//classes
use App\User;
use DB;
use Exception;
use Lang;
use Illuminate\Http\Request;

/**
 * SlaController.
 *
 * @author      Ladybird <info@ithelpdesk.com>
 */
class SlaController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return type void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('roles');
    }

    /**
     * Display a listing of the resource.
     *
     * @param type Sla_plan $sla
     *
     * @return type Response
     */
    public function index(Sla_plan $sla) {
        try {
            /* Declare a Variable $slas to store all Values From Sla_plan Table */
            /* Listing the values From Sla_plan Table */
            return view('themes.default1.admin.helpdesk.manage.sla.index');
        } catch (Exception $e) {

            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param type Sla_plan $sla
     *
     * @return type Response
     */
    public function getIndex() {
        // dd('kkk');
        try {
            return \Datatable::collection(Sla_plan::whereIn('status', [0, 1])->get())
                            ->searchColumns('name')
                            ->orderColumns('name')
                            ->showColumns('name')
                            ->addColumn('status', function($model) {
                                if ($model->status == 1) {
                                     return '<p class="btn btn-xs btn-success" style="pointer-events:none">'.Lang::get('lang.active').'</p>';
                                    // return "<a style='color:green'>active</a>";
                                } elseif ($model->status == 0) {
                                     return '<p class="btn btn-xs btn-danger" style="pointer-events:none">'.Lang::get('lang.inactive').'</p>';
                                    // return "<a style='color:red'>inactive</a>";
                                }
                            })
                            ->addColumn('priority',function($model){
                                $priority = "";
                                if($model->target && $model->target->priority){
                                   $priority = $model->target->priority->priority; 
                                }
                                return $priority;
                            })
                            ->addColumn('created_at',function($model){
                                return faveoDate($model->created_at);
                            })
                            
                            ->addColumn('Actions', function ($model) {
                                if ($model->id == 1) {
                                    // return '<a href="' . route('sla.edit', $model->id) . '" class="btn btn-info btn-xs">
                                    // <i class="fa fa-edit" style="color:black;"> </i>' . \Lang::get('lang.edit') . '</a>&nbsp; <a href="' . route('sla.plan.delete', $model->id) . '" class="btn btn-warning btn-xs" disabled="disabled"><i class="fa fa-trash" style="color:black;">' . \Lang::get('lang.delete') . '</a>';

                                    return '<a href="' . route('sla.edit', $model->id) . '" class="btn btn-primary btn-xs">
                                    <i class="fa fa-edit" style="color:white;"> </i>&nbsp;
                                     ' . \Lang::get('lang.edit') . '</a>&nbsp; <a href="#" class="btn btn-primary btn-xs" disabled="disabled">
                                     <i class="fa fa-trash" style="color:white;"> </i>&nbsp;
                                     ' . \Lang::get('lang.delete') . '</a>';
                                } else {
                                    // return '<a href="' . route('sla.edit', $model->id) . '" class="btn btn-warning btn-xs">' . \Lang::get('lang.edit') . '</a>&nbsp; <a href="' . route('sla.plan.delete', $model->id) . '" class="btn btn-primary btn-xs">' . \Lang::get('lang.delete') . '</a>';
                                    // return "<a href=" . route('sla.edit', $model->id) . " class='btn btn-info btn-xs'>
                                    // <i class='fa fa-edit' style='color:black;'> </i>Edit</a>&nbsp;<a class='btn btn-warning btn-xs' onclick='confirmDelete(" . $model->id . ")'>Delete </a>";

                                    return "<a href=" . route('sla.edit', $model->id) . " class='btn btn-primary btn-xs'>
                                   <i class='fa fa-edit' style='color:white;'> </i>&nbsp;
                                     Edit</a>&nbsp;&nbsp;<a class='btn btn-primary btn-xs' onclick='confirmDelete(" . $model->id . ")'>
 
                                  <i class='fa fa-trash' style='color:white;'> </i>&nbsp;
                                     Delete </a>";
                                  
                                }
                            })
                            ->make();
            // return view('themes.default1.admin.helpdesk.manage.sla.index', compact('slas'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    public function getDepartment(Request $request) {
        $term = $request->input('term');
        $department = new Department();
        $select = $department->where('name', 'LIKE', '%' . $term . '%')->pluck('name')->toArray();
        return $select;
    }

    /**
     * 
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function getTicketsources(Request $request) {
        $term = $request->input('term');
        $ticket_source = new Ticket_source();
        $select = $ticket_source->where('name', 'LIKE', '%' . $term . '%')->pluck('name')->toArray();
        return $select;
    }

    /**
     * 
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function getType(Request $request) {
        $term = $request->input('term');
        $select = Tickettype::where('name', 'LIKE', '%' . $term . '%')->where('status', '=', '1')->pluck('name')->toArray();
        return $select;
    }

    /**
     * 
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function getCompany(Request $request) {
        $term = $request->input('term');
        $organization = new Organization();
        $select = $organization->where('name', 'LIKE', '%' . $term . '%')->pluck('name')->toArray();
        return $select;
    }

    /**
     * 
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function getAssigner(Request $request) {
        $term = $request->input('term');
        $users = new User();

        $user = $users->where('email', 'LIKE', '%' . $term . '%')->where('role', '!=', 'user')->pluck('email')->toArray();
        $fixed = array('assigner', 'admin', 'department manager', 'team lead');
        $select = array_merge($user, $fixed);

      
       //  $user = $users->where('email', 'LIKE', '%' . $term . '%')->where('role', '!=', 'user')->pluck('email');
       // // dd($user);
       //  $fixed = collect(array('assigner', 'admin', 'department_manager', 'team_lead'));
       //  $select = $user->diff($fixed);
       
        return $select;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return type Response
     */
    public function create() {
        try {
            /* Direct to Create Page */
            return view('themes.default1.admin.helpdesk.manage.sla.create');
        } catch (Exception $e) {
            //dd($e);
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return type Response
     */
    public function autofill() {
        try {
            /* Direct to Create Page */
            return view('themes.default1.admin.helpdesk.manage.sla.getautocomplete');
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param type Sla_plan   $sla
     * @param type SlaRequest $request
     *
     * @return type Response
     */
    public function store(Sla_plan $sla, SlaRequest $request) {
        try {


            /* Fill the request values to Sla_plan Table  */
            /* Check whether function success or not */
            // dd($request->apply_sla_depertment);
            $sla->fill($request->input())->save();
            $apply_sla = Sla_plan::where('id', '=', $sla->id)->first();
            $apply_sla_depertment = $request->apply_sla_depertments;
            // dd($apply_sla_depertment);
            if ($apply_sla_depertment != null) {
                $apply_sla_depertment_id = Department::whereIn('name', $request->apply_sla_depertments)->pluck('id')->toArray();

                $apply_sla->apply_sla_depertment = implode(",", $apply_sla_depertment_id);
            } else {
                $apply_sla->apply_sla_depertment = "";
            }
            $apply_sla_company = $request->apply_sla_companys;
            if ($apply_sla_company != null) {

                $apply_sla_company_id = Organization::whereIn('name', $request->apply_sla_companys)->pluck('id')->toArray();

                $apply_sla->apply_sla_company = implode(",", $apply_sla_company_id);
            } else {

                $apply_sla->apply_sla_company = "";
            }
            $apply_sla_tickettype = $request->apply_sla_tickettypes;
            if ($apply_sla_tickettype != null) {

                $apply_sla_tickettype_id = Tickettype::whereIn('name', $request->apply_sla_tickettypes)->pluck('id')->toArray();
                $apply_sla->apply_sla_tickettype = implode(",", $apply_sla_tickettype_id);
            } else {

                $apply_sla->apply_sla_tickettype = "";
            }
            $apply_sla_ticketsource = $request->apply_sla_ticketsources;
            if ($apply_sla_ticketsource != null) {

                $apply_sla_ticketsource_id = Ticket_source::whereIn('name', $request->apply_sla_ticketsources)->pluck('id')->toArray();
                $apply_sla->apply_sla_ticketsource = implode(",", $apply_sla_ticketsource_id);
            } else {
                $apply_sla->apply_sla_ticketsource = "";
            }

            $apply_sla->save();

            //sla approaches
            $approaches_response_escalate_time = $request->approaches_response_escalate_time;
            $emails = $request->approaches_response_escalate_person;

            if ($approaches_response_escalate_time != 'null' && $emails != null) {
                $emails = $request->approaches_response_escalate_person;
                $definite_articles = array('assigner', 'department manager', 'team lead', 'admin');

                $users = array_diff($emails, $definite_articles);
                if ($users) {
                    foreach ($users as $user) {
                        $approaches_response_escalate_person_userids[] = user::where('email', '=', $user)->pluck('id')->first();
                    }
                    $selected_user = array_merge($emails, $approaches_response_escalate_person_userids);
                    $users11 = array_diff($selected_user, $users);

                    $response_escalate_person = implode(",", $users11);
                } else {
                    $response_escalate_person = implode(",", $emails);
                }
                if ($response_escalate_person) {
                    $sla_approaches_response = new SlaApproachEscalate();
                    $sla_approaches_response->sla_plan = $sla->id;
                    $sla_approaches_response->escalate_time = $request->approaches_response_escalate_time;
                    $sla_approaches_response->escalate_type = 'response';
                    $sla_approaches_response->escalate_person = $response_escalate_person;
                    $sla_approaches_response->save();
                }
            }
            $approaches_resolution_escalate_time = $request->approaches_resolution_escalate_time;
            $emails1 = $request->approaches_response_escalate_person;

            if ($approaches_resolution_escalate_time != 'null' && $emails1 != null) {
                $emails1 = $request->approaches_response_escalate_person;

                 $definite_articles = array('assigner', 'department manager', 'team lead', 'admin');

                $users = array_diff($emails1, $definite_articles);
                if ($users) {
                    foreach ($users as $user) {
                        $approaches_resolution_escalate_person_userids[] = user::where('email', '=', $user)->pluck('id')->first();
                    }

                    $selected_user = array_merge($emails1, $approaches_resolution_escalate_person_userids);

                    $users11 = array_diff($selected_user, $users);
                    $resolution_escalate_person = implode(",", $users11);
                } else {
                    $resolution_escalate_person = implode(",", $emails1);
                }
                if ($resolution_escalate_person) {
                    $sla_approaches_resolution = new SlaApproachEscalate();
                    $sla_approaches_resolution->sla_plan = $sla->id;
                    $sla_approaches_resolution->escalate_time = $request->approaches_resolution_escalate_time;
                    $sla_approaches_resolution->escalate_type = 'resolution';
                    $sla_approaches_resolution->escalate_person = $resolution_escalate_person;
                    $sla_approaches_resolution->save();
                }
            }

// violated
            $approaches_resolution_escalate_time = $request->approaches_resolution_escalate_time;
 $emails2 = $request->violated_response_escalate_person;
            if ($approaches_resolution_escalate_time != 'null' && $emails2 != null) {
                $emails2 = $request->violated_response_escalate_person;

                  $definite_articles = array('assigner', 'department manager', 'team lead', 'admin');
                $users = array_diff($emails2, $definite_articles);
                if ($users) {
                    foreach ($users as $user) {
                        $violated_responded_escalate_person_userids[] = user::where('email', '=', $user)->pluck('id')->first();
                    }

                    $selected_user = array_merge($emails2, $violated_responded_escalate_person_userids);

                    $users11 = array_diff($selected_user, $users);

                    $violated_responded_escalate_person = implode(",", $users11);
                } else {
                    $violated_responded_escalate_person = implode(",", $emails2);
                }
                if ($violated_responded_escalate_person) {
                    $sla_violated_responded = new SlaViolatedEscalate();
                    $sla_violated_responded->sla_plan = $sla->id;
                    $sla_violated_responded->escalate_time = $request->approaches_resolution_escalate_time;
                    $sla_violated_responded->escalate_type = 'response';
                    $sla_violated_responded->escalate_person = $violated_responded_escalate_person;
                    $sla_violated_responded->save();
                }
            }


            $violatedesresolved_times = $request->violated_resolution_escalate_time;


            for ($j = 0; $j < 5; $j++) {

                $violatedescalate_time = $request->input('violated_resolution_escalate_time' . $j);

                $violated_resolved_emails = $request->input('violated_resolution_escalate_person' . $j);

                if ($violated_resolved_emails != null) {

                    $violated_resolved_emails = $request->input('violated_resolution_escalate_person' . $j);
                    $violated_resolved_emailss = implode(",", $violated_resolved_emails);
                     $definite_articles = array('assigner', 'department manager', 'team lead', 'admin');
                    $users = array_diff($violated_resolved_emails, $definite_articles);
                    if ($users) {
                        $violated_resolution_resolution_escalate_person_userids = user::whereIn('email', $users)->pluck('id')->toArray();
                        $selected_user = array_merge($violated_resolved_emails, $violated_resolution_resolution_escalate_person_userids);
                        $sla_violated_resolved_users = array_diff($selected_user, $users);
                        $sla_violated_resolved_users = implode(",", $sla_violated_resolved_users);
                    } else {
                        $sla_violated_resolved_users = $violated_resolved_emailss;
                    }

                    $sla_violated_resolved = new SlaViolatedEscalate();
                    $sla_violated_resolved->sla_plan = $sla->id;
                    $sla_violated_resolved->escalate_time = implode(",", $request->input('violated_resolution_escalate_time' . $j));
                    $sla_violated_resolved->escalate_type = 'resolution';
                    $sla_violated_resolved->escalate_person = $sla_violated_resolved_users;
                    $sla_violated_resolved->save();
                }
                // dd('ooo');
            }


            $targets = new SlaTargets;
            $targets->sla_id = $sla->id;
            $targets->priority_id = $request->sla_priority;
            $targets->respond_within = $request->response_count . '-' . $request->response_duration;
            $targets->resolve_within = $request->resolve_count . '-' . $request->resolve_duration;
            $targets->business_hour_id = $request->business_hour;
            $targets->send_email = $request->status;
            if ($request->has('send_sms')) {
                $targets->send_sms = $request->send_sms;
            } else {
                $targets->send_sms = 0;
            }
            
            $targets->save();
            $sla_target_id = $targets->id;

            Sla_plan::where('id', $sla->id)->update(array('sla_target' => $sla_target_id));
            /* redirect to Index page with Success Message */
            return redirect('sla')->with('success', Lang::get('lang.sla_plan_saved_successfully'));
        } catch (Exception $e) {
            /* redirect to Index page with Fails Message */
            return redirect('sla')->with('fails', Lang::get('lang.sla_plan_can_not_create') . '<li>' . $e->getMessage() . '</li>');
        }
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param type int      $id
     * @param type Sla_plan $sla
     *
     * @return type Response
     */
    public function edit($id) {
        try {
            $approaches_response_escalate_person = [];
            $approaches_resolution_escalate_person = [];
            $violated_response_escalate_person = [];
            $violated_resolution_escalate_person = [];
            $apply_sla_depertment = [];
            $apply_sla_company = [];
            $apply_sla_tickettype = [];
            $apply_sla_ticketsource = [];
            $sla_target_priority_id = null;

            /* Direct to edit page along values of perticular field using Id */
            $slas = Sla_plan::whereId($id)->first();
            if ($slas) {
                if ($slas->apply_sla_depertment == "") {

                    $apply_sla_depertment = "";
                } else {
                    $apply_sla_depertment_id = $slas->apply_sla_depertment;
                    $apply_sla_depertment = Department::whereIn('id', $apply_sla_depertment_id)->pluck('name')->toArray();
                }
                if ($slas->apply_sla_company == "") {
                    $apply_sla_company = "";
                } else {
                    $apply_sla_company_id = $slas->apply_sla_company;
                    $apply_sla_company = Organization::whereIn('id', $apply_sla_company_id)->pluck('name')->toArray();
                }

                if ($slas->apply_sla_tickettype == "") {
                    $apply_sla_tickettype = "";
                } else {
                    $apply_sla_tickettype_id = $slas->apply_sla_tickettype;
                    $apply_sla_tickettype = Tickettype::whereIn('id', $apply_sla_tickettype_id)->pluck('name')->toArray();
                }

                if ($slas->apply_sla_ticketsource == "") {
                    $apply_sla_ticketsource = "";
                } else {
                    $apply_sla_ticketsource_id = $slas->apply_sla_ticketsource;
                    $apply_sla_ticketsource = Ticket_source::whereIn('id', $apply_sla_ticketsource_id)->pluck('name')->toArray();
                }
            }
            $sla_approaches_response_time = SlaApproachEscalate::where('sla_plan', '=', $id)->where('escalate_type', '=', 'response')->pluck('escalate_time')->toArray();
            $sla_approaches_response = SlaApproachEscalate::where('sla_plan', '=', $id)->where('escalate_type', '=', 'response')->pluck('escalate_person', 'escalate_time')->toArray();

// dd($sla_approaches_response_time);

            if (!empty($sla_approaches_response_time)) {


  foreach ($sla_approaches_response as $key => $value) {
                    $sla_approaches_response_person1 = str_replace('_', " ",  $value);

                    // dd($sla_approaches_response_person);
                     $sla_approaches_response_person=array_filter($sla_approaches_response_person1, 'strlen');

                    // dd($sla_approaches_response_person);

                    $definite_articles = array('assigner', 'department manager', 'team lead', 'admin');


                    $response_person_user_ids = array_diff($sla_approaches_response_person, $definite_articles);

                    // dd($response_person_user_ids);

                    if ($response_person_user_ids) {

                        foreach ($response_person_user_ids as $response_person_user_id) {
                            $sla_approaches_response_person_user_emails[] = user::where('id', '=', $response_person_user_id)->pluck('email')->first();
                        }

                      $sla_approaches_response_fixed_person = array_diff($sla_approaches_response_person, $response_person_user_ids);

                        //   $sla_violated_responded_fixed_person=str_replace('_', " ", subject);

                           $sla_approaches_response_person = [array_merge($sla_approaches_response_fixed_person, $sla_approaches_response_person_user_emails)];
                    $sla_approaches_response_persons = array_combine($sla_approaches_response_time,$sla_approaches_response_person);

                    } else {

                        // dd('heredd');

                          $sla_approaches_response_persons = array_combine($sla_approaches_response_time, [$sla_approaches_response_person]);
                        // $sla_approaches_resolution_person = [$sla_approaches_resolution_person];
                    }


                 }
             }

// dd($sla_approaches_response_persons);
                // $sla_approaches_response_person = $sla_approaches_response->escalate_person;
                //   $definite_articles = array('assigner', 'department manager', 'team lead', 'admin');
                // $response_person_user_ids = array_diff($sla_approaches_response_person, $definite_articles);
                // if ($response_person_user_ids) {
                //     foreach ($response_person_user_ids as $response_person_user_id) {
                //         $sla_approaches_response_person_user_emails[] = user::where('id', '=', $response_person_user_id)->pluck('email')->first();
                //     }
                //     $sla_approaches_response_fixed_person = array_diff($sla_approaches_response_person, $response_person_user_ids);
                //     $sla_approaches_response_person = [array_merge($sla_approaches_response_fixed_person, $sla_approaches_response_person_user_emails)];
                //     $sla_approaches_response_persons = array_combine($sla_approaches_response_time, $sla_approaches_response_person);
                // } else {
                //     $sla_approaches_response_fixedperson = [$sla_approaches_response_person];
                //     $sla_approaches_response_persons = array_combine($sla_approaches_response_time, $sla_approaches_response_fixedperson);
                // }
            // }

// approaches resolution 

            $sla_approaches_resolution_time = SlaApproachEscalate::where('sla_plan', '=', $id)->where('escalate_type', '=', 'resolution')->pluck('escalate_time')->toArray();

            $sla_approaches_resolution = SlaApproachEscalate::where('sla_plan', '=', $id)->where('escalate_type', '=', 'resolution')->pluck('escalate_person', 'escalate_time')->toArray();
             // $sla_approaches_resolution = str_replace('_', " ",  $sla_approaches_resolution1);
// trim($value);
// dd($sla_approaches_resolution);
            if (!empty($sla_approaches_resolution_time)) {
                // $sla_approaches_resolution_person = $sla_approaches_resolution->escalate_person;

                 foreach ($sla_approaches_resolution as $key => $value) {
                    $sla_approaches_resolution_person1 = str_replace('_', " ",  $value);
                     $sla_approaches_resolution_person=array_filter($sla_approaches_resolution_person1, 'strlen');

                    // dd($sla_approaches_resolution_person);

                    $definite_articles = array('assigner', 'department manager', 'team lead', 'admin');


                    $responded_person_user_ids = array_diff($sla_approaches_resolution_person, $definite_articles);

                    // dd($responded_person_user_ids);
// 
                    if ($responded_person_user_ids) {

                        foreach ($responded_person_user_ids as $responded_person_user_id) {
                            $sla_approaches_resolution_person_user_emails[] = user::where('id', '=', $responded_person_user_id)->pluck('email')->first();
                        }

                        $sla_approaches_resolution_fixed_person = array_diff($sla_approaches_resolution_person, $responded_person_user_ids);

                        //   $sla_violated_responded_fixed_person=str_replace('_', " ", subject);

                      

                        $sla_approaches_resolution_person = array_merge($sla_approaches_resolution_person_user_emails, $sla_approaches_resolution_fixed_person);

                          // dd($sla_approaches_resolution_person);

                        $sla_approaches_resolution_persons = array_combine($sla_approaches_resolution_time, [$sla_approaches_resolution_person]);

 // dd($sla_approaches_resolution_persons);
                    } else {

                        // dd('here');

                          $sla_approaches_resolution_persons = array_combine($sla_approaches_resolution_time, [$sla_approaches_resolution_person]);
                        // $sla_approaches_resolution_person = [$sla_approaches_resolution_person];
                    }


                 }
             }

// dd($sla_approaches_resolution_persons);
                // dd($sla_approaches_resolution_person);
               // $definite_articles = array('assigner', 'department_manager', 'team_lead', 'admin');

               //  $resolution_person_user_ids = array_diff($sla_approaches_resolution_person, $definite_articles);

               //  if ($resolution_person_user_ids) {
               //      foreach ($resolution_person_user_ids as $resolution_person_user_id) {
               //          $sla_approaches_resolution_person_user_emails[] = user::where('id', '=', $resolution_person_user_id)->pluck('email')->first();
               //      }


               //      $sla_approaches_resolution_fixed_person = array_diff($sla_approaches_resolution_person, $resolution_person_user_ids);

               //      $sla_approaches_resolution_person = [array_merge($sla_approaches_resolution_fixed_person, $sla_approaches_resolution_person_user_emails)];



               //      $sla_approaches_resolution_persons = array_combine($sla_approaches_resolution_time, $sla_approaches_resolution_person);
               //  } else {

               //      $sla_approaches_resolution_person = [$sla_approaches_resolution_person];
               //      $sla_approaches_resolution_persons = array_combine($sla_approaches_resolution_time, $sla_approaches_resolution_person);
               //  }
            // }

// violated responded  

            $sla_violated_responded_time = SlaViolatedEscalate::where('sla_plan', $id)->where('escalate_type', '=', 'response')->pluck('escalate_time')->toArray();
            $sla_violated_responded = SlaViolatedEscalate::where('sla_plan', $id)->where('escalate_type', '=', 'response')->pluck('escalate_person', 'escalate_time')->toArray();
            if (!empty($sla_violated_responded_time)) {

                foreach ($sla_violated_responded as $key => $value) {
                    # code...

                    $sla_violated_responded_person1 = str_replace('_', " ",  $value);

                      $sla_violated_responded_person=array_filter($sla_violated_responded_person1, 'strlen');


                    // dd($sla_violated_responded_person);

                    $definite_articles = array('assigner', 'department manager', 'team lead', 'admin');


                    $responded_person_user_ids = array_diff($sla_violated_responded_person, $definite_articles);

                    // dd($responded_person_user_ids);

                    if ($responded_person_user_ids) {

                        foreach ($responded_person_user_ids as $responded_person_user_id) {
                            $sla_violated_responded_person_user_emails[] = user::where('id', '=', $responded_person_user_id)->pluck('email')->first();
                        }

                        $sla_violated_responded_fixed_person = array_diff($sla_violated_responded_person, $responded_person_user_ids);

                        //   $sla_violated_responded_fixed_person=str_replace('_', " ", subject);

                        // dd($sla_violated_responded_fixed_person);

                        $sla_violated_responded_personss[] = array_merge($sla_violated_responded_person_user_emails, $sla_violated_responded_fixed_person);
                    } else {
                        $sla_violated_responded_personss = [$sla_violated_responded_person];
                    }
                }
                $violated_responded_persons = array_combine($sla_violated_responded_time, $sla_violated_responded_personss);
            }

// dd($violated_responded_persons);
// violated Resolved  
            $sla_violated_resolved_lists_times = SlaViolatedEscalate::where('sla_plan', '=', $id)->where('escalate_type', '=', 'resolution')->pluck('escalate_time')->toArray();


            $sla_violated_resolved_lists = SlaViolatedEscalate::where('sla_plan', '=', $id)->where('escalate_type', '=', 'resolution')->pluck('escalate_person', 'escalate_time')->toArray();
            // dd($sla_violated_resolved_lists);
            if ($sla_violated_resolved_lists) {
                foreach ($sla_violated_resolved_lists as $key => $value) {

                    $sla_violated_resolved_person1 =str_replace('_', " ",  $value);
                     $sla_violated_resolved_person=array_filter($sla_violated_resolved_person1, 'strlen');

                    $definite_articles = array('assigner', 'department manager', 'team lead', 'admin');

                    $resolved_person_user_ids = array_diff($sla_violated_resolved_person, $definite_articles);

                    $sla_violated_responded_person_user_emails = user::whereIn('id', $resolved_person_user_ids)->pluck('email')->toArray();

                    $sla_violated_responded_fixed_person = array_diff($sla_violated_resolved_person, $resolved_person_user_ids);
// $sla_violated_responded_persons[] = implode(",", array_merge($sla_violated_responded_fixed_person, $sla_violated_responded_person_user_emails));
                    $sla_violated_responded_persons[] = array_merge($sla_violated_responded_fixed_person, $sla_violated_responded_person_user_emails);
                }
                // dd($sla_violated_responded_persons);
                $violated_resolved_time_persons = array_combine($sla_violated_resolved_lists_times, $sla_violated_responded_persons);
            } else {
                $violated_resolved_time_persons = null;
            }

// dd($sla_approaches_response_persons);
            // dd($violated_resolved_time_persons);
            $sla_target = SlaTargets::where('sla_id', $id)->first();
            if ($sla_target) {
                $sla_target_priority_id = $sla_target->priority_id;
            }
            return view('themes.default1.admin.helpdesk.manage.sla.edit', compact('slas', 'sla_target', 'sla_target_priority_id', 'apply_sla_depertment', 'apply_sla_company', 'apply_sla_tickettype', 'apply_sla_ticketsource', 'sla_approaches_response_persons', 'sla_approaches_resolution_persons', 'sla_approaches_resolution_person_user_emails', 'sla_violated_responded_fixed_person', 'sla_violated_responded_person_user_emails', 'sla_violated_resolved_lists', 'violated_responded_persons', 'violated_resolved_time_persons', 'sla_violated_responded_persons', 'sla_approaches_response_time', 'sla_approaches_resolution_time', 'sla_violated_responded_time'));
        } catch (Exception $e) {
            //dd($e);
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param type int       $id
     * @param type Sla_plan  $sla
     * @param type SlaUpdate $request
     *
     * @return type Response
     */
    public function update($id, SlaUpdateRequest $request) {
        try {
            /* Fill values to selected field using Id except Check box */
            $sla = Sla_plan::whereId($id)->first();
            $default_sla = Sla_plan::where('id', '=', 1)->first();
            $sla->fill($request->except('transient', 'ticket_overdue'))->save();

            $apply_sla_depertment = $request->apply_sla_depertments;

            if ($apply_sla_depertment != null) {
                $apply_sla_depertment_id = Department::whereIn('name', $request->apply_sla_depertments)->pluck('id')->toArray();

                $sla->apply_sla_depertment = implode(",", $apply_sla_depertment_id);
            } else {
                $sla->apply_sla_depertment = "";
            }
            $apply_sla_company = $request->apply_sla_companys;
            if ($apply_sla_company != null) {

                $apply_sla_company_id = Organization::whereIn('name', $request->apply_sla_companys)->pluck('id')->toArray();

                $sla->apply_sla_company = implode(",", $apply_sla_company_id);
            } else {

                $sla->apply_sla_company = "";
            }

            $apply_sla_tickettype = $request->apply_sla_tickettypes;
            if ($apply_sla_tickettype != null) {

                $apply_sla_tickettype_id = Tickettype::whereIn('name', $request->apply_sla_tickettypes)->pluck('id')->toArray();
                $sla->apply_sla_tickettype = implode(",", $apply_sla_tickettype_id);
            } else {
                $sla->apply_sla_tickettype = "";
            }

            $apply_sla_ticketsource = $request->apply_sla_ticketsources;
            if ($apply_sla_ticketsource != null) {

                $apply_sla_ticketsource_id = Ticket_source::whereIn('name', $request->apply_sla_ticketsources)->pluck('id')->toArray();
                $sla->apply_sla_ticketsource = implode(",", $apply_sla_ticketsource_id);
            } else {
                $sla->apply_sla_ticketsource = "";
            }

            $sla->save();

            //sla approaches
            $approach_eacalate = SlaApproachEscalate::where('sla_plan', '=', $id)->delete();
            $violated_eacalate = SlaViolatedEscalate::where('sla_plan', '=', $id)->delete();
            $approaches_response_escalate_time = $request->approaches_response_escalate_time;
            $emails = $request->approaches_response_escalate_person;
            // dd($approaches_response_escalate_time);


// dd($emails);
            if ($approaches_response_escalate_time != 'null' &&  $emails != null) {
               
                $emails = $request->approaches_response_escalate_person;

                $definite_articles = array('assigner', 'department manager', 'team lead', 'admin');

                $users = array_diff($emails, $definite_articles);
                if ($users) {
                    foreach ($users as $user) {
                        $approaches_response_escalate_person_userids[] = user::where('email', '=', $user)->pluck('id')->first();
                    }


                    $selected_user = array_merge($emails, $approaches_response_escalate_person_userids);
                    $users11 = array_diff($selected_user, $users);

                    $response_escalate_person = implode(",", $users11);
                } else {
                    $response_escalate_person = implode(",", $emails);
                }
                if ($response_escalate_person) {
                    $sla_approaches_response = SlaApproachEscalate::where('sla_plan', $sla->id)->where('escalate_type', 'response')->first();

                    if ($sla_approaches_response) {

                        // $sla_approaches_response = new SlaApproachEscalate();
                        $sla_approaches_response->sla_plan = $sla->id;
                        $sla_approaches_response->escalate_time = $request->approaches_response_escalate_time;
                        $sla_approaches_response->escalate_type = 'response';
                        $sla_approaches_response->escalate_person = $response_escalate_person;
                        $sla_approaches_response->save();
                    } else {
                        // dd('hhhh');
                        $sla_approaches_response = new SlaApproachEscalate();
                        $sla_approaches_response->sla_plan = $sla->id;
                        $sla_approaches_response->escalate_time = $request->approaches_response_escalate_time;
                        $sla_approaches_response->escalate_type = 'response';
                        $sla_approaches_response->escalate_person = $response_escalate_person;
                        $sla_approaches_response->save();
                    }
                }
            }

// dd('ok');
            $approaches_resolution_escalate_time = $request->approaches_resolution_escalate_time;
            $emails1 = $request->approaches_response_escalate_person;
            if ($approaches_resolution_escalate_time != 'null' &&  $emails1 != null) {

                $emails1 = $request->approaches_response_escalate_person;
                  $definite_articles = array('assigner', 'department manager', 'team lead', 'admin');
                $users = array_diff($emails1, $definite_articles);
                if ($users) {
                    foreach ($users as $user) {
                        $approaches_resolution_escalate_person_userids[] = user::where('email', '=', $user)->pluck('id')->first();
                    }

                    $selected_user = array_merge($emails1, $approaches_resolution_escalate_person_userids);

                    $users11 = array_diff($selected_user, $users);
                    $resolution_escalate_person = implode(",", $users11);
                } else {
                    $resolution_escalate_person = implode(",", $emails1);
                }
                if ($resolution_escalate_person) {
                    $sla_approaches_resolution = SlaApproachEscalate::where('sla_plan', $sla->id)->where('escalate_type', 'resolution')->first();
                    if ($sla_approaches_resolution) {
                        // $sla_approaches_resolution = new SlaApproachEscalate();
                        $sla_approaches_resolution->sla_plan = $sla->id;
                        $sla_approaches_resolution->escalate_time = $request->approaches_resolution_escalate_time;
                        $sla_approaches_resolution->escalate_type = 'resolution';
                        $sla_approaches_resolution->escalate_person = $resolution_escalate_person;
                        $sla_approaches_resolution->save();
                    } else {
                        $sla_approaches_resolution = new SlaApproachEscalate();
                        $sla_approaches_resolution->sla_plan = $sla->id;
                        $sla_approaches_resolution->escalate_time = $request->approaches_resolution_escalate_time;
                        $sla_approaches_resolution->escalate_type = 'resolution';
                        $sla_approaches_resolution->escalate_person = $resolution_escalate_person;
                        $sla_approaches_resolution->save();
                    }
                }
            }

// violated
            $violated_response_escalate_time = $request->violated_response_escalate_time;
            $emails2 = $request->violated_response_escalate_person;
            if ($violated_response_escalate_time != 'null' &&  $emails2 != null) {
                $emails2 = $request->violated_response_escalate_person;
  $definite_articles = array('assigner', 'department manager', 'team lead', 'admin');

                $users = array_diff($emails2, $definite_articles);
                if ($users) {
                    foreach ($users as $user) {
                        $violated_responded_escalate_person_userids[] = user::where('email', '=', $user)->pluck('id')->first();
                    }

                    $selected_user = array_merge($emails2, $violated_responded_escalate_person_userids);

                    $users11 = array_diff($selected_user, $users);

                    $violated_responded_escalate_person = implode(",", $users11);
                } else {
                    $violated_responded_escalate_person = implode(",", $emails2);
                }

              if ($violated_responded_escalate_person) {

                    $sla_violated_responded = SlaViolatedEscalate::where('sla_plan', $sla->id)->where('escalate_type', 'response')->first();

                    if ($sla_violated_responded) {
                        $sla_violated_responded = SlaViolatedEscalate::where('sla_plan', $sla->id)->where('escalate_type', 'response')->first();
                        $sla_violated_responded->sla_plan = $sla->id;
                        $sla_violated_responded->escalate_time = $request->violated_response_escalate_time;
                        $sla_violated_responded->escalate_type = 'response';
                        $sla_violated_responded->escalate_person = $violated_responded_escalate_person;
                        $sla_violated_responded->save();
                    } else {
                        $sla_violated_responded = new SlaViolatedEscalate();
                        $sla_violated_responded->sla_plan = $sla->id;
                        $sla_violated_responded->escalate_time = $request->violated_response_escalate_time;
                        $sla_violated_responded->escalate_type = 'response';
                        $sla_violated_responded->escalate_person = $violated_responded_escalate_person;
                        $sla_violated_responded->save();
                    }
                }
            }
// dd('hfffere');
            $sla_violated_responded = SlaViolatedEscalate::where('sla_plan', $sla->id)->where('escalate_type', 'resolution')->delete();

            for ($j = 0; $j < 5; $j++) {

                $violatedescalate_time = $request->input('violated_resolution_escalate_time' . $j);
                // dd($violatedescalate_time);
                // dd($violatedescalate_time);
                if ($violatedescalate_time != 'null') {
                    // dd('yes');

                    $violated_resolved_emails = $request->input('violated_resolution_escalate_person' . $j);

                    if ($violated_resolved_emails != null) {

                        $violated_resolved_emails = $request->input('violated_resolution_escalate_person' . $j);
                        $violated_resolved_emailss = implode(",", $violated_resolved_emails);



                          $definite_articles = array('assigner', 'department manager', 'team lead', 'admin');

                        $users = array_diff($violated_resolved_emails, $definite_articles);


                        if ($users) {

                            $violated_resolution_resolution_escalate_person_userids = user::whereIn('email', $users)->pluck('id')->toArray();


                            $selected_user = array_merge($violated_resolved_emails, $violated_resolution_resolution_escalate_person_userids);

                            $sla_violated_resolved_users = array_diff($selected_user, $users);
                            $sla_violated_resolved_users = implode(",", $sla_violated_resolved_users);
                        } else {
                            $sla_violated_resolved_users = $violated_resolved_emailss;
                        }

                        $sla_violated_resolved = new SlaViolatedEscalate();
                        $sla_violated_resolved->sla_plan = $sla->id;
                        $sla_violated_resolved->escalate_time = implode(",", $request->input('violated_resolution_escalate_time' . $j));
                        $sla_violated_resolved->escalate_type = 'resolution';
                        $sla_violated_resolved->escalate_person = $sla_violated_resolved_users;
                        $sla_violated_resolved->save();
                    }
                }
            }


            //}

            $targets = $sla->target; //SlaTargets::where('sla_id', '=', $slas->id)->first();
            if (!$targets) {
                $targets = new SlaTargets();
            }
            // $targets = SlaTargets::whereId($sla_target_id->id)->first();
            //if ($targets) {
            $targets->sla_id = $sla->id;

            $targets->priority_id = $request->sla_priority;
            $response_count = 0;
            $resolve_count = 0;
            if ($request->response_count != "") {
                $response_count = $request->response_count;
            }
            if ($request->resolve_count != "") {
                $resolve_count = $request->resolve_count;
            }
            $targets->respond_within = $response_count . '-' . $request->response_duration;
            $targets->resolve_within = $resolve_count . '-' . $request->resolve_duration;
            $targets->business_hour_id = $request->business_hour;
            $targets->send_email = $request->status;
            if ($request->has('send_sms')) {
                $targets->send_sms = $request->send_sms;
            } else {
                $targets->send_sms = 0;
            }
            $targets->save();
            //$this->updateDuedate($sla);
            $sla_target_id = $targets->id;

            Sla_plan::where('id', $id)->update(array('sla_target' => $sla_target_id));
            //}
// dd('hooo');
            return redirect('sla')->with('success', Lang::get('lang.sla_plan_updated_successfully'));
        } catch (Exception $e) {
            /* redirect to Index page with Fails Message */
            return redirect('sla')->with('fails', Lang::get('lang.sla_plan_can_not_update') . '<li>' . $e->getMessage() . '</li>');
        }
    }

    public function updateDuedate($sla_model) {
        $apply = new \App\Http\Controllers\SLA\ApplySla();
        $slaid = $sla_model->id;
        $sla = $apply->sla($slaid);
        $sla_model->tickets()->chunk(100, function($tickets) use($sla, $slaid, $apply) {
            foreach ($tickets as $ticket) {
                $created_at = $ticket->created_at;
                $answered = $ticket->isanswered;
                if ($answered == 1) {
                    $time = $sla->resolveTime();
                } else {
                    $time = $sla->respondTime();
                }
                $total = $ticket->halt()->sum('halted_time');
                $resolve = $time + $total;
                $due = $apply->slaResolveDue($slaid, $created_at, $resolve, $ticket)->tz('UTC');
                $ticket->duedate = $due;
                $ticket->save();
            }
        });
    }

    public function responseOverdue($slaid, $ticket_created) {
        $sla = new \App\Http\Controllers\SLA\ApplySla();
        $responds_due = $sla->slaRespondsDue($slaid, faveotime($ticket_created->timezone(timezone()), true, true, true))->timezone('UTC');
        return $responds_due;
    }

    public function resolveOverdue($slaid, $ticket_created, $sla_time = "", $ticket) {
        $sla = new \App\Http\Controllers\SLA\ApplySla();
        $resolve_due = $sla->slaResolveDue($slaid, faveotime($ticket_created->timezone(timezone()), true, true, true), "", $ticket)->timezone('UTC');
        return $resolve_due;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param type int      $id
     * @param type Sla_plan $sla
     *
     * @return type Response
     */
    public function destroy($id) {
        // dd('here');
        $default_sla = Sla_plan::where('id', '=', '1')->first();



        if ($default_sla->id == $id) {
            return redirect('departments')->with('fails', Lang::get('lang.you_cannot_delete_default_department'));
        } else {
            // escalate delete
            $approach_eacalate = SlaApproachEscalate::where('sla_plan', '=', $id)->delete();
            $violated_eacalate = SlaViolatedEscalate::where('sla_plan', '=', $id)->delete();
            $tickets = DB::table('tickets')->where('sla', '=', $id)->update(['sla' => $default_sla->id]);
            if ($tickets > 0) {
                if ($tickets > 1) {
                    $text_tickets = 'Tickets';
                } else {
                    $text_tickets = 'Ticket';
                }
                $ticket = '<li>' . $tickets . ' ' . $text_tickets . Lang::get('lang.have_been_moved_to_default_sla') . '</li>';
            } else {
                $ticket = '';
            }
            // $dept = DB::table('department')->where('sla', '=', $id)->update(['sla' => $default_sla->id]);
            // if ($dept > 0) {
            //     if ($dept > 1) {
            //         $text_dept = 'Emails';
            //     } else {
            //         $text_dept = 'Email';
            //     }
            //     $dept = '<li>' . Lang::get('lang.associated_department_have_been_moved_to_default_sla') . '</li>';
            // } else {
            //     $dept = '';
            // }
            //$dflt_sla = Company::where('sla', '=', $id)->update(array('sla' => $default_sla->id, 'apply_sla_plan' => 1));
            //$dflt_sla = Tickettype::where('sla', '=', $id)->update(array('sla' => $default_sla->id, 'apply_sla_plan' => 1));
            //$dflt_sla = Ticket_source::where('sla', '=', $id)->update(array('sla' => $default_sla->id, 'apply_sla_plan' => 1));
            // $topic = DB::table('help_topic')->where('sla_plan', '=', $id)->update(['sla_plan' => $default_sla->id]);
            // if ($topic > 0) {
            //     if ($topic > 1) {
            //         $text_topic = 'Emails';
            //     } else {
            //         $text_topic = 'Email';
            //     }
            //     $topic = '<li>' . Lang::get('lang.associated_help_topic_have_been_moved_to_default_sla') . '</li>';
            // } else {
            //     $topic = '';
            // }
            // $message = $ticket . $dept . $topic;
            $message = $ticket;
            /* Delete a perticular field from the database by delete() using Id */
            $slas = Sla_plan::whereId($id)->first();
            /* Check whether function success or not */
            try {
                $slas->delete();
                /* redirect to Index page with Success Message */
                return redirect('sla')->with('success', Lang::get('lang.sla_plan_deleted_successfully') . $message);
            } catch (Exception $e) {
                /* redirect to Index page with Fails Message */
                return redirect('sla')->with('fails', Lang::get('lang.sla_plan_can_not_delete') . '<li>' . $e->getMessage() . '</li>');
            }
        }
    }

}
