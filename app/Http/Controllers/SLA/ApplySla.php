<?php

namespace App\Http\Controllers\SLA;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\helpdesk\Manage\Sla\Sla_plan;
use App\Http\Controllers\Agent\helpdesk\Notifications\NotificationController;
use Exception;
use App\Model\helpdesk\Ticket\Tickets;
use App\User;

class ApplySla extends Controller {

    public $respose_approach_persons;
    public $resolve_approach_persons;
    public $respose_violate_persons;
    public $resolve_violate_persons;
    public $not_assign_persons;
    public $ticket = NULL;
    public $sla = NULL;
    public $due_type = NULL;
    public $minutes = NULL;

    public function addPersons() {
        $this->respose_approach_persons = $this->resposeApproachPersons();
        $this->resolve_approach_persons = $this->resolveApproachPersons();
        $this->respose_violate_persons = $this->resposeViolatedPersons();
        $this->resolve_violate_persons = $this->resolutionViolatedPersons();
        //$this->not_assign_persons = $this->notAssignPersons();
    }

    public function resposeApproachPersons() {
        if ($this->sla->sla) {
            $sla = $this->sla->sla;
            $respose_approach_persons = $sla->approach()
                    ->where('escalate_type', 'response')
                    ->select('escalate_person', 'escalate_time')
                    ->pluck('escalate_person', 'escalate_time')
                    ->toArray();
            return $respose_approach_persons;
        }
    }

    public function resolveApproachPersons() {
        if ($this->sla->sla) {
            $sla = $this->sla->sla;
            $resolution_approach_persons = $sla->approach()
                    ->where('escalate_type', 'resolution')
                    ->select('escalate_person', 'escalate_time')
                    ->pluck('escalate_person', 'escalate_time')
                    ->toArray();
            return $resolution_approach_persons;
        }
    }

    public function resposeViolatedPersons() {
        if ($this->sla->sla) {
            $sla = $this->sla->sla;
            $respose_approach_persons = $sla->violated()
                    ->where('escalate_type', 'response')
                    ->select('escalate_person', 'escalate_time')
                    ->pluck('escalate_person', 'escalate_time')
                    ->toArray();
            return $respose_approach_persons;
        }
    }

    public function resolutionViolatedPersons() {
        if ($this->sla->sla) {
            $sla = $this->sla->sla;
            $respose_approach_persons = $sla->violated()
                    ->where('escalate_type', 'resolution')
                    ->select('escalate_person', 'escalate_time')
                    ->pluck('escalate_person', 'escalate_time')
                    ->toArray();
            return $respose_approach_persons;
        }
    }

    public function notAssignPersons() {
        //return ['-30'=>['admin']];
        if ($this->sla->sla) {
            $sla = $this->sla->sla;
            $respose_approach_persons = $sla->notAssign()
                    ->where('escalate_type', 'no_assign')
                    ->select('escalate_person', 'escalate_time')
                    ->pluck('escalate_person', 'escalate_time')
                    ->toArray();
            return $respose_approach_persons;
        }
    }

    public function sla($id) {
        $sla = new Sla($id);
        return $sla;
    }

    public function getSla($id = "") {
        if ($id == "") {
            $sla = Sla_plan::first();
            if (!$sla) {
                throw new Exception('SLA not found');
            }
            $id = $sla->id;
        }
        return $this->sla($id);
    }

    public function defaultSla() {
        $sla = $this->getSla();
        return $sla->getId();
    }

    public function departmentSla($depid) {
        $plans = Sla_plan::select('id')->get();
        if ($plans->count() > 0) {
            foreach ($plans as $plan) {
                $departments = $this->getSla($plan->id)->getDepartments();
                if (is_array($departments) && in_array($depid, $departments)) {
                    return $this->getSla($plan->id)->getId();
                }
            }
        }
    }

    public function companySla($orgId) {
        $plans = Sla_plan::select('id')->get();
        if ($plans->count() > 0) {
            foreach ($plans as $plan) {
                $companies = $this->getSla($plan->id)->getCompanies();
                if (is_array($companies) && in_array($orgId, $companies)) {
                    return $this->getSla($plan->id)->getId();
                }
            }
        }
    }

    public function typeSla($typeid) {
        $plans = Sla_plan::select('id')->get();
        if ($plans->count() > 0) {
            foreach ($plans as $plan) {
                $types = $this->getSla($plan->id)->getTicketTypes();
                if (is_array($types) && in_array($typeid, $types)) {
                    return $this->getSla($plan->id)->getId();
                }
            }
        }
    }

    public function sourceSla($sourceid) {
        $plans = Sla_plan::select('id')->get();
        if ($plans->count() > 0) {
            foreach ($plans as $plan) {
                $sources = $this->getSla($plan->id)->getTicketSources();
                if (is_array($sources) && in_array($sourceid, $sources)) {
                    return $this->getSla($plan->id)->getId();
                }
            }
        }
    }

    public function ticket($ticketid) {
        $tickets = new Tickets();
        $ticket = $tickets->find($ticketid);
        if (!$ticket) {
            throw new Exception('Ticket not found for SLA');
        }
        return $ticket;
    }

    public function slaRespondsDue($slaid, $ticket_created) {
        //echo "Created_at from Database => ".$ticket_created."(UTC)<br>";
        $sla = $this->sla($slaid);
        //echo "SLA name => ".$sla->getName()."<br>";
        $respond = $sla->respondTime();
        $ticket_created_with_bussiness_timezone = $ticket_created->tz($sla->timezone());
        //echo "Ticket Created with Bussiness Hour Time Zone => ".$ticket_created_with_bussiness_timezone." (".$sla->timezone().")<br>";
        $estimate = null;
        if ($respond > 0) {
            $estimate = $this->estimateRespondsDue($ticket_created_with_bussiness_timezone, $sla, 0, "", "", $sla->timezone());
        }
//        dd($estimate);
        //echo "Estimated Duedate => ".$estimate."(".$sla->timezone().")<br>";
        //echo "Estimated Duedate => ".$estimate->tz(timezone())."(".timezone().")<br>";
        //echo "Estimated Duedate Saving to database => ".$estimate->tz('UTC')."(UTC)<br>";
        return $estimate;
    }

    public function slaResolveDue($slaid, $ticket_created, $resolve = "") {
        $sla = $this->sla($slaid);
        if ($resolve == "") {
            $resolve = $sla->resolveTime();
        }
        $ticket_created_with_bussiness_timezone = $ticket_created->tz($sla->timezone());
        $estimate = null;
        if ($resolve > 0) {
            //dd($ticket_created->timezone(timezone()),$resolve);
            $estimate = $this->estimateRespondsDue($ticket_created_with_bussiness_timezone, $sla, 0, $resolve, $ticket_created, $sla->timezone());
        }
//        dd($estimate);
        return $estimate;
    }

    public function estimateRespondsDue($ticket_created, $sla, $i = 0, $respond = "", $today = "", $tz) {
        //dd($ticket_created);
        $ticket_c = faveotime($ticket_created, true, true, true, $tz);
        //echo $ticket_created . " -Created_at via faveo time<br>";
        $ticket_createds = faveotime($ticket_created, true, true, true, $tz);
        $ticket_created = faveotime($ticket_created, 0, 0, 0, $tz);
        $schedule = $sla->getBusinessSchedule($ticket_created);

        if ($respond == "") {
            $respond = $sla->respondTime();
        }
        //echo $respond . " -response time<br>";
        $estimate = $ticket_createds->addMinutes($respond);
        //echo json_encode($schedule) . " -  - " . $ticket_created . " - schedule<br>";
        if (!is_array($schedule) && $schedule == true) {
            $estimate = $ticket_c->addMinutes($respond);
            $schedule = $sla->getBusinessSchedule($estimate);
            if (!$schedule) {
                $respond = $estimate->diffInMinutes(faveotime($estimate, 0, 0, 0, $tz));
                $estimate = $this->estimateRespondsDue(faveotime($estimate, 0, 0, 0, $tz), $sla, 0, $respond, "", $tz);
            }
            //echo $estimate . " -schedule-true-estimate<br>";
        } elseif (!is_array($schedule) && $schedule == false) {
            $i = $i + 1;
            //echo "<b>function returing</b><br>";
            $estimate = $this->estimateRespondsDue($ticket_c->addDays($i), $sla, $i - 1, $respond, "", $tz);
            //echo $estimate . " -schedule-false-estimate<br>";
        } elseif (is_array($schedule)) {
            $estimate = $this->estmateWithArray($schedule, $respond, $ticket_c, $sla, $i, $today, $tz);
            //echo $estimate . " -schedule-array-estimate<br>";
        }
//        echo $estimate . " -actual-estimate<br>";
        return $estimate;
    }

    public function businessTime($start, $end, $slaid, $time = 0) {
        $sla = $this->sla($slaid);
        $timezone = $sla->timezone();
        $start = $start->tz($timezone);
        $end = $end->tz($timezone);
        $start_date = faveotime($start, 0, 0, 0, $timezone);
        // echo "Start Date : " . $start_date . "<br>";
        //echo "un halted Date : " . $end . "<br>";
        $start_at_24 = faveotime($start, 23, 59, 59, $timezone);
        $schedule = $sla->getBusinessSchedule($start);
        //echo "Schedule : " . json_encode($schedule) . "<br>";
        if (($schedule == true && !is_array($schedule)) && \Carbon\Carbon::now()->tz($timezone)->format('Y-m-d') == $start->format('Y-m-d')) {
            $time += $end->diffInMinutes($start);
            return $time;
        }
        if ($schedule == false && \Carbon\Carbon::now()->tz($timezone)->format('Y-m-d') == $start->format('Y-m-d')) {

            return $time;
        }
        if (is_array($schedule) && \Carbon\Carbon::now()->tz($timezone)->format('Y-m-d') == $start->format('Y-m-d')) {
            return $this->busnessTimeArray($schedule, $start, $time, $timezone);
        }
        if (!is_array($schedule) && $schedule == true) {
            $time += $start_at_24->diffInMinutes($start);
            return $this->businessTime($start_date->addDay(), $end, $slaid, $time);
        } elseif (!is_array($schedule) && $schedule == false) {
            return $this->businessTime($start_date->addDay(), $end, $slaid, $time);
        } elseif (is_array($schedule)) {
            $time += $this->busnessTimeArray($schedule, $start, $time, $timezone);
            return $this->businessTime($start_date->addDay(), $end, $slaid, $time);
        }
    }

    public function busnessTimeArray($schedule, $start, $time = 0, $timezone) {
        $open = explode(":", $schedule[0]['open_time']);
        $end = explode(":", $schedule[0]['close_time']);
        $shour = $open[0];
        $smin = $open[1];
        $ehour = $end[0];
        $emin = $end[1];
        $scheduled_start = faveotime($start, $shour, $smin, 0, $timezone);

        $scheduled_end = faveotime($start, $ehour, $emin, 0, $timezone);
        if ($scheduled_start <= $start) {
            $scheduled_start = $start;
        }
        // echo "Working Started at : " . $scheduled_start."<br>";
        if ($scheduled_end >= \Carbon\Carbon::now()->tz($timezone)) {
            $scheduled_end = \Carbon\Carbon::now()->tz($timezone);
        }
        //echo "Working Ended at : " . $scheduled_end."<br>";
        $time = $scheduled_start->diffInMinutes($scheduled_end) + $time;
        //echo "Time : $time<br><hr>";
        return $time;
    }

    public function estimateResolveDue($ticket_creat, $sla, $i = 0, $resolve = "") {
        //echo $ticket_creat . "estimateResolveDue-function-created<br>";
        $ticket_createds = faveotime($ticket_creat, true, true, true);
        $ticket_created = faveotime($ticket_creat);
        $schedule = $sla->getBusinessSchedule($ticket_created);
        //dd($schedule);
//        if ($schedule == false) {
//            echo "$ticket_created-i=>$i false<br>";
//        }
//        if ($schedule == true) {
//            echo "$ticket_created-i=>$i true<br>";
//        }
//        if (is_array($schedule)) {
//            echo "$ticket_created array<br>";
//        }
        if ($resolve == "") {
            $resolve = $sla->resolveTime();
        }
        // echo $resolve . "resole-time<br>";
        //dd($ticket_created);
        $estimate = "";
        if ($schedule == false) {

            $add = faveotime($ticket_creat, true, true, true)->addMinutes($resolve);
            // echo $add . "estimateResolveDue-if-false-add<br>";
            if ($resolve < 1440) {
                $add = $add->addDay($i);
            }
            $estimate = $this->estimateResolveDue($add, $sla, 1);
            //echo $estimate . "estimateResolveDue-if-false-estimate<br>";
        } elseif (is_array($schedule)) {
            //echo $ticket_created . "estimateResolveDue-if-array-ticket_created<br>";
            $estimate = $this->estmateWithArray($schedule, $resolve, $ticket_created, $sla, $i);
        } elseif ($schedule == true) {
            $add = faveotime($ticket_creat, true, true, true);
            $schedule = $sla->getBusinessSchedule($add);
            if ($schedule == false) {
                $day1 = faveotime($add, true, true, true)->subDay();
                $day2 = faveotime($day1, 23, 59, 59);
                $diff_min = $day2->diffInMinutes($day1);
                //dd($resolve,$diff_min);
                $resolve = $resolve - $diff_min;
                //dd($resolve);
                if ($resolve < 1440) {
                    $add = faveotime($ticket_creat, true, true, true)->addDay($i);
                }
                $estimate = $this->estimateRespondsDue($add, $sla, 1, $resolve, '');
                //dd($estimate);
            } else {
                $estimate = $add->addMinutes($resolve);
            }
        }
        //dd($estimate);
        return $estimate;
    }

    public function estmateWithArray($schedule, $resolve, $ticket_created, $sla, $i, $today = "", $tz) {
        //echo $ticket_created . "  ARRAY-created<br>";
        $open = explode(":", $schedule[0]['open_time']);
        $end = explode(":", $schedule[0]['close_time']);
        $shour = $open[0];
        $smin = $open[1];
        $ehour = $end[0];
        $emin = $end[1];
        $sticket_created = faveotime($ticket_created, 0, 0, 0, $tz);
        $eticket_created = faveotime($ticket_created, 0, 0, 0, $tz);
        $scheduled_start = faveotime($sticket_created, $shour, $smin, 0, $tz);
        $scheduled_end = faveotime($eticket_created, $ehour, $emin, 0, $tz);
        if ($today == "") {
            $today = \Carbon\Carbon::today()->tz($tz);
        }
        //echo "<b>" . $scheduled_end . " < " . $ticket_created . " && " . faveotime($today) . " != " . $sticket_created . "</b><br>";
        if ($scheduled_end < $ticket_created && faveotime($today)->ne($sticket_created)) {
            //echo "inside the condition<br>";
            $ticket_created = $scheduled_end;
        }
        // echo "<b> After condition for above" . $ticket_created . "</b><br>";
        //echo $scheduled_start . "  ARRAY_Scheduled_Start<br>";
        //echo "<b>condition " . faveotime($scheduled_start, true, true, true) . " <= " . faveotime($ticket_created, true, true, true) . " && " . faveotime($ticket_created, true, true, true) . " <= " . faveotime($scheduled_end, true, true, true) . "</b><br>";
//        dd($scheduled_start,$ticket_created);
        if (faveotime($scheduled_start, true, true, true, $tz) <= faveotime($ticket_created, true, true, true, $tz) && faveotime($ticket_created, true, true, true) <= faveotime($scheduled_end, true, true, true)) {
            //echo faveotime($today) ." == ". faveotime($scheduled_start)."-between<br>";
            //dd($today,$scheduled_start);
            if (faveotime($today, 0, 0, 0, $tz) == faveotime($scheduled_start, 0, 0, 0, $tz)) {
                $scheduled_start = faveotime($ticket_created, true, true, true, $tz);
            } else {
                $scheduled_start = faveotime($sticket_created, $shour, $smin, 0, $tz);
            }
            //echo $scheduled_start . "  ARRAY_Scheduled_Start_Between<br>";
        } elseif (faveotime($scheduled_start, true, true, true, $tz) > faveotime($ticket_created, true, true, true, $tz)) {
            $scheduled_start = faveotime($scheduled_start, true, true, true, $tz);
            //echo $scheduled_start . "  ARRAY_Scheduled_Start_Early<br>";
        } elseif (faveotime($scheduled_end, true, true, true, $tz) < faveotime($ticket_created, true, true, true, $tz)) {
            $today = \Carbon\Carbon::today()->tz($tz);
            //if ($today == faveotime($ticket_created)) {
            //echo $scheduled_start . "after<br>";
            //echo "<b>function returing</b><br>";
            $estimate = $this->estimateRespondsDue(faveotime($scheduled_start, true, true, true, $tz)->addDay(), $sla, $i, $resolve, "", $tz);
            //echo $estimate . " ARRAY_estimate-retun<br>";
            return $estimate;
            //}
        }
        $estimate_time = faveotime($scheduled_start, true, true, true, $tz)->addMinutes($resolve);

        //echo $estimate_time . "-etimate<br>";
        $dif = $scheduled_end->diffInMinutes($estimate_time, false);
        //echo $dif . "balance-minutes<br>";
        if ($estimate_time > $scheduled_end) {
            $estimate_time = $scheduled_end;
        }
        if ($dif > 0) {
            return $this->estimateRespondsDue(faveotime($estimate_time, true, true, true, $tz)->addDay(), $sla, $i, $dif, "", $tz);
        } else {
            return $estimate_time;
        }
    }

    public function minSpent($ticketid, $type = "response") {
        $ticket = $this->ticket($ticketid);
        $sla = $this->sla($ticket->sla);
        $now = \Carbon\Carbon::now()->tz($sla->timezone());
        if ($type == "response") {
            $response_time = $sla->respondTime();
            $duetime = $this->slaRespondsDue($ticket->sla, $ticket->created_at);
        }
        if ($type == "resolve") {
            $response_time = $sla->resolveTime();
            $duetime = $this->slaResolveDue($ticket->sla, $ticket->created_at);
        }

        $difference = $now->diffInMinutes($duetime, false);
        return $response_time - $difference;
    }

    public function send() {
        $date = \Carbon\Carbon::now();
        //echo "Now $date <br>";
        $ticket = new Tickets();
        $status = new \App\Model\helpdesk\Ticket\Ticket_Status();
        $closedid = $status->join('ticket_status_type','ticket_status.purpose_of_status','=','ticket_status_type.id')
                ->where('ticket_status_type.name','=','open')
                ->select('ticket_status.id')
                ->get()
                ->toArray();
        $tickets = $ticket
                ->whereIn('status', $closedid)
                //->whereDate('duedate', '<', $date)
                ->select('id')
                ->chunk(10, function($tickets) {
            foreach ($tickets as $ticket) {
                $this->sendReport($ticket->id);
            }
            //echo "chunck finished";
        });
    }

    public function sendEmail() {
        if ($this->sla->isSendEmail() == true) {
            $due_type = 'response_due';
            if ($this->ticket->isanswered == 1) {
                $due_type = 'resolve_due';
            }
            $this->due_type = $due_type;
            $this->dispatchEmail();

            if ($this->ticket->isanswred == 0 && !$this->ticket->assign_to) {
                $due_type = 'no_assign';
            }
            $this->due_type = $due_type;
            $this->dispatchEmail();
        }
    }

    public function getApproachesPersons() {
        $persons = $this->respose_approach_persons;
        if ($this->due_type == 'resolve_due') {
            $persons = $this->resolve_approach_persons;
        }
        return $persons;
    }

    public function getViolatePersons() {
        $persons = $this->respose_violate_persons;
        if ($this->due_type == 'resolve_due') {
            $persons = $this->resolve_violate_persons;
        }
        return $persons;
    }

    public function getNoAssignPersons() {
        $persons = [];
        if ($this->due_type == 'no_assign') {
            $persons = $this->not_assign_persons;
        }
        return $persons;
    }

    public function approachesPersons() {
        $persons = $this->getApproachesPersons();
        if (count($persons) > 0) {
            foreach ($persons as $key => $value) {
                $this->minutes = $key;
                if (is_array($value)) {
                    foreach ($value as $person) {
                        $result[$this->minutes][$person] = $this->getField($person);
                    }
                }
            }
            return $result;
        }
    }

    public function violatePersons() {
        $persons = $this->getViolatePersons();
        if (count($persons) > 0) {
            foreach ($persons as $key => $value) {
                $this->minutes = $key;
                if (is_array($value)) {
                    foreach ($value as $person) {
                        $result[$this->minutes][$person] = $this->getField($person);
                    }
                }
            }
            return $result;
        }
    }

    public function noAssignedPersons() {
        $persons = $this->getNoAssignPersons();
        if (count($persons) > 0) {
            foreach ($persons as $key => $value) {
                $this->minutes = $key;
                if (is_array($value)) {
                    foreach ($value as $person) {
                        $result[$this->minutes][$person] = $this->getField($person);
                    }
                }
            }
            return $result;
        }
    }

    public function dispatchEmail() {
        if ($this->due_type && $this->due_type == 'response_due') {
            echo $this->due_type . "<br>";
            $this->dispatchApproachMail();
            $this->dispatchViolateMail();
        }
        if ($this->due_type && $this->due_type == 'resolve_due') {
            echo $this->due_type . "<br>";
            $this->dispatchApproachMail();
            $this->dispatchViolateMail();
        }
//        if ($this->due_type && $this->due_type == 'no_assign') {
//            echo $this->due_type . "<br>";
//            $this->dispatchNoAssignMail();
//        }
    }

    public function dispatchApproachMail() {
        $persons = $this->approachesPersons();
        echo json_encode($persons) . "</br>";
        if ($persons) {
            foreach ($persons as $minute => $emails) {
                echo "is array emails => " . is_array($emails) . " " . json_encode($emails) . "<br>";
                
                if (is_array($emails) && $this->isApproaches($minute)) {
                    loging('sla-approach ticket number: '.$this->ticket->ticket_number, json_encode($emails), 'info');
                    echo "I have entered <br>";
                    foreach ($emails as $person => $email) {
                        echo "is array email => " . is_array($email) . " " . json_encode($email) . "<br>";
                        if (is_array($email)) {
                            foreach ($email as $name => $e_mail) {
                                echo $minute . " => approach =>" . $person . " => " . $name . " => " . $e_mail . "<br>";
                                $this->sendMail($e_mail, $name, 'approach');
                                $this->sendSms($e_mail, $name, 'approach');
                            }
                        }
                    }
                }else{
                     loging('sla-approach ticket number: '.$this->ticket->ticket_number, 'No escalation', 'info');
                }
            }
        }
    }

    public function isApproaches($minute) {
        $check = false;
        $now = \Carbon\Carbon::now();
        echo "Now => " . $now . "<br>";
        if ($this->ticket && $this->ticket->duedate) {
            echo "Minutes => " . $minute . "<br>";
            $due_actual = $this->ticket->duedate;
            $due_second = $this->ticket->duedate;
            echo "Approach due => " . $due_actual . "<br>";
            $due_add_minutes = $due_actual->addMinutes($minute);
            $now_plus_30 = \Carbon\Carbon::now()->addMinutes(30);
            echo "Approach added minutes=> " . $due_add_minutes . "<br>";
            echo $due_add_minutes . "(Approach added minutes) >= " . $now . "(now) && " . $due_second . "(Approach due) > " . $now . "(now) && " . $due_add_minutes . "(Approach added minutes) < " . $now_plus_30 . " (now +30)<br>-------<br><br><br><br><br><br>";
            if ($due_add_minutes >= $now && $due_second > $now && $due_add_minutes < $now_plus_30) {
                echo "true<br>";
                $check = true;
            }
        }

        return $check;
    }

    public function dispatchNoAssignMail() {
        $persons = $this->noAssignedPersons();
        echo json_encode($persons) . "</br>";
        if ($persons) {
            foreach ($persons as $minute => $emails) {
                echo "is array emails => " . is_array($emails) . " " . json_encode($emails) . "<br>";
                if (is_array($emails) && $this->isNoAssignApproaches($minute)) {
                    echo "I have entered <br>";
                    foreach ($emails as $person => $email) {
                        echo "is array email => " . is_array($email) . " " . json_encode($email) . "<br>";
                        if (is_array($email)) {
                            foreach ($email as $name => $e_mail) {
                                echo $minute . " => approach =>" . $person . " => " . $name . " => " . $e_mail . "<br>";
                                $this->sendMail($e_mail, $name, 'message');
                                $this->sendSms($e_mail, $name, 'message');
                            }
                        }
                    }
                }
            }
        }
    }

    public function isNoAssignApproaches($minute) {
        $now = \Carbon\Carbon::now();
        $check = false;
        $now_plus_minutes = \Carbon\Carbon::now()->addMinutes($minute);
        $created_plus_30 = $this->ticket->created_at->addMinutes(30);
        $created_plus_minutes = $this->ticket->created_at->addMinutes($minute);
        $created_plus_minutes_plus_30 = $this->ticket->created_at->addMinutes(30);
        $ticket_created_time = $this->ticket->created_at;
        //echo "$ticket_created_time>=$now_plus_minutes && $ticket_created_time<$created_plus_30<br>";
        if ($created_plus_minutes <= $now && $created_plus_minutes_plus_30 >= $now) {
            $check = true;
        }
        return $check;
    }

    public function dispatchViolateMail() {
        $persons = $this->violatePersons();
        echo json_encode($persons) . "</br>";
        if ($persons) {
            foreach ($persons as $minute => $emails) {
                echo "is array emails => " . is_array($emails) . " " . json_encode($emails) . "<br>";
                if (is_array($emails) && $this->isViolated($minute)) {
                    foreach ($emails as $person => $email) {
                        loging('sla-violate ticket number: '.$this->ticket->ticket_number, json_encode($emails), 'info');
                        echo "is array email => " . is_array($email) . " " . json_encode($email) . "<br>";
                        if (is_array($email)) {
                            foreach ($email as $name => $e_mail) {
                                $this->sendMail($e_mail, $name, 'violate');
                                $this->sendSms($e_mail, $name, 'violate');
                                echo $minute." => violated =>".$person." => ".$name . " => " . $e_mail . "<br>";
                            }
                        }
                    }
                }else{
                    loging('sla-violate ticket number: '.$this->ticket->ticket_number, 'no escalation', 'info');
                        
                }
            }
        }
    }

    public function isViolated($minute) {
        $check = false;
        $now = \Carbon\Carbon::now();
        echo "Now => " . $now . "<br>";
        if ($this->ticket && $this->ticket->duedate) {
            echo "Minutes => " . $minute . "<br>";
            $due_actual = $this->ticket->duedate;
            $due_second = $this->ticket->duedate;
            echo "Violate due => " . $due_actual . "<br>";
            $due_add_minutes = $due_actual->addMinutes($minute);
            echo "violate added minutes=> " . $due_add_minutes . "<br>";
            $half_minutes = 30;
            $due_add_minutes_extra = $due_second->addMinutes($minute)->addMinutes(30);
            echo "violate added extra minutes=> " . $due_add_minutes_extra . "<br>";
            echo $due_add_minutes . "(violate added minutes) <= " . $now . "(now) && " . $due_add_minutes_extra . "(violate added extra 30 minutes) > " . $now . "(now) <br>-------<br><br><br><br>";
            if ($due_add_minutes <= $now && $due_add_minutes_extra > $now) {
                $check = true;
            }
        }

        return $check;
    }

    public function getField($person, $field = "email", $schma = true) {
        $collection = collect();
        $collection->push($this->getAgentIdByDependency($person));
        $unique = $collection->flatten()->unique()->filter(function ($item) {
            return $item != null;
        });
        if ($schma == true) {
            $unique = \App\User::whereNotNull($field)->whereIn('id', $unique)->pluck($field, 'first_name')->toArray();
        }
        return $unique;
    }

    public function getAgentIdByDependency($person) {
        $agents = [];
        switch ($person) {
            case "department_members": // pass department id
                if ($this->ticket) {
                    $modelid = $this->ticket->dept_id;
                    $agents = \App\Model\helpdesk\Agent\DepartmentAssignAgents::where('department_id', $modelid)->select('agent_id as department_members')->get()->toArray();
                }
                return $agents;
            case "team_members": //pass team id
                if ($this->ticket) {
                    $modelid = $this->ticket->team_id;
                    $agents = \App\Model\helpdesk\Agent\Assign_team_agent::where('team_id', $modelid)->select('agent_id as team_members')->get()->toArray();
                }
                return $agents;
            case "agent":
                $agents = \App\User::where('role', 'agent')->select('id')->get()->toArray();
                return $agents;
            case "admin":
                $agents = \App\User::where('role', 'admin')->select('id as admin')->get()->toArray();
                return $agents;
            case "user": // pass ticket user id
                if ($this->ticket) {
                    $modelid = $this->ticket->user_id;
                    $agents = ['user' => $modelid];
                }
                return $agents;
            case "agent_admin":
                $agents = \App\User::where('role', '!=', 'user')->select('id as agent_admin')->get()->toArray();
                return $agents;
            case "department_manager"://pass department id
                if ($this->ticket) {
                    $modelid = $this->ticket->dept_id;
                    $agents = \App\Model\helpdesk\Agent\Department::where('id', $modelid)->select('manager as department_manager')->get()->toArray();
                }
                return $agents;
            case "team_lead": //pass team id
                if ($this->ticket) {
                    $modelid = $this->ticket->team_id;
                    $agents = \App\Model\helpdesk\Agent\Teams::where('id', $modelid)->where('status', 1)->select('team_lead as team_lead')->get()->toArray();
                }
                return $agents;
            case "organization_manager"://pass user id
                if ($this->ticket) {
                    $modelid = $this->ticket->user_id;
                } else {
                    $modelid = $this->userid;
                }
                if ($modelid) {
                    $org = \App\Model\helpdesk\Agent_panel\User_org::where('user_id', $modelid)->select('org_id')->first();
                    if ($org) {
                        $orgid = $org->org_id;
                        $agents = \App\Model\helpdesk\Agent_panel\Organization::where('id', $orgid)->select('head as organization_manager')->get()->toArray();
                    }
                }
                return $agents;
            case "last_respondent":
                if ($this->ticket) {
                    $agents = $this->ticket->thread()->whereNotNull('user_id')->orderBy('id', 'desc')->select('user_id as last_respondent')->first()->toArray();
                }
                return $agents;
            case "assigned_agent_team":
                if ($this->ticket) {
                    $agents = ['assigned_agent_team' => $this->ticket->assigned_to];
                }
                return $agents;
            case "assigner":
                if ($this->ticket) {
                    $agents = ['assigned_agent_team' => $this->ticket->assigned_to];
                }
                return $agents;
            case "all_department_manager":
                $agents = \App\Model\helpdesk\Agent\Department::select('manager as all_department_manager')->get()->toArray();
                return $agents;
            case "all_team_lead":
                $agents = \App\Model\helpdesk\Agent\Teams::where('status', 1)->select('team_lead as all_team_lead')->get()->toArray();
                return $agents;
            case "client":
                if ($this->ticket) {
                    $agents = ['client' => $this->ticket->user_id];
                }
                return $agents;
            default :
                if ($person) {
                    $agents = \App\User::where('id', $person)->select('id')->get()->toArray();
                    return $agents;
                }
        }
    }

    public function sendReport($ticketid) {
        try {
            $ticket = $this->ticket($ticketid);
            echo "Ticket : " . $ticket->id . "<br>";
            //echo "Sla : " . $ticket->sla . "<br>";
            $this->ticket = $ticket;
            $slaid = $ticket->sla;

            if ($slaid) {
                $sla = $this->sla($slaid);
                $this->sla = $sla;
            }
            $this->addPersons();
            $this->sendEmail();
            //echo "<hr>";
        } catch (\Exception $ex) {
            loging('sla-escaltion', $ex->getMessage());
        }
    }

    public function sendMail($email, $name, $condition)
    {
        try{
        $user_extra_details = User::where('email', '=', $email)->select('user_language', 'role')->first();
        $type = $this->due_type;
        $scenario = $type . "_" . $condition;
        $template_variables = $this->getTemplateVariables();
        echo "<b>$scenario</b><br>";
        $phpMail = new \App\Http\Controllers\Common\PhpMailController();
        $from = $phpMail->mailfrom('1', $this->ticket->dept_id);
        $to = ['email' => $email, 'name' => $name, 'preferred_language' => $user_extra_details->user_language, 'role' => $user_extra_details->role];
        $message = ['scenario' => $scenario];
        $encoded = json_encode(['from' => $from, 'to' => $to, 'message' => $message, 'template_variables' => $template_variables]) . "<br>";
        $phpMail->sendmail($from, $to, $message, $template_variables);
        echo "<b>Sent to $email</b><br>";
        loging('sla-escaltion', $encoded, 'info');
     } catch(\Exception $e) {
        dd($e);
     }
    }

    public function getTemplateVariables()
    {
        $client_name = '';
        $client_email = '';
        $client_contact = '';
        $agent_email = '';
        $agent_name = '';
        $agent_contact = '';
        $requester = $this->ticket->user;
        $assign_agent = $this->ticket->assigned;
        $ticketid = $this->ticket->id;
        if ($requester) {
            $client_name = ($requester->first_name != '' || $requester->last_name != null) ?$requester->first_name.' '.$requester->last_name : $requester->user_name;
            $client_email = $requester->email;
            $client_contact = $requester->mobile;
        }
        if ($assign_agent) {
            $agent_email = $assign_agent->email;
            $agent_name = ($assign_agent->first_name != '' || $assign_agent->last_name != null) ?$assign_agent->first_name.' '.$assign_agent->last_name : $assign_agent->user_name;
            $agent_contact = $assign_agent->mobile;
        }
        $template_variables = [
            'ticket_due_date' => $this->ticket->duedate->tz(timezone()),
            'ticket_subject' => title($ticketid),
            'ticket_number' => $this->ticket->ticket_number,
            'ticket_link' => faveoUrl('thread/' . $ticketid),
            'ticket_created_at' => $this->ticket->created_at->tz(timezone()),
            'agent_name' => $agent_name,
            'agent_email' => $agent_email,
            'agent_contact' => $agent_contact,
            'client_email' => $client_email,
            'client_name' => $client_name,
            'client_contact' => $client_contact,
        ];

        return $template_variables;
    }

    public function sendSms($email, $name, $condition)
    {
        try{
        if ($this->sla->isSendSms() == true) {
            $notification = new NotificationController();
            if ($notification->checkPluginSetup()) {
                $user_details = User::select('email', 'first_name', 'last_name', 'user_name', 'role', 'mobile', 'country_code', 'user_language')->where('email', '=', $email)->where('mobile', '!=', null)->first()->toArray();
                if (count($user_details) > 0) {
                    $type = $this->due_type;
                    $scenario = $type . "_" . $condition;
                    $template_variables = $this->getTemplateVariables();
                    $message = ['scenario' => $scenario];
                    $ticket = $this->ticket->toArray();
                    $sms_controller = new \App\Plugins\SMS\Controllers\MsgNotificationController;
                    $sms_controller->notifyBySMS($user_details, $template_variables, $message, $ticket);
                }
            }
        }
        } catch (\Exception $e) {
            dd($e);
        }
    }
}
