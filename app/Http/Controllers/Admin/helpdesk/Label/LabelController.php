<?php

namespace App\Http\Controllers\Admin\helpdesk\Label;

use Illuminate\Http\Request;
use App\Http\Requests\helpdesk\LableUpdate;
use App\Http\Controllers\Controller;
use Exception;
use App\Model\helpdesk\Filters\Label;
use Datatable;
use Lang;

class LabelController extends Controller {
    
    public function __construct() {
        $this->middleware(['auth', 'role.agent']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            return view('themes.default1.admin.helpdesk.label.index');
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        try {
            return view('themes.default1.admin.helpdesk.label.create');
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required|unique:labels|max:50',
            'color' => 'required|regex:/#([a-fA-F0-9]{3}){1,2}\b/',
            'order' => 'required|unique:labels|integer',
        ]);
        try {
            $model = new Label();
            $result = $this->save($model, $request->input());
            if ($result) {
                return redirect('labels')->with('success',Lang::get('lang.labels_saved_successfully'));
                
            }
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        try {
            $labels = new Label();
            $label = $labels->find($id);
            if (!$label) {
                throw new Exception('Sorry! We are not able to find your request');
            }
            return view('themes.default1.admin.helpdesk.label.edit', compact('label'));
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LableUpdate $request, $id) {
        try {
            $labels = new Label();
            $label = $labels->find($id);
            if (!$label) {
                throw new Exception('Sorry! We are not able to find your request');
            }
            $result = $this->save($label, $request->input());
            if ($result) {
                return redirect('labels')->with('success',Lang::get('lang.labels_updated_successfully'));
            }
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            Label::where('id', '=', $id)->first()->delete();
            return redirect()->back()->with('success',Lang::get('lang.labels_deleted_successfully'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function save($model, $request, $json = false) {
        try {
            $result = $model->fill($request)->save();
            if ($json == true) {
                $result = $model->get()->toJson();
            }
            return $result;
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function ajaxTable() {
        $label = new Label();
        $labels = $label->select('id', 'title', 'color', 'order', 'status')->get();
        return Datatable::collection($labels)
                        ->addColumn('title', function($model) {
                            return $model->titleWithColor();
                        })
                        ->showColumns('order')
                        ->addColumn('status', function($model) {
                            return $model->status();
                        })
                        ->addColumn('action', function($model) {
                            $url = url('labels/delete/' . $model->id);
                            $confirmation = $delete = \App\Plugins\ServiceDesk\Controllers\Library\UtilityController::deletePopUp($model->id, $url, "Delete $model->subject");
                            
                           
                            $edit = "<a href='" . url('labels/' . $model->id . '/edit') . "' class='btn btn-xs btn-primary'><i class='fa fa-edit' style='color:white;'> &nbsp;</i>Edit</a>&nbsp;&nbsp; $confirmation";
                           // $delete = "<a href='" . url('labels/delete/' . $model->id) . "' class='btn btn-xs btn-primary'><i class='fa fa-trash' style='color:white;'> &nbsp;</i> </i>Delete</a>";
                            return "<p>" . $edit . "</p>";
                        })
                        ->searchColumns('title', 'status', 'order')
                        ->orderColumns('title', 'status', 'order')
                        ->make();
    }

    public function attachTicket(Request $request) {
        $filters = new \App\Model\helpdesk\Filters\Filter();
        $ticketid = $request->input('ticket_id');
        $labels = $request->input('labels');
        $filter = $filters->where('ticket_id',$ticketid)->where('key','label')->get();
        if($filter->count()>0){
            foreach($filter as $f){
                $f->delete();
            }
        }
        if (count($labels) > 0) {
            foreach ($labels as $label) {
                $filters->create(
                        ['ticket_id' => $ticketid, 'key' => 'label', 'value' => $label]);
            }
        }
        return response('success', 200);
    }

    public function getLabel(Request $request) {
        $term = $request->input('term');
        $response = $this->labels($term, 'array');
        return response($response, 200);
    }

    public function labels($search = "", $type = 'collection') {
        $labels = new Label();
        if ($search == "") {
            $output = $labels->select('title');
        } else {
            $output = $labels->select('title')->where('title', 'LIKE', '%' . $search . '%');
        }
        switch ($type) {
            case "array":
                return $output->pluck('title')->toArray();
            case "json":
                return $output->get()->toJson();
            default :
                return $output->get();
        }
    }

}
