<?php

namespace App\Http\Controllers\Admin\helpdesk;

// controllers
use App\Http\Controllers\Controller;
// requests
use App\Http\Requests\helpdesk\TeamRequest;
use App\Http\Requests\helpdesk\TeamUpdate;
// models
use App\Model\helpdesk\Agent\Assign_team_agent;
use App\Model\helpdesk\Agent\Teams;
use App\Model\helpdesk\Agent\Department;
use App\Model\helpdesk\Agent\Groups;
use App\User;
// classes
use DB;
use Exception;
use Lang;
use Datatable;
use Datatables;

/**
 * TeamController.
 *
 * @author      Ladybird <info@ithelpdesk.com>
 */
class TeamController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return type void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('roles');
    }

    /**
     * get Index page.
     *
     * @param type Teams             $team
     * @param type Assign_team_agent $assign_team_agent
     *
     * @return type Response
     */
    public function index() {
        try {
            $table = Datatable::table()
                    ->addColumn(
                            Lang::get('lang.name'), Lang::get('lang.status'), Lang::get('lang.team_members'), Lang::get('lang.team_lead'), Lang::get('lang.action'))  // these are the column headings to be shown
                    ->noScript();
            return view('themes.default1.admin.helpdesk.agent.teams.index', compact('table'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    public function TeamShow($id) {
        try {

            $teams = Teams::where('id', '=', $id)->first();
            $team_members = Assign_team_agent::where('team_id', '=', $teams->id)->select('agent_id')->count();

            $users = user::where('id', '=', $teams->team_lead)->select('user_name', 'first_name', 'last_name')->first();
            if ($users) {
                if ($users->first_name || $users->last_name) {
                    $team_lead_name = $users->first_name . ' ' . $users->last_name;
                } else {
                    $team_lead_name = $users->user_name;
                }
            } else {
                $team_lead_name = "No Team Lead";
            }
            // dd($team_lead_name);

            return view('themes.default1.agent.helpdesk.user.teamshow', compact('assign_team_agent', 'teams', 'team_members', 'team_lead_name'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param type User $user
     *
     * @return type Response
     */
    public function create(User $user) {
        try {
            $user = $user->where('role', '<>', 'user')->where('active', '=', 1)->orderBy('first_name')->get();

            return view('themes.default1.admin.helpdesk.agent.teams.create', compact('user'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param type Teams       $team
     * @param type TeamRequest $request
     *
     * @return type Response
     */
    public function store(Teams $team, TeamRequest $request) {
        try {
            /* Check whether function success or not */
            $team->fill($request->except('team_lead'))->save();
            if ($request->team_lead) {
                $team_assign_agent = new Assign_team_agent;
                $team_assign_agent->create([
                    'team_id' => $team->id,
                    'agent_id' => $request->team_lead
                ]);
                $team->update([
                    'team_lead' => $request->team_lead
                ]);
            }
            /* redirect to Index page with Success Message */
            return redirect('teams')->with('success', Lang::get('lang.team_saved_successfully'));
        } catch (Exception $e) {
            /* redirect to Index page with Fails Message */
            return redirect('teams')->with('fails', Lang::get('lang.teams_can_not_create') . '<li>' . $e->getMessage() . '</li>');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param type                   $id
     * @param type User              $user
     * @param type Assign_team_agent $assign_team_agent
     * @param type Teams             $team
     *
     * @return type Response
     */
    public function show($id, User $user, Assign_team_agent $assign_team_agent, Teams $team) {
        try {
            $user = $user->whereId($id)->first();
            $teams = $team->whereId($id)->first();

            // $team_lead_name=User::whereId($teams->team_lead)->first();
            // $team_lead = $team_lead_name->first_name . " " . $team_lead_name->last_name;
            // $total_members = $assign_team_agent->where('team_id',$id)->count();

            return view('themes.default1.admin.helpdesk.agent.teams.show', compact('user', 'teams', 'id'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    public function getshow($id) {
        // dd($request);
// $id = $request->input('show_id');
// dd($id);

        $users = DB::table('team_assign_agent')->select('team_assign_agent.id', 'team_assign_agent.team_id', 'users.user_name', 'users.first_name', 'users.last_name', 'users.active', 'users.role')
                ->join('users', 'users.id', '=', 'team_assign_agent.agent_id')
                ->where('team_assign_agent.team_id', '=', $id);
          // ->get();
// dd($users);
        return \Datatable::query($users)
                        ->showColumns('user_name')
                        ->addColumn('first_name', function($model) {

                            $full_name = ucfirst($model->first_name) . ' ' . ucfirst($model->last_name);
                            return $full_name;
                        })
                        ->addColumn('active', function($model) {
                            $role = "<a class='btn btn-primary btn-xs'>" . 'Inactive' . "</a>";
                            if ($model->active == '1') {
                                $role = "<a class='btn btn-success btn-xs'>" . 'Active' . "</a>";
                            }
                            return $role;
                        })
                        // ->addColumn('assign_group', function($model) {
                        //     $group = Groups::whereId($model->assign_group)->first();
                        //     return ($group->name);
                        // })
                        ->addColumn('role', function($model) {
                            if ($model->role == 'admin') {
                                $role = "<a class='btn btn-success btn-xs'>" . $model->role . "</a>";
                            } elseif ($model->role == 'agent') {
                                $role = "<a class='btn btn-primary btn-xs'>" . $model->role . "</a>";
                            }
                            return $role;
                        })

                        // ->showColumns('role')
                        ->searchColumns('first_name', 'last_name')
                        ->orderColumns('first_name', 'last_name')
                        ->make();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param type                   $id
     * @param type User              $user
     * @param type Assign_team_agent $assign_team_agent
     * @param type Teams             $team
     *
     * @return type Response
     */
    public function edit($id, User $user, Assign_team_agent $assign_team_agent, Teams $team) {
        try {
            $teams = $team->whereId($id)->first();
            $agent_team = $assign_team_agent->where('team_id', $id)->get();
            $agent_id = $agent_team->pluck('agent_id', 'agent_id');
            $user = User::whereIn('id', $agent_id)->where('active', '=', 1)->orderBy('first_name')->get();
            return view('themes.default1.admin.helpdesk.agent.teams.edit', compact('agent_id', 'user', 'teams', 'allagents'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param type int        $id
     * @param type Teams      $team
     * @param type TeamUpdate $request
     *
     * @return type Response
     */
    public function update($id, Teams $team, TeamUpdate $request) {
        $teams = $team->whereId($id)->first();
        //updating check box
        if ($request->team_lead) {
            $team_lead = $request->team_lead;
        } else {
            $team_lead = null;
        }
        $teams->team_lead = $team_lead;
        $teams->save();

        $alert = $request->input('assign_alert');
        $teams->assign_alert = $alert;
        $teams->save(); //saving check box
        //updating whole field
        /* Check whether function success or not */
        try {
            $teams->fill($request->except('team_lead'))->save();
            /* redirect to Index page with Success Message */
            return redirect('teams')->with('success', Lang::get('lang.team_updated_successfully'));
        } catch (Exception $e) {
            /* redirect to Index page with Fails Message */
            return redirect('teams')->with('fails', Lang::get('lang.teams_can_not_update') . '<li>' . $e->getMessage() . '</li>');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param type int               $id
     * @param type Teams             $team
     * @param type Assign_team_agent $assign_team_agent
     *
     * @return type Response
     */
    public function destroy($id, Teams $team, Assign_team_agent $assign_team_agent) {
        try {
            $assign_team_agent->where('team_id', $id)->delete();
            $teams = $team->whereId($id)->first();
            $tickets = DB::table('tickets')->where('team_id', '=', $id)->update(['team_id' => null]);
            /* Check whether function success or not */
            $teams->delete();
            /* redirect to Index page with Success Message */
            return redirect('teams')->with('success', Lang::get('lang.team_deleted_successfully'));
        } catch (Exception $e) {
            /* redirect to Index page with Fails Message */
            return redirect('teams')->with('fails', Lang::get('lang.teams_can_not_delete') . '<li>' . $e->getMessage() . '</li>');
        }
    }

    /**
     *
     *
     *
     *
     */
    public function getTeamTable() {
        $teams = \DB::table('teams')
                        ->leftJoin("users as u", 'teams.team_lead', '=', 'u.id')
                        ->leftjoin("team_assign_agent as ta", 'teams.id', '=', 'ta.team_id')
                        ->select('teams.name as name', 'teams.status as status', \DB::raw('COUNT(ta.team_id) as count'), 'u.user_name as user_name', 'u.first_name', 'u.last_name', 'teams.id as t_id', 'u.id as u_id')
                        ->groupBy('t_id')
                        ->orderBy('name')->get();

        return Datatables::of($teams)
                        ->removeColumn('first_name', 'last_name', 'u_id', 't_id')
                        ->editColumn('name', function($teams) {
                            $url = 'teams/' . $teams->t_id . '/edit';
                            return '<a href="' . $url . '">' . $teams->name . '</a>';
                        })
                        ->editColumn('status', function($teams) {
                            if ($teams->status == 0) {
                                return '<p class="btn btn-xs btn-danger" span style="color:white; pointer-events:none" >' . Lang::get('lang.inactive') . '</span>';
                            }
                            return '<p class="btn btn-xs btn-success" span style="color:white; pointer-events:none">' . Lang::get('lang.active') . '</span>';
                        })
                        ->editColumn('count', function($teams) {
                            return $teams->count;
                        })
                        ->editColumn('user_name', function($teams) {
                            if ($teams->u_id != null) {
                                $name = $teams->user_name;
                                if ($teams->first_name != '' && $teams->first_name != null) {
                                    $name = $teams->first_name . ' ' . $teams->last_name;
                                }
                                return '<a  href="' . route('user.show', $teams->u_id) . '" title="' . $name . '">' . $name . '</a>';
                            }
                            return '';
                        })
                        ->addColumn('action', function($teams) {
                            $show = '';
                            if ($teams->status == 1) {
                                $show = '<a href="' . route('teams.profile.show', $teams->t_id) . '" class="btn btn-primary btn-xs " ><i class="fa fa-eye" style="color:white;">&nbsp;&nbsp;</i>' . Lang::get('lang.view') . '</a> ';
                            }
                            return \Form::open(['route' => ['teams.destroy', $teams->t_id], 'method' => 'DELETE']) .
                                    \Form::button('<i class="fa fa-trash" style="color:white;"> </i> &nbsp; ' . Lang::get('lang.delete'), ['type' => 'submit',
                                        'class' => 'btn btn-primary btn-xs',
                                        'onclick' => 'return confirm("Are you sure?")']) .
                                    '&nbsp;&nbsp;<a href="' . route('teams.edit', $teams->t_id) . '" class="btn btn-xs btn-primary"><i class="fa fa-edit" style="color:white;"> &nbsp;</i> ' . Lang::get('lang.edit') . '</a>&nbsp;  ' . $show
                                    . \Form::close();
                        })
                        ->make();
    }

}
