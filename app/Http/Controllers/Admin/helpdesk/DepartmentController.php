<?php

namespace App\Http\Controllers\Admin\helpdesk;

// controller
use App\Http\Controllers\Controller;
// request
use App\Http\Requests\helpdesk\DepartmentRequest;
use App\Http\Requests\helpdesk\DepartmentUpdate;
// model
use App\Model\helpdesk\Agent\Department;
use App\Model\helpdesk\Agent\Group_assign_department;
use App\Model\helpdesk\Agent\Groups;
use App\Model\helpdesk\Agent\Teams;
use App\Model\helpdesk\Email\Emails;
use App\Model\helpdesk\Email\Template;
use App\Model\helpdesk\Manage\Help_topic;
use App\Model\helpdesk\Manage\Sla\Sla_plan;
use App\Model\helpdesk\Settings\System;
use App\Model\helpdesk\Ticket\Tickets;
use App\Model\helpdesk\Agent\DepartmentAssignAgents;
use App\User;
// classes
use DB;
use Exception;
use Lang;
use Datatable;
use Datatables;

/**
 * DepartmentController.
 *
 * @author      Ladybird <info@ithelpdesk.com>
 */
class DepartmentController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('roles');
    }

    /**
     * Get index page.
     *
     * @param type Department $department
     *
     * @return type Response
     */
    public function index(Department $department) {
        try {
            $table = Datatable::table()
                    ->addColumn(
                            Lang::get('lang.name'), Lang::get('lang.type'), Lang::get('lang.department_manager'), Lang::get('lang.action'))  // these are the column headings to be shown
                    ->noScript();
            return view('themes.default1.admin.helpdesk.agent.departments.index', compact('table'));
        } catch (Exception $e) {
            return view('404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param type User                    $user
     * @param type Group_assign_department $group_assign_department
     * @param type Department              $department
     * @param type Sla_plan                $sla
     * @param type Template                $template
     * @param type Emails                  $email
     * @param type Groups                  $group
     *
     * @return type Response
     */
    public function DepartmentShow($id) {
        $departments = Department::where('id', '=', $id)->first();

        $department_members_count = DepartmentAssignAgents::where('department_id', '=', $departments->id)->select('agent_id')->count();

        $department_members = DepartmentAssignAgents::where('department_id', '=', $departments->id)->select('agent_id')->get();
//           dd( $department_members);

        if ($departments->manager) {
            $dept_manager_name = User::where('id', '=', $departments->manager)->select('first_name', 'last_name', 'user_name')->first();
        } else {
            $dept_manager_name = null;
        }

        if ($department_members_count != 0) {


            foreach ($department_members as $department_member) {
                $user[] = User::where('id', '=', $department_member->agent_id)->select('first_name', 'last_name', 'user_name', 'email', 'ban', 'active')->first();
            }
        } else {
            $user = "";
        }
//        dd($user); 
        return view('themes.default1.admin.helpdesk.agent.departments.show', compact('departments', 'dept_manager_name', 'department_members', 'user'));
    }

    public function create(User $user, Group_assign_department $group_assign_department, Department $department, Template $template, Emails $email, Groups $group) {
        try {
            $user = $user->where('role', '<>', 'user')
                    ->where('active', '=', 1)
                    ->get();
            $emails = $email->select('email_name', 'id')->get();
            $templates = $template->get();
            $department = $department->get();


            return view('themes.default1.admin.helpdesk.agent.departments.create', compact('department', 'templates', 'user', 'emails'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param type Department        $department
     * @param type DepartmentRequest $request
     *
     * @return type Response
     */
    public function store(Department $department, DepartmentRequest $request) {
        try {
            $department->fill($request->except('manager'))->save();
            $requests = $request->input('group_id');
            $id = $department->id;
            if ($request->manager) {
                $department->manager = $request->input('manager');
            } else {
                $department->manager = null;
            }
            /* Succes And Failure condition */
            /*  Check Whether the function Success or Fail */
            if ($department->save() == true) {
                if ($request->input('sys_department') == 'on') {
                    DB::table('settings_system')
                            ->where('id', 1)
                            ->update(['department' => $department->id]);
                }
                return redirect('departments')->with('success', Lang::get('lang.department_saved_sucessfully'));
            } else {
                return redirect('departments')->with('fails', Lang::get('lang.failed_to_create_department'));
            }
        } catch (Exception $e) {
            return redirect('departments')->with('fails', Lang::get('lang.failed_to_create_department'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param type int                     $id
     * @param type User                    $user
     * @param type Group_assign_department $group_assign_department
     * @param type Template                $template
     * @param type Teams                   $team
     * @param type Department              $department
     * @param type Sla_plan                $sla
     * @param type Emails                  $email
     * @param type Groups                  $group
     *
     * @return type Response
     */
    public function edit($id) {
        $user = new User();
        $group_assign_department = new Group_assign_department();
        $template = new Template();
        $team = new Teams();
        $department = new Department();
        $sla = new Sla_plan();
        $email = new Emails();

        try {
            $sys_department = \DB::table('settings_system')
                    ->select('department')
                    ->where('id', '=', 1)
                    ->first();

            $user_array = $this->getDepartmentMembers($id);
            $user = $user->whereIn('id', $user_array)
                    ->where('active', '=', 1)
                    ->get();
            $emails = $email->select('email_name', 'id')->get();
            $templates = $template->get();
            $departments = $department->whereId($id)->first();


            return view('themes.default1.admin.helpdesk.agent.departments.edit', compact('team', 'templates', 'departments', 'user', 'emails', 'sys_department'));
        } catch (Exception $e) {
            return redirect('departments')->with('fails', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param type int                     $id
     * @param type Group_assign_department $group_assign_department
     * @param type Department              $department
     * @param type DepartmentUpdate        $request
     *
     * @return type Response
     */
    public function update($id, DepartmentUpdate $request) {
        $user = new User();
        $group_assign_department = new Group_assign_department();
        $template = new Template();
        $team = new Teams();
        $department = new Department();
        $sla = new Sla_plan();
        $email = new Emails();
        $group = new Groups();
        try {
            $table = $group_assign_department->where('department_id', $id);
            $table->delete();
            $requests = $request->input('group_id');
            // foreach ($requests as $req) {
            // DB::insert('insert into group_assign_department (group_id, department_id) values (?,?)', [$req, $id]);
            // }
            $departments = $department->whereId($id)->first();

            if ($request->manager) {
                $departments->manager = $request->input('manager');
            } else {
                $departments->manager = null;
            }
            $departments->save();

            if ($request->input('sys_department') == 'on') {
                DB::table('settings_system')
                        ->where('id', 1)
                        ->update(['department' => $id]);
            }
            if ($departments->fill($request->except('group_access', 'manager', 'sla'))->save()) {
                return redirect('departments')->with('success', Lang::get('lang.department_updated_sucessfully'));
            } else {
                return redirect('departments')->with('fails', Lang::get('lang.department_not_updated'));
            }
        } catch (Exception $e) {
            return redirect('departments')->with('fails', Lang::get('lang.department_not_updated'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param type int                     $id
     * @param type Department              $department
     * @param type Group_assign_department $group_assign_department
     *
     * @return type Response
     */
    public function destroy($id, Department $department, Group_assign_department $group_assign_department, System $system, Tickets $tickets) {
        // try {

        $system = $system->where('id', '=', '1')->first();
        if ($system->department == $id) {
            return redirect('departments')->with('fails', Lang::get('lang.you_cannot_delete_default_department'));
        } else {
            $tickets = DB::table('tickets')->where('dept_id', '=', $id)->update(['dept_id' => $system->department]);
            if ($tickets > 0) {
                if ($tickets > 1) {
                    $text_tickets = 'Tickets';
                } else {
                    $text_tickets = 'Ticket';
                }
                $ticket = '<li>' . $tickets . ' ' . $text_tickets . Lang::get('lang.have_been_moved_to_default_department') . '</li>';
            } else {
                $ticket = '';
            }
            $users = DB::table('users')->where('primary_dpt', '=', $id)->update(['primary_dpt' => $system->department]);
            if ($users > 0) {
                if ($users > 1) {
                    $text_user = 'Users';
                } else {
                    $text_user = 'User';
                }
                $user = '<li>' . $users . ' ' . $text_user . Lang::get('lang.have_been_moved_to_default_department') . '</li>';
            } else {
                $user = '';
            }
            $emails = DB::table('emails')->where('department', '=', $id)->update(['department' => $system->department]);
            if ($emails > 0) {
                if ($emails > 1) {
                    $text_emails = 'Emails';
                } else {
                    $text_emails = 'Email';
                }
                $email = '<li>' . $emails . ' System ' . $text_emails . Lang::get('lang.have_been_moved_to_default_department') . ' </li>';
            } else {
                $email = '';
            }
            $helptopic = DB::table('help_topic')->where('department', '=', $id)->update(['department' => null], ['status' => '1']);
            if ($helptopic > 0) {
                $helptopic = '<li>' . Lang::get('lang.the_associated_helptopic_has_been_deactivated') . '</li>';
            } else {
                $helptopic = '';
            }
            $message = $ticket . $user . $email . $helptopic;
            /* Becouse of foreign key we delete group_assign_department first */
            $group_assign_department = $group_assign_department->where('department_id', $id);
            $group_assign_department->delete();
            $departments = $department->whereId($id)->first();
            /* Check the function is Success or Fail */
            if ($departments->delete() == true) {
                return redirect('departments')->with('success', Lang::get('lang.department_deleted_sucessfully') . $message);
            } else {
                return redirect('departments')->with('fails', Lang::get('lang.department_can_not_delete'));
            }
        }
    }

    /**
     * @category function to show departments table
     * @param null
     * @return json table data
     */
    public function getDepartmentTable() {
        // <?php
        //     $default_department = App\Model\helpdesk\Settings\System::where('id', '=', '1')->first();
        //     $default_department = $default_department->department;
        //     
        $department = \DB::table('department')
                        ->leftJoin("users as u", 'department.manager', '=', 'u.id')
                        ->select('department.name as dname', 'department.type as type', 'u.user_name as user_name', 'u.first_name', 'u.last_name', 'u.id as u_id', 'department.id as d_id')->orderBy('dname');
        // dd($department);
        return Datatables::of($department)
                        ->removeColumn('first_name', 'last_name', 'd_id', 'u_id')
                        ->editColumn('dname', function ($department) {
                            $default_department = System::where('id', '=', '1')->first();
                            $default_department = $default_department->department;

                            if ($default_department == $department->d_id) {
                                // $default_department = System::where('id', '=', '1')->first();
                                // $default_department = $default_department->department;
                                $department_name = $department->dname . "(Default)";
                                $url = 'departments/' . $department->d_id . '/edit';
                                return '<a href="' . $url . '">' . $department_name . '</a>';
                            } else {


                                $url = 'departments/' . $department->d_id . '/edit';
                                return '<a href="' . $url . '">' . $department->dname . '</a>';
                            }
                        })
                        ->editColumn('type', function ($department) {
                            if ($department->type == 0) {
                                $return_value = '<span style="color:red">' . Lang::get("lang.private") . '</span>';
                                ;
                            } else {
                                $return_value = '<span style="color:green">' . Lang::get("lang.public") . '</span>';
                            }
                            return $return_value;
                        })
                        ->editColumn('user_name', function($department) {
                            if ($department->u_id != null) {
                                $name = $department->user_name;
                                if ($department->first_name != '' && $department->first_name != null) {
                                    $name = $department->first_name . ' ' . $department->last_name;
                                }
                                return '<a  href="' . route('user.show', $department->u_id) . '" title="' . $name . '">' . $name . '</a>';
                            }
                            return '';
                        })
                        // ->addColumn('action', function($department){
                        //     $default_department = System::where('id', '=', '1')->first();
                        //     $default_department = $default_department->department;
                        //     $disable = '';
                        //     if($default_department == $department->d_id) {
                        //          $disable = 'disabled';
                        //     }
                        //     return \Form::open(['route'=>['departments.destroy', $department->d_id],'method'=>'DELETE']).
                        //             ['type' => 'submit',
                        //             'onclick'=>'return confirm("Are you sure?")']
                        //     .\Form::close();
                        // })
                        ->addColumn('action', function($department) {
                            $default_department = System::where('id', '=', '1')->first();
                            $default_department = $default_department->department;
                            $disable = '';
                            if ($default_department == $department->d_id) {
                                $disable = 'disabled';
                            }
                            return \Form::open(['route' => ['departments.destroy', $department->d_id], 'method' => 'DELETE']) .
                                    '<a href="' . route('departments.edit', $department->d_id) . '" class="btn btn-primary btn-xs "><i class="fa fa-edit" style="color:white;"> &nbsp;</i> ' . Lang::get('lang.edit') . '</a>&nbsp;&nbsp;<a href="' . route('department.profile.show', $department->d_id) . '" class="btn btn-primary btn-xs "><i class="fa fa-eye" style="color:white;"> &nbsp;</i> ' . Lang::get('lang.view') . '</a>&nbsp;&nbsp;' . \Form::button('<i class="fa fa-trash" style="color:white;"> &nbsp;</i> ' . Lang::get('lang.delete'), ['type' => 'submit',
                                        'class' => 'btn btn-primary btn-xs  ' . $disable,
                                        'onclick' => 'return confirm("Are you sure?")'])
                                    . \Form::close();
                        })
                        ->make();
    }

    /**
     *
     *
     *
     */
    public function getDepartmentMembers($id) {
        return DepartmentAssignAgents::where('department_id', '=', $id)->select('agent_id')->get()->pluck('agent_id')->all();
    }

    public function DepartmentUserprofile($id) {
        //Eloquent Relations- join queries
        $department = Department::join('department_assign_agents', 'department_assign_agents.department_id', '=', 'department.id')
                        ->join('users', 'department_assign_agents.agent_id', '=', 'users.id')
                        ->select(['users.user_name', 'users.email', 'users.active', 'users.ban'])
                        ->where('department.id', $id)
                        ->distinct()->get(['users.id']);
//                           dd($department->get());

        return Datatables::of($department)
                        ->addColumn('user_name', function($department) {
                            return $department->user_name;
                        })
                        ->addColumn('email', function($department) {
                            return $department->email;
                        })
                        ->addColumn('active', function($department) {
                            if ($department->active == 1) {
                                return '<p class="btn btn-xs btn-success" style="pointer-events:none">' . Lang::get('lang.active') . '</p>';
                            } else {
                                return '<p class="btn btn-xs btn-danger" style="pointer-events:none">' . Lang::get('lang.inactive') . '</p>';
                            }
                        })
                        ->addColumn('ban', function($department) {
                            if ($department->ban == 1) {
                                return '<p class="btn btn-xs btn-success" style="pointer-events:none">' . Lang::get('lang.yes') . '</p>';
                            } else {
                                return '<p class="btn btn-xs btn-danger" style="pointer-events:none">' . Lang::get('lang.no') . '</p>';
                            }


                            return $department->ban;
                        })
                        ->make(true);
    }

}        
//        return Datatables::of($assigned_agent)
//                        ->addColumn('user_name', function($assigned) {
//                            return $assigned->agent->first()->user_name;
//                        })
//                        ->addColumn('email', function($assigned) {
//                            return $assigned->agent->first()->email;
//                        })
//                        ->addColumn('active', function($assigned) {
//                            if ($assigned->agent->first()->active == 1) {
//                               return '<p class="btn btn-xs btn-success" style="pointer-events:none">' . Lang::get('lang.active') . '</p>';
//                              } else {
//                                return '<p class="btn btn-xs btn-danger" style="pointer-events:none">' . Lang::get('lang.inactive') . '</p>';
//                            }
//                        })
//                        ->addColumn('ban', function($assigned) {
//                            if($assigned->agent->first()->ban == 0){
//                              return '<p class="btn btn-xs btn-success" style="pointer-events:none">' . Lang::get('lang.no') . '</p>';
//                            }else{
//                              return '<p class="btn btn-xs btn-danger" style="pointer-events:none">' . Lang::get('lang.yes') . '</p>'; 
//                            }
//                        })
//                        ->removeColumn('agent')
//                        ->make(true);
//    }

