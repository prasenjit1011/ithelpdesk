<?php

namespace App\Http\Controllers\Admin\helpdesk;

// controller
use App\Http\Controllers\Controller;
use App\Http\Controllers\Common\PhpMailController;
use App\Http\Controllers\Agent\helpdesk\TicketController;
use App\Http\Controllers\Agent\helpdesk\Notifications\NotificationController as Notify;
// request
use App\Http\Requests\helpdesk\AgentRequest;
use App\Http\Requests\helpdesk\AgentUpdate;
// model
use App\Model\helpdesk\Agent\Assign_team_agent;
use App\Model\helpdesk\Agent\Department;
use App\Model\helpdesk\Agent\DepartmentAssignAgents;
use App\Model\helpdesk\Agent\Groups;
use App\Model\helpdesk\Agent\Teams;
use App\Model\helpdesk\Utility\CountryCode;
use App\Model\helpdesk\Utility\Timezones;
use App\User;
// classes
use Illuminate\Http\Request;
use DB;
use Exception;
use GeoIP;
use Hash;
use Lang;
use Datatable;
use Datatables;

/**
 * AgentController
 * This controller is used to CRUD Agents.
 *
 * @author      Ladybird <info@ithelpdesk.com>
 */
class AgentController extends Controller {

    /**
     * Create a new controller instance.
     * constructor to check
     * 1. authentication
     * 2. user roles
     * 3. roles must be agent.
     *
     * @return void
     */
    public function __construct(PhpMailController $PhpMailController) {
        // creating an instance for the PhpmailController
        $this->PhpMailController = $PhpMailController;
        // checking authentication
        $this->middleware('auth');
        // checking admin roles
        $this->middleware('roles');
        $this->middleware('limit.reached', ['only' => ['store']]);
    }

    /**
     * Get all agent list page.
     *
     * @return type view
     */
    public function index() {
        try {
            $table = Datatable::table()
                    ->addColumn(
                            Lang::get('lang.name'), Lang::get('lang.user_name'), Lang::get('lang.email'), Lang::get('lang.role'), Lang::get('lang.status'), Lang::get('lang.last_login'), Lang::get('lang.action'))  // these are the column headings to be shown
                    ->noScript();
            $departments = \DB::table('department')->select('id', 'name')->get();
            return view('themes.default1.admin.helpdesk.agent.agents.index', compact('table', 'departments'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * creating a new agent.
     *
     * @param Assign_team_agent $team_assign_agent
     * @param Timezones         $timezone
     * @param Groups            $group
     * @param Department        $department
     * @param Teams             $team_all
     *
     * @return type view
     */
    public function create(Timezones $timezone, Department $department, Teams $team_all)
    {
        try {
            // gte all the teams
            $team = $team_all->where('status', '=', 1)->get();
            // get all the timezones
            $timezones = $timezone->get();
            // get all the groups
            $groups = "";
            // get all department
            $departments = $department->get();
            // list all the teams in a single variable
            $teams = $team->pluck('id', 'name')->toArray();
            // account activation option
            $aoption = getAccountActivationOptionValue();
            return view('themes.default1.admin.helpdesk.agent.agents.create', compact('assign', 'teams', 'agents', 'timezones', 'groups', 'departments', 'team', 'aoption'));
        } catch (Exception $e) {
            // returns if try fails with exception meaagse
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    public function find(Request $request) {
        try {
            $term = trim($request->q);
            if (empty($term)) {
                return \Response::json([]);
            }
            $depts = Department::where('name', 'LIKE', '%' . $term . '%')->select('id', 'name')->get();
            $formatted_tags = [];

            foreach ($depts as $dept) {
                $formatted_depts[] = ['id' => $dept->id, 'text' => $dept->name];
            }

            return \Response::json($formatted_depts);
        } catch (Exception $e) {
            // returns if try fails with exception meaagse
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * store a new agent.
     *
     * @param User              $user
     * @param AgentRequest      $request
     * @param Assign_team_agent $team_assign_agent
     *
     * @return type Response
     */
    public function store(User $user, AgentRequest $request)
    {
        try {
            $user_name = strtolower($request->get('user_name'));
            $permission = $request->input('permission');
            $request->merge([ 'user_name' => $user_name ]);
            if ($request->get('country_code') == '' && ($request->get('phone_number') != '' || $request->get('mobile') != '')) {
                return redirect()->back()->with(['fails2' => Lang::get('lang.country-code-required-error'), 'country_code' => 1])->withInput();
            } else {
                $code = CountryCode::select('phonecode')->where('phonecode', '=', $request->get('country_code'))->get();
                if (!count($code)) {
                    return redirect()->back()->with(['fails2' => Lang::get('lang.incorrect-country-code-error'), 'country_code' => 1])->withInput();
                }
            }
            // fixing the user role to agent
            $user->fill($request->except(['permission', 'primary_department', 'agent_time_zone', 'mobile']))->save();
            if (count($permission)>0) {
                $user->permision()->create(['permision'=>$permission]);
            }
            if ($request->get('mobile')) {
                $user->mobile = $request->get('mobile');
            } else {
                $user->mobile = null;
            }
            $active_code = '';
            if ($request->get('active') == 0) {
                $active_code = str_random(60);
                $user->email_verify = $active_code;
            }
            $user->mobile_otp_verify = "verifymobileifenable";
            $user->assign_group = $request->group;
            // $user->primary_dpt = $request->primary_department;
            $primary_dpt = $request->primary_department;

            foreach ($primary_dpt as $primary_dpts) {
                $dept_assign_agent=new DepartmentAssignAgents;
                $dept_assign_agent->agent_id=$user->id;
                $dept_assign_agent->department_id=$primary_dpts;
                 $dept_assign_agent->save();
            }

            $user->primary_dpt = $primary_dpt[0];
            $user->agent_tzone = $request->agent_time_zone;
            // generate password and has immediately to store
            $password = $this->generateRandomString();
            $user->password = Hash::make($password);
            // fetching all the team details checked for this user
            $requests = $request->input('team');
            if ($requests != null) {
                // get user id of the inserted user detail
                $id = $user->id;
                // insert team
                foreach ($requests as $req) {
                    // insert all the selected team id to the team and agent relationship table
                    DB::insert('insert into team_assign_agent (team_id, agent_id) values (?,?)', [$req, $id]);
                }
            }
            // save user credentails
            if ($user->save() == true) {
                // fetch user credentails to send mail
                $name = $user->first_name;
                $email = $user->email;

                try {
                    $notification[] = [
                        'registration_notification_alert' => [
                            'userid'   => $user->id,
                            'from'     => $this->PhpMailController->mailfrom('1', '0'),
                            'message'  => ['subject' => null, 'scenario' => 'registration-notification'],
                            'variable' => ['new_user_name' => $name, 'new_user_email' => $email, 'user_password' => $password]
                        ],
                        'new_user_alert' => [
                            'model'=>$user,
                            'userid'=>$user->id,
                            'from' => $this->PhpMailController->mailfrom('1', '0'),
                            'message' => ['subject' => null, 'scenario' => 'new-user'],
                            'variable' => ['new_user_name' => $name, 'new_user_email' => $email, 'user_profile_link' =>faveoUrl('user/' . $user->id)]
                        ],
                    ];
                    if ($active_code != '') {
                        $notification[] = [
                            'registration_alert' => [
                                'userid'=>$user->id,
                                'from'=>$this->PhpMailController->mailfrom('1', '0'),
                                'message'=>['subject' => null, 'scenario' => 'registration'],
                                'variable'=>['new_user_name' => $name, 'new_user_email' => $request->input('email'), 'account_activation_link' => faveoUrl('account/activate/' . $active_code)],
                            ]
                        ];
                    }
                    $notify = new Notify();
                    if (!$request->input('email')) {
                        $notify->setParameter('send_mail', false);
                    }
                    $notify->setDetails($notification);
                } catch (Exception $e) {
                    // returns if try fails
                    return redirect('agents')->with('warning', Lang::get('lang.agent_send_mail_error_on_agent_creation'));
                }
                // returns for the success case
                \Event::fire(new \App\Events\LoginEvent($request));
                return redirect('agents')->with('success', Lang::get('lang.agent_saved_success'));
            } else {
                // returns if fails
                return redirect('agents')->with('fails', Lang::get('lang.failed_to_create_agent'));
            }
        } catch (Exception $e) {
            redirect('agents')->with('fails', Lang::get('lang.failed_to_create_agent'));
        }
    }

    /**
     * Editing a selected agent.
     *
     * @param type int               $id
     * @param type User              $user
     * @param type Assign_team_agent $team_assign_agent
     * @param type Timezones         $timezone
     * @param type Groups            $group
     * @param type Department        $department
     * @param type Teams             $team
     *
     * @return type Response
     */
    public function edit($id, User $user, Assign_team_agent $team_assign_agent, Timezones $timezone, Groups $group, Department $department, Teams $team, CountryCode $code) {
        try {
            $user = $user->whereId($id)->first();
            $country_code = "auto";
            $hascode = $code->select('iso')->where("phonecode", "=", $user->country_code)->first();
            if ($hascode && $user->country_code != 0) {
                $country_code = $hascode->iso;
            }
            $aoption = getAccountActivationOptionValue();
            $team = $team->where('status', '=', 1)->get();
            $teams1 = $team->pluck('name', 'id');
            $timezones = $timezone->get();
            $groups = "";
            $departments = $department->get();
            $dept = DepartmentAssignAgents::where('agent_id', '=', $id)->pluck('department_id')->toArray();
            $table = $team_assign_agent->where('agent_id', $id)->first();
            $teams = $team->pluck('id', 'name')->toArray();
            $assign = $team_assign_agent->where('agent_id', $id)->pluck('team_id')->toArray();

            return view('themes.default1.admin.helpdesk.agent.agents.edit', compact('teams', 'assign', 'table', 'teams1', 'selectedTeams', 'user', 'timezones', 'groups', 'departments', 'dept', 'team', 'exp', 'counted', 'country_code', 'aoption'));
        } catch (Exception $e) {
            return redirect('agents')->with('fails', $e->getMessage());
        }
    }

    /**
     * Update the specified agent in storage.
     *
     * @param type int               $id
     * @param type User              $user
     * @param type AgentUpdate       $request
     * @param type Assign_team_agent $team_assign_agent
     *
     * @return type Response
     */
    public function update($id, User $user, AgentUpdate $request, Assign_team_agent $team_assign_agent)
    {
        $permission = $request->input('permission');
        $user_name = strtolower($request->get('user_name'));
        $request->merge([ 'user_name' => $user_name]);
        if ($request->get('country_code') == '' && ($request->get('phone_number') != '' || $request->get('mobile') != '')) {
            return redirect()->back()->with(['fails2' => Lang::get('lang.country-code-required-error'), 'country_code' => 1])->withInput();
        } else {
            $code = CountryCode::select('phonecode')->where('phonecode', '=', $request->get('country_code'))->get();
            if (!count($code)) {
                return redirect()->back()->with(['fails2' => Lang::get('lang.incorrect-country-code-error'), 'country_code' => 1])->withInput();
            }
        }
        // storing all the details
        $user = $user->whereId($id)->first();
        $old_mobile_number = $user->mobile;
        $daylight_save = $request->input('daylight_save');
        $limit_access = $request->input('limit_access');
        $directory_listing = $request->input('directory_listing');
        $vocation_mode = $request->input('vocation_mode');
        //==============================================
        $table = $team_assign_agent->where('agent_id', $id);
        $table->delete();
        $requests = $request->input('team');
        if ($requests != null) {
            // inserting team details
            foreach ($requests as $req) {
                DB::insert('insert into team_assign_agent (team_id, agent_id) values (?,?)', [$req, $id]);
            }
        }
        //Todo For success and failure conditions
        try {
            if ($request->input('country_code') != '' or $request->input('country_code') != null) {
                $user->country_code = $request->input('country_code');
            }
            $user->mobile = ($request->input('mobile') == '') ? null : $request->input('mobile');
            $user->fill($request->except('daylight_save', 'limit_access', 'directory_listing', 'vocation_mode', 'assign_team', 'mobile'));
            $user->assign_group = $request->group;
            // $user->primary_dpt = $request->primary_department;
            //delect privious dept
            $delete_dept = DepartmentAssignAgents::where('agent_id', '=', $id)->delete();


            $primary_dpt = $request->primary_department;

            foreach ($primary_dpt as $primary_dpts) {
                $dept_assign_agent = new DepartmentAssignAgents;
                $dept_assign_agent->agent_id = $user->id;
                $dept_assign_agent->department_id = $primary_dpts;
                $dept_assign_agent->save();
            }

            $user->primary_dpt = $primary_dpt[0];


            $user->agent_tzone = $request->agent_time_zone;
            if ($old_mobile_number != $request->input('mobile')) {
                $user->mobile_otp_verify = "verifymobileifenable";
            }
            $user->save();

            $user->permision()->updateOrCreate(['user_id'=>$user->id], ['permision'=>json_encode($permission)]);
            return redirect('agents')->with('success', Lang::get('lang.agent_updated_sucessfully'));
        } catch (Exception $e) {
            return redirect('agents')->with('fails', Lang::get('lang.unable_to_update_agent') . $e->getMessage());
        }
    }

    /**
     * Remove the specified agent from storage.
     *
     * @param type              $id
     * @param User              $user
     * @param Assign_team_agent $team_assign_agent
     *
     * @throws Exception
     *
     * @return type Response
     */
    public function destroy($id, User $user, Assign_team_agent $team_assign_agent)
    {
        /* Becouse of foreign key we delete team_assign_agent first */
        error_reporting(E_ALL & ~E_NOTICE);
        $team_assign_agent = $team_assign_agent->where('agent_id', $id);
        $team_assign_agent->delete();
        $user = $user->whereId($id)->first();
        try {
            $error = Lang::get('lang.this_staff_is_related_to_some_tickets');
            $user->id;
            $user->delete();
            throw new \Exception($error);

            return redirect('agents')->with('success', Lang::get('lang.agent_deleted_sucessfully'));
        } catch (\Exception $e) {
            return redirect('agents')->with('fails', $error);
        }
    }

    /**
     * Generate a random string for password.
     *
     * @param type $length
     *
     * @return string
     */
    public function generateRandomString($length = 10) {
        // list of supported characters
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        // character length checked
        $charactersLength = strlen($characters);
        // creating an empty variable for random string
        $randomString = '';
        // fetching random string
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        // return random string
        return $randomString;
    }

    /**
     *
     *
     *
     *
     */
    public function getAgentList(Request $request) {
        $type = $request->input('profiletype');
        $dept = $request->input('filterBy');
        $search = $request->input('searchTerm');

        $agents = \DB::table('users')
                        ->where('role', '<>', 'user')->select('first_name', 'user_name', 'email', 'role', 'active', 'users.updated_at as updated_at', 'last_name', 'users.id as id');
        if ($dept !== '') {
            $agents = $agents->leftJoin('department_assign_agents', 'users.id', '=', 'department_assign_agents.agent_id')->where('department_assign_agents.department_id', '=', $dept);
        }

        if ($type === 'agents') {
            $agents = $agents->where('role', '=', 'agent')->where('is_delete', '=', 0);
        } elseif ($type === 'admins') {
            $agents = $agents->where('role', '=', 'admin')->where('is_delete', '=', 0);
        } elseif ($type === 'active-users') {
            $agents = $agents->where('active', '=', 1);
        } elseif ($type === 'inactive') {
            $agents = $agents->where('active', '=', 0);
        } elseif ($type === 'deleted') {
            $agents = $agents->where('is_delete', '=', 1);
        } elseif ($type === 'banned') {
            $agents = $agents->where('ban', '=', 1);
        } else {
            $agents = $agents->where('is_delete', '=', 0);
        }

        if ($search !== '') {
            $agents = $agents->where(function($query) use ($search) {
                $query->where('user_name', 'LIKE', '%' . $search . '%');
                $query->orWhere('email', 'LIKE', '%' . $search . '%');
                $query->orWhere('first_name', 'LIKE', '%' . $search . '%');
                $query->orWhere('last_name', 'LIKE', '%' . $search . '%');
                $query->orWhere('mobile', 'LIKE', '%' . $search . '%');
                $query->orWhere('users.updated_at', 'LIKE', '%' . $search . '%');
                $query->orWhere('country_code', 'LIKE', '%' . $search . '%');
            });
        }

        return Datatables::of($agents)
                        ->removeColumn('last_name', 'id')
                        ->editColumn('first_name', function($model) {
                            $name = Lang::get('lang.not-available');
                            if ($model->first_name !== '' && $model->first_name !== null) {
                                $name = $model->first_name . ' ' . $model->last_name;
                                if (strlen($model->first_name . ' ' . $model->last_name) > 30) {
                                    $name = mb_substr($model->first_name . ' ' . $model->last_name, 0, 30, 'UTF-8') . '...';
                                }
                                return '<a  href="' . route('user.show', $model->id) . '" title="' . $model->first_name . ' ' . $model->last_name . '">' . $name . '</a>';
                            }
                            return $name;
                        })
                        ->editColumn('user_name', function($model) {
                            $user_name = $model->user_name;
                            if (strlen($model->user_name) > 30) {
                                $user_name = mb_substr($model->user_name, 0, 30, 'UTF-8') . '...';
                            }
                            return '<a  href="' . route('user.show', $model->id) . '" title="' . $model->user_name . '">' . $user_name . '</a>';
                        })
                        ->editColumn('email', function($model) {
                            return $model->email;
                        })
                        ->editColumn('role', function($model) {
                            if ($model->role === 'admin') {
                                return '<p class="btn btn-xs btn-primary" style="pointer-events:none">' . ucfirst($model->role) . '</p>';
                            }
                            return '<p class="btn btn-xs btn-warning" style="pointer-events:none">' . ucfirst($model->role) . '</p>';
                        })
                        ->editColumn('active', function($model) {
                            if ($model->active == 1 || $model->active == '1') {
                                return '<p class="btn btn-xs btn-success" style="pointer-events:none">' . Lang::get('lang.active') . '</p>';
                            }
                            return '<p class="btn btn-xs btn-danger" style="pointer-events:none">' . Lang::get('lang.inactive') . '</p>';
                        })
                        ->editColumn('updated_at', function($model) {
                            $t = $model->updated_at;
                            return faveoDate($t);
                        })
                        ->addColumn('action', function($model) {
                            return '<a href="' . route('user.show', $model->id) . '" class="btn btn-xs btn-primary"><i class="fa fa-eye" style="color:white;"> </i>&nbsp;&nbsp;' . Lang::get('lang.view') . '</a>&nbsp;&nbsp;<a href="' . route('agents.edit', $model->id) . '" class="btn btn-primary btn-xs"><i class="fa fa-edit" style="color:white;"> </i>&nbsp;&nbsp;' . Lang::get('lang.edit') . '</a>';
                        })
                        ->make();
    }

}
