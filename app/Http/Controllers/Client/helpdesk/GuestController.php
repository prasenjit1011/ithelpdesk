<?php

namespace App\Http\Controllers\Client\helpdesk;

// controllers
use App\Http\Controllers\Common\PhpMailController;
use App\Http\Controllers\Controller;
// requests
use App\Http\Requests\helpdesk\ProfilePassword;
use App\Http\Requests\helpdesk\ProfileRequest;
use App\Http\Requests\helpdesk\TicketRequest;
use App\Model\helpdesk\Manage\Help_topic;
// models
use App\Model\helpdesk\Settings\Company;
use App\Model\helpdesk\Settings\System;
use App\Model\helpdesk\Ticket\Ticket_Thread;
use App\Model\helpdesk\Ticket\Tickets;
use App\Model\helpdesk\Utility\CountryCode;
use App\Model\helpdesk\Settings\CommonSettings;
use App\Http\Requests\helpdesk\OtpVerifyRequest;
use App\Model\helpdesk\Utility\Otp;
use App\User;
use Auth;
// classes
use Exception;
use Hash;
use Illuminate\Http\Request;
use Input;
use Lang;
use DateTime;
use DB;
use Socialite;

/**
 * GuestController.
 *
 * @author      Ladybird <info@ithelpdesk.com>
 */
class GuestController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return type void
     */
    public function __construct(PhpMailController $PhpMailController) {
        $this->middleware('board');
        $this->PhpMailController = $PhpMailController;
        // checking authentication
        $this->middleware('auth');
    }

    /**
     * Get profile.
     *
     * @return type Response
     */
    public function getProfile(CountryCode $code)
    {
        $user = Auth::user();
        $country_code = "auto";
        $code = CountryCode::select('iso')->where("phonecode", "=", $user->country_code)->first();
        if ($code && $user->country_code != 0) {
            $country_code = $code->iso;
        }
        $aoption = getAccountActivationOptionValue();
        return view('themes.default1.client.helpdesk.profile', compact('user', 'aoption', 'country_code'));
    }

    /**
     * Save profile data.
     *
     * @param type                $id
     * @param type ProfileRequest $request
     *
     * @return type Response
     */
    public function postProfile(ProfileRequest $request) {
        try {
            // geet authenticated user details
            $user = Auth::user();
            if ($request->get('country_code') == '' && ($request->get('phone_number') != '' || $request->get('mobile') != '')) {
                return redirect()->back()->with(['fails' => Lang::get('lang.country-code-required-error'), 'country_code_error' => 1])->withInput();
            } else {
                $code = CountryCode::select('phonecode')->where('phonecode', '=', $request->get('country_code'))->get();
                if (!count($code)) {
                    return redirect()->back()->with(['fails' => Lang::get('lang.incorrect-country-code-error'), 'country_code_error' => 1])->withInput();
                }
                $user->country_code = $request->country_code;
            }
            $user->fill($request->except('profile_pic', 'mobile'));
            $user->gender = $request->input('gender');
            $user->save();
            if (Input::file('profile_pic')) {
                // fetching picture name
                $name = Input::file('profile_pic')->getClientOriginalName();
                // fetching upload destination path
                $destinationPath = 'uploads/profilepic';
                // adding a random value to profile picture filename
                $fileName = rand(0000, 9999) . '.' . str_replace(" ", "_", $name);
                // moving the picture to a destination folder
                Input::file('profile_pic')->move($destinationPath, $fileName);
                // saving filename to database
                $user->profile_pic = $fileName;
            }
            if ($request->get('mobile')) {
                $user->mobile = $request->get('mobile');
            } else {
                $user->mobile = null;
            }
            if ($user->save()) {
                return redirect()->back()->with('success', Lang::get('lang.Profile-Updated-sucessfully'));
            } else {
                return redirect()->back()->route('profile')->with('fails', Lang::get('lang.Profile-Updated-sucessfully'));
            }
        } catch (Exception $e) {
            return redirect()->back()->route('profile')->with('fails', $e->getMessage());
        }
    }

    /**
     * @category fucntion to check if mobile number is unqique or not
     * @param string $mobile
     * @return boolean true(if mobile exists in users table)/false (if mobile does not exist in user table)
     */
    public function checkMobile($mobile) {
        if ($mobile) {
            $check = User::where('mobile', '=', $mobile)
                    ->where('id', '<>', \Auth::user()->id)
                    ->first();
            if (count($check) > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get Ticket page.
     *
     * @param type Help_topic $topic
     *
     * @return type Response
     */
    public function getTicket(Help_topic $topic) {
        $topics = $topic->get();

        return view('themes.default1.client.helpdesk.tickets.form', compact('topics'));
    }

    /**
     * getform.
     *
     * @param type Help_topic $topic
     *
     * @return type
     */
    public function getForm(Help_topic $topic) {
        if (\Config::get('database.install') == '%0%') {
            return \Redirect::route('licence');
        }
        if (System::first()->status == 1) {
            $topics = $topic->get();

            return view('themes.default1.client.helpdesk.form', compact('topics'));
        } else {
            return \Redirect::route('home');
        }
    }

    /**
     * Get my ticket.
     *
     * @param type Tickets       $tickets
     * @param type Ticket_Thread $thread
     * @param type User          $user
     *
     * @return type Response
     */
    public function getMyticket() {
        return view('themes.default1.client.helpdesk.mytickets');
    }

    /**
     * Get ticket-thread.
     *
     * @param type Ticket_Thread $thread
     * @param type Tickets       $tickets
     * @param type User          $user
     *
     * @return type Response
     */
    public function thread(Ticket_Thread $thread, Tickets $tickets, User $user) {
        $user_id = Auth::user()->id;
        //dd($user_id);
        /* get the ticket's id == ticket_id of thread  */
        $tickets = $tickets->where('user_id', '=', $user_id)->first();
        //dd($ticket);
        $thread = $thread->where('ticket_id', $tickets->id)->first();
        //dd($thread);
        // $tickets = $tickets->whereId($id)->first();
        return view('themes.default1.client.guest-user.view_ticket', compact('thread', 'tickets'));
    }

    /**
     * ticket Edit.
     *
     * @return
     */
    public function ticketEdit() {
        
    }

    /**
     * Post porfile password.
     *
     * @param type                 $id
     * @param type ProfilePassword $request
     *
     * @return type Response
     */
    public function postProfilePassword(ProfilePassword $request) {
        $user = Auth::user();
        //echo $user->password;
        if (Hash::check($request->input('old_password'), $user->getAuthPassword())) {
            $user->password = Hash::make($request->input('new_password'));
            try {
                $user->save();

                return redirect()->back()->with('success2', Lang::get('lang.password_updated_sucessfully'));
            } catch (Exception $e) {
                return redirect()->back()->with('fails2', $e->getMessage());
            }
        } else {
            return redirect()->back()->with('fails2', Lang::get('lang.password_was_not_updated_incorrect_old_password'));
        }
    }

    /**
     * Ticekt reply.
     *
     * @param type Ticket_Thread $thread
     * @param type TicketRequest $request
     *
     * @return type Response
     */
    public function reply(Ticket_Thread $thread, TicketRequest $request) {
        $thread->ticket_id = $request->input('ticket_ID');
        $thread->title = $request->input('To');
        $thread->user_id = Auth::user()->id;
        $thread->body = $request->input('reply_content');
        $thread->poster = 'user';
        $thread->save();
        $ticket_id = $request->input('ticket_ID');
        $tickets = Tickets::where('id', '=', $ticket_id)->first();
        $thread = Ticket_Thread::where('ticket_id', '=', $ticket_id)->first();

        return Redirect('thread/' . $ticket_id);
    }

    /**
     * Get Checked ticket.
     *
     * @param type Tickets $ticket
     * @param type User    $user
     *
     * @return type response
     */
    public function getCheckTicket(Tickets $ticket, User $user) {
        return view('themes.default1.client.helpdesk.guest-user.newticket', compact('ticket'));
    }

    /**
     * Post Check ticket.
     *
     * @param type CheckTicket   $request
     * @param type User          $user
     * @param type Tickets       $ticket
     * @param type Ticket_Thread $thread
     *
     * @return type Response
     */
    public function PostCheckTicket(Request $request) {
        dd($request->all());
        $validator = \Validator::make($request->all(), [
                    'email' => 'required|email',
                    'ticket_number' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput()
                            ->with('check', '1');
        }
        $Email = $request->input('email');
        $Ticket_number = $request->input('ticket_number');
        $ticket = Tickets::where('ticket_number', '=', $Ticket_number)->first();
        if ($ticket == null) {
            return \Redirect::route('form')->with('fails', Lang::get('lang.there_is_no_such_ticket_number'));
        } else {
            $userId = $ticket->user_id;
            $user = User::where('id', '=', $userId)->first();
            if ($user->role == 'user') {
                $username = $user->first_name;
            } else {
                $username = $user->first_name . ' ' . $user->last_name;
            }
            if ($user->email != $Email) {
                return \Redirect::route('form')->with('fails', Lang::get("lang.email_didn't_match_with_ticket_number"));
            } else {
                $code = $ticket->id;
                $code = \Crypt::encrypt($code);

                $company = $this->company();

                $this->PhpMailController->sendmail(
                        $from = $this->PhpMailController->mailfrom('1', '0'), $to = ['name' => $username, 'email' => $user->email], $message = ['subject' => 'Ticket link Request [' . $Ticket_number . ']', 'scenario' => 'check-ticket'],
                        $template_variables = [
                            'user' => $username,
                            'ticket_link' =>\URL::route('check_ticket', $code),
                            'ticket_subject' => title($ticket->id),
                            'ticket_number' => $ticket->ticket_number,
                            'ticket_due_date' => $ticket->duedate->tz(timezone()),
                            'ticket_created_at' => $ticket->created_at->tz(timezone()),
                            ]
                );

                return \Redirect::back()
                                ->with('success', Lang::get('lang.we_have_sent_you_a_link_by_email_please_click_on_that_link_to_view_ticket'));
            }
        }
    }

    /**
     * get ticket email.
     *
     * @param type $id
     *
     * @return type
     */
    public function get_ticket_email($id, CommonSettings $common_settings) {
        $common_setting = $common_settings->select('status')
                ->where('option_name', '=', 'user_set_ticket_status')
                ->first();
        return view('themes.default1.client.helpdesk.ckeckticket', compact('id', 'common_setting'));
    }

    /**
     * get ticket status.
     *
     * @param type Tickets $ticket
     *
     * @return type
     */
    public function getTicketStat(Tickets $ticket) {
        return view('themes.default1.client.helpdesk.ckeckticket', compact('ticket'));
    }

    /**
     * get company.
     *
     * @return type
     */
    public function company() {
        $company = Company::Where('id', '=', '1')->first();
        if ($company->company_name == null) {
            $company = 'Support Center';
        } else {
            $company = $company->company_name;
        }

        return $company;
    }

    public function resendOTP(OtpVerifyRequest $request) {
        dd('asd');
        if (\Schema::hasTable('sms')) {
            $sms = DB::table('sms')->get();
            if (count($sms) > 0) {
                \Event::fire(new \App\Events\LoginEvent($request));
                return 1;
            }
        } else {
            return "Plugin has not been setup successfully.";
        }
    }

    public function verifyOTP()
    {
        $user = User::select('id', 'mobile', 'country_code', 'user_name', 'mobile_otp_verify', 'updated_at')->where('id', '=', Input::get('u_id'))->first();
        $otp = Input::get('otp');
        if ($otp != null || $otp != '') {
            $otp_length = strlen(Input::get('otp'));
            if (($otp_length == 6 && !preg_match("/[a-z]/i", Input::get('otp')))) {
                $otp2 = Hash::make(Input::get('otp'));
                $date1 = date_format($user->updated_at, "Y-m-d h:i:sa");
                $date2 = date("Y-m-d h:i:sa");
                $time1 = new DateTime($date2);
                $time2 = new DateTime($date1);
                $interval = $time1->diff($time2);
                if ($interval->i > 10 || $interval->h > 0) {
                    $message = Lang::get('lang.otp-expired');
                    return $message;
                } else {
                    if (Hash::check(Input::get('otp'), $user->mobile_otp_verify)) {
                        User::where('id', '=', $user->id)
                            ->update([
                                'mobile_otp_verify' => '1',
                                'mobile' => Input::get('mobile'),
                                'country_code' => str_replace('+', '', Input::get('country_code'))
                            ]);
                        return 1;
                    } else {
                        $message = Lang::get('lang.otp-not-matched');
                        return $message;
                    }
                }
            } else {
                $message = Lang::get('lang.otp-invalid');
                return $message;
            }
        } else {
            $message = Lang::get('lang.otp-not-matched');
            return $message;
        }
    }

    public function sync() {
        try {
            $provider = $this->getProvider();
            $this->changeRedirect();
            $users = Socialite::driver($provider)->user();
            $this->forgetSession();
            $user['provider'] = $provider;
            $user['social_id'] = $users->id;
            $user['name'] = $users->name;
            $user['email'] = $users->email;
            $user['username'] = $users->nickname;
            $user['avatar'] = $users->avatar;
            return redirect('client-profile')->with('success', 'Additional informations fetched');
        } catch (Exception $ex) {
            dd($ex);
            return redirect('client-profile')->with('fails', $ex->getMessage());
        }
    }

    public function getProvider() {
        $provider = \Session::get('provider');
        return $provider;
    }

    public function changeRedirect() {
        $provider = \Session::get('provider');
        $url = \Session::get($provider . 'redirect');
        \Config::set("services.$provider.redirect", $url);
    }

    public function forgetSession() {
        $provider = $this->getProvider();
        \Session::forget('provider');
        \Session::forget($provider . 'redirect');
    }

    public function checkArray($key, $array) {
        $value = "";
        if (array_key_exists($key, $array)) {
            $value = $array[$key];
        }
        return $value;
    }

    public function updateUser($user = []) {
        $userid = \Auth::user()->id;
        $useremail = \Auth::user()->email;
        $email = $this->checkArray('email', $user); //$user['email'];
        if ($email !== "" && $email !== $useremail) {
            throw new Exception("Sorry! your current email and " . ucfirst($user['provider']) . " email is different so system can not sync");
        }
        $this->update($userid, $user);
    }

    public function update($userid, $user, $provider) {
        $email = $this->checkArray('email', $user);
        $this->deleteUser($userid, $user, $provider);
        $this->insertAdditional($userid, $provider, $user);
        $this->changeEmail($email);
    }

    public function deleteUser($userid, $user, $provider) {
        $info = new \App\UserAdditionalInfo();
        $infos = $info->where('owner', $userid)->where('service', $provider)->get();
        if ($infos->count() > 0 && count($user) > 0) {
            foreach ($infos as $key => $detail) {
                //if ($user[$key] !== $detail->$key) {
                $detail->delete();
                //}
            }
        }
    }

    public function insertAdditional($id, $provider, $user = []) {
        $info = new \App\UserAdditionalInfo();
        if (count($user) > 0) {
            foreach ($user as $key => $value) {

                $info->create([
                    'owner' => $id,
                    'service' => $provider,
                    'key' => $key,
                    'value' => $value,
                ]);
            }
        }
    }

    public function changeEmail($email) {
        $user = \Auth::user();
        if ($user && $email && !$user->email) {
            $user->email = $email;
            $user->save();
        }
    }

// Eng. Kashif
    public function clientOrgManager($user_id, Organization $org) {


        try {

            $id = User_org::where('user_id', '=', $user_id)->where('role', '=', 'manager')->pluck('org_id')->toArray();
            $orgs = $org->whereId($id)->first();
            $org_heads_check = User_org::where('org_id', '=', $id)->where('role', '=', 'manager')->select('user_id')->get();


            if (!$org_heads_check->isEmpty()) {

                $org_heads_ids = User_org::where('org_id', '=', $id)->where('role', '=', 'manager')->pluck('user_id')->toArray();
                foreach ($org_heads_ids as $org_heads_id) {
                    $org_heads_emails[] = user::where('id', '=', $org_heads_id)->pluck('email')->first();
                }
            } else {
                $org_heads_emails = 0;
            }


            $org_heads = User_org::where('org_id', '=', $id)->where('role', '=', 'manager')->select('user_id')->get();



            if (!$org_heads->isEmpty()) {

                foreach ($org_heads as $org_head) {

                    $userss[] = User::where('id', '=', $org_head->user_id)->select('id', 'email', 'user_name', 'phone_number')->first();
                }
            } else {
                $userss = 0;
            }

            // dd($orgs);

            /* To view page */
            return view('themes.default1.client.helpdesk.organization.show', compact('orgs', 'userss', 'org_heads_emails'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    public function managerLicensesDocs($org_id) {
        try {
// dd($org_id);
            // $licenses_docs = new OrgAttachment();
            // $license_docs_list = $licenses_docs->select('id','org_id','file_name','created_at')->get();
            $license_docs_list = OrgAttachment::where('org_id', '=', $org_id)->select('id', 'org_id', 'file_name', 'created_at');


            return \Datatable::query($license_docs_list)
                            ->showColumns('file_name', 'created_at')
                            ->addColumn('action', function($model) {

                                return "<a href=" . url('Client/download/' . $model->file_name) . " class='btn btn-info btn-xs'>download</a>";
                            })
                            ->searchColumns('file_name')
                            ->orderColumns('file_name')
                            ->make();
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

// public function clientDownloadDocs($file_name) {
//     // dd($file_name);
//     $file_path = public_path('uploads/organizations/'.$file_name);
//     // dd($file_path);
//     return response()->download($file_path);
//   }

    public function orgEditUser($id, CountryCode $code) {
        try {


            // dd('here');
            $settings = CommonSettings::select('status')->where('option_name', '=', 'send_otp')->first();
            $email_mandatory = CommonSettings::select('status')->where('option_name', '=', 'email_mandatory')->first();

            $user = new User();
            /* select the field where id = $id(request Id) */
            $users = $user->whereId($id)->first();
            $location = GeoIP::getLocation();
            $phonecode = $code->where('iso', '=', $location->iso_code)->first();
            $orgs = Organization::all();
            // dd($org);
            $organization_id = User_org::where('user_id', '=', $id)->pluck('org_id')->first();

            // $org_name=Organization::where('id','=',$org_id)->lists('name')->first();
            // dd($users);

            return view('themes.default1.client.helpdesk.organization.orgedituser', compact('users', 'orgs', '$settings', '$email_mandatory', 'organization_id'))->with('phonecode', $phonecode->phonecode);
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    public function updateOrgUser(Request $request) {

        // dd('kkk');
        $id = $request->user_id;
        $user = new User();
        /* select the field where id = $id(request Id) */
        $users = $user->whereId($id)->first();
        /* Update the value by selected field  */
        /* Check whether function success or not */
        try {
            $id = $request->user_id;
            if ($request->get('country_code') == '' && ($request->get('phone_number') != '' || $request->get('mobile') != '')) {
                return redirect()->back()->with(['fails' => Lang::get('lang.country-code-required-error'), 'country_code_error' => 1])->withInput();
            } else {
                $code = CountryCode::select('phonecode')->where('phonecode', '=', $request->get('country_code'))->get();
                if (!count($code)) {
                    return redirect()->back()->with(['fails' => Lang::get('lang.incorrect-country-code-error'), 'country_code_error' => 1])->withInput();
                } else {
                    $users->country_code = $request->country_code;
                }
            }
            $users->mobile = ($request->input('mobile') == '') ? null : $request->input('mobile');
            $users->fill($request->except('mobile'));
            $users->save();

            if ($request->input('org_id') == "") {
                $delete_user_from_org = User_org::where('user_id', '=', $id)->delete();
            } else {

                $add_user_from_org = User_org::where('user_id', $id)->first();
                if ($add_user_from_org) {
                    $add_user_from_org->user_id = $id;
                    $add_user_from_org->org_id = $request->org_id;
                    $add_user_from_org->save();
                } else {
                    $add_user_from_org = new User_org();
                    $add_user_from_org->user_id = $id;
                    $add_user_from_org->org_id = $request->org_id;
                    $add_user_from_org->save();
                }
            }
            // if ($request->input('org_id') != "") {
            //     $orgid = $request->input('org_id');
            //     $this->storeUserOrgRelation($id, $orgid);                    
            // }
            /* redirect to Index page with Success Message */
            return redirect()->route('organizations.client', Auth::user()->id)->with('success', Lang::get('lang.User-profile-Updated-Successfully'));
            // return redirect()->back()->with('success', Lang::get('lang.User-profile-Updated-Successfully'));
        } catch (Exception $e) {
            /* redirect to Index page with Fails Message */
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    public function orgDeleteUser($id) {
        // dd($id);
        // $delete_all = Input::get('delete_all');
        $users = User::where('id', '=', $id)->first();
        if ($users->role == 'user') {

            $users = User::where('id', '=', $id)->first();
            $users->is_delete = 1;
            $users->active = 0;
            $users->ban = 1;
            $users->save();

            return redirect()->back()->with('success', Lang::get('lang.user_delete_successfully'));
        }



        if ($users->role == 'agent') {
            // if ($delete_all == null) {
            $UserEmail = Input::get('assign_to');
            $assign_to = explode('_', $UserEmail);
            $ticket = Tickets::where('assigned_to', '=', $id)->get();
            if ($assign_to[0] == 'user') {
                if ($users->id == $assign_to[1]) {
                    return redirect()->back()->with('warning', Lang::get('lang.select_another_agent'));
                }

                $tickets = Tickets::where('assigned_to', '=', $id)->get();


                foreach ($tickets as $ticket) {
                    # code...

                    $ticket->assigned_to = $assign_to[1];
                    $user_detail = User::where('id', '=', $assign_to[1])->first();
                    $assignee = $user_detail->first_name . ' ' . $user_detail->last_name;
                    $ticket_number = $ticket->ticket_number;
                    $ticket->save();


                    $thread = new Ticket_Thread();
                    $thread->ticket_id = $ticket->id;
                    $thread->user_id = Auth::user()->id;
                    $thread->is_internal = 1;
                    $thread->body = 'This Ticket has been assigned to ' . $assignee;
                    $thread->save();
                }
                $user = User::find($id);
                $users->is_delete = 1;
                $users->active = 0;
                $users->ban = 1;
                $users->save();


                return redirect()->back()->with('success', Lang::get('lang.agent_delete_successfully_and_ticket_assign_to_another_agent'));
            }



            $user = User::find($id);
            $users->is_delete = 1;
            $users->active = 0;
            $users->ban = 1;
            $users->save();

            return redirect('user')->with('success', Lang::get('lang.agent_delete_successfully'));
            // } elseif ($delete_all == 1) {
            // if ($delete_all) {
            // dd('here');
            $tickets = Tickets::where('assigned_to', '=', $id)->get();
            // dd($tickets);
            foreach ($tickets as $ticket) {
                $ticket->assigned_to = NULL;
                $ticket_number = $ticket->ticket_number;
                $ticket->save();


                $thread = new Ticket_Thread();
                $thread->ticket_id = $ticket->id;
                $thread->user_id = Auth::user()->id;
                $thread->is_internal = 1;
                $thread->body = 'This Ticket has been unassigned ';
                $thread->save();
            }
            // $users = User::where('id', '=', $id)->get();
            $user = User::find($id);
            $users->is_delete = 1;
            $users->active = 0;
            $users->save();

            return redirect()->back()->with('success', Lang::get('lang.agent_delete_successfully'));
        } else {
            // Assign_team_agent::where('agent_id', '=', $id)->delete();
            // User_org::where('user_id', '=', $id)->delete();
            $user = User::find($id);
            $users->is_delete = 1;
            $users->active = 0;
            $users->save();

            return redirect()->back()->with('success', Lang::get('lang.agent_delete_successfully'));
        }
        // } else {
        // }
    }

    // } catch (Exception $e) {
    /* redirect to Index page with Fails Message */
    // return redirect('user')->with('fails', $e->getMessage());
    // }
    // }

    public function managerCreateUser(CountryCode $code) {
        try {
            $settings = CommonSettings::select('status')->where('option_name', '=', 'send_otp')->first();
            $email_mandatory = CommonSettings::select('status')->where('option_name', '=', 'email_mandatory')->first();
            $location = GeoIP::getLocation();
            $phonecode = $code->where('iso', '=', $location->iso_code)->first();
            // $org = Organization::lists('name', 'id')->toArray();
            return view('themes.default1.client.helpdesk.organization.create', compact('settings', 'email_mandatory'))->with('phonecode', $phonecode->phonecode);
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->errorInfo[2]);
        }
    }

    /**
     * Generate a random string for password.
     *
     * @param type $length
     *
     * @return string
     */
    public function generateRandomString($length = 10) {
        // list of supported characters
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        // character length checked
        $charactersLength = strlen($characters);
        // creating an empty variable for random string
        $randomString = '';
        // fetching random string
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        // return random string
        return $randomString;
    }

    public function storeUserOrgRelation($userid, $orgid) {
        $org_relations = new User_org();
        $org_relation = $org_relations->where('user_id', $userid)->first();
        if ($org_relation) {
            $org_relation->delete();
        }
        $org_relations->create([
            'user_id' => $userid,
            'org_id' => $orgid,
            'role' => 'member',
        ]);
    }

    public function storeUser(User $user, Sys_userRequest $request) {
        // dd('uuu');
        /* insert the input request to sys_user table */
        /* Check whether function success or not */
        if ($request->input('email') != '') {
            $user->email = $request->input('email');
        } else {
            $user->email = null;
        }
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->user_name = $request->input('user_name');
        if ($request->input('mobile') != '') {
            $user->mobile = $request->input('mobile');
        } else {
            $user->mobile = null;
        }
        $user->ext = $request->input('ext');
        $user->phone_number = $request->input('phone_number');
        $user->country_code = $request->input('country_code');
        $user->active = $request->input('active');
        $user->internal_note = $request->input('internal_note');
        $password = $this->generateRandomString();
        $user->password = Hash::make($password);
        $user->role = 'user';
        try {
            if ($request->get('country_code') == '' && ($request->get('phone_number') != '' || $request->get('mobile') != '')) {
                return redirect()->back()->with(['fails' => Lang::get('lang.country-code-required-error'), 'country_code_error' => 1])->withInput();
            } else {
                $code = CountryCode::select('phonecode')->where('phonecode', '=', $request->get('country_code'))->get();
                if (!count($code)) {
                    return redirect()->back()->with(['fails' => Lang::get('lang.incorrect-country-code-error'), 'country_code_error' => 1])->withInput();
                }
            }
            // save user credentails
            if ($user->save() == true) {
                if ($request->input('org_id') != "") {
                    $orgid = $request->input('org_id');
                    $this->storeUserOrgRelation($user->id, $orgid);
                }
                // fetch user credentails to send mail
                $name = $user->first_name;
                $email = $user->email;
                if ($request->input('send_email')) {
                    try {
                        // send mail on registration
                        $this->PhpMailController->sendmail($from = $this->PhpMailController->mailfrom('1', '0'), $to = ['name' => $name, 'email' => $email], $message = ['subject' => null, 'scenario' => 'registration-notification'], $template_variables = ['user' => $name, 'email_address' => $email, 'user_password' => $password]);
                    } catch (Exception $e) {
                        // returns if try fails
                        return redirect('user')->with('warning', Lang::get('lang.user_send_mail_error_on_user_creation'));
                    }
                }
                // returns for the success case
                // returns for the success case
                $email_mandatory = CommonSettings::select('status')->where('option_name', '=', 'email_mandatory')->first();
                if (($request->input('active') == '0' || $request->input('active') == 0) || ($email_mandatory->status == '0') || $email_mandatory->status == 0) {
                    \Event::fire(new \App\Events\LoginEvent($request));
                }
                // return view('organizations.client')->with('success', Lang::get('lang.User-Created-Successfully'));
                return redirect()->route('organizations.client', Auth::user()->id)->with('success', Lang::get('lang.User-Created-Successfully'));
                // return redirect()->back()->with('success', Lang::get('lang.User-Created-Successfully'));
            }
//            $user->save();
            /* redirect to Index page with Success Message */
            // return redirect()->back()->with('success', Lang::get('lang.User-Created-Successfully'));
            // return redirect()->route('organizations.client',Auth::user()->id)->with('success', Lang::get('lang.User-Created-Successfully'));
        } catch (Exception $e) {
            /* redirect to Index page with Fails Message */
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    public function managerViewUser($id) {



        try {
            $users = User::where('id', '=', $id)->first();
            if (count($users) > 0) {
                return view('themes.default1.client.helpdesk.organization.usershow', compact('users'));
            } else {
                return redirect()->back()->with('fails', Lang::get('lang.user-not-found'));
            }
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Random Password Genetor for users
     *
     * @param type int  $id
     * @param type User $user
     *
     * @return type view
     */
    public function randomPassword() {
        try {
            $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~!@#$%^&*(){}[]';
            $pass = array(); //remember to declare $pass as an array
            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
            for ($i = 0; $i < 10; $i++) {
                $n = rand(0, $alphaLength);
                $pass[] = $alphabet[$n];
            }

            return implode($pass);
        } catch (Exception $e) {
            /* redirect to Index page with Fails Message */
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Random Password Genetor for users
     *
     * @param type int  $id
     * @param type User $user
     *
     * @return type view
     */
    public function randomPostPassword($id, ChangepasswordRequest $request) {
        try {
            $changepassword = $request->change_password;
            $user = User::whereId($id)->first();
            $password = $request->change_password;
            $user->password = Hash::make($password);
            $user->save();
            $name = $user->first_name;
            $email = $user->email;

            $this->PhpMailController->sendmail($from = $this->PhpMailController
                    ->mailfrom('1', '0'), $to = ['name' => $name, 'email' => $email], $message = ['subject' => null, 'scenario' => 'reset_new_password'], $template_variables = ['user' => $name, 'user_password' => $password]);

            return redirect()->back()->with('success11', Lang::get('lang.password_change_successfully'));
        } catch (Exception $e) {

            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    public function restoreUser($id) {
        // dd($id);
        // $delete_all = Input::get('delete_all');
        $users = User::where('id', '=', $id)->first();
        $users->is_delete = 0;
        $users->active = 1;
        $users->ban = 0;
        $users->save();
        return redirect()->back()->with('success11', Lang::get('lang.user_restore_successfully'));
    }

    public function system_mail() {
        $email = Email::where('id', '=', '1')->first();

        return $email->sys_email;
    }

    /**
     * function to Ticket Close.
     *
     * @param type         $id
     * @param type Tickets $ticket
     *
     * @return type string
     */
    public function close($id, Tickets $ticket) {
        $ticket = Tickets::where('id', '=', $id)->first();

        $approval = Approval::where('id', '=', 1)->first();
        //Admin can close direce
        // if (Auth::user()->role == 'admin' || Auth::user()->role == 'user') {

        if (Auth::user()->role == 'user') {
            // dd($id);  
            // $ticket_status = $ticket->where('id', '=', $id)->where('user_id', '=', Auth::user()->id)->first();
            $ticket_status = $ticket->where('id', '=', $id)->first();
        } else {
            $ticket_status = $ticket->where('id', '=', $id)->first();
        }
        // dd($ticket_status);
        // checking for unautherised access attempt on other than owner ticket id
        if ($ticket_status == null) {
            return redirect()->route('unauth');
        }
        $ticket_status->status = 3;
        $ticket_status->closed = 1;
        $ticket_status->closed_at = date('Y-m-d H:i:s');
        $ticket_status->save();

        // dd('pooppo');
        $ticket_thread = Ticket_Thread::where('ticket_id', '=', $ticket_status->id)->first();
        $ticket_subject = $ticket_thread->title;
        $ticket_status_message = Ticket_Status::where('id', '=', $ticket_status->status)->first();
        $thread = new Ticket_Thread();
        $thread->ticket_id = $ticket_status->id;
        $thread->user_id = Auth::user()->id;
        $thread->is_internal = 1;
        $thread->body = $ticket_status_message->message . ' ' . Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $thread->save();

        $user_id = $ticket_status->user_id;
        $user = User::where('id', '=', $user_id)->first();
        $email = $user->email;
        $user_name = $user->user_name;
        $ticket_number = $ticket_status->ticket_number;

        $system_from = $this->company();
        $sending_emails = Emails::where('department', '=', $ticket_status->dept_id)->first();
        if ($sending_emails == null) {
            $from_email = $this->system_mail();
        } else {
            $from_email = $sending_emails->id;
        }
        try {
            $this->PhpMailController->sendmail($from = $this->PhpMailController->mailfrom('0', $ticket_status->dept_id), $to = ['name' => $user_name, 'email' => $email], $message = ['subject' => $ticket_subject . '[#' . $ticket_number . ']', 'scenario' => 'close-ticket'], $template_variables = ['ticket_number' => $ticket_number]);
        } catch (\Exception $e) {
            return 0;
        }
        $data = [
            'id' => $ticket_status->ticket_number,
            'status' => 'Closed',
            'first_name' => Auth::user()->first_name,
            'last_name' => Auth::user()->last_name,
        ];
        \Event::fire('change-status', [$data]);

        return 'your ticket' . $ticket_status->ticket_number . ' has been closed';
        // }




        if ($approval->status == 0) {
            $ticket_status = $ticket->where('id', '=', $id)->first();
            // checking for unautherised access attempt on other than owner ticket id
            if ($ticket_status == null) {
                return redirect()->route('unauth');
            }
            $ticket_status->status = 3;
            $ticket_status->closed = 1;
            $ticket_status->closed_at = date('Y-m-d H:i:s');
            $ticket_status->save();
            $ticket_thread = Ticket_Thread::where('ticket_id', '=', $ticket_status->id)->first();
            $ticket_subject = $ticket_thread->title;
            $ticket_status_message = Ticket_Status::where('id', '=', $ticket_status->status)->first();
            $thread = new Ticket_Thread();
            $thread->ticket_id = $ticket_status->id;
            $thread->user_id = Auth::user()->id;
            $thread->is_internal = 1;
            $thread->body = $ticket_status_message->message . ' ' . Auth::user()->first_name . ' ' . Auth::user()->last_name;
            $thread->save();

            $user_id = $ticket_status->user_id;
            $user = User::where('id', '=', $user_id)->first();
            $email = $user->email;
            $user_name = $user->user_name;
            $ticket_number = $ticket_status->ticket_number;

            $system_from = $this->company();
            $sending_emails = Emails::where('department', '=', $ticket_status->dept_id)->first();
            if ($sending_emails == null) {
                $from_email = $this->system_mail();
            } else {
                $from_email = $sending_emails->id;
            }
            try {
                $this->PhpMailController->sendmail($from = $this->PhpMailController->mailfrom('0', $ticket_status->dept_id), $to = ['name' => $user_name, 'email' => $email], $message = ['subject' => $ticket_subject . '[#' . $ticket_number . ']', 'scenario' => 'close-ticket'], $template_variables = ['ticket_number' => $ticket_number]);
            } catch (\Exception $e) {
                return 0;
            }
            $data = [
                'id' => $ticket_status->ticket_number,
                'status' => 'Closed',
                'first_name' => Auth::user()->first_name,
                'last_name' => Auth::user()->last_name,
            ];
            \Event::fire('change-status', [$data]);

            return 'your ticket' . $ticket_status->ticket_number . ' has been closed';
        }



        if ($approval->status == 1) {
            $ticket_status = $ticket->where('id', '=', $id)->first();
            // checking for unautherised access attempt on other than owner ticket id
            if ($ticket_status == null) {
                return redirect()->route('unauth');
            }

            $ticket_status->status = 7;
            $ticket_status->closed = 0;
            // $ticket_status->closed_at = date('Y-m-d H:i:s');
            $ticket_status->save();
            $ticket_thread = Ticket_Thread::where('ticket_id', '=', $ticket_status->id)->first();
            $ticket_subject = $ticket_thread->title;
            $ticket_status_message = Ticket_Status::where('id', '=', $ticket_status->status)->first();
            $thread = new Ticket_Thread();
            $thread->ticket_id = $ticket_status->id;
            $thread->user_id = Auth::user()->id;
            // $thread->is_internal = 1;
            $thread->body = $ticket_status_message->message . ' ' . Auth::user()->first_name . ' ' . Auth::user()->last_name;
            $thread->save();

            $user_id = $ticket_status->user_id;
            $user = User::where('id', '=', $user_id)->first();
            $email = $user->email;
            $user_name = $user->user_name;
            $ticket_number = $ticket_status->ticket_number;

            $system_from = $this->company();
            $sending_emails = Emails::where('department', '=', $ticket_status->dept_id)->first();
            if ($sending_emails == null) {
                $from_email = $this->system_mail();
            } else {
                $from_email = $sending_emails->id;
            }
            try {
                $this->PhpMailController->sendmail($from = $this->PhpMailController->mailfrom('0', $ticket_status->dept_id), $to = ['name' => $user_name, 'email' => $email], $message = ['subject' => $ticket_subject . '[#' . $ticket_number . ']', 'scenario' => 'close-ticket'], $template_variables = ['ticket_number' => $ticket_number]);
            } catch (\Exception $e) {
                return 0;
            }
            $data = [
                'id' => $ticket_status->ticket_number,
                'status' => 'Closed',
                'first_name' => Auth::user()->first_name,
                'last_name' => Auth::user()->last_name,
            ];

            \Event::fire('change-status', array($data));
            return 'your ticket' . $ticket_status->ticket_number . ' has been closed request';
        }
    }

    /**
     * function to Open Ticket.
     *
     * @param type         $id
     * @param type Tickets $ticket
     * @return type
     */
    public function open($id, Tickets $ticket) {
        if (Auth::user()->role == 'user') {
            $ticket_status = $ticket->where('id', '=', $id)->first();
        } else {
            $ticket_status = $ticket->where('id', '=', $id)->first();
        }
        // checking for unautherised access attempt on other than owner ticket id
        if ($ticket_status == null) {
            return redirect()->route('unauth');
        }
        $ticket_status->status = 1;
        $ticket_status->reopened_at = date('Y-m-d H:i:s');
        $ticket_status->save();
        $ticket_status_message = Ticket_Status::where('id', '=', $ticket_status->status)->first();
        $thread = new Ticket_Thread();
        $thread->ticket_id = $ticket_status->id;
        $thread->user_id = Auth::user()->id;
        $thread->is_internal = 1;
        $thread->body = $ticket_status_message->message . ' ' . Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $thread->save();
        $data = [
            'id' => $ticket_status->ticket_number,
            'status' => 'Open',
            'first_name' => Auth::user()->first_name,
            'last_name' => Auth::user()->last_name,
        ];
        \Event::fire('change-status', array($data));
        return 'your ticket' . $ticket_status->ticket_number . ' has been opened';
    }

    /**
     * Function to delete ticket.
     *
     * @param type         $id
     * @param type Tickets $ticket
     *
     * @return type string
     */
    public function delete($id, Tickets $ticket) {
        $ticket_delete = $ticket->where('id', '=', $id)->first();
        if ($ticket_delete->status == 5) {
            $ticket_delete->delete();
            $ticket_threads = Ticket_Thread::where('ticket_id', '=', $id)->get();
            foreach ($ticket_threads as $ticket_thread) {
                $ticket_thread->delete();
            }
            $ticket_attachments = Ticket_attachments::where('ticket_id', '=', $id)->get();
            foreach ($ticket_attachments as $ticket_attachment) {
                $ticket_attachment->delete();
            }
            $data = [
                'id' => $ticket_delete->ticket_number,
                'status' => 'Deleted',
                'first_name' => Auth::user()->first_name,
                'last_name' => Auth::user()->last_name,
            ];
            \Event::fire('change-status', array($data));
            return 'your ticket has been delete';
        } else {
            $ticket_delete->is_deleted = 1;
            $ticket_delete->status = 5;
            $ticket_delete->save();
            $ticket_status_message = Ticket_Status::where('id', '=', $ticket_delete->status)->first();
            $thread = new Ticket_Thread();
            $thread->ticket_id = $ticket_delete->id;
            $thread->user_id = Auth::user()->id;
            $thread->is_internal = 1;
            $thread->body = $ticket_status_message->message . ' ' . Auth::user()->first_name . ' ' . Auth::user()->last_name;
            $thread->save();
            $data = [
                'id' => $ticket_delete->ticket_number,
                'status' => 'Deleted',
                'first_name' => Auth::user()->first_name,
                'last_name' => Auth::user()->last_name,
            ];
            \Event::fire('change-status', array($data));
            return 'your ticket' . $ticket_delete->ticket_number . ' has been delete';
        }
    }

    /**
     * select_all.
     *
     * @return type
     */
    public function select_all() {
        if (Input::has('select_all')) {
            $selectall = Input::get('select_all');

            $value = Input::get('submit');
            // dd( $value);
            foreach ($selectall as $delete) {
                $ticket = Tickets::whereId($delete)->first();
                if ($value == 'Delete') {
                    $this->delete($delete, new Tickets);
                } elseif ($value == 'Close') {
                    // dd('herer');
                    $this->close($delete, new Tickets);
                } elseif ($value == 'Open') {
                    $this->open($delete, new Tickets);
                } elseif ($value == 'Delete forever') {
                    $notification = Notification::select('id')->where('model_id', '=', $ticket->id)->get();
                    foreach ($notification as $id) {
                        $user_notification = UserNotification::where(
                                        'notification_id', '=', $id->id);
                        $user_notification->delete();
                    }
                    $notification = Notification::select('id')->where('model_id', '=', $ticket->id);
                    $notification->delete();
                    $thread = Ticket_Thread::where('ticket_id', '=', $ticket->id)->get();
                    foreach ($thread as $th_id) {
                        // echo $th_id->id." ";
                        $attachment = Ticket_attachments::where('thread_id', '=', $th_id->id)->get();
                        if (count($attachment)) {
                            foreach ($attachment as $a_id) {
                                // echo $a_id->id . ' ';
                                $attachment = Ticket_attachments::find($a_id->id);
                                $attachment->delete();
                            }
                            // echo "<br>";
                        }
                        $thread = Ticket_Thread::find($th_id->id);
//                        dd($thread);
                        $thread->delete();
                    }
                    $collaborators = Ticket_Collaborator::where('ticket_id', '=', $ticket->id)->get();
                    if (count($collaborators)) {
                        foreach ($collaborators as $collab_id) {
                            // echo $collab_id->id;
                            $collab = Ticket_Collaborator::find($collab_id->id);
                            $collab->delete();
                        }
                    }
                    $tickets = Tickets::find($ticket->id);
                    $tickets->delete();
                    $data = ['id' => $ticket->id];
                    \Event::fire('ticket-permanent-delete', [$data]);
                }
            }
            if ($value == 'Delete') {
                return redirect()->back()->with('success12', lang::get('lang.moved_to_trash'));
            } elseif ($value == 'Close') {
                return redirect()->back()->with('success12', Lang::get('lang.tickets_have_been_closed'));
            } elseif ($value == 'Open') {
                return redirect()->back()->with('success12', Lang::get('lang.tickets_have_been_opened'));
            } else {
                return redirect()->back()->with('success12', Lang::get('lang.hard-delete-success-message'));
            }
        }

        return redirect()->back()->with('fails', 'None Selected!');
    }

}
