<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Model\helpdesk\Settings\Company;
use Auth;
use App\User;
use App\Model\helpdesk\Ticket\Tickets;
use App\Model\helpdesk\Agent\Department;
use App\Model\helpdesk\Settings\Approval;
use App\Model\helpdesk\Email\Emails;
use App\Model\helpdesk\Agent\DepartmentAssignAgents;
Use App\Model\helpdesk\Settings\CommonSettings;

class AgentLayout {

    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $company;
    protected $users;
    protected $tickets;
    protected $department;
    protected $emails;

    /**
     * Create a new profile composer.
     *
     * @param  
     * @return void
     */
    public function __construct(Company $company, User $users, Tickets $tickets, Department $department, Approval $approval, Emails $emails, CommonSettings $common_settings) {
        $this->company = $company;
        $this->auth = Auth::user();
        $this->users = $users;
        $this->tickets = $tickets;
        $this->department = $department;
        $this->approval = $approval;
        $this->emails = $emails;
        $this->common_settings = $common_settings;

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view) {
        $notifications = \App\Http\Controllers\Common\NotificationController::getNotifications();
        $view->with([
            'company' => $this->company,
            //'notifications' => $notifications,
            'myticket' => $this->myTicket(),
            'unassigned' => $this->unassigned(),
            'followup_ticket' => $this->followupTicket(),
            'closingapproval' => $this->closingApproval(),
            'closed' => $this->closed(),
            'deleted' => $this->deleted(),
            'tickets' => $this->inbox(),
            'overdues' => $this->overdues(),
            'approval_enable' => $this->getApprovalEnableOrNot(),
            'due_today' => $this->getDueToday(),
            'is_mail_conigured'   => $this->getEmailConfig(),
            'dummy_installation'  => $this->getDummyDataInstallation(),
            'ticket_policy'=> new \App\Policies\TicketPolicy(),
        ]);
    }

    public function users() {
        return $this->users->select('id', 'profile_pic');
    }

    public function tickets() {
        return $this->tickets->select('id', 'ticket_number');
    }

    public function departments() {
        $array = [];
        $tickets = $this->tickets;
        if (\Auth::user()->role == 'agent') {

             $id=Auth::user()->id;
             $dept=DepartmentAssignAgents::where('agent_id','=',$id)->pluck('department_id')->toArray();
             $tickets = $tickets->whereIn('tickets.dept_id',$dept);




            // $tickets = $tickets->where('tickets.dept_id', '=', \Auth::user()->primary_dpt);
        }
        $tickets = $tickets
                ->leftJoin('department as dep', 'tickets.dept_id', '=', 'dep.id')
                ->leftJoin('ticket_status', 'tickets.status', '=', 'ticket_status.id')
                ->select('dep.name as name', 'ticket_status.name as status', \DB::raw('COUNT(ticket_status.name) as count'))
                ->groupBy('dep.name', 'ticket_status.name')
                ->get();
        $grouped = $tickets->groupBy('name');
        $status = [];
        foreach ($grouped as $key => $group) {
            $status[$key] = $group->keyBy('status');
        }
        return collect($status);
    }

    public function myTicket() {
        $ticket = $this->tickets()
                    ->Join('ticket_status', 'ticket_status.id','=','tickets.status')
                    ->Join('ticket_status_type',function($join){
                         $join->on('ticket_status.purpose_of_status','=','ticket_status_type.id')
                                 ->whereIn('ticket_status_type.name',['open']);
                    });
        //if ($this->auth->role == 'admin') {
            return $ticket->where('assigned_to', $this->auth->id);
                    //->where('status', '1');
//        } elseif ($this->auth->role == 'agent') {
//            return $ticket->where('assigned_to', $this->auth->id);
                    //->where('status', '1');
        //}
    }

    public function unassigned() {
        $ticket = $this->tickets()
                ->LeftJoin('ticket_status', 'ticket_status.id','=','tickets.status')
                    ->Join('ticket_status_type',function($join){
                         $join->on('ticket_status.purpose_of_status','=','ticket_status_type.id')
                                 ->whereIn('ticket_status_type.name',['open']);
                    });
        if ($this->auth->role == 'admin') {
            return $ticket->where(function($query){
                        $query->where(function($query2){
                            $query2->where('tickets.team_id', '=', 0)
                            ->orWhere('tickets.team_id', '=', null);
                        })->where(function ($query3){
                            $query3->where('tickets.assigned_to', '=', 0)
                                ->orWhere('tickets.assigned_to', '=', null);
                        });
                    })
                    ->select('id');
        } elseif ($this->auth->role == 'agent') {

        $id=Auth::user()->id;
        $dept=DepartmentAssignAgents::where('agent_id','=',$id)->pluck('department_id')->toArray();
        $ticket = $ticket->whereIn('tickets.dept_id',$dept);

            return $ticket->where(function($query){
                        $query->where(function($query2){
                            $query2->where('tickets.team_id', '=', 0)
                            ->orWhere('tickets.team_id', '=', null);
                        })->where(function ($query3){
                            $query3->where('tickets.assigned_to', '=', 0)
                                ->orWhere('tickets.assigned_to', '=', null);
                        });
                    })
                    ->select('id');
        }
    }

    public function followupTicket() {
        $ticket = $this->tickets()
                ->Join('ticket_status', 'ticket_status.id','=','tickets.status')
                    ->Join('ticket_status_type',function($join){
                         $join->on('ticket_status.purpose_of_status','=','ticket_status_type.id')
                                 ->whereIn('ticket_status_type.name',['open']);
                    });
        if ($this->auth->role == 'admin') {
            return $ticket->where('follow_up', '1')->select('id');
        } elseif ($this->auth->role == 'agent') {
            return $ticket->where('follow_up', '1')->select('id');
        }
    }

    public function closingApproval() {
        $ticket = $this->tickets()
                ->Join('ticket_status', 'ticket_status.id','=','tickets.status')
                    ->Join('ticket_status_type',function($join){
                         $join->on('ticket_status.purpose_of_status','=','ticket_status_type.id')
                                 ->whereIn('ticket_status_type.name',['approval']);
                    });
        if ($this->auth->role == 'admin') {
            return $ticket->select('id');
        } elseif ($this->auth->role == 'agent') {
            return $ticket->select('id');
        }
    }

    public function deleted()
    {
        $ticket = $this->tickets()
                ->Join('ticket_status', 'ticket_status.id', '=', 'tickets.status')
                ->Join('ticket_status_type', function ($join) {
                    $join->on('ticket_status.purpose_of_status', '=', 'ticket_status_type.id')
                        ->whereIn('ticket_status_type.name', ['deleted']);
                });
        if ($this->auth->role == 'admin') {
            return $ticket->select('id');
        } elseif ($this->auth->role == 'agent') {
            $id = Auth::user()->id;
            $dept = DepartmentAssignAgents::where('agent_id', '=', $id)->pluck('department_id')->toArray();
            $ticket = $ticket->whereIn('tickets.dept_id', $dept)->orWhere('assigned_to', '=', Auth::user()->id);
            return $ticket->where('status', '5')->select('id');
        }
    }

    public function inbox()
    {
        $table = $this->tickets();
        if (Auth::user()->role == 'agent') {

             $id=Auth::user()->id;
             $dept=DepartmentAssignAgents::where('agent_id','=',$id)->pluck('department_id')->toArray();
             $table = $table->whereIn('tickets.dept_id',$dept)->orWhere('assigned_to', '=', Auth::user()->id);



            // $id = Auth::user()->primary_dpt;
            // $table = $table->where('tickets.dept_id', '=', $id)->orWhere('assigned_to', '=', Auth::user()->id);
        }

       return $table
                    ->Join('ticket_status', 'ticket_status.id','=','tickets.status')
                    ->Join('ticket_status_type',function($join){
                         $join->on('ticket_status.purpose_of_status','=','ticket_status_type.id')
                                 ->whereIn('ticket_status_type.name',['open']);
                    });
    }

    public function overdues() {
        $ticket = $this->tickets()
                ->Join('ticket_status', 'ticket_status.id','=','tickets.status')
                    ->Join('ticket_status_type',function($join){
                         $join->on('ticket_status.purpose_of_status','=','ticket_status_type.id')
                                 ->whereIn('ticket_status_type.name',['open']);
                    });

        

        if ($this->auth->role == 'admin') {
            return $ticket
                            ->where('isanswered', '=', 0)
                            ->whereNotNull('tickets.duedate')
                            ->where('tickets.duedate', '!=', '00-00-00 00:00:00')
                            ->where('tickets.duedate', '<', \Carbon\Carbon::now())
                            ->select('tickets.id');
        } elseif ($this->auth->role == 'agent') {

        $id=Auth::user()->id;
        $dept=DepartmentAssignAgents::where('agent_id','=',$id)->pluck('department_id')->toArray();
        $ticket = $ticket->whereIn('tickets.dept_id',$dept);

             return $ticket
                            ->where('isanswered', '=', 0)
                            ->whereNotNull('tickets.duedate')
                            // ->where('dept_id', '=', $this->auth->primary_dpt)
                            ->where('tickets.duedate', '!=', '00-00-00 00:00:00')
                            ->where('tickets.duedate', '<', \Carbon\Carbon::now())
                            ->select('tickets.id');
        }
    }

    public function getApprovalEnableOrNot()
    {
        return $this->approval->where('id', '=', 1)
            ->select('status');
    }

    public function getDueToday()
    {
        $ticket = $this->tickets()
                ->Join('ticket_status', 'ticket_status.id','=','tickets.status')
                    ->Join('ticket_status_type',function($join){
                         $join->on('ticket_status.purpose_of_status','=','ticket_status_type.id')
                                 ->whereIn('ticket_status_type.name',['open']);
                    });
        if ($this->auth->role == 'admin') {
            return $ticket
                            ->where('isanswered', '=', 0)
                            ->whereNotNull('duedate')
                            ->whereRaw('date(duedate) = ?', [date('Y-m-d')]);
        } elseif ($this->auth->role == 'agent') {

            $id=Auth::user()->id;
            $dept=DepartmentAssignAgents::where('agent_id','=',$id)->pluck('department_id')->toArray();
            $ticket = $ticket->whereIn('tickets.dept_id',$dept);

            return $ticket
                            ->where('isanswered', '=', 0)
                            ->whereNotNull('tickets.duedate')
                            ->whereDate('tickets.duedate','=', \Carbon\Carbon::now()->format('Y-m-d'));
        }

    }

    /**
     *@category function to check configured mails
     *@param null
     *@var $emails
     *@return boolean true/false
     */
    public function getEmailConfig()
    {
        $emails = $this->emails->select('id')->where('sending_status', '=', 1)->where('fetching_status', '=', 1);
        return $emails;
    }

    /** 
     * @category function to fetch closed tickets count
     * @param null
     * @return builder
     */
    public function closed()
    {
        $table = $this->tickets();
        if (Auth::user()->role == 'agent') {

             $id=Auth::user()->id;
             $dept=DepartmentAssignAgents::where('agent_id','=',$id)->pluck('department_id')->toArray();
             $table = $table->whereIn('tickets.dept_id',$dept)->orWhere('assigned_to', '=', Auth::user()->id);



            // $id = Auth::user()->primary_dpt;
            // $table = $table->where('tickets.dept_id', '=', $id)->orWhere('assigned_to', '=', Auth::user()->id);
        }

       return $table
                    ->Join('ticket_status', 'ticket_status.id','=','tickets.status')
                    ->Join('ticket_status_type',function($join){
                         $join->on('ticket_status.purpose_of_status','=','ticket_status_type.id')
                                 ->whereIn('ticket_status_type.name',['closed']);
                    }); 
    }

    /**
     * @category function to check if dummy data is insatlled in the system or not
     * @param null
     * @return builder
     */
    public function getDummyDataInstallation()
    {
        $return_collection = $this->common_settings->select('status')->where('option_name', '=', 'dummy_data_installation')->first();
        if(!$return_collection) {
            $return_collection = collect(['status' => 0]);    
            
            return $return_collection['status'];
        }
        return $return_collection->status;
    }
}
