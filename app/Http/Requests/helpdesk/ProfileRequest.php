<?php

namespace App\Http\Requests\helpdesk;

use App\Http\Requests\Request;

/**
 * ProfileRequest.
 *
 * @author  Ladybird <info@ithelpdesk.com>
 */
class ProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'  => 'required',
            'profile_pic' => 'mimes:png,jpeg',
            'mobile'      => $this->checkMobile(),
            'country_code'=> 'max:5',
            'ext'=>'max:5',
            'phone_number'=>'max:20',
        ];
    }

    /**
     *
     *Check the mobile number is unique or not
     *@return string
     */
    public function checkMobile()
    {
        $rule = 'numeric';
        if (\Auth::user()->mobile != Request::get('mobile')) {
            $rule .= '|unique:users';
        }
        if (getAccountActivationOptionValue() == "mobile" || getAccountActivationOptionValue() == "email,mobile") {
            $rule .= '|required';
        }
        return $rule;
    }
}
