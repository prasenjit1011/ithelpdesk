<?php

namespace App\Http\Requests\helpdesk;

use App\Http\Requests\Request;

/**
 * BanRequest.
 *
 * @author  Ladybird <info@ithelpdesk.com>
 */
class BusinessRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {




        return [
            'name' => 'unique:business_hours,name|required|max:25',
            'description' => 'required',
            'time_zone' => 'required',
            'status' => 'required',
            'hours' => 'required',
        ];
    }
}