<?php

namespace App\Http\Requests\helpdesk;

use App\Http\Requests\Request;

/**
 * TicketRequest.
 *
 * @author  Ladybird <info@ithelpdesk.com>
 */
class TicketRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $panel = 'agent';
        $required = \App\Model\Custom\Required::
                where('form','ticket')
                ->pluck($panel,'field')
                ->toArray();
        //dd($required);
        return $required;
        
    }
    

}
