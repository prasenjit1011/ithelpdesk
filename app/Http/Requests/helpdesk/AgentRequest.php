<?php

namespace App\Http\Requests\helpdesk;
use App\Model\helpdesk\Settings\CommonSettings;

use App\Http\Requests\Request;

/**
 * AgentRequest.
 *
 * @author  Ladybird <info@ithelpdesk.com>
 */
class AgentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_name'  => [
                'required',
                'unique:users',
                'regex:/^(?:[A-Z\d][A-Z\d._-]{2,30}|[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})$/i',
                'max:30'
            ],
            'first_name'          => 'required|max:30|alpha',
            'email'               => 'required|email|max:50|unique:users',
            'active'              => 'required',
            // 'account_status'   => 'required',
            'primary_department'  => 'required',
            'agent_time_zone'     => 'required',
            // 'phone_number'     => 'phone:IN',
            'country_code'        => 'max:5',
            'mobile'              => getMobileValidation('mobile'),
            'ext'                 => 'max:5',
            'phone_number'        => 'max:15',
            // 'team'             => 'required',
        ];
    }
}
