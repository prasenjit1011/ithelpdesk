<?php

namespace App\Http\Middleware;

use Closure;

class CronUrl {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (isInstall() && isCronUrl()) {
            return $next($request);
        } else {
            return redirect()->route('error404', []);
        }
    }

}
