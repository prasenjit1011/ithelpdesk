<?php

namespace App\Http\Middleware;

use Closure;
use Lang;

class UserLimitExceeded {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if(!isInstall()){
            return $next($request);
        }
        if (isInstall()&&!$this->isUserExces() && !$this->isAgentExces()) {
            return $next($request);
        }
        return redirect('/')->with('fails', Lang::get('lang.user-limit-exceeded-message'));
    }

    public function isLimitedUser() {
        $user_limit = \Config::get('auth.user_limit');
        $check = false;
        if ($user_limit) {
            $check = true;
        }
        return $check;
    }

    public function isLimitedAgent() {
        $agent_limit = \Config::get('auth.agent_limit');
        $check = false;
        if ($agent_limit) {
            $check = true;
        }
        return $check;
    }

    public function isUserExces() {
        $check = false;
        $user_limit = \Config::get('auth.user_limit');

        if ($this->isLimitedUser()) {
            $user_count = \App\User::where('role', 'user')->select('id')->count();
            if ($user_limit < $user_count) {
                $check = true;
            }
        }
        return $check;
    }

    public function isAgentExces() {
        $check = false;
        $user_limit = \Config::get('auth.agent_limit');

        if ($this->isLimitedAgent()) {
            $user_count = \App\User::where('role', '!=', 'user')->where('active', '=', 1)->select('id')->count();
            if ($user_limit < $user_count) {
                $check = true;
            }
        }
        return $check;
    }

}
